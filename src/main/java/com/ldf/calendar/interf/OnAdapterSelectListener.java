package com.ldf.calendar.interf;

public interface OnAdapterSelectListener {
    void cancelSelectState();
    void updateSelectState();
}
