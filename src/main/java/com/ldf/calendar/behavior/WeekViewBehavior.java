package com.ldf.calendar.behavior;

import android.content.Context;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.alamkanak.weekview.WeekView;
import com.ldf.calendar.Utils;
import com.ldf.calendar.view.MonthDayPager;
import com.ldf.calendar.view.MonthPager;

/**
 * sunlp on 2017/10/16
 */

public class WeekViewBehavior extends CoordinatorLayout.Behavior<WeekView> {
    private int initOffset = -1;
    private int minOffset = -1;
    private Context context;
    private boolean initiated = false;

    public WeekViewBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    @Override
    public boolean onLayoutChild(CoordinatorLayout parent, WeekView child, int layoutDirection) {

        parent.onLayoutChild(child, layoutDirection);
        MonthDayPager monthDayPager = getMonthDayPager(parent);
        initMinOffsetAndInitOffset(parent, child, monthDayPager);
        return true;
    }

    private void initMinOffsetAndInitOffset(CoordinatorLayout parent,
                                            WeekView child,
                                            MonthDayPager monthDayPager) {
        if (monthDayPager.getBottom() > 0 && initOffset == -1) {
            initOffset = monthDayPager.getViewHeight();
            saveTop(initOffset);
        }
        if(!initiated) {
            initOffset = monthDayPager.getViewHeight();
            saveTop(initOffset);
            initiated = true;
        }
        child.offsetTopAndBottom(Utils.loadTop());
        minOffset = getMonthDayPager(parent).getCellHeight();
    }

    @Override
    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, WeekView child,
                                       View directTargetChild, View target, int nestedScrollAxes) {
        Log.e("ldf","onStartNestedScroll");

        MonthPager monthPager = (MonthPager) coordinatorLayout.getChildAt(0);
        if (monthPager.getPageScrollState() != ViewPager.SCROLL_STATE_IDLE) {
            return false;
        }
        monthPager.setScrollable(true);
        return true;
    }

    @Override
    public void onNestedPreScroll(CoordinatorLayout coordinatorLayout, WeekView child, View target, int dx, int dy, int[] consumed) {
        Log.e("ldf","onNestedPreScroll");
        super.onNestedPreScroll(coordinatorLayout, child, target, dx, dy, consumed);
        if (child.getTop() <= initOffset
                && child.getTop() >= getMonthDayPager(coordinatorLayout).getCellHeight()) {
            consumed[1] = Utils.scroll(child, dy,
                    getMonthDayPager(coordinatorLayout).getCellHeight(),
                    getMonthDayPager(coordinatorLayout).getViewHeight());
            saveTop(child.getTop());
        }
    }

    @Override
    public void onStopNestedScroll(final CoordinatorLayout parent,final WeekView child, View target) {
        Log.e("ldf","onStopNestedScroll");
        super.onStopNestedScroll(parent, child, target);
        MonthPager monthPager = (MonthPager) parent.getChildAt(0);
        monthPager.setScrollable(true);
        if (!Utils.isScrollToBottom()) {
            if (initOffset - Utils.loadTop() > Utils.getTouchSlop(context)) {
                Utils.weekScrollTo(parent, child, getMonthDayPager(parent).getCellHeight(), 200);
            } else {
                Utils.weekScrollTo(parent, child, getMonthDayPager(parent).getViewHeight(), 80);
            }
        } else {
            if (Utils.loadTop() - minOffset > Utils.getTouchSlop(context)) {
                Utils.weekScrollTo(parent, child, getMonthDayPager(parent).getViewHeight(), 200);
            } else {
                Utils.weekScrollTo(parent, child, getMonthDayPager(parent).getCellHeight(), 80);
            }
        }
    }

    private MonthDayPager getMonthDayPager(CoordinatorLayout coordinatorLayout) {
        MonthDayPager monthDayPager = (MonthDayPager) coordinatorLayout.getChildAt(0);
        return monthDayPager;
    }

    private void saveTop(int top) {
        Utils.saveTop(top);
        if (Utils.loadTop() == initOffset) {
            Utils.setScrollToBottom(false);
        } else if (Utils.loadTop() == minOffset) {
            Utils.setScrollToBottom(true);
        }
    }
}
