package com.efounder.login;

import android.os.Environment;

import com.efounder.chat.model.Constant;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.model.RefreshViewEvent;
import com.efounder.ospmobilelib.R;
import com.efounder.util.AppContext;
import com.efounder.util.LoadingDataUtilBlack;
import com.efounder.util.ToastUtil;
import com.efounder.utils.ResStringUtil;
import com.pansoft.resmanager.ResFileManager;
import com.utilcode.util.ActivityUtils;
import com.utilcode.util.LogUtils;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * 资源文件本地更新（不从服务器下载）
 *
 * @autor yqs
 * @date 2018/12/17 10:23
 **/
public class LocalResUpdateManger {

    /**
     * 检查资源文件是否需要更新
     *
     * @return
     */
    public static boolean checkNeedUpdate() {
        //检查本地资源文件版本
        String path = Constant.UNZIP_DIR + "/" + EnvironmentVariable.getProperty("res_version");
        File resVersionFile = new File(path);
        boolean isNeedUpdateRES = !resVersionFile.exists();
        return isNeedUpdateRES;
    }

    /**
     * 更新本地资源文件
     */
    public static void updateLocalRes(final ResUpdateCallback callback) {
        LogUtils.e("更新本地资源文件");
        Disposable disposable = Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> emitter) throws Exception {
                ResFileManager.unZipAssetsToSDUsingAnt(ActivityUtils.getTopActivity(), "res.zip", Environment.getExternalStorageDirectory()
                        .getAbsolutePath()
                        + File.separator + Constant.appSdcardBaseLocation, true);
                emitter.onNext(1);
                emitter.onComplete();

            }
        }).subscribeOn(Schedulers.io()).doOnSubscribe(new Consumer<Disposable>() {
            @Override
            public void accept(Disposable disposable) throws Exception {
                LoadingDataUtilBlack.show(ActivityUtils.getTopActivity(), ResStringUtil.getString(R.string.ospmobilelib_tab_res_updating));

            }
        }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Object>() {
            @Override
            public void accept(Object o) throws Exception {
                LoadingDataUtilBlack.dismiss();
                if (callback != null) {
                    callback.updateFinish(true);
                }
                //发送事件刷新首页界面
                EventBus.getDefault().post(new RefreshViewEvent(RefreshViewEvent.VIEW_TABOTTOMACTIVITY));
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                LoadingDataUtilBlack.dismiss();
                if (callback != null) {
                    callback.updateFinish(false);
                }
                ToastUtil.showToast(AppContext.getInstance(), ResStringUtil.getString(R.string.ospmobilelib_tab_unpacking_resources_failed));
            }
        });
    }

    public interface ResUpdateCallback {
        void updateFinish(boolean isSuccess);
    }
}
