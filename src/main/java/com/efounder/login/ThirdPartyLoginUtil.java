package com.efounder.login;


import android.util.Log;

import com.efounder.constant.EnvironmentVariable;
import com.efounder.http.EFHttpRequest;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

/**
 * Created by will on 18-3-8.
 */
@Deprecated
public class ThirdPartyLoginUtil {

    private static final String ROOT_URL = EnvironmentVariable.getProperty("DefaultServerHttpType") + "://"
            + EnvironmentVariable.getProperty("DefaultServerAddress") + ":"
            + EnvironmentVariable.getProperty("DefaultServerPort") + "/"
            + EnvironmentVariable.getProperty("DefaultServerPath") + "/user/thirdLogin";

    public static void thirdPartyLoginRequest(String tag, HashMap<String, String> paramsMap, final ThirdPartyLoginCallback callback) {

        StringBuilder tempParams = new StringBuilder();
        try {
            int pos = 0;
            for (String key : paramsMap.keySet()) {
                if (pos > 0) {
                    tempParams.append("&");
                }
                tempParams.append(String.format("%s=%s", key, URLEncoder.encode(paramsMap.get(key), "utf-8")));
                pos++;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String requestUrl = String.format("%s?%s", ROOT_URL, tempParams.toString());
        EFHttpRequest httpRequest = new EFHttpRequest(tag);
        httpRequest.setHttpRequestListener(new EFHttpRequest.HttpRequestListener() {
            @Override
            public void onRequestSuccess(int requestCode, String response) {
                callback.onSuccess(response);
            }

            @Override
            public void onRequestFail(int requestCode, String message) {
                callback.onFail(message);
            }
        });
        Log.d("ThirdLoginRequest", requestUrl);
        httpRequest.httpGet(requestUrl);

    }

    public interface ThirdPartyLoginCallback {

        void onSuccess(String response);

        void onFail(String error);
    }

}
