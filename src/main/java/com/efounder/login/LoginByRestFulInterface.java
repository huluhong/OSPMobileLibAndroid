package com.efounder.login;

import android.content.Context;

import java.util.HashMap;


public interface LoginByRestFulInterface {
    /**
     * 开始登录
     *
     * @param context
     * @param userName1 用户名
     * @param passWord1 用户密码
     */
    void loginBegin(Context context, String userName1, String passWord1);

    /**
     * 开始登录
     *
     * @param context
     * @param params  map参数,注意！！！map中必须含有 userId passWord字段，不可缺少
     */
    void loginBegin(Context context, HashMap<String, String> params);

}
