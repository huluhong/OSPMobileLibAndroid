package com.efounder.login;

/**
 * Created by pansoft on 2017/5/11.
 */
@Deprecated
public abstract class AbstractLogin {
    abstract void login(String name, String passWord);

    abstract void checkApkVersion(String jsonResult);

    abstract void checkResVersion(String jsonResult);

    abstract void checkIM(String jsonResult);


}
