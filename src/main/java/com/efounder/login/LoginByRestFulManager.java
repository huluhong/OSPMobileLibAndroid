package com.efounder.login;

import android.content.Context;

import com.efounder.constant.EnvironmentVariable;

import java.lang.reflect.Constructor;
import java.util.HashMap;


public class LoginByRestFulManager {
    private int loginType = 0;

    public LoginByRestFulManager(int type) {
        loginType = type;
    }


    /**
     * 第一步 登录
     *
     * @param context
     * @param userName1
     * @param passWord1
     */
    public void loginBegin(final Context context, String userName1, final String passWord1) {

        String interfaceName = EnvironmentVariable.getProperty("LoginExposedClass", "com.efounder.login.LoginByRestFul");
        try {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            Class<?> clazz = loader.loadClass(interfaceName);

            Constructor<?> cons1 = clazz.getDeclaredConstructor(int.class);
            LoginByRestFulInterface loginByRestFulInterface = (LoginByRestFulInterface) cons1.newInstance(loginType);
            loginByRestFulInterface.loginBegin(context, userName1, passWord1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //不建议使用这个了
    public void loginBegin(final Context context, HashMap<String, String> paramsMap) {

        String interfaceName = EnvironmentVariable.getProperty("LoginExposedClass", "com.efounder.login.LoginByRestFul");
        try {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            Class<?> clazz = loader.loadClass(interfaceName);
            Constructor<?> cons1 = clazz.getDeclaredConstructor(int.class);
            LoginByRestFulInterface loginByRestFulInterface = (LoginByRestFulInterface) cons1.newInstance(loginType);
            loginByRestFulInterface.loginBegin(context, paramsMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}