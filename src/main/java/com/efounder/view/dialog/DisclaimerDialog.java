package com.efounder.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.efounder.ospmobilelib.R;

/**
 * 以前给糖足项目使用的，现在用不到
 * Created by zhangshunyun on 2017/10/10.
 */
@Deprecated
public class DisclaimerDialog extends Dialog {

    private final Context context;
    ImageView buttonClose;
    private ScrollView mScrollView;
    private View mBottomContainer;
    private int _calCount;
    private OnDisclaimerDialogLisener onDisclaimerDialogLisener;
    private TextView contentText;
    private TextView mTitle;
    private RelativeLayout contentContainer;
    private Button agreeButton;
    private Button cancelButton;
    private FrameLayout outerContainer;


    public DisclaimerDialog(Context context, int themeResId) {
        super(context, themeResId);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.disclaimer_dialog);
        initView();
    }

    private void initView() {

        mScrollView = (ScrollView) findViewById(R.id.scrollview_disclaimer);
        buttonClose = (ImageView) findViewById(R.id.close);
        contentText = (TextView) findViewById(R.id.scrolview_content);
        mBottomContainer = findViewById(R.id.bottom_container);
        mTitle = (TextView) findViewById(R.id.disclaimer_title);
        contentContainer = (RelativeLayout) findViewById(R.id.content_container);
        agreeButton = (Button) findViewById(R.id.btn_agree);
        agreeButton.setTextColor(Color.parseColor("#DFDFDF"));
        agreeButton.setEnabled(false);
        cancelButton = (Button) findViewById(R.id.btn_cancel);
        outerContainer = (FrameLayout) findViewById(R.id.outer_container);
//        contentText.setText();
        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DisclaimerDialog.this.dismiss();
            }
        });
        agreeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != onDisclaimerDialogLisener)
                    onDisclaimerDialogLisener.agree();
                DisclaimerDialog.this.dismiss();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != onDisclaimerDialogLisener)
                    onDisclaimerDialogLisener.disagree();
                DisclaimerDialog.this.dismiss();
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mScrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View view, int i, int i1, int i2, int i3) {
//                    System.out.println("---i--"+i);
//                    System.out.println("---i1--"+i1);
//                    System.out.println("---i2--"+i2);
//                    System.out.println("---i3--"+i3);
//                    System.out.println("---mScrollView.getHeight()--"+mScrollView.getHeight());
//                    System.out.println("---mScrollView.getScrollY()--"+mScrollView.getScrollY());
//                    System.out.println("---view.getHeight()--"+view.getHeight());

                    View childAt = mScrollView.getChildAt(0);
                    if (mScrollView.getHeight() + mScrollView.getScrollY() == childAt.getHeight()) {
                        _calCount++;
                        if (_calCount == 1) {
                            agreeButton.setTextColor(Color.parseColor("#000000"));
                            agreeButton.setEnabled(true);
//                            Toast.makeText(context, "11", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        _calCount = 0;
                    }
                }
            });
        }

    }

    /**
     * 设置右上角关闭按钮显示或者隐藏
     *
     * @param isShow
     */
    public void setCloseButtonShowOrHide(Boolean isShow) {
        buttonClose.setVisibility(isShow ? View.VISIBLE : View.INVISIBLE);
    }

    /**
     * 设置下端同意与取消按钮的显示与隐藏
     *
     * @param isShow
     */
    public void setCancelOrAgreeButtonShowOrHide(Boolean isShow) {
        mBottomContainer.setVisibility(isShow ? View.VISIBLE : View.INVISIBLE);
    }

    public void setContentContainerHeight(int height){
        ViewGroup.LayoutParams layoutParams = contentContainer.getLayoutParams();
        layoutParams.height = height;
        contentContainer.setLayoutParams(layoutParams);
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        ViewGroup.LayoutParams outerLayoutParams = outerContainer.getLayoutParams();
        outerLayoutParams.height = height+wm.getDefaultDisplay().getHeight() *  2/ 7;
        outerContainer.setLayoutParams(outerLayoutParams);
    }
    /**
     * 设置dialog内容
     * @param content
     */
    public void setDisclaimerContentText(String content){
        contentText.setText(content);
    }

    /**
     * 设置dialog的标题
     * @param title
     */
    public void setDisclaimerDialogTitle(String title){
        mTitle.setText(title);
    }

    /**
     * 设置左侧button的文字
     * @param leftButtonText
     */
    public void setLeftButtonText(String leftButtonText){
        cancelButton.setText(leftButtonText);
    }

    /**
     * 设置右侧button的文字
     * @param rightButtonText
     */
    public void setRightButtonText(String rightButtonText){
        agreeButton.setText(rightButtonText);
    }

    public interface OnDisclaimerDialogLisener {
        void disagree();

        void agree();
    }

    public void registerOnDisclaimerDialogLisener(OnDisclaimerDialogLisener onDisclaimerDialogLisener) {
        this.onDisclaimerDialogLisener = onDisclaimerDialogLisener;
    }

    public void unRegisterOnDisclaimerDialogLisener() {
        this.onDisclaimerDialogLisener = null;
    }
}
