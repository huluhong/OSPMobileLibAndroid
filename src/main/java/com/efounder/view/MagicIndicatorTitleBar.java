package com.efounder.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.efounder.ospmobilelib.R;
import com.efounder.view.titlebar.ScaleTransitionPagerTitleView;
import com.utilcode.util.SizeUtils;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.badge.BadgeAnchor;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.badge.BadgePagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.badge.BadgeRule;

import java.util.List;

/**
 * Created by Marcello on 2019/11/12
 * tab 很多时候左右滑动的titlebar
 */
public class MagicIndicatorTitleBar extends BaseTitleBar {

    private View centerCustomView;

    public MagicIndicatorTitleBar(Context context) {
        this(context, null);
    }

    public MagicIndicatorTitleBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MagicIndicatorTitleBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void loadAttributesCenterView(TypedArray array) {

    }

    @Override
    protected void initMainCenterViews(Context context) {
        // 初始化中间自定义布局
        centerCustomView = LayoutInflater.from(context).inflate(R.layout.magic_indicator_titlebar_center_view, rlMain, false);
        if (centerCustomView.getId() == View.NO_ID) {
            centerCustomView.setId(generateViewId());
        }
        LayoutParams layoutParams = (LayoutParams) centerCustomView.getLayoutParams();
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        if (tvLeft != null || imgLeft != null || viewCustomLeft != null) {
            layoutParams.addRule(RIGHT_OF, tvLeft != null ? tvLeft.getId() : imgLeft != null ? imgLeft.getId() : viewCustomLeft.getId());
        }
        if (tvRight != null || imgRight != null || viewCustomRight != null) {
            layoutParams.addRule(LEFT_OF, tvRight != null ? tvRight.getId() : imgRight != null ? imgRight.getId() : viewCustomRight.getId());
        } else {
            layoutParams.rightMargin = dpToPx(40f);
        }
        rlMain.addView(centerCustomView);
    }

    //设置左右滑动的样式
    public void setMagicIndicatorSimple(MagicIndicator topMagicTabs, final List<String> titles, final OnClickListener onClickListener) {
        CommonNavigator commonNavigator = new CommonNavigator(getContext());
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return titles == null ? 0 : titles.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {

                final BadgePagerTitleView badgePagerTitleView = new BadgePagerTitleView(context);
                //只颜色变化
//                SimplePagerTitleView simplePagerTitleView = new ColorTransitionPagerTitleView(context);
                //文字大小同时变化
                SimplePagerTitleView simplePagerTitleView = new ScaleTransitionPagerTitleView(context);
                int padding = SizeUtils.dp2px(5);
                simplePagerTitleView.setPadding(padding, 0, padding, 0);
                simplePagerTitleView.setText(titles.get(index));

                simplePagerTitleView.setTextSize(TypedValue.COMPLEX_UNIT_PX, SizeUtils.dp2px(17));
                simplePagerTitleView.setNormalColor(getResources().getColor(com.efounder.ospmobilelib.R.color.title_NormalTextColor));
                if (titles.size() == 1) {
                    simplePagerTitleView.setSelectedColor(getResources().getColor(com.efounder.ospmobilelib.R.color.title_TextColor));
                } else {
                    simplePagerTitleView.setSelectedColor(getResources().getColor(com.efounder.ospmobilelib.R.color.title_SelectTextColor));
                }
                simplePagerTitleView.setOnClickListener(onClickListener);
                badgePagerTitleView.setInnerPagerTitleView(simplePagerTitleView);
                TextView badgeTextView = (TextView) LayoutInflater.from(context).inflate(com.efounder.ospmobilelib.R.layout.simple_count_badge_layout, null);
                badgeTextView.setVisibility(View.GONE);
                badgePagerTitleView.setBadgeView(badgeTextView);
                badgePagerTitleView.setXBadgeRule(new BadgeRule(BadgeAnchor.CONTENT_RIGHT, -UIUtil.dip2px(context, 3)));
                badgePagerTitleView.setYBadgeRule(new BadgeRule(BadgeAnchor.CONTENT_TOP, -UIUtil.dip2px(context, 2)));
                badgePagerTitleView.setAutoCancelBadge(false);
                return badgePagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setStartInterpolator(new AccelerateInterpolator());
                indicator.setEndInterpolator(new DecelerateInterpolator(1.6f));
                indicator.setYOffset(UIUtil.dip2px(context, 39));
                indicator.setLineHeight(UIUtil.dip2px(context, 0));
                indicator.setColors(getResources().getColor(com.efounder.ospmobilelib.R.color.title_SelectTextColor));
                return indicator;
            }
        });
        topMagicTabs.setNavigator(commonNavigator);
    }

    //获取MagicIndicator
    public MagicIndicator getMagicIndicator() {
        return (MagicIndicator) centerCustomView;
    }
}
