package com.efounder.view;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import android.content.Context;

import net.lucode.hackware.magicindicator.buildins.ArgbEvaluatorHolder;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

public class ColorTransitionPagerTitleView extends SimplePagerTitleView {
	public ColorTransitionPagerTitleView(Context context) {
		super(context);
	}

	public void onLeave(int index, int totalCount, float leavePercent, boolean leftToRight) {
		int color = ArgbEvaluatorHolder.eval(leavePercent, this.mSelectedColor, this.mNormalColor);
		this.setTextColor(color);
	}

	public void onEnter(int index, int totalCount, float enterPercent, boolean leftToRight) {
		int color = ArgbEvaluatorHolder.eval(enterPercent, this.mNormalColor, this.mSelectedColor);
		this.setTextColor(color);
	}

	public void onSelected(int index, int totalCount) {
	}

	public void onDeselected(int index, int totalCount) {
	}
}
