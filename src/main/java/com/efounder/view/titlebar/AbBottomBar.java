package com.efounder.view.titlebar;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.core.xml.StubObject;
import com.efounder.ospmobilelib.R;
import com.efounder.util.AbViewUtil;
import com.efounder.util.ImageLoaderUtil;
import com.efounder.widget.EFImage;
import com.efounder.widget.TabBarNew;
import com.pansoft.resmanager.ResFileManager;
import com.utilcode.util.SizeUtils;

import java.io.File;

public class AbBottomBar extends FrameLayout {

    /**
     * 所属Activity.
     */
    private Activity mActivity;

    /**
     * 副标题栏布局ID.
     */
    public int mBottomBarID = 2;

    /**
     * 全局的LayoutInflater对象，已经完成初始化.
     */
    public LayoutInflater mInflater;

    /**
     * 下拉选择.
     */
    private PopupWindow popupWindow;

    /**
     * Window 管理器.
     */
    private WindowManager mWindowManager = null;

    /**
     * 屏幕宽度.
     */
    public int diaplayWidth = 320;

    /**
     * 中间大图标
     */
    private EFImage efImage;

    /**
     * tab 大图标position
     */
    private int position = -1;

//    private Bitmap normalBitmap;
//    private Bitmap selectedBitmap;

    private String normalIconPath;
    private String selectedIconPath;

    /**
     * Instantiates a new ab bottom bar.
     *
     * @param context the context
     * @param attrs   the attrs
     */
    public AbBottomBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        ininBottomBar(context);
    }

    /**
     * Instantiates a new ab bottom bar.
     *
     * @param context the context
     */
    public AbBottomBar(Context context) {
        super(context);
        ininBottomBar(context);

    }

    @Override
    public void addView(final View child, ViewGroup.LayoutParams params) {
        if (child instanceof TabBarNew && ((TabBarNew) child).getTopOutPosition() != -1) {
            position = ((TabBarNew) child).getTopOutPosition();
            StubObject stubObject = ((TabBarNew) child).getTopOutStubObject();
//			String normalIcon = stubObject.getObject(TabBarNew.NORMALICON, "").toString();
            String normalIcon = TabBarNew.getLocalIconFileName(stubObject, true);
            normalIconPath = ResFileManager.IMAGE_DIR + "/" + normalIcon;
//            normalBitmap = getBitmap(normalIcon);
//			String selectedIcon = stubObject.getObject(TabBarNew.SELECTEDICON, "").toString();
            String selectedIcon = TabBarNew.getLocalIconFileName(stubObject, false);
            selectedIconPath = ResFileManager.IMAGE_DIR + "/" + selectedIcon;
//            selectedBitmap = getBitmap(selectedIcon);
            FrameLayout.LayoutParams layoutParams;
            efImage = new EFImage(getContext());
            if(stubObject.getString("isIconCenter","").equals("true")){
                layoutParams = new FrameLayout.LayoutParams(params.height, params.height);
                layoutParams.gravity = Gravity.CENTER_HORIZONTAL|Gravity.BOTTOM;
                efImage.setPadding(15,15,15,15);
            }else {
                layoutParams = new FrameLayout.LayoutParams(SizeUtils.dp2px(60), SizeUtils.dp2px(60));
                layoutParams.gravity = Gravity.CENTER_HORIZONTAL | Gravity.TOP;
            }


//            efImage.setImageBitmap(normalBitmap);
            Drawable drawable = new BitmapDrawable(getResources(), normalIconPath);
            efImage.setImageDrawable(drawable);
            efImage.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((TabBarNew) child).setCurrentTab(position);
                }
            });
            super.addView(child, params);
            this.addView(efImage, layoutParams);
        } else {
            super.addView(child, params);
        }
    }

    /**
     * tabBarNew tab切换时，改变大图标
     *
     * @param index
     */
    public void onTabChanged(int index) {
        //position == -1, 没有大图标，return
        if (position == -1) {
            return;
        }
        if (position == index) {
            Drawable drawable = new BitmapDrawable(getResources(), selectedIconPath);
            efImage.setImageDrawable(drawable);
        } else {
            Drawable drawable = new BitmapDrawable(getResources(), normalIconPath);
            efImage.setImageDrawable(drawable);
        }
    }

    private Bitmap getBitmap(String path) {
        File file = new File(path);
        if (file.exists()) {
            return ImageLoaderUtil.getBitmapfromFilewithoutfixes(path, 1);
        } else {
            return BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_blank);
        }
    }

    /**
     * Inin bottom bar.
     *
     * @param context the context
     */
    public void ininBottomBar(Context context) {

        mActivity = (Activity) context;
        //水平排列
//		this.setOrientation(LinearLayout.HORIZONTAL);
        this.setId(mBottomBarID);
        this.setPadding(0, 0, 0, 0);

        mInflater = LayoutInflater.from(context);

        mWindowManager = mActivity.getWindowManager();
        Display display = mWindowManager.getDefaultDisplay();
        diaplayWidth = display.getWidth();

    }


    /**
     * 描述：标题栏的背景图.
     *
     * @param res 背景图资源ID
     */
    public void setBottomBarBackground(int res) {
        this.setBackgroundResource(res);
    }


    /**
     * 描述：标题栏的背景图.
     *
     * @param color 背景颜色值
     */
    public void setBottomBarBackgroundColor(int color) {
        this.setBackgroundColor(color);
    }


    /**
     * 描述：设置标题背景.
     *
     * @param d 背景图
     */
    public void setBottomBarBackgroundDrawable(Drawable d) {
        this.setBackgroundDrawable(d);
    }

    /**
     * 描述：下拉菜单的的实现方法.
     *
     * @param parent     the parent
     * @param view       要显示的View
     * @param offsetMode 不填满的模式
     */
    private void showWindow(View parent, View view, boolean offsetMode) {
        AbViewUtil.measureView(view);
        int popWidth = parent.getMeasuredWidth();
        if (view.getMeasuredWidth() > parent.getMeasuredWidth()) {
            popWidth = view.getMeasuredWidth();
        }
        int popMargin = this.getMeasuredHeight();

        if (offsetMode) {
            popupWindow = new PopupWindow(view, popWidth, LayoutParams.WRAP_CONTENT, true);
        } else {
            popupWindow = new PopupWindow(view, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, true);
        }

        int[] location = new int[2];
        parent.getLocationInWindow(location);
        int startX = location[0] - parent.getLeft();
        if (startX + popWidth >= diaplayWidth) {
            startX = diaplayWidth - popWidth - 2;
        }

        // 使其聚集
        popupWindow.setFocusable(true);
        // 设置允许在外点击消失
        popupWindow.setOutsideTouchable(true);
        // 这个是为了点击“返回Back”也能使其消失，并且并不会影响你的背景
        popupWindow.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));

        popupWindow.showAtLocation(parent, Gravity.BOTTOM | Gravity.LEFT, startX, popMargin + 2);
    }

    /**
     * 描述：设置下拉的View.
     *
     * @param parent the parent
     * @param view   the view
     */
    public void setDropDown(final View parent, final View view) {
        if (parent == null || view == null) {
            return;
        }
        parent.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                showWindow(parent, view, true);
            }
        });

    }

    /**
     * 描述：设置副标题栏界面显示.
     *
     * @param view the new bottom view
     */
    public void setBottomView(View view) {
        removeAllViews();
        addView(view, new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
    }

    /**
     * 描述：用指定资源ID表示的View填充主界面.
     *
     * @param resId 指定的View的资源ID
     */
    public void setBottomView(int resId) {
        setBottomView(mInflater.inflate(resId, null));
    }


}
