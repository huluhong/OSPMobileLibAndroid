package com.efounder.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.core.xml.StubObject;
import com.efounder.agency.activity.AgentFgNew;

import java.util.List;

/**
 * Created by apple on 2018/2/26.
 */

public class AgentFgAdapter extends FragmentPagerAdapter {
    private final List<String> mDataList;
    private final StubObject stubObject;

    public AgentFgAdapter(FragmentManager fm, List<String> mDataList, StubObject stubObject) {
        super(fm);
        this.mDataList = mDataList;
        this.stubObject = stubObject;
    }
//processed
    @Override
    public Fragment getItem(int position) {
        Object agentFgNew;
        if(position == 0){
            agentFgNew = AgentFgNew.newInstance(false,"pending",stubObject);
        }else {
            agentFgNew = AgentFgNew.newInstance(false,"processed",stubObject);

        }
        return (Fragment) agentFgNew;
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }
}
