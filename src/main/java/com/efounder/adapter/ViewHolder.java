package com.efounder.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.efounder.util.ImageLoader;
import com.efounder.util.ImageLoader.Type;

/**
 * 匹配通用adapter
 * @author long
 *
 */
public class ViewHolder
{
	private final SparseArray<View> mViews;
	private int mPosition;
	private View mConvertView;
	Context context;

	private ViewHolder(Context context, ViewGroup parent, int layoutId,
			int position)
	{
		this.mPosition = position;
		this.context = context;
		this.mViews = new SparseArray<View>();
		mConvertView = LayoutInflater.from(context).inflate(layoutId, parent,
				false);
		// setTag
		mConvertView.setTag(this);
	}

	/**
	 * 拿到一个ViewHolder对象
	 * 
	 * @param context
	 * @param convertView
	 * @param parent
	 * @param layoutId
	 * @param position
	 * @return
	 */
	public static ViewHolder get(Context context, View convertView,
								 ViewGroup parent, int layoutId, int position)
	{
		if (convertView == null)
		{
			return new ViewHolder(context, parent, layoutId, position);
		}
		return (ViewHolder) convertView.getTag();
	}

	public View getConvertView()
	{
		return mConvertView;
	}

	/**
	 * 通过控件的Id获取对于的控件，如果没有则加入views
	 * 
	 * @param viewId
	 * @return
	 */
	public <T extends View> T getView(int viewId)
	{
		View view = mViews.get(viewId);
		if (view == null)
		{
			view = mConvertView.findViewById(viewId);
			mViews.put(viewId, view);
		}
		return (T) view;
	}
	
	/**
	 *设置会议开始结束时间
	 * @param viewId
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public ViewHolder setMeetingTime(int viewId, String hyzy, String startTime, String endTime)
	{
		TextView view = getView(viewId);
	
		//0:未开始;1:进行中;2:结束，3:取消
		if (!startTime.equals(" ")&& !endTime.equals(" ") && ("0".equals(hyzy) || "1".equals(hyzy) )) {
			view.setText("会议时间："+startTime+"-"+endTime);
			 view.setVisibility(view.VISIBLE);
			 
		}else {
			view.setVisibility(view.GONE);
			
		}
		
		return this;
	}
	/**
	 * 设置会议室是否正在使用
	 * 2015/12/18 yqs
	 * @param viewId
	 * @param isUsing
	 * @param imageView
	 * @return
	 */
	public ViewHolder setisUsing(int viewId2, int viewId3, int viewbg, String hyszt, String hyjj)
	{
		ImageView view3 = getView(viewId3);
		TextView view2=getView(viewId2);
		View view4=getView(viewbg);
		if ("0".equals(hyszt) || "1".equals(hyszt)) {
			 //view.setVisibility(view.VISIBLE);
			 view2.setVisibility(view2.VISIBLE);
			 view2.setText(hyjj);
			 view3.setVisibility(view3.VISIBLE);
			 view4.setVisibility(view4.VISIBLE);
		}else {
			//view.setVisibility(view.GONE);
			view2.setVisibility(view2.GONE);
			view3.setVisibility(view3.GONE);
			 view4.setVisibility(view4.GONE);
			 //view2.setText("空闲");
		}
		
		return this;
	}
	/**
	 * 为TextView设置字符串
	 * 
	 * @param viewId
	 * @param text
	 * @return
	 */
	public ViewHolder setText(int viewId, String text)
	{
		TextView view = getView(viewId);
		view.setText(text);
		return this;
	}

	/**
	 * 为ImageView设置图片
	 * 
	 * @param viewId
	 * @param drawableId
	 * @return
	 */
	public ViewHolder setImageResource(int viewId, int drawableId)
	{
		ImageView view = getView(viewId);
		view.setImageResource(drawableId);

		return this;
	}

	/**
	 * 为ImageView设置图片
	 * 
	 * @param viewId
	 * @param drawableId
	 * @return
	 */
	public ViewHolder setImageBitmap(int viewId, Bitmap bm)
	{
		ImageView view = getView(viewId);
		view.setImageBitmap(bm);
		return this;
	}

	/**
	 * 为ImageView设置图片
	 * 
	 * @param viewId
	 * @param drawableId
	 * @return
	 */
	public ViewHolder setImageByUrl(int viewId, String url)
	{
		ImageLoader.getInstance(3, Type.LIFO).loadImage(url,
				(ImageView) getView(viewId));
		//第二种加载图片方式  实现内存缓存以及本地缓存
//		((ImageView) getView(viewId)).setImageResource(R.drawable.ic_launcher);
//		com.efounder.image.utils.ImageLoader.getInstance(context).loadBitmaps((ImageView) getView(viewId), url);
		return this;
	}

	public int getPosition()
	{
		return mPosition;
	}

}