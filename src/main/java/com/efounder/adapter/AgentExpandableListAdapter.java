package com.efounder.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.efounder.activity.AgentDetailAct;
import com.efounder.builder.base.data.EFRowSet;
import com.efounder.ospmobilelib.R;
import com.efounder.util.AbViewUtil;
import com.efounder.util.MyDialog;
import com.efounder.widget.SlideView;
import com.efounder.widget.SlideView.onRightItemClickListener1;
import com.efounder.widget.SlideView.onRightItemClickListener2;
import com.pansoft.espflow.util.FlowTaskUtil;
import com.pansoft.espmodel.FormModel;
import com.pansoft.xmlparse.FormatSet;
import com.pansoft.xmlparse.FormatTable;
import com.pansoft.xmlparse.MobileFormatUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author long
 *
 */
public class AgentExpandableListAdapter extends BaseExpandableListAdapter {
	private static final String TAG = "AgentExpandableListAdapter";
	Context mContext;
	FormatSet formatSet;
	FormatTable ft;
	Map<String ,List<EFRowSet>>  map;
	List<List<EFRowSet>> maplist;
	public List<String> mapKey;
	private MyDialog dialog;
	FormModel formModel;
	
    public AgentExpandableListAdapter(Context context,Map<String ,List<EFRowSet>> result) {
		this.mContext = context;
		this.map = result;
		maplist = new ArrayList<List<EFRowSet>>();
		mapKey =  new ArrayList<String>();
		for (String key : map.keySet()) {
			   Log.i(TAG,"key= "+ key + " and value= " + map.get(key));
			   maplist.add(map.get(key));
			   mapKey.add(key);
	    }
		

		formatSet =	MobileFormatUtil.getInstance().getFormatSet();
		ft = formatSet.getFormatTableById("TASKPendingDataSet");
		
	}


	
    
    //重写ExpandableListAdapter中的各个方法
    @Override
    public int getGroupCount() {
        // TODO Auto-generated method stub
        return maplist.size();
    }

    @Override
    public List<EFRowSet> getGroup(int groupPosition) {
        // TODO Auto-generated method stub
        return maplist.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        // TODO Auto-generated method stub
        return groupPosition;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        // TODO Auto-generated method stub
        return maplist.get(groupPosition).size();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return maplist.get(groupPosition).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return childPosition;
    }
    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return true;
    }

    @SuppressLint("ResourceAsColor")
	@Override
    public View getGroupView(int groupPosition, boolean isExpanded,
            View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
    	 RelativeLayout parentLayout=(RelativeLayout) View.inflate(mContext, R.layout.layout_group, null);
    	 TextView titleName=(TextView) parentLayout.findViewById(R.id.title_name);
    	 TextView titleNum = (TextView) parentLayout.findViewById(R.id.title_num);
         ImageView parentImageViw=(ImageView) parentLayout.findViewById(R.id.select_down);
         //判断isExpanded就可以控制是按下还是关闭，同时更换图片 
         if(isExpanded){
             parentImageViw.setBackgroundResource(R.drawable.up_select);
        	           
         }else{
             parentImageViw.setBackgroundResource(R.drawable.right_select);
        	
        	// parentImageViw.setBackgroundResource(R.drawable.up_select);
         } 
         titleName.setText(mapKey.get(groupPosition));
         titleNum.setText(getChildrenCount(groupPosition)+"");
        
        return parentLayout;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
            boolean isLastChild, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
    	//注意 子布局不能是RelativeLayout 格式
    	
    	LinearLayout childLayout=(LinearLayout) View.inflate(mContext, R.layout.item_body, null);
    	
    	SlideView slideView = new SlideView(mContext,mContext.getResources(),childLayout);
    	slideView.setOnRightItemClickListener1(new onRightItemClickListener1() {
			
			@Override
			public void onRightItemClick() {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext,AgentDetailAct.class);
				EFRowSet child = (EFRowSet) getChild(groupPosition, childPosition);
				Bundle bundle = new Bundle();
				bundle.putSerializable("data", child);
				intent.putExtra("dataSource", bundle);
				mContext.startActivity(intent);
			}
		});
    	slideView.setOnRightItemClickListener2(new onRightItemClickListener2() {
			@Override
			public void onRightItemClick() {
				// TODO Auto-generated method stub
				try {
					formModel = FlowTaskUtil.openTaskWithRowSet((EFRowSet) getChild(groupPosition, childPosition));
				} catch (Exception e) {
					e.printStackTrace();
				}
				dialog = new MyDialog(mContext, R.style.shenhedialog, MyDialog.DIALOG_TAKE_BACK,formModel);
				// 设置它的ContentView
				dialog.setContentView(R.layout.shen_he_dialog);
				dialog.show();
//				Toast.makeText(mContext, "退回 " + groupPosition+"...."+childPosition, Toast.LENGTH_SHORT)
//                .show();
			}
		});
        
    	 EFRowSet rowset = maplist.get(groupPosition).get(childPosition);
         List<Map<String,String>> list = FlowTaskUtil.openDetaiFromRowsetToMap(rowset,ft);
        LinearLayout container = (LinearLayout) childLayout.findViewById(R.id.container);
        
//        List<Map<String,String>> mapList = maplist.get(groupPosition).get(childPosition);
        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        for(int i=0;i<list.size();i++){
        	LinearLayout layoutchild = new LinearLayout(mContext);
        	layoutchild.setLayoutParams(params);
      	  	layoutchild.setPadding(5, 5, 5, 5);
        	Map<String,String> mapchild = list.get(i);
      	  for (String key : mapchild.keySet()) {
      		TextView tv = new TextView(mContext);
      		tv.setText( key  + mapchild.get(key));
      		tv.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimension(R.dimen.size_14sp));
      		tv.setWidth((int) AbViewUtil.dip2px(mContext, 200));
      		tv.setTextColor(mContext.getResources().getColor(R.color.gray));
      		layoutchild.addView(tv);
      	  }
      	  container.addView(layoutchild);
        }
        
        return slideView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition,
            int childPosition) {
        // TODO Auto-generated method stub
        return true;
    }

}
