package com.efounder.adapter;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.ospmobilelib.R;
import com.efounder.utils.JfResourceUtil;
import com.pansoft.resmanager.ResFileManager;

import java.io.File;
import java.util.List;

public class MenuListViewAdapter extends BaseAdapter {

    /* 数据集合 */
    private List<StubObject> data;

    private LayoutInflater layoutInflater;
    private Context context;
//    /*全选回调*/
//    ViewTagInterface viewTagInterFace = null;

//    //这个是为首页上刷新界面写的回调，等有空去掉这个回调
//    private RefreshforDefineMenu mListener;
//    //持久化
//    private StorageUtil storageUtil;

    private String fromString;

    public MenuListViewAdapter(Context context, List<StubObject> data,
                                String fromString) {
        this.context = context;
        this.data = data;
        this.fromString = fromString;
        this.layoutInflater = LayoutInflater.from(context);
    }

    /**
     * 组件集合，对应list.xml中的控件
     *
     * @author Administrator
     */
    public  class Zujian {
        public ImageView image;
        public TextView title;
        public ImageView view;
        public TextView info;
        public Button button;
    }

    @Override
    public int getCount() {
        // 屏蔽掉原来的更多
        return data.size() - 1;
    }

    /**
     * 获得某一位置的数据
     */
    @Override
    public Object getItem(int position) {
        return data.get(position);
    }
    //返回当前布局的样式type
      @Override
      public int getItemViewType(int position) {
          String infoStr = ((StubObject) data.get(position)).getString("info",
                  "").toString();
          int type = 0;
          if(infoStr.equals("")){
              //  zujian.info.setVisibility(View.GONE);
              type = 0;
          }else{
             type =1;
          }
               return type;
             }

             //返回你有多少个不同的布局
             @Override
             public int getViewTypeCount() {
            return 2;
        }
    /**
     * 获得唯一标识
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Zujian zujian = null;
        String infoStr = ((StubObject) data.get(position)).getString("info",
                "").toString();
        if (convertView == null) {
            if (!infoStr.equals("")) {
                zujian = new Zujian();
                // 获得组件，实例化组件
                convertView = layoutInflater.inflate(R.layout.menulistitem, null);
                zujian.image = (ImageView) convertView.findViewById(R.id.icon);
                zujian.title = (TextView) convertView.findViewById(R.id.title);
                zujian.view = (ImageView) convertView
                        .findViewById(R.id.imagebutton);
                zujian.button = (Button) convertView
                        .findViewById(R.id.imagebutton1);
                zujian.info = (TextView) convertView.findViewById(R.id.info);
                convertView.setTag(zujian);
            } else {
                zujian = new Zujian();
                // 获得组件，实例化组件
                convertView = layoutInflater.inflate(R.layout.menulistitem1, null);
                zujian.image = (ImageView) convertView.findViewById(R.id.icon);
                zujian.title = (TextView) convertView.findViewById(R.id.title);
                zujian.view = (ImageView) convertView
                        .findViewById(R.id.imagebutton);
                zujian.button = (Button) convertView
                        .findViewById(R.id.imagebutton1);
                zujian.info = (TextView) convertView.findViewById(R.id.info);
                convertView.setTag(zujian);
            }
        }

        else {
            zujian = (Zujian) convertView.getTag();
        }
        // 绑定数据

        ApplicationInfo appInfo = context.getApplicationInfo();

        String menuIcon = (String) ((StubObject) data.get(position)).getString(
                "menuIcon", "");
        menuIcon = ResFileManager.IMAGE_DIR + "/" + menuIcon;
        File file = new File(menuIcon);
        Bitmap bm = null;
        if (file.exists()) {
            bm = BitmapFactory.decodeFile(menuIcon);
        } else if (bm == null) {
            int resID = context.getResources().getIdentifier("ic_blank_white",
                    "drawable", appInfo.packageName);
            bm = BitmapFactory.decodeResource(context.getResources(), resID);
        }

        zujian.image.setImageBitmap(bm);
        zujian.title.setText(((StubObject) data.get(position)).getString(
                "caption", "").toString());

        if(infoStr.equals("")){
          //  zujian.info.setVisibility(View.GONE);
        }else{
            zujian.info.setText(infoStr);

        }



        if ((Boolean) ((StubObject) data.get(position)).getBoolean("ischecked",
                false)) {
            zujian.view.setBackgroundResource(R.drawable.menufragment_cancle);
            zujian.button.setText("移除");
            zujian.button.setBackgroundResource(R.drawable.button_shape_menu);
            zujian.button.setTextColor(JfResourceUtil.getSkinColor(R.color.chat_red));
        } else {
            zujian.view.setBackgroundResource(R.drawable.menufragment_choice);
            zujian.button.setText("添加");
            zujian.button.setBackgroundResource(R.drawable.button_shape_menu_add);
            zujian.button.setTextColor(context.getResources().getColor(R.color.white));
        }

        zujian.button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                boolean ischecked = false;
                // TODO Auto-generated method stub
                if ((Boolean) ((StubObject) data.get(position)).getBoolean(
                        "ischecked", false)) {

//                    ((ImageView) v)
//                            .setBackgroundResource(R.drawable.menufragment_choice);
                    ((StubObject) data.get(position)).setBoolean("ischecked",
                            false);
                    ((Button) v).setText("添加");

                    ((Button) v).setBackgroundResource(R.drawable.button_shape_menu_add);
                    ((Button) v).setTextColor(context.getResources().getColor(R.color.white));


                    String appId = ((StubObject) data.get(position)).getString("appId","").toString();

                    if (!appId.equals("")) {
                        String chatIdNotReachedByChatListItem = EnvironmentVariable.getProperty("ChatIdNotReachedByChatListItem", "");
                        if (!chatIdNotReachedByChatListItem.equals("")) {
                            String[] chatIdNotReachedByChatListItemArray = chatIdNotReachedByChatListItem.split(",");
                            String chatIdNotReachedByChatListItemNow = "";
                            for (int i = 0; i < chatIdNotReachedByChatListItemArray.length; i++) {
                                String chatId = chatIdNotReachedByChatListItemArray[i];
                                if (!chatId.equals(appId) && !chatId.equals("")) {
                                    chatIdNotReachedByChatListItemNow += chatId + ",";
                                }

                            }
                            EnvironmentVariable.setProperty("ChatIdNotReachedByChatListItem", chatIdNotReachedByChatListItemNow);
                        }
                    }

                } else {
                    ischecked = true;
//                    ((ImageView) v)
//                            .setBackgroundResource(R.drawable.menufragment_cancle);
                    ((Button) v).setText("移除");
                    ((Button) v).setBackgroundResource(R.drawable.button_shape_menu);
                    ((Button) v).setTextColor(context.getResources().getColor(R.color.chat_red));
                    ((StubObject) data.get(position)).setBoolean("ischecked",
                            true);
                    String appId = ((StubObject) data.get(position)).getString("appId",
                            "").toString();
                    if (!appId.equals("")) {
                        EnvironmentVariable.setProperty("ChatIdNotReachedByChatListItem", EnvironmentVariable.getProperty("ChatIdNotReachedByChatListItem", "") + "," + appId);
                    }

                }

            }
        });
        return convertView;
    }



}
