package com.efounder.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.efounder.builder.base.data.EFDataSet;
import com.efounder.builder.base.data.EFRowSet;
import com.efounder.ospmobilelib.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * expandableListView适配器
 *
 */
public class MyExpandListviewAdapter extends BaseExpandableListAdapter {
	private Context context;
	private List<HashMap<String ,String >> group;
	private List<List<HashMap<String ,String >>> child;
	EFDataSet efDataSet;
	String gridTitle ;
	public MyExpandListviewAdapter(Context context, EFDataSet efDataSet, String title) {
		this.context = context;
		this.efDataSet = efDataSet;
		gridTitle = title;
		
        group = new ArrayList<HashMap<String, String>>();
		
		HashMap<String, String> group1 = new HashMap<String, String>();
		group1.put("imgid", R.drawable.expandlistview_group_img2 + "");
		group1.put("title", gridTitle);
		group.add(group1);
		
		child = new ArrayList<List<HashMap<String, String>>>();
		EFRowSet flowRowSet = null;
		List<HashMap<String, String>> list1 = new ArrayList<HashMap<String, String>>();
		for (int i = 0; i < efDataSet.getRowCount(); i++) {
			flowRowSet = efDataSet.getRowSet(i);
			HashMap<String, String> child1 = new HashMap<String, String>();
			child1.put("F_XWBT", flowRowSet.getString("F_XWBT", ""));
			child1.put("F_URL", flowRowSet.getString("F_URL", ""));
			child1.put("F_XWRQ", flowRowSet.getString("F_XWRQ", ""));
			list1.add(child1);
		}
		child.add(list1);
	}

	@Override
	public int getGroupCount() {
		return group.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return child.get(groupPosition).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return group.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return child.get(groupPosition).get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}
	
	/**
	 * 显示：group
	 */
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		ViewHolderGroup holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.expandlistview_header, null);
			holder = new ViewHolderGroup();
			holder.expandlistview_group_img = (ImageView) convertView.findViewById(R.id.expandlistview_group_img);
			holder.title = (TextView) convertView
					.findViewById(R.id.headertitle);
			holder.num =(TextView) convertView
					.findViewById(R.id.headernum);
			holder.arrow =(ImageView) convertView
					.findViewById(R.id.arrows);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolderGroup) convertView.getTag();
		}
		holder.expandlistview_group_img.setBackgroundResource(Integer.parseInt(((HashMap<String,String>)group.get(groupPosition)).get("imgid")));
		holder.title.setText(((HashMap<String,String>)group.get(groupPosition)).get("title"));
		holder.num.setText(child.get(groupPosition).size()+"");
		
		  if(isExpanded){
			  convertView.setBackgroundResource(R.drawable.back1);
			  holder.arrow.setBackgroundResource(R.drawable.arrow_down);
          }else{
        	  convertView.setBackgroundResource(R.drawable.back21);
        	  holder.arrow.setBackgroundResource(R.drawable.arrow_left);
          } 
		return convertView;

	}
	
	/**
	 * 显示：child
	 */
	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		ViewHolderChild holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.expandlistviewmodel, null);
			holder = new ViewHolderChild();
			holder.title = (TextView) convertView
					.findViewById(R.id.title);
			holder.date = (TextView) convertView
					.findViewById(R.id.date);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolderChild) convertView.getTag();
		}
		holder.title.setText(((HashMap<String,String>)child.get(groupPosition).get(childPosition)).get("F_XWBT"));
		holder.date.setText(((HashMap<String,String>)child.get(groupPosition).get(childPosition)).get("F_XWRQ"));
		convertView.setBackgroundResource(R.drawable.back4);
		if(isLastChild){
		convertView.setBackgroundResource(R.drawable.back3);
		
		}
		return convertView;
	}

	class ViewHolderGroup {
		ImageView expandlistview_group_img;
		TextView title;
		TextView num;
		ImageView arrow;
		
	}
	class ViewHolderChild {
		TextView title;
		TextView date;
		
	}
	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}
