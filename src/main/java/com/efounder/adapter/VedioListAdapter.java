//package com.efounder.adapter;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.efounder.data.model.ItemData;
//import com.efounder.ospmobilelib.R;
//
//import java.util.List;
//
//public class VedioListAdapter extends BaseAdapter {
//	private List<ItemData> listDatas;
//	private LayoutInflater mInflater;
//
//	public VedioListAdapter(Context context,List<ItemData> list){
//		this.listDatas = list;
//		mInflater = LayoutInflater.from(context);
//	}
//	@Override
//	public int getCount() {
//		// TODO Auto-generated method stub
//		return listDatas.size();
//	}
//
//	@Override
//	public Object getItem(int position) {
//		// TODO Auto-generated method stub
//		return listDatas.get(position);
//	}
//
//	@Override
//	public long getItemId(int position) {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//	@Override
//	public View getView(int position, View convertView, ViewGroup parent) {
//		// TODO Auto-generated method stub
//		ViewHolder holder;
//		if(convertView==null||(holder = (ViewHolder) convertView.getTag()).flag != position){
//			holder = new ViewHolder();
//			holder.flag = position;
//			convertView = mInflater.inflate(R.layout.vedio_list_item,null);
//			holder.im = (ImageView)convertView.findViewById(R.id.iv_icon);
//			holder.title = (TextView)convertView.findViewById(R.id.tv_title);
//			convertView.setTag(holder);
//		}else{
//			holder = (ViewHolder) convertView.getTag();
//		}
//
//		ItemData data = listDatas.get(position);
//		if(data!=null){
//			holder.im.setImageBitmap(data.getMenuIcon());
//			holder.title.setText(data.getCaption());
//		}else{
//			holder.title.setText("测试数据");
//		}
//
//		return convertView;
//	}
//
//	public class ViewHolder{
//		private TextView title ;
//		private ImageView im;
//		private int flag;
//	}
//}
