package com.efounder.adapter;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.efounder.ospmobilelib.R;
import com.efounder.util.DatesUtil;

import java.util.List;
import java.util.Map;

/**
 * @author ruanjianjiagou@163.com
 * @date 2015.1.24
 */
public class TimelineAdapter extends BaseAdapter {

    private Context context;
    private List<Map<String, Object>> list;
    private LayoutInflater inflater;

    // 待审批 0 正在审批 1 审批完毕 2
    private final String noApproval = "0", approvalIng = "1", approvaled = "2",
            approvalback = "3";

    public TimelineAdapter(Context context, List<Map<String, Object>> list) {
        super();
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {

        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder controller = null;
        inflater = LayoutInflater.from(parent.getContext());
        if (convertView == null || (controller = (ViewHolder) convertView.getTag()).flag != position) {
            controller = new ViewHolder();
            if (position == 0) {
                controller.flag = position;
                convertView = inflater.inflate(R.layout.header_approval_layout, null);
                controller.approver1 = (TextView) convertView
                        .findViewById(R.id.approver1);
                controller.preparer1 = (TextView) convertView
                        .findViewById(R.id.preparer1);
                controller.make_date1 = (TextView) convertView
                        .findViewById(R.id.make_date1);
                controller.taskName = (TextView) convertView
                        .findViewById(R.id.textView1);

            } else if (position == list.size() - 1) {
                controller.flag = position;
                convertView = inflater.inflate(R.layout.bottom_approval_layout, null);
            } else {
                controller.flag = position;
                //20160105
                //convertView = inflater.inflate(R.layout.approval_listview_item, null);
                convertView = inflater.inflate(R.layout.new_approval_list_items, null);

                controller.task = (TextView) convertView
                        .findViewById(R.id.task);
                controller.approver = (TextView) convertView
                        .findViewById(R.id.approver);
                controller.rooticon = (ImageView) convertView
                        .findViewById(R.id.rooticon);
                controller.preparer = (TextView) convertView
                        .findViewById(R.id.preparer);
                controller.make_date = (TextView) convertView
                        .findViewById(R.id.make_date);
                controller.note = (TextView) convertView.findViewById(R.id.state);
                controller.stateRl = (RelativeLayout) convertView.findViewById(R.id.right_state);
            }
        } else {
            controller = (ViewHolder) convertView.getTag();
        }
        convertView.setTag(controller);

        if (controller.flag == 0) {

            controller.approver1.setText(list.get(1).get("approver").toString());
            controller.preparer1.setText(list.get(1).get("preparer").toString());
            String time = list.get(1).get("make_date").toString();
            controller.make_date1.setText(DatesUtil.getStringByFormat(time, "yyyyMMdd"));
            controller.taskName.setText(list.get(1).get("FLOW_NAME").toString());
        }

        if (controller.flag != list.size() - 1 && controller.flag != 0) {

            if (list.size() > 4 && position == 3) {//测试用
                controller.stateRl.setVisibility(View.VISIBLE);
            }

            controller.task.setText(list.get(position).get("NODE_TAG_NAME").toString());

            // 审批角色
            controller.approver.setText(list.get(position).get("approver").toString());
            // 审批人
            controller.preparer.setText(list.get(position).get("preparer").toString());
            // 制单时间
            String time = list.get(position).get("make_date").toString();
            controller.make_date.setText(DatesUtil.getStringByFormat(time, "yyyy-MM-dd"));
            // 审批意见
            controller.note.setText(list.get(position).get("note").toString());
            // 审批状态
            String statestr = list.get(position).get("state").toString();
            if (noApproval.equals(statestr)) {
                controller.rooticon.setBackgroundResource(R.drawable.noapproval);
            } else if (approvalIng.equals(statestr)) {
                controller.rooticon.setBackgroundResource(R.drawable.animation);
                AnimationDrawable animationDrawable = (AnimationDrawable) controller.rooticon.getBackground();
                //启动动画
                animationDrawable.start();
            } else if (approvaled.equals(statestr)) {
                controller.rooticon.setBackgroundResource(R.drawable.aproved_new);
            } else if (approvalback.equals(statestr)) {
                controller.rooticon.setBackgroundResource(R.drawable.aprovedback_new);

            }
        }


        return convertView;
    }

    class ViewHolder {
        //审批任务的名称
        public TextView taskName;
        /**
         * 节点任务名称
         */
        public TextView task;
        /**
         * 中心轴节点
         */
        public ImageView rooticon;
        /**
         * 审批角色
         */
        public TextView approver;
        public TextView approver1;

        /**
         * 节点负责人
         */
        public TextView preparer;
        public TextView preparer1;
        /**
         * 审批时间
         */
        public TextView make_date;
        public TextView make_date1;
        /**
         * 审批状态
         */
        public TextView note;
        /**
         * 右侧状态
         */
        public RelativeLayout stateRl;
        int flag = -1;
    }
}
