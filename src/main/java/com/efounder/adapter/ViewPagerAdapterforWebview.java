package com.efounder.adapter;

import android.os.Parcelable;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.webkit.WebView;

import java.util.ArrayList;

/**
 * 
 * @author 巨蟹巨蟹
 * @email zansl@qq.com
 * 
 */
public class ViewPagerAdapterforWebview extends PagerAdapter {

	private ArrayList<WebView> mPageViews;

	public ViewPagerAdapterforWebview(ArrayList<WebView> mPageViews) {

		this.mPageViews = mPageViews;

	}

	@Override
	public int getCount() {
		return mPageViews.size();
	}

	@Override
	public boolean isViewFromObject(View v, Object arg1) {
		return v == arg1;
	}

	@Override
	public int getItemPosition(Object object) {
		// TODO Auto-generated method stub
		return super.getItemPosition(object);
	}

	@Override
	public void destroyItem(View v, int p, Object arg2) {
		// TODO Auto-generated method stub
		((ViewPager) v).removeView(mPageViews.get(p));
	}

	@Override
	public Object instantiateItem(View v, int p) {
		// TODO Auto-generated method stub

		WebView webview = mPageViews.get(p);
		webview.loadUrl("file:///android_asset/lead0" +(p+1)+ ".html");
		((ViewPager) v).addView(webview);
		return webview;
	}

	@Override
	public void restoreState(Parcelable arg0, ClassLoader arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public Parcelable saveState() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void startUpdate(View arg0) {

	}

	@Override
	public void finishUpdate(View arg0) {
		// TODO Auto-generated method stub

	}

}
