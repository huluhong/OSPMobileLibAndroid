package com.efounder.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.efounder.builder.base.data.EFRowSet;
import com.efounder.ospmobilelib.R;
import com.efounder.util.AbViewUtil;
import com.efounder.util.MyDialog;
import com.pansoft.espflow.util.FlowTaskUtil;
import com.pansoft.espmodel.FormModel;
import com.pansoft.xmlparse.FormatSet;
import com.pansoft.xmlparse.FormatTable;
import com.pansoft.xmlparse.MobileFormatUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * expandableListView适配器
 * 
 */
public class MyExpandListviewAdapterSlide extends BaseExpandableListAdapter {
	private Context mContext;
	FormatSet formatSet;
	FormatTable ft;
	Map<String, List<EFRowSet>> map;
	List<List<EFRowSet>> maplist;
	public List<String> mapKey;
	
	
	/**提交审核*/
	private static final int DIALOG_SHEN_HE = 0x0000;
	/**取回动作*/
	private static final int DIALOG_TAKE_BACK = 0x0001;
	/**退回动作*/
	private static final int DIALOG_ROLL_BACK = 0x0002;
	
	ChildClick childClick ;

	public ChildClick getChildClick() {
		return childClick;
	}

	public void setChildClick(ChildClick childClick) {
		this.childClick = childClick;
	}

	public MyExpandListviewAdapterSlide(Context context,
			Map<String, List<EFRowSet>> result) {
		this.mContext = context;
		this.map = result;
		maplist = new ArrayList<List<EFRowSet>>();
		mapKey = new ArrayList<String>();
		for (String key : map.keySet()) {
			maplist.add(map.get(key));
			mapKey.add(key);
		}
		formatSet = MobileFormatUtil.getInstance().getFormatSet();
		ft = formatSet.getFormatTableById("TASKPendingDataSet");

	}

	@Override
	public int getGroupCount() {
		return maplist.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return maplist.get(groupPosition).size();
	}

	@Override
	public List<EFRowSet> getGroup(int groupPosition) {
		return maplist.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return maplist.get(groupPosition).get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	/**
	 * 显示：group
	 */
	@SuppressLint("ResourceAsColor")
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		ViewHolderGroup holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.expandlistview_header, null);
			holder = new ViewHolderGroup();
			holder.title = (TextView) convertView
					.findViewById(R.id.headertitle);
			holder.num = (TextView) convertView.findViewById(R.id.headernum);
			holder.arrow = (ImageView) convertView.findViewById(R.id.arrows);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolderGroup) convertView.getTag();
		}
		holder.title.setText(mapKey.get(groupPosition));
		holder.num.setText(getChildrenCount(groupPosition) + "");

		if (isExpanded) {
			convertView.setBackgroundResource(R.drawable.back1);
			// holder.arrow.setBackgroundResource(R.drawable.arrow_down);
			holder.arrow.setBackgroundResource(R.drawable.arrow_down);
		} else {
			convertView.setBackgroundResource(R.drawable.back21);
			// holder.arrow.setBackgroundResource(R.drawable.arrow_left);
			holder.arrow.setBackgroundResource(R.drawable.arrow_left);
		}
		return convertView;

	}

	/**
	 * 显示：child
	 */
	@Override
	public View getChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// ViewHolderChild holder;
		if (convertView == null) {
			convertView = new com.efounder.widget.CoustomerItemView(mContext,
					R.layout.item);
			Button btn = (Button) convertView.findViewById(R.id.side_btn);
			Button btn2 = (Button) convertView.findViewById(R.id.side_btn2);
			LinearLayout childLayout = (LinearLayout) convertView
					.findViewById(R.id.front_view);
			final EFRowSet rowset = maplist.get(groupPosition).get(childPosition);
			List<Map<String, String>> list = FlowTaskUtil
					.openDetaiFromRowsetToMap(rowset, ft);
			LayoutParams params = new LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

			for (int i = 0; i < list.size(); i++) {
				//LinearLayout layoutchild = new LinearLayout(mContext);
				//layoutchild.setLayoutParams(params);
				//layoutchild.setPadding(30, 5, 5, 5);// 第一个参数可以修改回忆申请流程，提交人
													// 距离左边的距离
				/*Map<String, String> mapchild = list.get(i);
				if(i == 0 )
				dwmc.setText(mapchild.get(""));
				if(i == 1){
				tjPeople.setText("提交人："+mapchild.get("提交人: "));
				if(null!=mapchild.get("日期: "))
				tjTime.setText("提交时间："+mapchild.get("日期: "));
				}*/
				LinearLayout layoutchild = new LinearLayout(mContext);
	        	layoutchild.setLayoutParams(params);
	      	  	layoutchild.setPadding(45, 5, 5, 5);//第一个参数可以修改回忆申请流程，提交人 距离左边的距离
	        	Map<String,String> mapchild = list.get(i);
	      	    for (String key : mapchild.keySet()) {
	      		TextView tv = new TextView(mContext);
	      		tv.setText( key  + mapchild.get(key));

	      		tv.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimension(R.dimen.size_14sp));
	      		tv.setWidth((int) AbViewUtil.dip2px(mContext, 200));
	      		tv.setTextColor(mContext.getResources().getColor(R.color.gray));
	      		layoutchild.addView(tv);
	      	  }
	        	childLayout.addView(layoutchild);



			/*	for (String key : mapchild.keySet()) {
					TextView tv = new TextView(mContext);
					tv.setText(key + mapchild.get(key));

					tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext
							.getResources().getDimension(R.dimen.size_16sp));
					tv.setWidth((int) AbViewUtil.dip2px(mContext, 150));
					tv.setTextColor(mContext.getResources().getColor(
							R.color.black));
					btn.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

						}
					});
					btn2.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

						}
					});
					layoutchild.addView(tv);

				}*/
				//childLayout.addView(layoutchild);

			}
			childLayout.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					childClick.onClick(groupPosition,childPosition);
				}
			});
			btn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					createDialog(DIALOG_SHEN_HE,rowset);

				}
			});
			btn2.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					createDialog(DIALOG_ROLL_BACK,rowset);

				}
			});
			// convertView.setTag(holder);
		} else {
			// holder = (ViewHolderChild) convertView.getTag();
		}
		convertView.setBackgroundResource(R.drawable.back4);
		if (isLastChild) {
			convertView.setPadding(0, 0, 0, 9);
			convertView.setBackgroundResource(R.drawable.back3);
			
		}
		return convertView;
	}

	class ViewHolderGroup {
		TextView title;
		TextView num;
		ImageView arrow;

	}

	class ViewHolderChild {
		TextView title;
		TextView tjr;
		TextView dwmc;

	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
	
	  protected Dialog createDialog(int id,EFRowSet rowset) {
		  FormModel formModel = null;
			try {
				formModel = FlowTaskUtil.openTaskWithRowSet(rowset);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Dialog dialog = null;
			/*dialog = new MyDialog(mContext, R.style.shenhedialog,DIALOG_SHEN_HE,formModel);
			dialog.setContentView(R.layout.shen_he_dialog);
			dialog.setContentView(R.layout.new_shen_he_dialog);*/
			switch (id) {
			case DIALOG_SHEN_HE:
				dialog = new MyDialog(mContext, R.style.shenhedialog,DIALOG_SHEN_HE,formModel);
				// 设置它的ContentView
				dialog.setContentView(R.layout.new_shen_he_dialog);
				dialog.show();
				dialog.show();
				break;
			case DIALOG_TAKE_BACK:
				dialog = new MyDialog(mContext, R.style.shenhedialog,DIALOG_TAKE_BACK,formModel);
				// 设置它的ContentView
				dialog.setContentView(R.layout.new_shen_he_dialog);
				dialog.show();
				break;
			case DIALOG_ROLL_BACK:
				dialog = new MyDialog(mContext, R.style.shenhedialog,DIALOG_ROLL_BACK,formModel);
				// 设置它的ContentView
				dialog.setContentView(R.layout.new_shen_he_dialog);
				dialog.show();
				break;
				
			}
			
			return dialog;
		}
	  
	  public interface ChildClick{
		  public void onClick(int parentp, int childp);
	  }

}
