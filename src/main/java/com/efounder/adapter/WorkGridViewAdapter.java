package com.efounder.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.ospmobilelib.R;
import com.pansoft.resmanager.ResFileManager;

import java.util.List;

/**
 *
 * @author cimu
 * @date 2018/10/19
 * RecyclerView中的Adapter
 */

public class WorkGridViewAdapter extends BaseAdapter{
    private Context mContext;
    private List<StubObject> mList;
    private StubObject list;

    public WorkGridViewAdapter(Context context, List<StubObject> list) {
        super();
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public StubObject getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = View.inflate(mContext, R.layout.grid_item_currency_group, null);
            new ViewHolder(view);
        }
        //取得图标url
        String menuIcon = (String) mList.get(i).getString(
                "menuIcon", "");
        menuIcon = "file://" + ResFileManager.IMAGE_DIR + "/" + menuIcon;
        ViewHolder holder = (ViewHolder) view.getTag();
        list = getItem(i);
        holder.tvText.setText(list.getCaption());
        LXGlideImageLoader.getInstance().displayImage(mContext,holder.ivItemImage,menuIcon,
                R.drawable.iframe_icon_refresh,R.drawable.iframe_icon_refresh);
        return view;
    }
    class ViewHolder{
        private ImageView ivItemImage;
        private TextView tvText;
        public ViewHolder(View view){
        ivItemImage = view.findViewById(R.id.itemImage);
        tvText = view.findViewById(R.id.tv_work_produce);
        view.setTag(this);
        }
    }
}
