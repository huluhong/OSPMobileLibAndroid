package com.efounder.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.efounder.builder.base.data.EFRowSet;
import com.efounder.ospmobilelib.R;
import com.efounder.util.AbViewUtil;
import com.pansoft.espflow.util.FlowTaskUtil;
import com.pansoft.xmlparse.FormatSet;
import com.pansoft.xmlparse.FormatTable;
import com.pansoft.xmlparse.MobileFormatUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * expandableListView适配器
 */
public class MyExpandListviewAdapterNew extends BaseExpandableListAdapter {
    private Context mContext;
    FormatSet formatSet;
    FormatTable ft;
    Map<String, List<EFRowSet>> map;
    List<List<EFRowSet>> maplist;
    public List<String> mapKey;

    public MyExpandListviewAdapterNew(Context context, Map<String, List<EFRowSet>> result) {
        this.mContext = context;
        this.map = result;
        maplist = new ArrayList<List<EFRowSet>>();
        mapKey = new ArrayList<String>();
        for (String key : map.keySet()) {
            maplist.add(map.get(key));
            mapKey.add(key);
        }
        formatSet = MobileFormatUtil.getInstance().getFormatSet();
        ft = formatSet.getFormatTableById("TASKPendingDataSet");


    }
    public void setSystemType(String type) {
        if ("YJWX".equals(type)) {
            ft = formatSet.getFormatTableById("YJWXTASKPendingDataSet");
        }

    }
    @Override
    public int getGroupCount() {
        return maplist.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return maplist.get(groupPosition).size();
    }

    @Override
    public List<EFRowSet> getGroup(int groupPosition) {
        return maplist.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return maplist.get(groupPosition).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    /**
     * 显示：group
     */
    @SuppressLint("ResourceAsColor")
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        ViewHolderGroup holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.expandlistview_header, null);
            holder = new ViewHolderGroup();
            holder.title = (TextView) convertView
                    .findViewById(R.id.headertitle);
            holder.num = (TextView) convertView
                    .findViewById(R.id.headernum);
            holder.arrow = (ImageView) convertView
                    .findViewById(R.id.arrows);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolderGroup) convertView.getTag();
        }
        holder.title.setText(mapKey.get(groupPosition));
        //holder.title.setText("会议申请流程");
        holder.num.setText(getChildrenCount(groupPosition) + "");


        if (isExpanded) {
            convertView.setBackgroundResource(R.drawable.back1);
            //holder.arrow.setBackgroundResource(R.drawable.arrow_down);
            holder.arrow.setBackgroundResource(R.drawable.arrow_down);
        } else {
            convertView.setBackgroundResource(R.drawable.back21);
            // holder.arrow.setBackgroundResource(R.drawable.arrow_left);
            holder.arrow.setBackgroundResource(R.drawable.arrow_left);
        }
        return convertView;

    }

    /**
     * 显示：child
     */
    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
//		ViewHolderChild holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.expandlistviewchildnew, null);
//			holder = new ViewHolderChild();
            LinearLayout childLayout = (LinearLayout) convertView.findViewById(R.id.childLayout);
            EFRowSet rowset = maplist.get(groupPosition).get(childPosition);
            List<Map<String, String>> list = FlowTaskUtil.openDetaiFromRowsetToMap(rowset, ft);
            LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);


            for (int i = 0; i < list.size(); i++) {
                LinearLayout layoutchild = new LinearLayout(mContext);
                layoutchild.setLayoutParams(params);
                layoutchild.setOrientation(LinearLayout.VERTICAL);
                layoutchild.setPadding(45, 5, 5, 5);//第一个参数可以修改回忆申请流程，提交人 距离左边的距离
                Map<String, String> mapchild = list.get(i);
                for (String key : mapchild.keySet()) {
                    TextView tv = new TextView(mContext);
                    tv.setText(key + mapchild.get(key));

                    //tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimension(R.dimen.size_14sp));
                    tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
                    tv.setWidth((int) AbViewUtil.dip2px(mContext, 480));
                    tv.setTextColor(mContext.getResources().getColor(R.color.gray));
                    layoutchild.addView(tv);
                }
                childLayout.addView(layoutchild);
            }
//			convertView.setTag(holder);
        } else {
//			holder = (ViewHolderChild) convertView.getTag();
        }
        convertView.setBackgroundResource(R.drawable.back4);
        if (isLastChild) {
            convertView.setBackgroundResource(R.drawable.back3);
        }
        return convertView;
    }

    class ViewHolderGroup {
        TextView title;
        TextView num;
        ImageView arrow;

    }

    class ViewHolderChild {
        TextView title;
        TextView tjr;
        TextView dwmc;

    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
