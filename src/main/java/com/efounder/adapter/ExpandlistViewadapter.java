package com.efounder.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.efounder.model.ExpandListviewData;
import com.efounder.ospmobilelib.R;

import java.util.ArrayList;


public class ExpandlistViewadapter extends BaseAdapter {

    private ArrayList<ExpandListviewData> listdata = null;
    private Context             context       = null;

    /**
     * 构造函数,初始化Adapter,将数据传入
     * @param bookshelfList
     * @param context
     */
    public ExpandlistViewadapter(ArrayList<ExpandListviewData> listdata, Context context) {
        this.listdata = listdata;
        this.context = context;
    }

    @Override
    public int getCount() {
        return listdata == null ? 0 : listdata.size();
    }

    @Override
    public Object getItem(int position) {
        return listdata == null ? null : listdata.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //装载view
        LayoutInflater layoutInflater = LayoutInflater.from(this.context);
        View view = layoutInflater.inflate(R.layout.expandlistviewmodel, null);

        //获取控件
        TextView title = (TextView) view.findViewById(R.id.title);
        TextView date = (TextView) view.findViewById(R.id.date);
        //对控件赋值
        ExpandListviewData listitemData = (ExpandListviewData) getItem(position);
        if (listitemData != null) {
        	title.setText(listdata.get(position).getTitle());
        	date.setText(listdata.get(position).getDate());
        }

        return view;
    }
}