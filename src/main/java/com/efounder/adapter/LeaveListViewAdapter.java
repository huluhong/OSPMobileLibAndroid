//package com.efounder.adapter;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.efounder.builder.base.data.EFRowSet;
//import com.efounder.ospmobilelib.R;
//import com.pansoft.espflow.util.FlowTaskUtil;
//import com.pansoft.xmlparse.FormatSet;
//import com.pansoft.xmlparse.FormatTable;
//import com.pansoft.xmlparse.MobileFormatUtil;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
///**
// * 请销假 2016/11/22
// * @author yqs
// *
// */
//public class LeaveListViewAdapter extends BaseAdapter {
//	private Context mContext;
//	private List<EFRowSet> mEfRowSets;
//	FormatTable ft2;// 请销假读取的xml信息
//	FormatSet formatSet;
//	private static String QJSP = "QJSP";// 请假审批
//	private static String XJSP = "XJSP";// 销假审批
//	private static String MYQJ = "MYQJ";// 我的请假审批
//	private String typeString;
//
//	public LeaveListViewAdapter(Context context, List<EFRowSet> efRowSets,
//			String type) {
//		mContext = context;
//		if (efRowSets != null) {
//			mEfRowSets = efRowSets;
//		} else
//			mEfRowSets = new ArrayList<EFRowSet>();
//		this.typeString = type;
//		formatSet = MobileFormatUtil.getInstance().getFormatSet();
//		if (XJSP.equals(typeString)) {
//			ft2 = formatSet.getFormatTableById("CancelLeaveDataSet");
//		} else {
//			ft2 = formatSet.getFormatTableById("LeaveDataSet");
//		}
//
//	}
//
//	@Override
//	public int getCount() {
//
//		return mEfRowSets.size();
//	}
//
//	@Override
//	public EFRowSet getItem(int position) {
//
//		return mEfRowSets.get(position);
//	}
//
//	@Override
//	public long getItemId(int position) {
//		return position;
//	}
//
//	@Override
//	public View getView(int position, View convertView, ViewGroup parent) {
//		boolean canopen = true;
//		EFRowSet rowset = mEfRowSets.get(position);
//		if (rowset.hasKey("STATUSNAME")) {
//			String status = rowset.getString("STATUSNAME", "");
//			// STATUSNAME=<font color=green>正在审批</font><br>杨作海
//			status = status.replace("<font color=green>", "")
//					.replace("<font color=blue>", "")
//					.replace("<font color=black>", "")
//					.replace("<font color=red>", "")
//					.replace("</font>", "")
//					.replace("<br>", "--");
//
//			rowset.putString("STATUSNAME", status);
//		}
//		if (rowset.hasKey("XJSTATUS")) {
//			// 设置销假审批的状态文字
//			String xjspString = rowset.getString("STATUS", "");
//			if ("".equals(xjspString)) {
//				rowset.putString("STATUS", "未送审");
//			} else if ("s0".equals(xjspString)) {
//				rowset.putString("STATUS", "未送审");
//			} else if ("s1".equals(xjspString)) {
//				rowset.putString("STATUS", "正在审批");
//			} else if ("s2".equals(xjspString)) {
//				rowset.putString("STATUS", "审批通过");
//			} else if ("s9".equals(xjspString)) {
//				rowset.putString("STATUS", "退回");
//			}
//		}
//
//		List<Map<String, String>> list = null;
//		list = FlowTaskUtil.openDetaiFromRowsetToMap(rowset, ft2);// 请销假
//		ViewHolderChild holder;
//		if (convertView == null) {
//			convertView = LayoutInflater.from(mContext).inflate(
//					R.layout.leavelistitem, null);
//			holder = new ViewHolderChild();
//			LinearLayout childLayout = (LinearLayout) convertView
//					.findViewById(R.id.childLayout);
//			holder.textView1 = (TextView) childLayout
//					.findViewById(R.id.textView1);
//			holder.textView2 = (TextView) childLayout
//					.findViewById(R.id.textView2);
//			holder.textView3 = (TextView) childLayout
//					.findViewById(R.id.textView3);
//			holder.textView4 = (TextView) childLayout
//					.findViewById(R.id.textView4);
//			holder.textView5 = (TextView) childLayout
//					.findViewById(R.id.textView5);
//			holder.textView6 = (TextView) childLayout
//					.findViewById(R.id.textView6);
//			holder.lineView = (TextView) convertView
//					.findViewById(R.id.agentline);
//			holder.lastView = convertView.findViewById(R.id.lastbacground);
//
//			convertView.setTag(holder);
//		} else {
//			holder = (ViewHolderChild) convertView.getTag();
//
//		}
//
//		for (int i = 0; i < list.size(); i++) {
//
//			Map<String, String> mapchild1 = list.get(i);
//			for (String key : mapchild1.keySet()) {
//				if (key.equals("假条编号:")) {
//					holder.textView1.setText(mapchild1.get(key));
//					// holder.textView1.setTextColor(mContext.getResources().getColor(R.color.black));
//				} else if (key.equals("工作单位:")) {
//					// holder.textView2.setTextColor(mContext.getResources().getColor(R.color.black));
//					holder.textView2.setText(mapchild1.get(key));
//				} else if (key.equals("姓名:")) {
//					// holder.textView3.setTextColor(mContext.getResources().getColor(R.color.black));
//					holder.textView3.setText(mapchild1.get(key));
//				} else if (key.equals("请假天数:")) {
//					// holder.textView4.setTextColor(mContext.getResources().getColor(R.color.black));
//					holder.textView4.setText(mapchild1.get(key));
//				} else if (key.equals("考勤员:")) {
//					// holder.textView5.setTextColor(mContext.getResources().getColor(R.color.black));
//					holder.textView5.setText(mapchild1.get(key));
//				} else if (key.equals("请假审批:")) {
//					holder.textView6.setTextColor(mContext.getResources()
//							.getColor(R.color.green_approval));
//					holder.textView6.setText(mapchild1.get(key));
//				}
//			}
//
//		}
//		holder.lineView.setVisibility(View.VISIBLE);
//		convertView.setBackgroundResource(R.drawable.back4_9meddle);
//		if (position == 0) {
//			convertView.setBackgroundResource(R.drawable.back_1top);
//		} else if (position == mEfRowSets.size() - 1) {
//			convertView.setBackgroundResource(R.drawable.back3_bottom);
//			holder.lineView.setVisibility(View.INVISIBLE);
//		}
//		if (mEfRowSets.size() == 1) {
//
//			convertView.setBackgroundResource(R.drawable.back_21all);
//			holder.lineView.setVisibility(View.INVISIBLE);
//		}
//
//		return convertView;
//	}
//
//	class ViewHolderChild {
//		TextView textView1;
//		TextView textView2;
//		TextView textView3;
//		TextView textView4;
//		TextView textView5;
//		TextView textView6;
//		TextView lineView;// 黑线
//		View lastView;
//	}
//}
