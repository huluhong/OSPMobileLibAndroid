package com.efounder.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.bean.CurrencyGroupBean;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.ospmobilelib.R;
import com.efounder.util.AbFragmentManager;
import com.pansoft.resmanager.ResFileManager;

import java.util.List;

/**
 * @author cimu
 * @date 2018/10/19
 * 通用分组Fragment中RecyclerView的Adapter
 */

public class CurrencyGroupGridViewAdapter extends RecyclerView.Adapter<CurrencyGroupGridViewAdapter.MyHolder> {

    private Context mContext;
    private List<CurrencyGroupBean> mList;
    private WorkGridViewAdapter mWorkGridViewAdapter;
    private List<StubObject> fourMenuList;
    private AbFragmentManager abFragmentManager;

    public CurrencyGroupGridViewAdapter(Context context, List<CurrencyGroupBean> list) {
        super();
        this.mContext = context;
        this.mList = list;
        abFragmentManager = new AbFragmentManager(mContext);

    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_list_currency_group, parent, false);
        return new MyHolder(view);

    }

    @Override
    public void onBindViewHolder(final MyHolder holder, final int position) {
        //去除item顶部留空
        if (position == 0) {
            holder.grayLine.setVisibility(View.GONE);
        }
        CurrencyGroupBean currencyGroupBean = mList.get(position);
        holder.tvTitle.setText(currencyGroupBean.getTitle());
        String menuIcon = currencyGroupBean.getMenuIcon();
        menuIcon = "file://" + ResFileManager.IMAGE_DIR + "/" + menuIcon;
        LXGlideImageLoader.getInstance().displayImage(mContext, holder.ivMenuIcon,
                menuIcon, R.drawable.left_line, R.drawable.left_line);
        fourMenuList = mList.get(position).getGridView();
        //判断是否配置文件为空
        if (fourMenuList != null) {
            mWorkGridViewAdapter = new WorkGridViewAdapter(mContext, fourMenuList);
            holder.gvWork.setAdapter(mWorkGridViewAdapter);
        }
        //根据配置文件跳转
        holder.gvWork.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (adapterView == holder.gvWork) {
                    try {
                        abFragmentManager.startActivity(mList.get(position).getGridView().get(i), 0, 0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;
        private GridView gvWork;
        private View grayLine;
        private ImageView ivMenuIcon;


        public MyHolder(View itemView) {
            super(itemView);
            ivMenuIcon = (ImageView) itemView.findViewById(R.id.iv_menu_icon);
            tvTitle = itemView.findViewById(R.id.tv_title);
            gvWork = itemView.findViewById(R.id.gv_work);
            grayLine = itemView.findViewById(R.id.gray_line);
        }
    }
}
