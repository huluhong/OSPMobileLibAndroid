package com.efounder.adapter;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * 
 * @author zhenglaikun 
 * 2014.11.26
 * viewPage对应的adapter
 *
 */
public class ViewPagerAdapter extends PagerAdapter {
	
	private ImageView[] views;

	public ViewPagerAdapter(ImageView[] views) {
		this.views = views;
	}

	// 获取当前页面数
	public int getCount() {

		return this.views.length;
	}

	// 初始化position位置的界面
	public Object instantiateItem(ViewGroup container, int position) {
		
		((ViewPager)container).addView(views[position % views.length], 0);
		return views[position % views.length];
	}

	// 判断是否有对象生成界面
	public boolean isViewFromObject(View arg0, Object arg1) {

		return arg0 == arg1;
	}

	@Override
	// 销毁页面
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager)container).removeView(views[position % views.length]);
	}
}

