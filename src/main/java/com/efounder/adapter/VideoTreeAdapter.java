//package com.efounder.adapter;
//
//import android.content.Context;
//import android.util.Log;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.widget.CheckBox;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.TextView;
//
//import com.efounder.data.model.ItemData;
//import com.zhy.bean.TreeBean;
//import com.zhy.tree.bean.TreeNode;
//import com.zhy.tree.bean.VideoTreeListViewAdapter;
//import com.zhy.tree_view.R;
//
//import java.util.Iterator;
//import java.util.List;
//import java.util.Set;
//
//public class VideoTreeAdapter extends VideoTreeListViewAdapter {
//	private SelectedCountCallBack selectedCountCallBack;
//	private Set<ItemData> collectListData;
//
//	public VideoTreeAdapter(ListView mTree, Context context,
//			List<TreeBean> datas, int defaultExpandLevel,
//			Set<ItemData> collectListData) throws IllegalArgumentException,
//			IllegalAccessException {
//		super(mTree, context, datas, defaultExpandLevel);
//		this.collectListData = collectListData;
//	}
//
//	@Override
//	public View getConvertView(TreeNode node, final int position,
//			View convertView, ViewGroup parent) {
//		// boolean isExists = isExist(node);
//		// if (isExists) {
//		// node.setChecked(true);
//		// }
//		ViewHolder viewHolder = null;
//		if (convertView == null) {
//			convertView = mInflater.inflate(R.layout.video_list_item, parent,
//					false);
//			viewHolder = new ViewHolder();
//			viewHolder.icon = (ImageView) convertView
//					.findViewById(R.id.id_treenode_icon);
//			viewHolder.checkBox = (CheckBox) convertView
//					.findViewById(R.id.id_treenode_checkbox);
//
//			viewHolder.label = (TextView) convertView
//					.findViewById(R.id.id_treenode_label);
//			convertView.setTag(viewHolder);
//
//		} else {
//			viewHolder = (ViewHolder) convertView.getTag();
//		}
//		viewHolder.checkBox.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				CheckBox checkBox = (CheckBox) v;
//				boolean isChecked = checkBox.isChecked();
//				Log.i("", "------ischecked:" + isChecked);
//				// XXX node final不行，position final可以
//				mVisibleNodes.get(position).setChecked(isChecked);
//				if ((isChecked && mVisibleNodes.get(position).getAttrs()
//						.containsKey("CameraInfo"))) {
//					mVisibleNodes.get(position).setIcon(
//							R.drawable.video_camera_checked);
//				} else if (mVisibleNodes.get(position).getAttrs()
//						.containsKey("CameraInfo")) {
//					mVisibleNodes.get(position).setIcon(
//							R.drawable.video_camera_nocheck);
//
//				}
//				if (selectedCountCallBack != null) {
//					selectedCountCallBack.getSelectedCount();
//				}
//				notifyDataSetChanged();
//
//			}
//		});
//		// /if (node.getIcon() == -1) {
//		viewHolder.icon.setVisibility(View.VISIBLE);
//		if (node.getpId().equals("1")) {// 控制中心节点
//			viewHolder.icon.setImageResource(R.drawable.video_parent1);
//			node.setIcon(R.drawable.video_parent1);
//		} else {// 区域节点
//			viewHolder.icon.setImageResource(R.drawable.video_parent2);
//			node.setIcon(R.drawable.video_parent2);
//		}
//		if (node.getAttrs().containsKey("CameraInfo")) {// 包含摄像头信息的节点
//
//			if (node.isChecked()) {
//				viewHolder.icon
//						.setImageResource(R.drawable.video_camera_checked);
//				node.setIcon(R.drawable.video_camera_checked);
//			} else {
//				viewHolder.icon
//						.setImageResource(R.drawable.video_camera_nocheck);
//				node.setIcon(R.drawable.video_camera_nocheck);
//			}
//
//		}
//
//		// } else {
//		// viewHolder.icon.setVisibility(View.VISIBLE);
//		// viewHolder.icon.setImageResource(node.getIcon());
//		//
//		//
//		// }
//
//		if (node.isHasCheckBox()) {
//			viewHolder.checkBox.setVisibility(View.VISIBLE);
//			if (node.getIcon() == -1) {
//				viewHolder.icon.setVisibility(View.INVISIBLE);
//			}
//
//		} else {
//			viewHolder.checkBox.setVisibility(View.GONE);
//		}
//		viewHolder.checkBox.setChecked(node.isChecked());
//		viewHolder.label.setText(node.getName());
//
//		return convertView;
//	}
//
//	private final class ViewHolder {
//		ImageView icon;
//		CheckBox checkBox;
//		TextView label;
//	}
//
//	//
//	// /**
//	// * 判断收藏列表中是否存在 该摄像头
//	// *
//	// * @param node
//	// * @return
//	// */
//	// private boolean isExist(TreeNode node) {
//	// if (collectListData != null && collectListData.size() > 0
//	// && node.getAttrs().containsKey("CameraInfo")) {
//	// for (ItemData data : collectListData) {
//	// if (data.getCameraID().equals(
//	// ((com.hikvision.vmsnetsdk.CameraInfo) node.getAttrs()
//	// .get("CameraInfo")).cameraID)
//	// && data.getDeviceID()
//	// .equals(((com.hikvision.vmsnetsdk.CameraInfo) node
//	// .getAttrs().get("CameraInfo")).deviceID)) {
//	// node.setChecked(true);
//	// return true;
//	//
//	// }
//	// }
//	// return false;
//	// }
//	// return false;
//	//
//	// }
//
//	/**
//	 * 设置被收藏的状态
//	 *
//	 * @param node
//	 */
//	public void setExistState(TreeBean node) {
//		if (collectListData != null && collectListData.size() > 0
//				&& node.getAttrs().containsKey("CameraInfo")) {
//			Iterator<ItemData> it = collectListData.iterator();
//			while (it.hasNext()) {
//				try {
//					ItemData data = it.next();
//					if (data.getCameraID().equals(
//							((com.hikvision.vmsnetsdk.CameraInfo) node
//									.getAttrs().get("CameraInfo")).cameraID)
//							&& data.getDeviceID()
//									.equals(((com.hikvision.vmsnetsdk.CameraInfo) node
//											.getAttrs().get("CameraInfo")).deviceID)) {
//						node.setChecked(true);
//						it.remove();
//					}
//				} catch (Exception e) {
//
//				}
//			}
//		}
//
//	}
//
//	/**
//	 * 回调获取选中的数量
//	 *
//	 * @author yqs
//	 *
//	 */
//	public interface SelectedCountCallBack {
//		public void getSelectedCount();
//	}
//
//	public void SetSelectedCountCallBack(
//			SelectedCountCallBack selectedCountCallBack) {
//		this.selectedCountCallBack = selectedCountCallBack;
//
//	};
//}
