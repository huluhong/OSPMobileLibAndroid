package com.efounder.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.efounder.agency.activity.TaskDetaiSecondActivity;
import com.efounder.agency.utils.TaskDataParser;
import com.efounder.builder.base.data.EFRowSet;
import com.efounder.ospmobilelib.R;
import com.inqbarna.tablefixheaders.adapters.BaseTableAdapter;

public class MatrixTableAdapter<T> extends BaseTableAdapter {

    private final static int WIDTH_DIP = 110;
    private final static int HEIGHT_DIP = 50;

    private final Context context;

    private T[][] table;

    private final int width;
    private final int height;

    public MatrixTableAdapter(Context context) {
        this(context, null);
    }

    public MatrixTableAdapter(Context context, T[][] table) {
        this.context = context;
        Resources r = context.getResources();


        width = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, WIDTH_DIP, r.getDisplayMetrics()));
        height = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, HEIGHT_DIP, r.getDisplayMetrics()));

        setInformation(table);
    }

    public void setInformation(T[][] table) {
        this.table = table;
    }

    @Override
    public int getRowCount() {
        return table.length - 1;
    }

    @Override
    public int getColumnCount() {
        return table[0].length - 1;
    }

    @Override
    public View getView(int row, int column, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_table_border, parent, false);

            //convertView = new TextView(context);

//			((TextView) convertView).setTextColor(Color.BLACK);
            //((TextView) convertView).setBackgroundColor(context.getResources().getColor(R.color.white));
//			((TextView) convertView).setGravity(Gravity.CENTER_VERTICAL);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.formtext);
        EFRowSet rowSet = (EFRowSet) table[row + 1][column + 1];
        // 未知rowse为何为null  可能是某条特定单据
        if (rowSet == null) {
            textView.setText("");
            return convertView;
        }
        String elementName = rowSet.getString(TaskDataParser.KEY_ELEMENTNAME,
                "");
        //如果类型是input  得到 value的值，并将该textvierw字体设置为蓝色，跳转根据  value
        if ("input".equals(elementName)) {
            final String value = rowSet.getString("value", "");
            if (value != null && !value.equals("")) {
                textView.setTextColor(Color.BLUE);
                textView.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        //System.out.println(value);
                        System.out.println("adapter86行+value:" + value);
                        Intent intent = new Intent(context, TaskDetaiSecondActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("value", value);
                        bundle.putString("type", "form");
                        intent.putExtra("dataSource", bundle);
                        context.startActivity(intent);
                    }
                });
            }
        }

        textView.setBackgroundColor(context.getResources().getColor(R.color.billdetail_form1));
        //textView.setGravity(Gravity.CENTER);

        String text = getTdText(rowSet);
        //textView.setText(table[row + 1][column + 1].toString());
        textView.setText(text);
        return convertView;
    }

    @Override
    public int getHeight(int row) {
        return height;
    }

    @Override
    public int getWidth(int column) {
        return width;
    }

    @Override
    public int getItemViewType(int row, int column) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    /**
     * 获取一个网格的文本
     *
     * @param tdRowSet
     * @return
     */
    private String getTdText(EFRowSet tdRowSet) {
        String text = null;

        String elementName = tdRowSet.getString(TaskDataParser.KEY_ELEMENTNAME,
                "");

        if ("input".equals(elementName)) {
            text = tdRowSet.getString("title", "");
        } else {

            text = tdRowSet.getString(TaskDataParser.KEY_ELEMENTTEXT, "");

        }
        return text;
    }
}
