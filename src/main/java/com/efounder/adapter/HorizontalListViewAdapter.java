//package com.efounder.adapter;
//
//import android.content.Context;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.efounder.data.model.ItemData;
//import com.efounder.ospmobilelib.R;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.InputStream;
//import java.util.List;
//
//public class HorizontalListViewAdapter extends BaseAdapter{
//
//	List<ItemData> listData;
//	public List<ItemData> getListData() {
//		return listData;
//	}
//	public void setListData(List<ItemData> listData) {
//		this.listData = listData;
//	}
//	Context context;
//	public HorizontalListViewAdapter(Context con,List<ItemData> listData){
//		context = con;
//		mInflater=LayoutInflater.from(con);
//		this.listData = listData;
//	}
//	@Override
//	public int getCount() {
//		return listData.size();
//	}
//	private LayoutInflater mInflater;
//	@Override
//	public Object getItem(int position) {
//		return position;
//	}
//	private static class ViewHolder {
//		private TextView time ;
//		private TextView title ;
//		private ImageView im;
//		private int flag;
//	}
//	@Override
//	public long getItemId(int position) {
//		return position;
//	}
//
//	@Override
//	public View getView(int position, View convertView, ViewGroup parent) {
//		ViewHolder holder;
//		if(convertView==null||(holder = (ViewHolder) convertView.getTag()).flag != position){
//			holder = new ViewHolder();
//			holder.flag = position;
//			convertView = mInflater.inflate(R.layout.horizontallistview_item,null);
//			holder.im = (ImageView)convertView.findViewById(R.id.iv_pic);
//			holder.time = (TextView)convertView.findViewById(R.id.tv_time);
//			holder.title = (TextView)convertView.findViewById(R.id.tv_name);
//			convertView.setTag(holder);
//		}else{
//			holder = (ViewHolder) convertView.getTag();
//		}
//		ItemData data = listData.get(position);
//
//		String bitmapURL = data.getBitmapURL();
//		if(bitmapURL!=null&&!bitmapURL.equals("")){
//			/* Bitmap bm = BitmapFactory.decodeFile(bitmapURL);
//			 holder.im.setImageBitmap(bm);*/
//			 File file = new File(bitmapURL);
//				if (file.exists()) {
//	            	//Bitmap bm = BitmapFactory.decodeFile(menuIcon);
//					FileInputStream is1;
//					try {
//
//						is1 = new FileInputStream(bitmapURL);
//						//2.为位图设置100K的缓存
//
//						BitmapFactory.Options opts=new BitmapFactory.Options();
//
//						opts.inTempStorage = new byte[100 * 1024];
//
//						//3.设置位图颜色显示优化方式
//
//						//ALPHA_8：每个像素占用1byte内存（8位）
//
//						//ARGB_4444:每个像素占用2byte内存（16位）
//
//						//ARGB_8888:每个像素占用4byte内存（32位）
//
//						//RGB_565:每个像素占用2byte内存（16位）
//
//						//Android默认的颜色模式为ARGB_8888，这个颜色模式色彩最细腻，显示质量最高。但同样的，占用的内存//也最大。也就意味着一个像素点占用4个字节的内存。我们来做一个简单的计算题：3200*2400*4 bytes //=30M。如此惊人的数字！哪怕生命周期超不过10s，Android也不会答应的。
//
//						opts.inPreferredConfig = Bitmap.Config.RGB_565;
//
//						//4.设置图片可以被回收，创建Bitmap用于存储Pixel的内存空间在系统内存不足时可以被回收
//
//						opts.inPurgeable = true;
//
//						//5.设置位图缩放比例
//
//						//width，hight设为原来的四分一（该参数请使用2的整数倍）,这也减小了位图占用的内存大小；例如，一张//分辨率为2048*1536px的图像使用inSampleSize值为4的设置来解码，产生的Bitmap大小约为//512*384px。相较于完整图片占用12M的内存，这种方式只需0.75M内存(假设Bitmap配置为//ARGB_8888)。
//
//						opts.inSampleSize = 4;
//
//						//6.设置解码位图的尺寸信息
//
//						opts.inInputShareable = true;
//
//						//7.解码位图
//
//						Bitmap btp =BitmapFactory.decodeStream(is1,null, opts);
//
//
//						holder.im.setImageBitmap(btp);
//					} catch (FileNotFoundException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//						InputStream is = context.getResources().openRawResource(R.drawable.vediodefault);
//			            BitmapFactory.Options options=new BitmapFactory.Options();
//			            options.inJustDecodeBounds = false;
//			            options.inSampleSize = 4;   //width，hight设为原来的十分一
//			            Bitmap btp =BitmapFactory.decodeStream(is,null,options);
//			            holder.im.setImageBitmap(btp);
//					}
//
//			// bm.recycle();
//				}
//		}else{
//			InputStream is = context.getResources().openRawResource(R.drawable.vediodefault);
//            BitmapFactory.Options options=new BitmapFactory.Options();
//            options.inJustDecodeBounds = false;
//            options.inSampleSize = 4;   //width，hight设为原来的十分一
//            Bitmap btp =BitmapFactory.decodeStream(is,null,options);
//            holder.im.setImageBitmap(btp);
//		}
//
//
//	holder.title.setText(data.getCaption());
//    return convertView;
//}
//}