package com.efounder.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.efounder.ospmobilelib.R;
import com.efounder.widget.PublicNumberMessageItem;

import java.util.List;
import java.util.Map;

public class NoticeAdapter extends BaseAdapter {

	private List<List<Map<String, Object>>>data;
	private LayoutInflater layoutInflater;
	private Context context;
	public NoticeAdapter(Context context,List<List<Map<String, Object>>> data){
		this.context=context;
		this.data=data;
		this.layoutInflater=LayoutInflater.from(context);
	}
	/**
	 * 组件集合，对应list.xml中的控件
	 * @author Administrator
	 */
	public final class Zujian{
		public PublicNumberMessageItem publicNumberMessageItem;
	}
	@Override
	public int getCount() {
		return data.size();
	}
	/**
	 * 获得某一位置的数据
	 */
	@Override
	public Object getItem(int position) {
		return data.get(position);
	}
	/**
	 * 获得唯一标识
	 */
	@Override
	public long getItemId(int position) {
		return position;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Zujian zujian = null;
		if(convertView==null){
			zujian = new Zujian();
			convertView = layoutInflater.inflate(R.layout.noticeitem,null);
			zujian.publicNumberMessageItem = (PublicNumberMessageItem) convertView.findViewById(R.id.publicnumbermessageitem);
			
			convertView.setTag(zujian);  
			
		}else{
		zujian = (Zujian) convertView.getTag();	
		}
		List<Map<String, Object>> map = data.get(position);
		zujian.publicNumberMessageItem.setCard(map);
		return convertView;
	}


}
