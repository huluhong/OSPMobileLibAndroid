//package com.efounder.adapter;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.efounder.data.model.HYZLModel;
//import com.efounder.ospmobilelib.R;
//
//import java.util.List;
//
//
//public class MeetingFileAdapter<T> extends BaseAdapter {
//	private List<T> listData;
//	private LayoutInflater mInflater;
//
//	public MeetingFileAdapter(Context context,List<T> list){
//		mInflater = LayoutInflater.from(context);
//		listData = list;
//	}
//	@Override
//	public int getCount() {
//		// TODO Auto-generated method stub
//		return listData.size();
//	}
//
//	@Override
//	public Object getItem(int position) {
//		// TODO Auto-generated method stub
//		return listData.get(position);
//	}
//
//	@Override
//	public long getItemId(int position) {
//		// TODO Auto-generated method stub
//		return 0;
//	}
//
//	@Override
//	public View getView(int position, View convertView, ViewGroup parent) {
//		// TODO Auto-generated method stub
//		ViewHolder holder;
//		if(convertView==null||(holder = (ViewHolder) convertView.getTag()).flag != position){
//			holder = new ViewHolder();
//			holder.flag = position;
//			convertView = mInflater.inflate(R.layout.meeting_file_list_item, null);
//			holder.imageView = (ImageView) convertView.findViewById(R.id.iv_im);
//			holder.textView = (TextView) convertView.findViewById(R.id.tv_tt);
//			convertView.setTag(holder);
//		}else{
//			holder = (ViewHolder) convertView.getTag();
//		}
//		HYZLModel meetingFile = (HYZLModel) listData.get(position);
//		if(meetingFile!=null){
//			holder.imageView.setImageBitmap(meetingFile.getHeadImg());
//			holder.textView.setText(meetingFile.getF_NAME());
//		}
//		return convertView;
//	}
//
//	public class ViewHolder{
//		ImageView imageView;
//		TextView textView;
//		int flag = -1;
//	}
//}
