package com.efounder.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.efounder.builder.base.data.EFRowSet;
import com.efounder.ospmobilelib.R;
import com.pansoft.espflow.util.FlowTaskUtil;
import com.pansoft.xmlparse.FormatSet;
import com.pansoft.xmlparse.FormatTable;
import com.pansoft.xmlparse.MobileFormatUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * expandableListView适配器
 * 
 */
public class MyExpandListAdpFileManager extends BaseExpandableListAdapter {
	private Context mContext;
	FormatSet formatSet;
	FormatTable ft;// OA和文件审批读取的xml信息
	FormatTable ft1;// 会议申请读取的xml信息
	Map<String, List<EFRowSet>> map;
	List<List<EFRowSet>> maplist;
	public List<String> mapKey;

	public MyExpandListAdpFileManager(Context context,
			Map<String, List<EFRowSet>> result) {
		this.mContext = context;
		this.map = result;
		System.out.println(result.keySet());
		maplist = new ArrayList<List<EFRowSet>>();
		mapKey = new ArrayList<String>();
		// for (String key : map.keySet()) {

		// }
//		maplist.add(map.get("ZYYTJG"));
//		mapKey.add("ZYYTJG");
//
//		maplist.add(map.get("ZYYTFGS"));
//		mapKey.add("ZYYTFGS");
//
//		maplist.add(map.get("PGYTFGS"));
//		mapKey.add("PGYTFGS");
//
//		maplist.add(map.get("KTJFGS"));
//		mapKey.add("KTJFGS");
//
//		maplist.add(map.get("NMYTFGS"));
//		mapKey.add("NMYTFGS");
//
//		maplist.add(map.get("CONTRACT"));
//		mapKey.add("CONTRACT");

		maplist.add(map.get("FLOW_TASK_LIST"));
		mapKey.add("FLOW_TASK_LIST");

		formatSet = MobileFormatUtil.getInstance().getFormatSet();
		ft1 = formatSet.getFormatTableById("TASKPendingDataSet");
		ft = formatSet.getFormatTableById("FilePendingDataSet");
	}

	@Override
	public int getGroupCount() {
		return maplist.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		if (maplist.get(groupPosition) != null)
			return maplist.get(groupPosition).size();
		else
			return 0;
	}

	@Override
	public List<EFRowSet> getGroup(int groupPosition) {
		return maplist.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return maplist.get(groupPosition).get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	/**
	 * 根据groupPosition 得到group的名字
	 * 
	 * @param groupPosition
	 * @return
	 */
	public String getGroupName(int groupPosition) {
		return mapKey.get(groupPosition);

	}

	/**
	 * 显示：group
	 */
	@SuppressLint("ResourceAsColor")
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		ViewHolderGroup holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.expandlistview_header, null);
			holder = new ViewHolderGroup();
			holder.title = (TextView) convertView
					.findViewById(R.id.headertitle);
			holder.num = (TextView) convertView.findViewById(R.id.headernum);
			holder.arrow = (ImageView) convertView.findViewById(R.id.arrows);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolderGroup) convertView.getTag();
		}
		String type = mapKey.get(groupPosition);
//		if (type.equals("CONTRACT")) {
//			holder.title.setText("合同管理");
//		} else if (type.equals("ZYYTJG")) {
//			holder.title.setText("中原油田机关公文审批");
//		} else if (type.equals("NMYTFGS")) {
//			holder.title.setText("内蒙古油田分公司公文审批");
//		} else if (type.equals("PGYTFGS")) {
//			holder.title.setText("普光油田分公司公文审批");
//
//		} else if (type.equals("KTJFGS")) {
//			holder.title.setText("勘探局分公司公文审批");
//		} else if (type.equals("ZYYTFGS")) {
//			holder.title.setText("中原油田分公司公文审批");
//		} else if(type.equals("FLOW_TASK_LIST")){
//			holder.title.setText("会议申请流程");
//		}

		 holder.title.setText("会议申请流程");
		holder.num.setText(getChildrenCount(groupPosition) + "");

		if (isExpanded) {
			convertView.setBackgroundResource(R.drawable.back1);
			// 如果展开并且child个数是0
			if (getChildrenCount(groupPosition) == 0) {
				convertView.setBackgroundResource(R.drawable.back21);
			}
			// holder.arrow.setBackgroundResource(R.drawable.arrow_down);
			holder.arrow.setBackgroundResource(R.drawable.arrow_down);
		} else {
			convertView.setBackgroundResource(R.drawable.back21);
			// holder.arrow.setBackgroundResource(R.drawable.arrow_left);
			holder.arrow.setBackgroundResource(R.drawable.arrow_left);
		}
		return convertView;

	}

	/**
	 * 显示：child
	 */
	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		ViewHolderChild holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.expandlistviewchild_fix, null);
			holder = new ViewHolderChild();
			LinearLayout childLayout = (LinearLayout) convertView
					.findViewById(R.id.childLayout);
			holder.textView1 = (TextView) childLayout
					.findViewById(R.id.textView1);
			holder.textView2 = (TextView) childLayout
					.findViewById(R.id.textView2);
			holder.textView3 = (TextView) childLayout
					.findViewById(R.id.textView3);
			holder.textView4 = (TextView) childLayout
					.findViewById(R.id.textView4);
			holder.textView5 = (TextView) childLayout
					.findViewById(R.id.textView5);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolderChild) convertView.getTag();
		}

		// 重新给每个TextView赋值
		EFRowSet rowset = maplist.get(groupPosition).get(childPosition);

		List<Map<String, String>> list = null;
		if (rowset.getString("title", "") != null
				&& !rowset.getString("title", "").equals("")) {
			list = FlowTaskUtil.openDetaiFromRowsetToMap(rowset, ft);
		} else {
			list = FlowTaskUtil.openDetaiFromRowsetToMap(rowset, ft1);
		}

		for (int i = 0; i < list.size(); i++) {

			Map<String, String> mapchild = list.get(i);

			for (String key : mapchild.keySet()) {
				// System.out.println("zheshi"+key +""+mapchild.get(key));
				if (key.equals("title")) {
					holder.textView1.setText(mapchild.get(key));
				} else if (key.equals("field1")) {
					holder.textView2.setText(mapchild.get(key));
				} else if (key.equals("field2")) {
					holder.textView3.setText(mapchild.get(key));
				} else if (key.equals("field3")) {
					holder.textView4.setText(mapchild.get(key));
				}

			}

		}

		convertView.setBackgroundResource(R.drawable.back4);
		if (isLastChild) {
			convertView.setBackgroundResource(R.drawable.back3);
		}
		return convertView;
	}

	class ViewHolderGroup {
		TextView title;
		TextView num;
		ImageView arrow;

	}

	class ViewHolderChild {
		TextView textView1;
		TextView textView2;
		TextView textView3;
		TextView textView4;
		TextView textView5;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}
