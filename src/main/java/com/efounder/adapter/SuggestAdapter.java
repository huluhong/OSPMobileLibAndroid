//package com.efounder.adapter;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.efounder.builder.base.data.EFRowSet;
//import com.efounder.ospmobilelib.R;
//
//import java.util.List;
//
//public class SuggestAdapter<T> extends BaseAdapter {
//    private List<T> listData;
//    private LayoutInflater mInflater;
//    private final int OUTTYPE = 0;//发出去的建议
//    private final int INTYPE = 1; //收到的建议
//    private String userId;
//
//    public SuggestAdapter(Context context, List<T> list,String userId) {
//        mInflater = LayoutInflater.from(context);
//        listData = list;
//        this.userId= userId;
//    }
//
//    @Override
//    public int getCount() {
//
//        return listData.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//
//        return listData.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//
//        return 0;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//
//        ViewHolder holder = null;
//        int type = getItemViewType(position);
//        EFRowSet efRowSet = (EFRowSet) listData.get(position);
//        switch (type) {
//            case OUTTYPE:
//                if (convertView == null) {
//                    holder = new ViewHolder();
//                    convertView = mInflater.inflate(R.layout.suggest_out_list_item, null);
//                    holder.imageView = (ImageView) convertView.findViewById(R.id.iv_avatar);
//                    holder.textView = (TextView) convertView.findViewById(R.id.tv_text);
//                    holder.timeView = (TextView) convertView.findViewById(R.id.tv_time);
//                    holder.nameTextView = (TextView) convertView.findViewById(R.id.tv_name);
//                    // holder.nameTextView.setText( AndroidEnvironmentVariable.getUserName());
//                    convertView.setTag(holder);
//                } else {
//                    holder = (ViewHolder) convertView.getTag();
//                }
//                break;
//            case INTYPE:
//                if (convertView == null) {
//                    holder = new ViewHolder();
//                    convertView = mInflater.inflate(R.layout.suggest_in_list_item, null);
//                    holder.imageView = (ImageView) convertView.findViewById(R.id.iv_avatar);
//                    holder.textView = (TextView) convertView.findViewById(R.id.tv_text);
//                    holder.timeView = (TextView) convertView.findViewById(R.id.tv_time);
//                    holder.nameTextView = (TextView) convertView.findViewById(R.id.tv_name);
//                    convertView.setTag(holder);
//                } else {
//                    holder = (ViewHolder) convertView.getTag();
//                }
//                break;
//        }
//
//        holder.timeView.setText(efRowSet.getString("TIME", ""));
//        holder.textView.setText(efRowSet.getString("MESSAGE", ""));
//        holder.nameTextView.setText(efRowSet.getString("FROMUSERNAME", ""));
//        return convertView;
//    }
//
//    public class ViewHolder {
//        ImageView imageView;
//        TextView textView;
//        TextView timeView;
//        TextView nameTextView;
//
//    }
//
//    @Override
//    public int getViewTypeCount() {
//        return 2;
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        EFRowSet efRowSet = (EFRowSet) listData.get(position);
//        if (efRowSet.getString("FROMUSERID", "").equals(userId)) {
//            return OUTTYPE;
//        } else {
//            return INTYPE;
//        }
//    }
//}
