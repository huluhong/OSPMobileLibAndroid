//package com.efounder.adapter;
//
//import android.content.Context;
//import android.util.TypedValue;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.efounder.builder.base.data.EFRowSet;
//import com.efounder.ospmobilelib.R;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
//public class ZhaobiaoListViewAdapter extends BaseAdapter {
//	private Context mContext;
//	private List<EFRowSet> mEfRowSets;
//
//	public ZhaobiaoListViewAdapter(Context context, List<EFRowSet> efRowSets) {
//		mContext = context;
//		if (efRowSets != null) {
//			mEfRowSets = efRowSets;
//		} else
//			mEfRowSets = new ArrayList<EFRowSet>();
//
//	}
//
//	@Override
//	public int getCount() {
//
//		return mEfRowSets.size();
//	}
//
//	@Override
//	public EFRowSet getItem(int position) {
//		// TODO Auto-generated method stub
//		return mEfRowSets.get(position);
//	}
//
//	@Override
//	public long getItemId(int position) {
//		// TODO Auto-generated method stub
//		return position;
//	}
//
//	@Override
//	public View getView(int position, View convertView, ViewGroup parent) {
//		boolean canopen = true;
//		EFRowSet rowset = mEfRowSets.get(position);
//		Map<String, String> map = rowset.getAttriMap();
//		String flag ="";
//		// 过滤一些用不到的标志如<font>，转换审批类型
//		if (map.containsKey("SPFLAG")) {
//			String spFlag = map.get("SPFLAG");
//			if ("zbba".equals(spFlag)) {
//				// map.put("SPFLAG", "招标备案");
//				flag = "招标备案";
//			} else if ("tpxx".equals(spFlag)) {
//				// map.put("SPFLAG", "谈判项目");
//				flag = "谈判项目";
//			} else if ("zbwj".equals(spFlag)) {
//				// map.put("SPFLAG", "招标文件");
//				flag = "招标文件";
//			} else if ("zbtz".equals(spFlag)) {
//				// map.put("SPFLAG", "中标通知");
//				flag = "中标通知";
//			}else if ("zjbl".equals(spFlag)) {
//				// map.put("SPFLAG", "中标通知");
//				flag = "两证办理";
//			}
//		}
//		// STATUSNAME=<font color=green>等待单位领导审批</font>,
//		String statusName = map.get("STATUSNAME");
//		statusName = statusName.replace("<font color=green>", "")
//				.replace("</font>", "").replace("<font color=red>", "")
//				.replace("<font color=blue>", "");
//		 map.put("STATUSNAME",statusName);
//
//		ViewHolderChild holder;
//		if (convertView == null) {
//			convertView = LayoutInflater.from(mContext).inflate(
//					R.layout.expandlistviewchild_fix, null);
//			holder = new ViewHolderChild();
//			LinearLayout childLayout = (LinearLayout) convertView
//					.findViewById(R.id.childLayout);
//			holder.textView1 = (TextView) childLayout
//					.findViewById(R.id.textView1);
//			holder.textView2 = (TextView) childLayout
//					.findViewById(R.id.textView2);
//			holder.textView3 = (TextView) childLayout
//					.findViewById(R.id.textView3);
//			holder.textView4 = (TextView) childLayout
//					.findViewById(R.id.textView4);
//			holder.textView5 = (TextView) childLayout
//					.findViewById(R.id.textView5);
//			holder.lineView = (TextView) convertView
//					.findViewById(R.id.agentline);
//			holder.lastView = convertView.findViewById(R.id.lastbacground);
//
//			convertView.setTag(holder);
//		} else {
//			holder = (ViewHolderChild) convertView.getTag();
//
//		}
//
//		//holder.textView1.setText("审批类型:" + map.get("ZBFS"));
//		holder.textView1.setText("审批类型:" + flag);
//		holder.textView2.setText("项目编号:" + map.get("XMID"));
//		holder.textView3.setText("项目标题:" + map.get("XMNAME"));
//		holder.textView4.setText("审批状态:" + map.get("STATUSNAME"));
//		if (flag.equals("两证办理")){
//			holder.textView2.setText("办证供应商:" + map.get("PROVIDERNAME"));
//			holder.textView3.setText("推荐单位:" + map.get("DEPNAME"));
//
//		}
//		holder.textView1.setTextColor(mContext.getResources().getColor(
//				R.color.black));
//
//		holder.textView2.setTextColor(mContext.getResources().getColor(
//				R.color.black));
//		holder.textView3.setTextColor(mContext.getResources().getColor(
//				R.color.black));
//		holder.textView4.setTextColor(mContext.getResources().getColor(
//				R.color.green_approval));
//		holder.textView1.setTextSize(TypedValue.COMPLEX_UNIT_DIP,15);
//		holder.textView2.setTextSize(TypedValue.COMPLEX_UNIT_DIP,15);
//		holder.textView3.setTextSize(TypedValue.COMPLEX_UNIT_DIP,15);
//		holder.textView4.setTextSize(TypedValue.COMPLEX_UNIT_DIP,15);
//		holder.lineView.setVisibility(View.VISIBLE);
//		convertView.setBackgroundResource(R.drawable.back4_9meddle);
//		// 下main两个if 如果使用 agentfg.java 请屏蔽 2016.0927
//		if (position == 0) {
//			convertView.setBackgroundResource(R.drawable.back_1top);
//		} else if (position == mEfRowSets.size() - 1) {
//			convertView.setBackgroundResource(R.drawable.back3_bottom);
//			holder.lineView.setVisibility(View.INVISIBLE);
//		}
//		if (mEfRowSets.size() == 1) {
//			// holder.lastView.setVisibility(View.VISIBLE);
//			convertView.setBackgroundResource(R.drawable.back_21all);
//			holder.lineView.setVisibility(View.INVISIBLE);
//		}
//
//		return convertView;
//	}
//
//	class ViewHolderChild {
//		TextView textView1;
//		TextView textView2;
//		TextView textView3;
//		TextView textView4;
//		TextView textView5;
//		TextView lineView;// 黑线
//		View lastView;
//	}
//}
