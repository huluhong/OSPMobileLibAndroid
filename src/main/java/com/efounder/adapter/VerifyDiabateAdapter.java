package com.efounder.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.efounder.imageloader.GlideImageLoader;
import com.utilcode.util.SizeUtils;

import java.util.List;

/**
 * Created by meijun on 17-9-18.
 */

public class VerifyDiabateAdapter extends RecyclerView.Adapter<VerifyDiabateAdapter.MyViewHolder>{

    private List<String> mImageUrl;
    private Context mContext;

    public VerifyDiabateAdapter(List<String> imageurl, Context context) {
        mImageUrl = imageurl;
        mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ImageView imageView = new ImageView(parent.getContext());
        MyViewHolder myViewHolder = new MyViewHolder(imageView);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
//        Picasso.with(mContext).load(mImageUrl.get(position)).into(holder.mImageView);
        GlideImageLoader.getInstance().displayImage(mContext,holder.mImageView, mImageUrl.get(position));

    }

    @Override
    public int getItemCount() {
        return mImageUrl==null ? 0:mImageUrl.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder{

            ImageView  mImageView;
        public MyViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView;
            mImageView.setLayoutParams(new RecyclerView.LayoutParams(SizeUtils.dp2px(70),SizeUtils.dp2px(70)));
        }
    }
}
