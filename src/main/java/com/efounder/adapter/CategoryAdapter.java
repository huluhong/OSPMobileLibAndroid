package com.efounder.adapter;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.efounder.chat.widget.BadgeView;
import com.efounder.imageselector.bean.Image;
import com.efounder.model.Category;
import com.efounder.ospmobilelib.R;
import com.efounder.util.ImageLoaderUtil;
import com.pansoft.resmanager.ResFileManager;

import java.io.File;
import java.util.ArrayList;


/**
 * Created by pansoft on 2017/5/25.
 */

public class CategoryAdapter extends BaseAdapter {

    private static final int TYPE_CATEGORY_ITEM = 0;
    private static final int TYPE_ITEM = 1;

    private ArrayList<Category> mListData;
    private LayoutInflater mInflater;
    private Context mContext;


    public CategoryAdapter(Context context, ArrayList<Category> pData) {
        mListData = pData;
        mInflater = LayoutInflater.from(context);
        mContext = context;
    }

    @Override
    public int getCount() {
        int count = 0;

        if (null != mListData) {
            //  所有分类中item的总和是ListVIew  Item的总个数
            for (Category category : mListData) {
                count += category.getItemCount();
            }
        }

        return count;
    }

    @Override
    public Object getItem(int position) {

        // 异常情况处理
        if (null == mListData || position <  0|| position > getCount()) {
            return null;
        }

        // 同一分类内，第一个元素的索引值
        int categroyFirstIndex = 0;

        for (Category category : mListData) {
            int size = category.getItemCount();
            // 在当前分类中的索引值
            int categoryIndex = position - categroyFirstIndex;
            // item在当前分类内
            if (categoryIndex < size) {
                return  category.getItem( categoryIndex );
            }

            // 索引移动到当前分类结尾，即下一个分类第一个元素索引
            categroyFirstIndex += size;
        }

        return null;
    }

    @Override
    public int getItemViewType(int position) {
        // 异常情况处理
        if (null == mListData || position <  0|| position > getCount()) {
            return TYPE_ITEM;
        }


        int categroyFirstIndex = 0;

        for (Category category : mListData) {
            int size = category.getItemCount();
            // 在当前分类中的索引值
            int categoryIndex = position - categroyFirstIndex;
            if (categoryIndex == 0) {
                return TYPE_CATEGORY_ITEM;
            }

            categroyFirstIndex += size;
        }

        return TYPE_ITEM;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        int itemViewType = getItemViewType(position);
        switch (itemViewType) {
            case TYPE_CATEGORY_ITEM:
                if (null == convertView) {
                    convertView = mInflater.inflate(R.layout.listview_item_header, null);
                }

                TextView textView = (TextView) convertView.findViewById(R.id.header);
                String  itemValue = (String) getItem(position);
                textView.setText( itemValue );
                break;

            case TYPE_ITEM:
                ViewHolder viewHolder = null;
                if (null == convertView) {

                    convertView = mInflater.inflate(R.layout.list_view_item, null);

                    viewHolder = new ViewHolder();
                    viewHolder.content = (TextView) convertView.findViewById(R.id.content);
                    viewHolder.contentIcon = (ImageView) convertView.findViewById(R.id.content_icon);
                    viewHolder.badgeView = (TextView) convertView.findViewById(R.id.wode_badge_view);
                    convertView.setTag(viewHolder);
                } else {
                    viewHolder = (ViewHolder) convertView.getTag();
                }

                // 绑定数据
                Category.ChildModel childModel = (Category.ChildModel) getItem(position);
                viewHolder.content.setText(childModel.getTitle());

                int badgeNum = childModel.getBadgeNum();
                if (badgeNum <= 0 && badgeNum != -1) {
                    viewHolder.badgeView.setVisibility(View.INVISIBLE);
                } else {
                    viewHolder.badgeView.setText(badgeNum + "");
                    viewHolder.badgeView.setVisibility(View.VISIBLE);
                }


              //.setImageResource(R.drawable.ic_launcher);

                String menuIcon = ResFileManager.IMAGE_DIR + "/" + ((Category.ChildModel) getItem(position)).getMenuIcon();
                Bitmap bm = null;

                File file = new File(menuIcon);

                if (file.exists()) {
                    // bm = BitmapFactory.decodeFile(menuIcon);
                    bm = ImageLoaderUtil.getBitmapfromFilewithoutfixes(menuIcon, 1);
                    // bitmapList.add(bm);
                }
                if (bm == null) {
                    // yqs修改
                    // int resID =
                    // context.getResources().getIdentifier("iframe_icon_refresh",
                    // "drawable", appInfo.packageName);
                    ApplicationInfo appInfo = mContext.getApplicationInfo();

                    int resID = mContext.getResources().getIdentifier(
                            "ic_blank_white", "drawable", appInfo.packageName);
                    bm = BitmapFactory
                            .decodeResource(mContext.getResources(), resID);
                }
                viewHolder.contentIcon.setImageBitmap(bm);
                break;
        }

        return convertView;
    }


    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return getItemViewType(position) != TYPE_CATEGORY_ITEM;
    }


    private class ViewHolder {
        TextView content;
        ImageView contentIcon;
        TextView badgeView;
        Image last_message_avatar;
    }

}