//package com.efounder.adapter;
//
//import android.content.Context;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.efounder.email.model.AttachFile;
//import com.efounder.ospmobilelib.R;
//
//import java.util.ArrayList;
//
///**
// * 请假单上传附件的grid
// */
//public class LeaveFileGridAdapter extends BaseAdapter {
//
//	ArrayList<AttachFile> files = new ArrayList<AttachFile>();
//	Context context;
//
//	public LeaveFileGridAdapter(ArrayList<AttachFile> files, Context context) {
//		super();
//		this.files = files;
//		this.context = context;
//	}
//
//	@Override
//	public int getCount() {
//
//		return files.size() + 1;
//	}
//
//	@Override
//	public AttachFile getItem(int position) {
//if (position == files.size()) {
//	return null;
//}
//		return files.get(position);
//	}
//
//	@Override
//	public long getItemId(int position) {
//
//		return position;
//	}
//
//	@SuppressWarnings("deprecation")
//	@Override
//	public View getView(int position, View convertView, ViewGroup parent) {
//		if (convertView == null) {
//			convertView = View.inflate(context, R.layout.grid_items, null);
//			new ViewHolder(convertView);
//		}
//		ViewHolder holder = (ViewHolder) convertView.getTag();
//		AttachFile file = getItem(position);
//		if (position == files.size()) {
//			holder.image.setImageDrawable(context.getResources().getDrawable(
//					R.drawable.jy_drltsz_btn_addperson));
//			holder.fileName.setVisibility(View.GONE);
//			holder.fileSize.setVisibility(View.GONE);
//		} else {
//			holder.image.setImageDrawable(file.getAttachImage());
//			holder.fileName.setText(file.getAttachName());
//			holder.fileSize.setText(file.getAttachSize());
//
//		}
//
//		return convertView;
//	}
//
//	class ViewHolder {
//
//		public ImageView image;
//		public TextView fileName;
//		public TextView fileSize;
//
//		public ViewHolder(View view) {
//			image = (ImageView) view.findViewById(R.id.itemImage);
//			fileName = (TextView) view.findViewById(R.id.email_attachmentname);
//			fileSize = (TextView) view.findViewById(R.id.email_attachsize);
//			view.setTag(this);
//		}
//	}
//
//}
