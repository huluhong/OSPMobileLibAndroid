package com.efounder.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.efounder.builder.base.data.EFRowSet;
import com.efounder.ospmobilelib.R;
import com.pansoft.espflow.util.FlowTaskUtil;
import com.pansoft.xmlparse.FormatSet;
import com.pansoft.xmlparse.FormatTable;
import com.pansoft.xmlparse.MobileFormatUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TaskListViewAdapter extends BaseAdapter {
    private Context mContext;
    private List<EFRowSet> mEfRowSets;
    FormatTable ft;// OA和文件审批读取的xml信息
    FormatTable ft1;// 会议申请读取的xml信息
    FormatTable ft2;// 请销假读取的xml信息
    FormatSet formatSet;
    boolean isOA = true;//是否是oa的数据

    public TaskListViewAdapter(Context context, List<EFRowSet> efRowSets) {
        mContext = context;
        if (efRowSets != null) {
            mEfRowSets = efRowSets;
        } else
            mEfRowSets = new ArrayList<EFRowSet>();

        formatSet = MobileFormatUtil.getInstance().getFormatSet();
        ft1 = formatSet.getFormatTableById("TASKPendingDataSet");
        ft = formatSet.getFormatTableById("FilePendingDataSet");
        ft2 = formatSet.getFormatTableById("LeaveDataSet");

    }

    @Override
    public int getCount() {

        return mEfRowSets.size();
    }

    @Override
    public EFRowSet getItem(int position) {
        // TODO Auto-generated method stub
        return mEfRowSets.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        boolean canopen = true;
        EFRowSet rowset = mEfRowSets.get(position);

        if (rowset.hasKey("count") || rowset.getString("title", "").equals("待办") || rowset.getString("title", "").equals("保留")|| rowset.getString("title", "").equals("已办")) {
            Log.i("tasklistadapter","过滤错误数据");
            return new View(mContext);//过滤数据显示不正确的问题
        }

        if (rowset.getString("canopen", "").equals("0")) {
            canopen = false;
        }

        List<Map<String, String>> list = null;
        if (rowset.hasKey("title")) {
            list = FlowTaskUtil.openDetaiFromRowsetToMap(rowset, ft);//OA
            isOA = true;
        }
        if (!rowset.getString("DEPNAME", "").equals("")) {
            list = FlowTaskUtil.openDetaiFromRowsetToMap(rowset, ft2);//请销假
            isOA = false;
        }
        if (list == null) {
            list = new ArrayList<Map<String, String>>();

        }
        ViewHolderChild holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.expandlistviewchild_fix, null);
            holder = new ViewHolderChild();
            LinearLayout childLayout = (LinearLayout) convertView
                    .findViewById(R.id.childLayout);
            holder.textView1 = (TextView) childLayout
                    .findViewById(R.id.textView1);
            holder.textView2 = (TextView) childLayout
                    .findViewById(R.id.textView2);
            holder.textView3 = (TextView) childLayout
                    .findViewById(R.id.textView3);
            holder.textView4 = (TextView) childLayout
                    .findViewById(R.id.textView4);
            holder.textView5 = (TextView) childLayout
                    .findViewById(R.id.textView5);
            holder.lineView = (TextView) convertView
                    .findViewById(R.id.agentline);
            holder.lastView = convertView.findViewById(R.id.lastbacground);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolderChild) convertView.getTag();

        }


        for (int i = 0; i < list.size(); i++) {

            Map<String, String> mapchild1 = list.get(i);
            for (String key : mapchild1.keySet()) {
//				if (!mapchild1.keySet().contains("field3")) {
//					holder.textView4.setVisibility(View.GONE);
//				}else {
//					holder.textView4.setVisibility(View.VISIBLE);	
//				}
                if (isOA) {
                    if (key.equals("title")) {
                        holder.textView1.setText(mapchild1.get(key));
                        if (canopen) {
                            holder.textView1.setTextColor(mContext.getResources().getColor(R.color.black));
                        } else {
                            holder.textView1.setTextColor(mContext.getResources().getColor(R.color.shenhui));
                        }
                    } else if (key.equals("field1")) {
                        holder.textView2.setText(mapchild1.get(key));
                    } else if (key.equals("field2")) {
                        holder.textView3.setText(mapchild1.get(key));
                    } else if (key.equals("field3")) {
                        holder.textView4.setText(mapchild1.get(key));
                    }
                } else {
                    if (key.equals("假条编号:")) {
                        holder.textView1.setText(key + mapchild1.get(key));
                        holder.textView1.setTextColor(mContext.getResources().getColor(R.color.black));
                    } else if (key.equals("工作单位:")) {
                        holder.textView2.setTextColor(mContext.getResources().getColor(R.color.black));
                        holder.textView2.setText(key + mapchild1.get(key));
                    } else if (key.equals("姓名:")) {
                        holder.textView3.setTextColor(mContext.getResources().getColor(R.color.black));
                        holder.textView3.setText(key + mapchild1.get(key));
                    } else if (key.equals("请假天数:")) {
                        holder.textView4.setTextColor(mContext.getResources().getColor(R.color.black));
                        holder.textView4.setText(key + mapchild1.get(key));
                    }
                }


            }


        }
        holder.lineView.setVisibility(View.VISIBLE);
        convertView.setBackgroundResource(R.drawable.back4_9meddle);
        //下main两个if 如果使用 agentfg.java  请屏蔽   2016.0927
        if (position == 0) {
            convertView.setBackgroundResource(R.drawable.back_1top);
        } else if (position == mEfRowSets.size() - 1) {
            convertView.setBackgroundResource(R.drawable.back3_bottom);
            holder.lineView.setVisibility(View.INVISIBLE);
        }
        if (mEfRowSets.size() == 1) {
            //holder.lastView.setVisibility(View.VISIBLE);
            convertView.setBackgroundResource(R.drawable.back_21all);
            holder.lineView.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }

    class ViewHolderChild {
        TextView textView1;
        TextView textView2;
        TextView textView3;
        TextView textView4;
        TextView textView5;
        TextView lineView;//黑线
        View lastView;
    }
}
