package com.efounder.model;

import com.google.gson.annotations.Expose;

public class JsonHolder<T> {
	
	@Expose
	public String msg;
	
	@Expose
	public boolean success;
	
	@Expose
	public T obj;

	@Override
	public String toString() {
		return "JsonHolder [msg=" + msg + ", success=" + success + ", obj="
				+ obj + "]";
	}

}