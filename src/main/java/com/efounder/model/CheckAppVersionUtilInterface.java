package com.efounder.model;

/**
 * Created by Administrator on 2016/8/25 0025.
 */

public interface CheckAppVersionUtilInterface {

    void startDown(float DownBeginFromPercent);
    void updateProgress(float progress);
    void downloadOver(String filePath);
    void error();
    void stop();


}
