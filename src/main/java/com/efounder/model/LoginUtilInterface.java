package com.efounder.model;

import com.efounder.eai.data.JResponseObject;

/**
 * Created by Administrator on 2016/8/25 0025.
 */

public interface LoginUtilInterface {

    void startProgress();
    void loginSuccess(JResponseObject result);
    void loginFailed(String errorString);
    void exception(String exceptionString);

}
