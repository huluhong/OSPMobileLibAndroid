package com.efounder.model;

/**
 * 清除角标类
 * @author will
 */
public class ClearBadgeEvent {
    private String nodeId;
    public ClearBadgeEvent(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getNodeId() {
        return nodeId;
    }
}
