package com.efounder.model;

/**
 * 首页tabottomactivity  bottombarshi否显示的事件
 *
 * @author YQS 20180516
 */
public class BottombarVisibleEvent {

    private boolean visible;


    public boolean isVisible() {
        return visible;
    }

    public BottombarVisibleEvent setVisible(boolean visible) {
        this.visible = visible;
        return this;
    }

    public BottombarVisibleEvent(boolean visible) {
        this.visible = visible;
    }
}
