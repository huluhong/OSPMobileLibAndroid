package com.efounder.model;

import android.os.AsyncTask;

import com.efounder.chat.model.Constant;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;

import java.util.Map;

import static com.efounder.frame.utils.Constants.KEY_LOGIN_CHECK_TYPE;
import static com.efounder.frame.utils.Constants.KEY_SETTING_APPID;
import static com.efounder.frame.utils.Constants.KEY_SETTING_SERVER_KEY;
import static com.efounder.frame.utils.Constants.KEY_SIGN;

/**
 * Created by lch on 2016/8/25 0025.
 */

public class LoginUtil {

    LoginUtilInterface mLoginUtilInterface;
    private String userName;

    public void start(String userName, String password, LoginUtilInterface loginUtilInterface) {
        mLoginUtilInterface = loginUtilInterface;
        String[] params = {userName, password};
        this.userName = userName;
        new LoginAsyncTask().execute(params);


    }

    /*
     * 内部AsyncTask类
	 */
    class LoginAsyncTask extends AsyncTask<String, Integer, JResponseObject> {
        long time = System.currentTimeMillis();


        // 进度显示
        protected void onPreExecute() {

            // 进度显示
            super.onPreExecute();
            mLoginUtilInterface.startProgress();

        }

        @Override
        protected JResponseObject doInBackground(String... params) {

            JParamObject PO = JParamObject.Create();

            PO.setEnvValue("UserName", params[0]);
            PO.setEnvValue("UserPass", params[1]);

            PO.setEnvValue("DBNO", EnvironmentVariable.getProperty(KEY_SIGN));// 数据标示
            PO.setEnvValue("DataBaseName", EnvironmentVariable.getProperty("DataBaseName"));// 数据标示
            //PO.setEnvValue("Product", EnvironmentVariable.getProperty(KEY_SETTING_APPID));// 服务名称

            //PO.setEnvValue("LoginCheckType", Constant.LoginCheckType);
            PO.setEnvValue("LoginCheckType", EnvironmentVariable.getProperty(KEY_LOGIN_CHECK_TYPE));


            PO.SetValueByParamName("get_mobile_resource", "1");
            PO.SetValueByParamName("resource_key", Constant.Resource_Key);

            // 防止盗用
            PO.setEnvValue(KEY_SETTING_APPID, "PGYDYYM");

            // 权限管理
            PO.SetValueByParamName("LOAD_GNQX", "1");
            //XXX 调用登录接口
            PO.SetValueByParamName("method", "login");
            PO.SetValueByParamName("loginName", String.valueOf(params[0]));
            JParamObject.assign(PO);
            //TODO 出现了网络异常的提示
            String isNewInstall = EnvironmentVariable.getProperty(params[0]+"IsNewInstall","0");
            if(isNewInstall.equals("0")) {
                PO.SetValueByParamName("isFirstInstall", "true");
            }

            try {
                // 连接服务器
                // JResponseObject RO = EAI.DAL.SVR("WXExternalInterfaceDataService", PO);

                String serverKey = EnvironmentVariable.getProperty(KEY_SETTING_SERVER_KEY);

                JResponseObject RO = EAI.DAL.SVR(serverKey, PO);

//                PO.SetValueByParamName("url", "http://211.87.224.230:3301/api/class/category/list");
//                PO.SetValueByParamName("args", "1=1&2=2");
//                JResponseObject RO = EAI.DAL.SVR("CommonHttp", PO);

                return RO;
            } catch (Exception e) {
                e.printStackTrace();
                mLoginUtilInterface.exception("网络异常");
                return null;
            }
        }

        // 当后台操作结束时，此方法将会被调用，计算结果将做为参数传递到此方法中，直接将结果显示到UI组件上
        protected void onPostExecute(JResponseObject result) {

            if (result == null) {
                mLoginUtilInterface.exception("网络异常");
                return;
            }
            Map<?, ?> responseMap = (Map<?, ?>) result.getResponseMap();
            JResponseObject pingtaiRO = (JResponseObject) responseMap.get("pingtai");

            // RO返回值为空
            if (pingtaiRO == null) {
                mLoginUtilInterface.exception("用户名或者密码错误");
                return;
            } else if (pingtaiRO.ErrorCode == -1) {
                mLoginUtilInterface.loginFailed(pingtaiRO.getErrorString());
                return;
            } else {
                mLoginUtilInterface.loginSuccess(result);
                EnvironmentVariable.setProperty(userName+"IsNewInstall","1");

            }
        }
    }
}