package com.efounder.model;

/**
 * 刷新界面的事件
 * Created by YQS on 2017/11/10.
 */

public class RefreshViewEvent {

    public static int VIEW_TABOTTOMACTIVITY = 0;//刷新首页

    private int RefreshViewID;//要刷新的view


    public int getRefreshViewID() {
        return RefreshViewID;
    }

    public void setRefreshViewID(int viewID) {
        this.RefreshViewID = viewID;
    }

    public RefreshViewEvent(int refreshViewID) {
        RefreshViewID = refreshViewID;
    }
}
