package com.efounder.model;

import java.io.Serializable;

public class AgentTaskModel  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String type;
	private String nameString;//标题
	private  int dbCount;//代办数量
	private  int dyCount;//待阅数量
	
	public AgentTaskModel(){
		
	}

	public AgentTaskModel(String type, String nameString, int dbCount,int dyCount) {
		super();
		this.type = type;
		this.nameString = nameString;
		this.dyCount = dyCount;
		this.dbCount = dbCount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNameString() {
		return nameString;
	}

	public void setNameString(String nameString) {
		this.nameString = nameString;
	}

	public int getDbCount() {
		return dbCount;
	}

	public void setDbCount(int dbCount) {
		this.dbCount = dbCount;
	}

	public int getDyCount() {
		return dyCount;
	}

	public void setDyCount(int dyCount) {
		this.dyCount = dyCount;
	}

	

	

}
