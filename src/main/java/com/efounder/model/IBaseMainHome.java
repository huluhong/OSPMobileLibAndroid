package com.efounder.model;

import com.core.xml.StubObject;
import com.efounder.frame.baseui.BaseFragment;

import java.util.List;

public interface IBaseMainHome {

    /**
     * 在setContentView()之前调用
     */
    void doBeforeSetContentView();


    /**
     * 加载读取配置文件
     */
    void loadProfile();


    /**
     * 检查版本更新
     * 检查功能权限
     * 以及其他一些数据请求操作
     */
    void checkUpdateAndGnqx();

    /**
     * 刷新界面
     * 角标等
     */
    void refreshUi();


    /**
     * 是否使用左侧侧滑出的抽屉布局
     */
    boolean isAllowLeftDrawer();

    /**
     * 获取左抽屉的fragment
     *
     * @return
     */
    BaseFragment getLeftDrawerFragment();

    /**
     * 获取首页配置的菜单数组
     *
     * @return
     */
    List<StubObject> getMainMenuList();
}
