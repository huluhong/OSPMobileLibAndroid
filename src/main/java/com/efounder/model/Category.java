package com.efounder.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pansoft on 2017/5/25.
 */

public class Category {

    private String mCategoryName;

    public List<ChildModel> getmCategoryItem() {
        return mCategoryItem;
    }

    public void setmCategoryItem(List<ChildModel> mCategoryItem) {
        this.mCategoryItem = mCategoryItem;
    }

    private List<ChildModel> mCategoryItem = new ArrayList<ChildModel>();

    public Category(String mCategroyName) {
        mCategoryName = mCategroyName;
    }

    public String getmCategoryName() {
        return mCategoryName;
    }

    public void addItem(ChildModel pItemName) {
        mCategoryItem.add(pItemName);
    }

    /**
     * 获取Item内容
     *
     * @param pPosition
     * @return
     */
    public Object getItem(int pPosition) {
        // Category排在第一位
        if (pPosition == 0) {
            return mCategoryName;
        } else {
            return mCategoryItem.get(pPosition - 1);
        }
    }

    /**
     * 当前类别Item总数。Category也需要占用一个Item
     *
     * @return
     */
    public int getItemCount() {
        return mCategoryItem.size() + 1;
    }

    public class ChildModel {
        public ChildModel() {

        }

        private String id;
        private String title;
        private String menuIcon;
        private String applyId;
        private int badgeNum = 0;
//        zjk 修改成可配置
        private String viewType;
        private String androidShow;

        public String getViewType() {
            return viewType;
        }

        public void setViewType(String viewType) {
            this.viewType = viewType;
        }

        public String getAndroidShow() {
            return androidShow;
        }

        public void setAndroidShow(String androidShow) {
            this.androidShow = androidShow;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getApplyId() {
            return applyId;
        }

        public void setApplyId(String applyId) {
            this.applyId = applyId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMenuIcon() {
            return menuIcon;
        }

        public void setMenuIcon(String menuIcon) {
            this.menuIcon = menuIcon;
        }

        public int getBadgeNum() {
            return badgeNum;
        }

        public void setBadgeNum(int badgeNum) {
            this.badgeNum = badgeNum;
        }
    }


}