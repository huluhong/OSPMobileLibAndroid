package com.efounder.model;

import org.apache.commons.collections.map.HashedMap;

import java.util.List;
import java.util.Map;

/**
 * 共享组织树数据结构
 * Created by YQS on 2017/12/26.
 */

public class GXOrgBeanList {

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * BH : 1011
         * MC : 勘探与生产-国内非合作
         * MX : 0
         * JS : 2
         * TYPE : ZZJG
         */

        private String BH;
        private String MC;
        private String MX;
        private int JS;
        private String TYPE;
        private String F_IM_GROUPID;
        private String F_IM_GROUP_CREATE;
        private int imUserId;


        public String getBH() {
            return BH;
        }

        public void setBH(String BH) {
            this.BH = BH;
        }

        public String getMC() {
            return MC;
        }

        public void setMC(String MC) {
            this.MC = MC;
        }

        public String getMX() {
            return MX;
        }

        public void setMX(String MX) {
            this.MX = MX;
        }

        public int getJS() {
            return JS;
        }

        public void setJS(int JS) {
            this.JS = JS;
        }

        public String getTYPE() {
            return TYPE;
        }

        public void setTYPE(String TYPE) {
            this.TYPE = TYPE;
        }

        public String getF_IM_GROUPID() {
            return F_IM_GROUPID;
        }

        public DataBean setF_IM_GROUPID(String f_IM_GROUPID) {
            F_IM_GROUPID = f_IM_GROUPID;
            return this;
        }

        public String getF_IM_GROUP_CREATE() {
            return F_IM_GROUP_CREATE;
        }

        public DataBean setF_IM_GROUP_CREATE(String f_IM_GROUP_CREATE) {
            F_IM_GROUP_CREATE = f_IM_GROUP_CREATE;
            return this;
        }

        public int getImUserId() {
            return imUserId;
        }

        public DataBean setImUserId(int imUserId) {
            this.imUserId = imUserId;
            return this;
        }

        public Map<String,Object> getAttrMap(){
            Map<String, Object> attrMap = new HashedMap();

//            private String BH = null;
//            private String MC;
//            private String MX;
//            private int JS;
//            private String TYPE;
//            private String F_IM_GROUPID;
//            private String F_IM_GROUP_CREATE;
//            private int imUserId;
            if (BH!=null){
                attrMap.put("BH",BH);
            }
            if (MC!=null){
                attrMap.put("MC",MC);
            }
            if (MX!=null){
                attrMap.put("MX",MX);
            }

                attrMap.put("JS",JS);

            if (TYPE!=null){
                attrMap.put("TYPE",TYPE);
            }
            if (F_IM_GROUPID!=null){
                attrMap.put("F_IM_GROUPID",F_IM_GROUPID);
            }
            if (F_IM_GROUP_CREATE!=null){
                attrMap.put("F_IM_GROUP_CREATE",F_IM_GROUP_CREATE);
            }
            if (imUserId!=0){
                attrMap.put("imUserId",imUserId);
            }

           return  attrMap;
        }
    }
}
