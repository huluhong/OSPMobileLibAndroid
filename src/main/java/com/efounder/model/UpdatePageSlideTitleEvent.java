package com.efounder.model;

import com.core.xml.StubObject;

public class UpdatePageSlideTitleEvent {
    String title;
    StubObject stubObject;

    public UpdatePageSlideTitleEvent(String title, StubObject stubObject) {
        this.title = title;
        this.stubObject = stubObject;
    }

    public String getTitle() {
        return title;
    }

    public UpdatePageSlideTitleEvent setTitle(String title) {
        this.title = title;
        return this;
    }

    public StubObject getStubObject() {
        return stubObject;
    }

    public UpdatePageSlideTitleEvent setStubObject(StubObject stubObject) {
        this.stubObject = stubObject;
        return this;
    }
}
