//package com.efounder.dm.vedio;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.List;
//import java.util.Random;
//
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.content.pm.ActivityInfo;
//import android.content.res.Configuration;
//import android.graphics.Color;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.util.Log;
//import android.view.MotionEvent;
//import android.view.SurfaceHolder;
//import android.view.SurfaceHolder.Callback;
//import android.view.SurfaceView;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.View.OnTouchListener;
//import android.view.Window;
//import android.widget.AdapterView;
//import android.widget.AdapterView.OnItemClickListener;
//import android.widget.Button;
//import android.widget.ImageButton;
//import android.widget.LinearLayout;
//import android.widget.ProgressBar;
//import android.widget.RadioGroup;
//import android.widget.RadioGroup.OnCheckedChangeListener;
//import android.widget.RelativeLayout;
//import android.widget.ScrollView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.efounder.ospmobilelib.R;
//import com.efounder.adapter.HorizontalListViewAdapter;
//import com.efounder.constant.Constant;
//import com.efounder.constant.ConstantLive;
//import com.efounder.data.model.ItemData;
//import com.efounder.dm.vedio.utils.UtilAudioPlay;
//import com.efounder.dm.vedio.utils.UtilFilePath;
//import com.efounder.image.utils.ImageUtils;
//import com.hik.mcrsdk.util.CLog;
//import com.hikvision.vmsnetsdk.DeviceInfo;
//import com.hikvision.vmsnetsdk.RealPlayURL;
//import com.hikvision.vmsnetsdk.VMSNetSDK;
//import com.live.LiveCallBack;
//import com.live.LiveControl;
//import com.pansoft.widget.HorizontalListView;
///**
// * 直播主页面  通过接口数据 加载直播流
// * @author long
// *2015年9月16日15:48:24
// */
//public class LiveActN extends Activity implements OnClickListener,OnCheckedChangeListener,OnTouchListener, Callback, LiveCallBack{
//    private static final String TAG             = "LiveActN";
//    /**
//     * 码流类型
//     */
//    private int                 mStreamType     = -1;
//    /**
//     * 通过VMSNetSDK返回的预览地址对象
//     */
//    private RealPlayURL         mRealPlayURL;
//    /**
//     * 登录设备的用户名
//     */
//    private String              mName = "admin";
//    /**
//     * 登录设备的密码
//     */
//    private String              mPassword = "12345";
//    /**
//     * 控制层对象
//     */
//    private LiveControl         mLiveControl;
//    /**
//     * 播放视频的控件对象
//     */
//    private SurfaceView         mSurfaceView;
//    /**
//     * 创建取流等待bar
//     */
//    private ProgressBar         mProgressBar;
//    /**
//     * 创建消息对象
//     */
//    private Handler             mMessageHandler = new MyHandler();
//    /**
//     * 音频是否开启
//     */
//    private boolean             mIsAudioOpen;
//    /**
//     * 是否正在录像
//     */
//    private boolean             mIsRecord;
//    /**
//     * 播放流量
//     */
//    private long                mStreamRate     = 0;
//    /**
//     * 监控点信息对象
//     */
////    private CameraInfo          cameraInfo;
//    private ItemData          itemData;
//    /**
//     * 云台控制对话框
//     */
//    private AlertDialog         mDialog;
//
//    private VMSNetSDK           mVmsNetSDK      = null;
//
//    private HorizontalListView hListView;
//    private HorizontalListViewAdapter hlvadapter;
//    /**列表数据*/
//    List<ItemData> listData;
//    /**初次进入该界面播放视频在list中的位置*/
//    private int pos;
//
//    private ImageButton cloudL,cloudR,cloudT,cloudB;
//    private TextView cloudNear,cloudRemote;
//    private TextView cloudVideo,cloudVoice,cloudCamera;
//    private ScrollView mScrollView;
//
//    private Button backBtn,rightBtn;
//    private TextView titleTv;
//    private RelativeLayout controlContener;
//
////    --------------------------
//	private String              mDeviceID       = "";
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//    	requestWindowFeature(Window.FEATURE_NO_TITLE);
//        super.onCreate(savedInstanceState);
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
//        initData();
//        if(getResources().getConfiguration().orientation==Configuration.ORIENTATION_PORTRAIT){
//        setContentView(R.layout.activity_live);
//        initUI();
//		initListener();
//        }else if(getResources().getConfiguration().orientation==Configuration.ORIENTATION_LANDSCAPE){
//        	setContentView(R.layout.activity_live_land);
//        	mSurfaceView.getHolder().addCallback(this);
//    		mProgressBar = (ProgressBar) findViewById(R.id.liveProgressBar);
//    		startBtnOnClick();
//        }
//    }
//
//
//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//    	super.onConfigurationChanged(newConfig);
//    	int direction = getResources().getConfiguration().orientation;
//    	if(!mIsRecord){
//    		if(direction==Configuration.ORIENTATION_LANDSCAPE){
//    			setContentView(R.layout.activity_live_land);
//    			mSurfaceView = (SurfaceView) findViewById(R.id.surfaceView);
//    			mSurfaceView.getHolder().addCallback(this);
//    			mProgressBar = (ProgressBar) findViewById(R.id.liveProgressBar);
//    			startBtnOnClick();
//    			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//    		}else if(direction==Configuration.ORIENTATION_PORTRAIT){
//    			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//    			setContentView(R.layout.activity_live);
//    			initUI();
//    			initListener();
//    		}
//    		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
//    	}
//    }
//	private void initListener() {
//    	hListView.setOnItemClickListener(new OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view,
//					int position, long id) {
//				if(pos==position){
//					return;
//				}
//				TempData.getIns().setData(listData.get(position));
//				TempData.getIns().setPos(position);
//				stopBtnOnClick();
//				initData();
//				titleTv.setText(itemData.getCaption());
//				startBtnOnClick();
//			}
//		});
//	}
//	/**
//     * 初始化网络库和控制层对象
//     *
//     * @since V1.0
//     */
//    private void initData() {
//    	listData = TempData.getIns().getListData();
//    	pos = TempData.getIns().getPos();
//    	mStreamType = ConstantLive.SUB_STREAM;
//        mRealPlayURL = new RealPlayURL();
//        mLiveControl = new LiveControl();
//        mLiveControl.setLiveCallBack(this);
//        itemData = TempData.getIns().getData();
//        mDeviceID = itemData.getDeviceID();
//        mVmsNetSDK = VMSNetSDK.getInstance();
//        final DeviceInfo deviceInfo = new DeviceInfo();
//        if(mVmsNetSDK == null){
//            Log.i(TAG, "mVmsNetSDK is null");
//            return ;
//        }
//        new Thread(new Runnable() {
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				boolean ret = mVmsNetSDK.getDeviceInfo(Constant.SERVER_ADDRESS,
//						TempData.getIns().getLoginData().sessionID, mDeviceID, deviceInfo);
//				Log.i(TAG, "sessionID="+TempData.getIns().getLoginData().sessionID+",mDeviceID="+mDeviceID+",ret="+ret);
//				if (ret && deviceInfo != null) {
//					Log.i(TAG, "---line 213---");
//					mName = deviceInfo.userName;
//					mPassword = deviceInfo.password;
//				} else {
//					Log.i(TAG, "---line 218---");
//					mName = "admin";
//					mPassword = "12345";
//				}
//				Log.i(TAG, "mName is " + mName + "---" + mPassword + "-----" + mDeviceID);
//			}
//		}).start();
//    }
//
//    /**
//     * 初始化控件
//     *
//     * @since V1.0
//     */
//    private void initUI() {
//
//    	/*backBtn = (Button) findViewById(R.id.button_back);
//    	backBtn.setVisibility(View.VISIBLE);
//    	backBtn.setOnClickListener(this);
//    	rightBtn = (Button) findViewById(R.id.main);
////    	rightBtn.setVisibility(View.INVISIBLE);
//    	rightBtn.setText("分享");
//    	titleTv = (TextView) findViewById(R.id.textview_title);
//    	titleTv.setText(itemData.getCaption());*/
//    	 RelativeLayout include = (RelativeLayout) findViewById(R.id.include);
// 		//include.setBackgroundResource(R.drawable.meetingnav);
// 		include.setBackgroundResource(R.color.red_ios);
// 		LinearLayout leftbacklayout = (LinearLayout) findViewById(R.id.leftbacklayout);
// 		leftbacklayout.setVisibility(View.VISIBLE);
// 		leftbacklayout.setOnClickListener(this);
// 		TextView title = (TextView) findViewById(R.id.fragmenttitle);
// 		title.setText("视频监控");
// 		title.setTextColor(Color.WHITE);
// 		//include.setVisibility(View.GONE);
// 		include.setVisibility(View.VISIBLE);
//
//
//    	controlContener = (RelativeLayout) findViewById(R.id.cotrol_contener);
////    	if(itemData.getItemBean().getAttributes().getPtzType()==1&&user!=null&&user.getUserType()!=null&&!UserInfo.TYPE_USER_COMMEN.equals(user.getUserType())){
////    		controlContener.setVisibility(View.VISIBLE);
////    	}else{
//    		controlContener.setVisibility(View.GONE);
////    	}
//    	//测试   正式时  删除该部分
////    	controlContener.setVisibility(View.VISIBLE);
//    	mScrollView = (ScrollView) findViewById(R.id.cloud_areaContent);
//
//    	cloudB = (ImageButton) findViewById(R.id.cloud_b);
//    	cloudT = (ImageButton) findViewById(R.id.cloud_t);
//    	cloudL = (ImageButton) findViewById(R.id.cloud_l);
//    	cloudR = (ImageButton) findViewById(R.id.cloud_r);
//    	cloudNear = (TextView) findViewById(R.id.cloud_near_focuse);
//    	cloudRemote = (TextView) findViewById(R.id.cloud_remote_focuse);
//
//    	cloudVideo = (TextView) findViewById(R.id.cloud_vedio);
//    	cloudVoice = (TextView) findViewById(R.id.cloud_voice);
//    	cloudCamera = (TextView) findViewById(R.id.cloud_cut);
//    	cloudVideo.setOnClickListener(this);
//    	cloudVoice.setOnClickListener(this);
//    	cloudCamera.setOnClickListener(this);
//
////    	if("true".equals(itemData.getItemBean().getAttributes().getSound())){
////    		cloudVoice.setClickable(true);
////    	}else{
////    		cloudVoice.setClickable(false);
////    	}
//
//    	cloudB.setOnTouchListener(this);
//    	cloudT.setOnTouchListener(this);
//    	cloudR.setOnTouchListener(this);
//    	cloudL.setOnTouchListener(this);
//    	cloudNear.setOnTouchListener(this);
//    	cloudRemote.setOnTouchListener(this);
//
//    	hListView = (HorizontalListView) findViewById(R.id.horizontalListView);
//    	hlvadapter = new HorizontalListViewAdapter(this,listData);
//    	hlvadapter.notifyDataSetChanged();
//    	hListView.setAdapter(hlvadapter);
//
//    	//加载ListView后 将scrollView置顶
//    	mScrollView.setFocusable(true);
//    	mScrollView.setFocusableInTouchMode(true);
//    	mScrollView.requestFocus();
//
//
//        mSurfaceView = (SurfaceView) findViewById(R.id.surfaceView);
//        mSurfaceView.getHolder().addCallback(this);
//
//        mProgressBar = (ProgressBar) findViewById(R.id.liveProgressBar);
//        mProgressBar.setVisibility(View.INVISIBLE);
//
//        startBtnOnClick();
//    }
//
//    @Override
//    public void onCheckedChanged(RadioGroup group, int checkedId) {
//    }
//
//    @Override
//    public void onClick(View v) {
//    		switch (v.getId()) {
//            case R.id.cloud_vedio:
//            	recordBtnOnClick();
//            	break;
//            case R.id.cloud_voice:
////            	if("true".equals(itemData.getItemBean().getAttributes().getSound())){
//            		audioBtnOnClick();
////            	}else{
////            		Toast.makeText(LiveActN.this, "该视频暂不可进行语音控制", 0).show();
////            	}
//            	break;
//            case R.id.cloud_cut:
//            	captureBtnOnClick();
//            	break;
//
//            case R.id.leftbacklayout:
//            	LiveActN.this.finish();
//            	break;
//			default:
//				break;
//			}
//    }
//
//    /**
//     * 发送云台控制命令
//     *
//     * @param gestureID 1-云台转上 、2-云台转下 、3-云台转左 、4-云台转右、 11-云台左上 、12-云台右上 13-云台左下 、14-云台右下、7-镜头拉近、8-镜头拉远、9-镜头近焦、10-镜头远焦
//     */
////    private void sendCtrlCmd(final int gestureID) {
////        new Thread(new Runnable() {
////
////            @Override
////            public void run() {
////                String sessionID = TempData.getIns().getLoginData().sessionID;
////                // 云台控制速度 取值范围(1-10)
////                int speed = 5;
////                Log.i(TAG, "ip:" + cameraInfo.acsIP + ",port:" + cameraInfo.acsPort + ",isPTZControl:"
////                        + cameraInfo.isPTZControl);
////                // 发送控制命令
////                boolean ret = VMSNetSDK.getInstance().sendStartPTZCmd(cameraInfo.acsIP, cameraInfo.acsPort, sessionID,
////                        cameraInfo.cameraID, gestureID, speed, 600);
////                Log.i(TAG, "sendStartPTZCmd ret:" + ret);
////            }
////        }).start();
////    }
//
//    /**
//     * 停止云台控制
//     */
////    private void stopCloudCtrl() {
////        new Thread(new Runnable() {
////
////            @Override
////            public void run() {
////                String sessionID = TempData.getIns().getLoginData().sessionID;
////                boolean ret = VMSNetSDK.getInstance().sendStopPTZCmd(cameraInfo.acsIP, cameraInfo.acsPort, sessionID,
////                        cameraInfo.cameraID);
////                Log.i(TAG, "stopPtzCmd sent,ret:" + ret);
////            }
////        }).start();
////    }
//
//    /**
//     * 启动播放 void
//     *
//     * @since V1.0
//     */
//    String url = "";
//    private void startBtnOnClick() {
//        mProgressBar.setVisibility(View.VISIBLE);
//        new Thread() {
//            @Override
//            public void run() {
//                super.run();
//                Log.i(TAG, "---line---484---mStreamType="+mStreamType+",getPlayUrl(mStreamType)="+getPlayUrl(mStreamType)+",mName="+mName+",mPassword="+mPassword);
//                url = getPlayUrl(mStreamType);
//                Log.i(TAG, "------line---486---url---"+url+",mName="+mName+",mPassword="+mPassword);
//                mLiveControl.setLiveParams(url, mName, mPassword);
//                if (mLiveControl.LIVE_PLAY == mLiveControl.getLiveState()) {
//                    mLiveControl.stop();
//                }
//
//                if (mLiveControl.LIVE_INIT == mLiveControl.getLiveState()) {
//                    mLiveControl.startLive(mSurfaceView);
//                }
//            }
//        }.start();
//    }
//
//
//
//
//    /**
//     * 该方法是获取播放地址的，当mStreamType=2时，获取的是MAG，当mStreamType =1时获取的子码流，当mStreamType = 0时获取的是主码流
//     * 由于该方法中部分参数是监控点的属性，所以需要先获取监控点信息，具体获取监控点信息的方法见resourceActivity。
//     *
//     * @param streamType 2、表示MAG取流方式；1、表示子码流取流方式；0、表示主码流取流方式；
//     * @return String 播放地址 ：2、表示返回的是MAG的播放地址;1、表示返回的是子码流的播放地址；0、表示返回的是主码流的播放地址。
//     * @since V1.0
//     */
//    private String getPlayUrl(int streamType) {
//        String url = "";
//        // 登录平台地址
//        String mAddress = Constant.SERVER_ADDRESS;
//        // 登录返回的sessiond
//        String mSessionID = TempData.getIns().getLoginData().sessionID;
//        if (itemData == null) {
//            return url;
//        }
//        if (streamType == 2) {
//            // TODO 原有代码streamType传0
//            VMSNetSDK.getInstance().getRealPlayURL(mAddress, mSessionID, itemData.getCameraID(), streamType, mRealPlayURL);
//            if (null == mRealPlayURL) {
//                return "";
//            }
//            // MAG地址
//            url = mRealPlayURL.url2;
//        } else {
//            VMSNetSDK.getInstance().getRealPlayURL(mAddress, mSessionID, itemData.getCameraID(), streamType, mRealPlayURL);
//            if (null == mRealPlayURL) {
//                return "";
//            }
//            // mRealPlayURL.url1 是主码流还是子码流取决于 streamType，见上面注释
//            url = mRealPlayURL.url1;
//        }
//        DeviceInfo deviceInfo = new DeviceInfo();
//        Log.i(TAG, "cameraInfo.deviceID:"+itemData.getDeviceID()+",cameraInfo.cameraID:"+itemData.getCameraID());
//        boolean ret = VMSNetSDK.getInstance().getDeviceInfo(mAddress, mSessionID, mDeviceID, deviceInfo);
//        if (ret && deviceInfo != null) {
//            mName = deviceInfo.userName;
//            mPassword = deviceInfo.password;
//            Log.i(TAG, "------line430---");
//        } else {
//        	Log.i(TAG, "------line432---");
//            mName = "admin";
//            mPassword = "12345";
//        }
//
//        if(null == mName || "".equals(mName))
//        {
//        	Log.i(TAG, "------line439---");
//            mName = "admin";
//        }
//
//        if(null == mPassword || "".equals(mPassword))
//        {
//        	Log.i(TAG, "------line445---");
//            mPassword = "12345";
//        }
//        return url;
//    }
//
//
//    /**
//     * 停止播放 void
//     *
//     * @since V1.0
//     */
//    private void stopBtnOnClick() {
//        if (null != mLiveControl) {
//            mLiveControl.stop();
//        }
//    }
//
//    /**
//     * 抓拍 void
//     *
//     * @since V1.0
//     */
//    private void captureBtnOnClick() {
//        if (null != mLiveControl) {
//            // 随即生成一个1到10000的数字，用于抓拍图片名称的一部分，区分图片，开发者可以根据实际情况修改区分图片名称的方法
//            int recordIndex = new Random().nextInt(10000);
//            boolean ret = mLiveControl.capture(UtilFilePath.getPictureDirPath().getAbsolutePath(), "Picture"
//                    + recordIndex + ".jpg");
////            boolean ret = mLiveControl.capture(UtilFilePath.getSysCameraPath(this), "Picture"
////            		+ recordIndex + ".jpg");
//            if (ret) {
//            	Toast.makeText(LiveActN.this, "抓拍成功", 0).show();
//                UtilAudioPlay.playAudioFile(LiveActN.this, R.raw.paizhao);
//                String filePath = UtilFilePath.getPictureDirPath().getAbsolutePath()+File.separator+"Picture"
//                        + recordIndex + ".jpg";
////                String filePath = UtilFilePath.getSysCameraPath(this)+File.separator+"Picture"
////                		+ recordIndex + ".jpg";
//                try {
//					ImageUtils.saveImageToSD(LiveActN.this, filePath, ImageUtils.getSmallBitmap(filePath), 80);
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//
//            } else {
//            	Toast.makeText(LiveActN.this, "抓拍失败", 0).show();
//                Log.e(TAG, "---line---678---captureBtnOnClick():: 抓拍失败");
//            }
//        }
//    }
//
//    /**
//     * 录像 void
//     *
//     * @since V1.0
//     */
//    String vedioPath=null;
//    private void recordBtnOnClick() {
//        if (null != mLiveControl) {
//            if (!mIsRecord) {
//                // 随即生成一个1到10000的数字，用于录像名称的一部分，区分图片，开发者可以根据实际情况修改区分录像名称的方法
//                int recordIndex = new Random().nextInt(10000);
//                vedioPath = UtilFilePath.getVideoDirPath().getAbsolutePath()+File.separator+"Video" + recordIndex
//                        + ".mp4";
//                mLiveControl.startRecord(UtilFilePath.getVideoDirPath().getAbsolutePath(), "Video" + recordIndex
//                        + ".mp4");
//                mIsRecord = true;
//                Toast.makeText(LiveActN.this, "启动录像成功", 0).show();
//                cloudVideo.setText("停止");
//            } else {
//                mLiveControl.stopRecord();
//                mIsRecord = false;
//                if(vedioPath!=null){
//                	ImageUtils.updateGallery(LiveActN.this, vedioPath);
//                }
//                Toast.makeText(LiveActN.this, "停止录像成功", 0).show();
//                cloudVideo.setText("录像");
//            }
//        }
//    }
//
//    /**
//     * 音频 void
//     *
//     * @since V1.0
//     */
//    private void audioBtnOnClick() {
//        if (null != mLiveControl) {
//            if (mIsAudioOpen) {
//                mLiveControl.stopAudio();
//                mIsAudioOpen = false;
//                Toast.makeText(LiveActN.this, "关闭音频", 0).show();
//            } else {
//                boolean ret = mLiveControl.startAudio();
//                if (!ret) {
//                    mIsAudioOpen = false;
//                    Toast.makeText(LiveActN.this, "开启音频失败", 0).show();
//                } else {
//                    mIsAudioOpen = true;
//                    // 开启音频成功，并不代表一定有声音，需要设备开启声音。
//                    Toast.makeText(LiveActN.this, "开启音频成功", 0).show();
//                }
//            }
//        }
//
//    }
//
//    @Override
//    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
//
//    }
//
//    @Override
//    public void surfaceCreated(SurfaceHolder holder) {
//
//    }
//
//    @Override
//    public void surfaceDestroyed(SurfaceHolder holder) {
//        if (null != mLiveControl) {
//            if (mIsRecord) {
////                mRecordBtn.setText("开始录像");
//                mLiveControl.stopRecord();
//                mIsRecord = false;
//            }
//            mLiveControl.stop();
//        }
//    }
//
//    @Override
//    public void onMessageCallback(int messageID) {
//        sendMessageCase(messageID);
//    }
//
//    /**
//     * 返回已经播放的流量 void
//     *
//     * @return long
//     * @since V1.0
//     */
//    public long getStreamRate() {
//        return mStreamRate;
//    }
//
//    /**
//     * 发送消息
//     *
//     * @param i void
//     * @since V1.0
//     */
//    private void sendMessageCase(int i) {
//        if (null != mMessageHandler) {
//            Message msg = Message.obtain();
//            msg.arg1 = i;
//            mMessageHandler.sendMessage(msg);
//        }
//    }
//
//    /**
//     * 消息类
//     *
//     * @author huangweifeng
//     * @Data 2013-10-23
//     */
//    @SuppressLint("HandlerLeak")
//    private final class MyHandler extends Handler {
//        public void handleMessage(Message msg) {
//            switch (msg.arg1) {
//                case ConstantLive.RTSP_SUCCESS:
////                	Toast.makeText(LiveAct.this, "启动取流成功", 0).show();
//                break;
//
//                case ConstantLive.STOP_SUCCESS:
////                	Toast.makeText(LiveAct.this, "停止成功", 0).show();
//                break;
//
//                case ConstantLive.START_OPEN_FAILED:
//                	Toast.makeText(LiveActN.this, "开启播放库失败", 0).show();
//                    if (null != mProgressBar) {
//                        mProgressBar.setVisibility(View.GONE);
//                    }
//                break;
//
//                case ConstantLive.PLAY_DISPLAY_SUCCESS:
////                	Toast.makeText(LiveAct.this, "播放成功", 0).show();
//                    if (null != mProgressBar) {
//                        mProgressBar.setVisibility(View.GONE);
//                    }
//                break;
//
//                case ConstantLive.RTSP_FAIL:
////                	Toast.makeText(LiveActN.this, "RTSP链接失败", 0).show();
//                	Toast.makeText(LiveActN.this, "网络异常，请稍后再试", 0).show();
//                    if (null != mProgressBar) {
//                        mProgressBar.setVisibility(View.GONE);
//                    }
//                    if (null != mLiveControl) {
//                        mLiveControl.stop();
//                    }
//                break;
//
//                case ConstantLive.GET_OSD_TIME_FAIL:
//                	Toast.makeText(LiveActN.this, "获取OSD时间失败", 0).show();
//                break;
//
//                case ConstantLive.SD_CARD_UN_USEABLE:
//                	Toast.makeText(LiveActN.this, "SD卡不可用", 0).show();
//                break;
//
//                case ConstantLive.SD_CARD_SIZE_NOT_ENOUGH:
//                	Toast.makeText(LiveActN.this, "SD卡空间不足", 0).show();
//                break;
//                case ConstantLive.CAPTURE_FAILED_NPLAY_STATE:
//                	Toast.makeText(LiveActN.this, "非播放状态不能抓拍", 0).show();
//                break;
//                case Constant.START_VEDIO:
//                	startBtnOnClick();
//                	break;
//                case Constant.LOAD_PIEMENU:
//
//                	break;
//            }
//        }
//    }
//
//	@Override
//	public boolean onTouch(View v, MotionEvent event) {
//		switch (event.getAction()) {
//		case MotionEvent.ACTION_DOWN:
//			v.setPressed(true);
//			switch (v.getId()) {
//			case R.id.cloud_b:
////				sendCtrlCmd(2);
//				Toast.makeText(this, "---云台转下---", 0).show();
//				break;
//			case R.id.cloud_t:
////				sendCtrlCmd(1);
//				Toast.makeText(this, "---云台转上---", 0).show();
//				break;
//			case R.id.cloud_r:
////				sendCtrlCmd(4);
//				Toast.makeText(this, "---云台转右---", 0).show();
//				break;
//			case R.id.cloud_l:
////				sendCtrlCmd(3);
//				Toast.makeText(this, "---云台转左---", 0).show();
//				break;
//			case R.id.cloud_near_focuse:
//				if(controlContener.getVisibility()==View.VISIBLE){
////					sendCtrlCmd(7);
//				}else{
//					Toast.makeText(this, "---近焦不可用---", 0).show();
//				}
//				break;
//			case R.id.cloud_remote_focuse:
//				if(controlContener.getVisibility()==View.VISIBLE){
////					sendCtrlCmd(8);
//				}else{
//					Toast.makeText(this, "---远焦不可用---", 0).show();
//				}
//				break;
//			}
//			break;
//		case MotionEvent.ACTION_MOVE:
//
//			break;
//		case MotionEvent.ACTION_UP:
//			v.setPressed(false);
//			switch (v.getId()) {
//			case R.id.cloud_b:
//			case R.id.cloud_t:
//			case R.id.cloud_r:
//			case R.id.cloud_l:
//			case R.id.cloud_remote_focuse:
//			case R.id.cloud_near_focuse:
////				stopCloudCtrl();
//				break;
//			}
//			break;
//
//		default:
//			break;
//		}
//		return true;
//	}
//}
