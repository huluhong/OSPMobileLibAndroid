//package com.efounder.dm.vedio.utils;
//
//import android.content.Context;
//import android.os.Environment;
//import android.widget.Toast;
//
//import com.efounder.util.FileUtil;
//
//import java.io.File;
//import java.io.IOException;
//
///**
// * 获取抓拍、录像路径类
// * @author long
// */
//public class UtilFilePath {
//
//    /**
//     * 获取图片目录
//     *
//     * @return Pictrue dir path.
//     * @since V1.0
//     */
//    public static File getPictureDirPath() {
////        File SDFile = null;
//        File mIVMSFolder = null;
//        try {
////            SDFile = android.os.Environment.getExternalStorageDirectory();
////            String path = SDFile.getAbsolutePath() + File.separator + "HIKVISION";
//            String path = FileUtil.FILE_MY_IMG;
//            mIVMSFolder = new File(path);
//            if ((null != mIVMSFolder) && (!mIVMSFolder.exists())) {
//                mIVMSFolder.mkdir();
//                mIVMSFolder.createNewFile();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return mIVMSFolder;
//    }
//
//    /**
//     * 获取录像目录
//     *
//     * @return Video dir path.
//     * @since V1.0
//     */
//    public static File getVideoDirPath() {
////        File SDFile = null;
//        File mIVMSFolder = null;
//        try {
////            SDFile = android.os.Environment.getExternalStorageDirectory();
////            mIVMSFolder = new File(SDFile.getAbsolutePath() + File.separator + "HIKVISION");
//            mIVMSFolder = new File(FileUtil.FILE_MY_VEDIO);
//            if ((null != mIVMSFolder) && (!mIVMSFolder.exists())) {
//                mIVMSFolder.mkdir();
//                mIVMSFolder.createNewFile();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return mIVMSFolder;
//    }
//    /**
//     * 获取系统相册
//     * @return
//     */
//    public static String getSysCameraPath(Context context){
//    	if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())){
//    		return Environment.getExternalStorageDirectory() + File.separator + Environment.DIRECTORY_DCIM + File.separator +"Camera";
//    	}else{
//    		Toast.makeText(context, "未检测到SD卡", 0).show();
//    		return "";
//    	}
//
//    }
//}