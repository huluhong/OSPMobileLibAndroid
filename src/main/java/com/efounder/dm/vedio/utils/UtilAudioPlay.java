//package com.efounder.dm.vedio.utils;
//
//import android.content.Context;
//import android.media.AudioManager;
//import android.media.SoundPool;
//import android.os.AsyncTask;
//import android.os.Handler;
//import android.util.Log;
//
//import com.efounder.constant.Constant;
//import com.efounder.dm.vedio.TempData;
//import com.efounder.util.AbStrUtil;
//import com.hikvision.vmsnetsdk.ServInfo;
//import com.hikvision.vmsnetsdk.VMSNetSDK;
//
///**
// * 抓拍时的声音类
// * @author long
// */
//public class UtilAudioPlay {
//
//    protected static final String TAG = "UtilAudioPlay";
//	private static SoundPool     mSoundPool     = null;
//    private static UtilAudioPlay mPlayAudioTask = new UtilAudioPlay();
//    private static int           mSoundId       = -1;
//
//    /** 登录返回的数据 */
//    private static ServInfo servInfo;
//    static boolean ret;
//
//    private UtilAudioPlay() {
//    };
//
//    public static void loadCameraInfo(final Context context) {
//		//获取登陆信息
//		servInfo = new ServInfo();
//		new Thread(){
//			public void run() {
//				ret = VMSNetSDK.getInstance().login(Constant.SERVER_ADDRESS, Constant.USER_NAME, Constant.USER_PWD, 1,AbStrUtil.getMac(context), servInfo);
//				if (servInfo != null) {
//		            // 打印出登录时返回的信息
//		            TempData.getIns().setLoginData(servInfo);
//		         // 打印出登录时返回的信息
//                    Log.i(TAG, "login ret : " + ret);
//                    Log.i(TAG, "login response info[" + "sessionID:" + servInfo.sessionID + ",userID:"
//                            + servInfo.userID + ",magInfo:" + servInfo.magInfo + ",picServerInfo:"
//                            + servInfo.picServerInfo + ",ptzProxyInfo:" + servInfo.ptzProxyInfo + ",userCapability:"
//                            + servInfo.userCapability + ",vmsList:" + servInfo.vmsList + ",vtduInfo:"
//                            + servInfo.vtduInfo + ",webAppList:" + servInfo.webAppList + "]");
//		        }
//				if (ret) {
//                    TempData.getIns().setLoginData(servInfo);
//                }
//			};
//		}.start();
//	}
//
//    /**
//     * 这里对方法做描述
//     *
//     * @param context 上下文
//     * @param rawFile 音频文件
//     * @since V1.0
//     */
//    public synchronized static void playAudioFile(Context context, int rawFile) {
//
//        if (null == mSoundPool) {
//            mSoundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 100);
//            mSoundId = mSoundPool.load(context, rawFile, 1);
//
//            new Handler().postDelayed(mPlayAudioTask.new PlayAudioTask1(), 100);
//        } else {
//            if (-1 != mSoundId)
//                mPlayAudioTask.new PlayAudioTask().execute(mSoundId);
//        }
//    }
//
//    private class PlayAudioTask1 implements Runnable {
//        @Override
//        public void run() {
//            mPlayAudioTask.new PlayAudioTask().execute(mSoundId);
//        }
//    }
//
//    private class PlayAudioTask extends AsyncTask<Integer, Integer, Void> {
//        @Override
//        protected Void doInBackground(Integer... soundId) {
//            mSoundPool.play(soundId[0], 1, 1, 1, 0, 1);
//            return null;
//        }
//    }
//}