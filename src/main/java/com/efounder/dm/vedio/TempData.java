//package com.efounder.dm.vedio;
//
//import java.util.List;
//
//import com.efounder.data.model.ItemData;
//import com.hikvision.vmsnetsdk.CameraInfo;
//import com.hikvision.vmsnetsdk.ServInfo;
//
//public final class TempData {
//	private static TempData ins = new TempData();
//
//	/**
//	 * 登录返回的数据
//	 */
//	private ServInfo loginData;
//
//	/**
//	 * 监控管理功能数据传递
//	 */
//	List<CameraInfo> cameraList;
//	/**
//	 * 传递数据
//	 */
//	private ItemData  data;
//
//	public ItemData getData() {
//		return data;
//	}
//
//	public void setData(ItemData data) {
//		this.data = data;
//	}
//
//	public List<CameraInfo> getCameraList() {
//		return cameraList;
//	}
//
//	public void setCameraList(List<CameraInfo> cameraList) {
//		this.cameraList = cameraList;
//	}
//
//	/**
//	 * 选中数据的位置
//	 */
//	private int pos;
//
//	public int getPos() {
//		return pos;
//	}
//
//	public void setPos(int pos) {
//		this.pos = pos;
//	}
//
//	/**
//	 * 中转数据 暂存
//	 */
//	private List<ItemData>  listData;
//
//	public List<ItemData> getListData() {
//		return listData;
//	}
//
//	public void setListData(List<ItemData> listData) {
//		this.listData = listData;
//	}
//
//	/**
//	 * 监控点信息，用作临时传递数据用
//	 */
//	private CameraInfo cameraInfo;
//
//	public static TempData getIns() {
//		return ins;
//	}
//
//	/**
//	  * 设置登录成功返回的信息
//	  * @param loginData
//	  * @since V1.0
//	  */
//	public void setLoginData(ServInfo loginData) {
//		this.loginData = loginData;
//	}
//
//	/**
//	  * 获取登录成功返回的信息
//	  * @return
//	  * @since V1.0
//	  */
//	public ServInfo getLoginData() {
//		return loginData;
//	}
//
//	/**
//	  * 保存监控点信息
//	  * @param cameraInfo
//	  * @since V1.0
//	  */
//	public void setCameraInfo(CameraInfo cameraInfo) {
//		this.cameraInfo = cameraInfo;
//	}
//
//	/**
//	  * 获取监控点信息
//	  * @return
//	  * @since V1.0
//	  */
//	public CameraInfo getCameraInfo() {
//		return cameraInfo;
//	}
//}