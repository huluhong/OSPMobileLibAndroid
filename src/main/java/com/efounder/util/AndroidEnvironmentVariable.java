package com.efounder.util;

import java.util.HashMap;
import java.util.Map;

public class AndroidEnvironmentVariable {
	private static StorageUtil storageutil = new StorageUtil(AppContext.getInstance(), "AndroidEnvironmentVariable");
	private static Map<String, Object> evMap = new HashMap();

	///用户名
	private static String userName;
	///邮箱
	private static String email;
	///用户ID
	private static String userID;
	///密码
	private static String password;
	///邮箱加密
	private static String MD5Email;
	///用户ID加密
	private static String MD5UserID;
	///密码加密
	private static String MD5PassWord;
	///加密
	private static String MD5;
	///设备平台 手机 平板
	private static String platform;
	///记录菜单选中项
	private static String MenuSelectedID;
	///推送的token
	private static String DeviceToken;
	///获取服务器的url
	private static String serverURL;
	///获取version路径
	private static String versionPath;
	///科室
	private static String F_DWMC;
	///电话
	private static String F_PHONE;
	private static String F_DWBH;
	private static String F_CHDATE;
	private static String F_ZGBH;
	private static String F_CRDATE;
	//权限
	private static Map<String, Object> powerMap;
	
	public static Map<String, Object> getPowerMap() {
		return powerMap;
	}
	public static void setPowerMap(Map<String, Object> powerMap) {
		AndroidEnvironmentVariable.powerMap = powerMap;
	}
	///其他变量
	private static Map<String,String> property;
	
	public static String getUserName() {
		return (String) get("userName");
	}
	public static void setUserName(String userName) {
		set("userName",userName);
	}
	public static String getEmail() {
		return (String) get("email");

	}
	public static void setEmail(String email) {
		//AndroidEnvironmentVariable.email = email;
		set("email",email);
	}
	public static String getUserID() {
		return (String) get("userID");

	}
	public static void setUserID(String userID) {
		AndroidEnvironmentVariable.userID = userID;
		set("userID",userID);
	}
	public static String getPassword() {
		return (String) get("password");
	}
	public static void setPassword(String password) {
		//AndroidEnvironmentVariable.password = password;
		set("password",password);
	}
	public static String getMD5Email() {
		return MD5Email;
	}
	public static void setMD5Email(String mD5Email) {
		MD5Email = mD5Email;
	}
	public static String getMD5UserID() {
		return MD5UserID;
	}
	public static void setMD5UserID(String mD5UserID) {
		MD5UserID = mD5UserID;
	}
	public static String getMD5PassWord() {
		return MD5PassWord;
	}
	public static void setMD5PassWord(String mD5PassWord) {
		MD5PassWord = mD5PassWord;
	}
	public static String getMD5() {
		return MD5;
	}
	public static void setMD5(String mD5) {
		MD5 = mD5;
	}
	public static String getPlatform() {
		return (String) get("platform");
		//return platform;
	}
	public static void setPlatform(String platform) {
		//AndroidEnvironmentVariable.platform = platform;
		set("platform",platform);
	}
	public static String getMenuSelectedID() {
		return MenuSelectedID;
	}
	public static void setMenuSelectedID(String menuSelectedID) {
		MenuSelectedID = menuSelectedID;
	}
	public static String getDeviceToken() {
		return DeviceToken;
	}
	public static void setDeviceToken(String deviceToken) {
		DeviceToken = deviceToken;
	}
	public static String getServerURL() {
		return serverURL;
	}
	public static void setServerURL(String serverURL) {
		AndroidEnvironmentVariable.serverURL = serverURL;
	}
	public static String getVersionPath() {
		return versionPath;
	}
	public static void setVersionPath(String versionPath) {
		AndroidEnvironmentVariable.versionPath = versionPath;
	}
	public static String getF_DWMC() {
		//return F_DWMC;
		return (String) get("F_DWMC");
	}
	public static void setF_DWMC(String f_DWMC) {
		//F_DWMC = f_DWMC;
		set("f_DWMC",f_DWMC);
	}
	public static Map<String, String> getProperty() {
		return property;

	}
	public static void setProperty(Map<String, String> property) {
		AndroidEnvironmentVariable.property = property;
	}
	public static void setPropertyMap(String key ,String value) {
		//property.put(key, value);
		set(key, value);
	}

	public static String getProperty(String key, String defaultValue) {
		String value = (String)get(key);
		if(value == null) {
			value = defaultValue;
		}

		return value;
	}
	public static void setProperty(String key, Object Value) {
		set(key,Value);
	}
	private static void set(String key, Object value) {
		evMap.put(key, value);
		storageutil.putString(key, (String)value);
		storageutil.commit();
	}

	private static Object get(String key) {
		String mapValue = (String)evMap.get(key);
		if(mapValue == null) {
			mapValue = storageutil.getString(key);
			if(mapValue == null || "".equals(mapValue)) {
				mapValue = null;
			}

			evMap.put(key, mapValue);
		}

		return mapValue;
	}
}
