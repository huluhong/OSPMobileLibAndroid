package com.efounder.util;

import android.util.Log;

import com.efounder.builder.base.data.EFDataSet;
import com.efounder.builder.base.data.EFRowSet;
import com.efounder.builder.base.data.ESPRowSet;
import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;

import java.util.HashMap;
import java.util.List;

public class GetTokenDataByPO {

	/**
	 * 得到邮箱的tokenid
	 * 
	 * @param type
	 *            邮箱类型
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static String getEmailTokenid(String type) {
		// 创建PO

		JParamObject PO = JParamObject.Create();
		CommonPo.setPoToNull();
		JResponseObject RO = null;
		EFDataSet tokenEmailDataSet = null;
		String tokenId = null;

		String usereId = AndroidEnvironmentVariable.getUserID();
		String password = AndroidEnvironmentVariable.getPassword();
//		String usereId = "hxu.sipc";
//		String password = "196603113182";

		PO.SetValueByParamName("WB_SYS_KEY", type);
		PO.SetValueByParamName(type + "_params_Userid", usereId);
		PO.SetValueByParamName(type + "_params_Pwd", password);
		PO.SetValueByParamName(type + "_systemmethod", "GetTokenID");

		try {
			// 连接服务器
			RO = EAI.DAL.SVR("ExternalInterfaceDataService", PO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (RO != null) {
			HashMap<String, Object> map = (HashMap<String, Object>) RO
					.getResponseMap();
			if (map.containsKey(type)) {
				tokenEmailDataSet = (EFDataSet) map.get(type);// 普光邮箱 token
				List<ESPRowSet> EmailrowSets = tokenEmailDataSet
						.getRowSetList();
				if (EmailrowSets != null) {

					EFRowSet efORowSet = (EFRowSet) EmailrowSets.get(0);
					tokenId = (String) efORowSet.getDataMap().get("TokenID");
					
					Log.i("EmailGetDataByPo", type + "的token：" + tokenId);
					GlobalMap.setProperty(type + "TokenId", tokenId);
					//Log.i("EmailGetDataByPo", "GlobalMap: " + GlobalMap.getProperty(type + "TokenId", ""));
				} else {
					Log.i("token", type + "tokenid:" + "tokenid不存在");
				}

			}

		}



		return tokenId;

	}
	/**
	 * 得到招标管理系统的的tokenid
	 * 
	 * @param type
	 *            邮箱类型
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static String getPGZBTokenid(String type) {
		// 创建PO

		JParamObject PO = JParamObject.Create();
		CommonPo.setPoToNull();
		JResponseObject RO = null;
		EFDataSet tokenEmailDataSet = null;
		String tokenId = null;

		String usereId = AndroidEnvironmentVariable.getUserID();
		String password = AndroidEnvironmentVariable.getPassword();

		PO.SetValueByParamName("WB_SYS_KEY", type);
		PO.SetValueByParamName(type + "_params_Userid", usereId);
		PO.SetValueByParamName(type + "_params_Pwd", password);
		PO.SetValueByParamName(type + "_systemmethod", "GetTokenID");

		try {
			// 连接服务器
			RO = EAI.DAL.SVR("ExternalInterfaceDataService", PO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (RO != null) {
			HashMap<String, Object> map = (HashMap<String, Object>) RO
					.getResponseMap();
			if (map.containsKey(type)) {
				tokenEmailDataSet = (EFDataSet) map.get(type);// 招标系统 token
				List<ESPRowSet> EmailrowSets = tokenEmailDataSet
						.getRowSetList();
				if (EmailrowSets != null) {

					EFRowSet efORowSet = (EFRowSet) EmailrowSets.get(0);
					tokenId = (String) efORowSet.getDataMap().get("TokenID");
					
					GlobalMap.setProperty(type + "TokenId", tokenId);
				} else {
					Log.i("token", type + "tokenid:" + "tokenid不存在");
				}

			}

		}
		return tokenId;

	}

}
