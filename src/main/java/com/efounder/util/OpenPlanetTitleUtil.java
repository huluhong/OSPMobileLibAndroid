package com.efounder.util;

import com.efounder.constant.EnvironmentVariable;

/**
 * 星际通讯标题工具
 */
public class OpenPlanetTitleUtil {

    /**
     * 获取星球的中文名称
     *
     * @return
     */
    public static String getPlanetName() {
        String planet = EnvironmentVariable.getProperty("tc_planet", "");
        String planetInChinese = "";
        switch (planet) {
            case "mercury":
                planetInChinese = "水星";
                break;
            case "venus":
                planetInChinese = "金星";
                break;
            case "earth":
                planetInChinese = "地球";
                break;
            case "mars":
                planetInChinese = "火星";
                break;
            case "jupiter":
                planetInChinese = "木星";
                break;
            case "saturn":
                planetInChinese = "土星";
                break;
            case "uranus":
                planetInChinese = "天王星";
                break;
            case "neptune":
                planetInChinese = "海王星";
                break;
            case "pluto":
                planetInChinese = "冥王星";
                break;
            default:
                planetInChinese = "";
                break;
        }
        return planetInChinese;
    }
}
