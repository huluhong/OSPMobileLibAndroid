//package com.efounder.util;
//
//import android.content.Context;
//import android.text.TextUtils;
//
//import androidx.annotation.NonNull;
//
//import com.efounder.constant.EnvironmentVariable;
//import com.efounder.tbs.FileDisplayActivity;
//import com.tencent.smtt.sdk.QbSdk;
//import com.tencent.smtt.sdk.TbsVideo;
//import com.utilcode.util.FileUtils;
//
///**
// * 使用tbs 展示文档
// *
// * @autor yqs
// * @date 2018/12/13 14:47
// **/
//public class DisplayFileWithTbsUtil {
//
//
//    /**
//     * 展示文档
//     *
//     * @param filePath 文件路径
//     */
//    @Deprecated
//    public static boolean displayFile(Context mcontext, String filePath) {
//        try {
//            if (EnvironmentVariable.getProperty("tbsLoadResult", "").equals("false")) {
//                //tbs内核没有加载成功，那就不使用了
//                return false;
//            }
//            if (filePath.toLowerCase().endsWith("pdf")
//                    || filePath.toLowerCase().endsWith("epub")
//                    || filePath.toLowerCase().endsWith("docx")
//                    || filePath.toLowerCase().endsWith("doc")
//                    || filePath.toLowerCase().endsWith("xls")
//                    || filePath.toLowerCase().endsWith("xlsx")
//                    || filePath.toLowerCase().endsWith("ppt")
//                    || filePath.toLowerCase().endsWith("pptx")) {
//                //调用腾讯浏览服务
//                FileDisplayActivity.show(mcontext, filePath);
////                Intent intent = new Intent(mcontext, ChatWebViewActivity.class);
////                intent.putExtra("url", url);
////                mcontext.startActivity(intent);
//                return true;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return false;
//    }
//
//
//    /**
//     * 是否支持tbs
//     *
//     * @return
//     */
//    public static boolean supportTbs() {
//        if (EnvironmentVariable.getProperty("tbsLoadResult", "").equals("false")) {
//            //tbs内核没有加载成功，相当于不支持
//            return false;
//        }
//        return true;
//    }
//
//    /**
//     * 文件预览
//     *
//     * @param mcontext      上下文
//     * @param localFilePath 文件本地路径
//     * @return 是否支持此文件格式
//     */
//    public static boolean previewLocalFile(Context mcontext, String localFilePath) {
//        try {
//            if (!supportTbs()) {
//                //tbs内核没有加载成功，那就不使用了
//                return false;
//            }
//            String fileType = FileUtils.getFileExtension(localFilePath);
//            if (TextUtils.isEmpty(fileType)) {
//                return false;
//            }
//            if (isDocument(localFilePath)) {
//                //调用腾讯浏览服务
//                FileDisplayActivity.show(mcontext, localFilePath);
//                return true;
//            } else if (isVideo(localFilePath)) {
//                //调用腾讯视频浏览服务
//                if (TbsVideo.canUseTbsPlayer(mcontext)) {
//                    TbsVideo.openVideo(mcontext, localFilePath);
//                    return true;
//                }
//            } else {
//                QbSdk.openFileReader(mcontext, localFilePath, null, null);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return false;
//    }
//
//    /**
//     * 文件预览
//     *
//     * @param mcontext
//     * @param url      文件下载的url
//     * @param fileName 文件名称（url可能获取不到文件后缀名，我们需要从文件名换获取，例如 abc.pdf）,不能温控
//     * @return 是否支持打开
//     */
//    public static boolean previewFileByUrl(Context mcontext, String url,@NonNull String fileName) {
//        try {
//            if (!supportTbs()) {
//                //tbs内核没有加载成功，那就不使用了
//                return false;
//            }
//
//            if (isDocument(fileName)) {
//                //调用腾讯浏览服务
//                FileDisplayActivity.show(mcontext, url, fileName);
//                return true;
//            } else if (isVideo(fileName)) {
//                //调用腾讯视频浏览服务
//                if (TbsVideo.canUseTbsPlayer(mcontext)) {
//                    TbsVideo.openVideo(mcontext, url);
//                    return true;
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return false;
//    }
//
//
//    /**
//     * 通过文件名判断是否是视频
//     *
//     * @param filePathOrFileName
//     * @return
//     */
//    public static boolean isVideo(String filePathOrFileName) {
//        String fileType = FileUtils.getFileExtension(filePathOrFileName);
//        if (TextUtils.isEmpty(fileType)) {
//            return false;
//        }
//        if ("mp4".equals(fileType.toLowerCase())
//                || "avi".equals(fileType.toLowerCase())
//                || "rmvb".equals(fileType.toLowerCase())
//                || "3gp".equals(fileType.toLowerCase())
//                || "mpg".equals(fileType.toLowerCase())
//                || "mpeg".equals(fileType.toLowerCase())) {
//            return true;
//        }
//        return false;
//    }
//
//    /**
//     * 是否是文档
//     *
//     * @param filePathOrFileName
//     * @return
//     */
//    public static boolean isDocument(String filePathOrFileName) {
//        String fileType = FileUtils.getFileExtension(filePathOrFileName);
//        if (TextUtils.isEmpty(fileType)) {
//            return false;
//        }
//        if (fileType.toLowerCase().equals("pdf")
//                || fileType.toLowerCase().equals("epub")
//                || fileType.toLowerCase().equals("docx")
//                || fileType.toLowerCase().equals("doc")
//                || fileType.toLowerCase().equals("xls")
//                || fileType.toLowerCase().equals("xlsx")
//                || fileType.toLowerCase().equals("ppt")
//                || fileType.toLowerCase().equals("pptx")) {
//            return true;
//        }
//        return false;
//    }
//}
