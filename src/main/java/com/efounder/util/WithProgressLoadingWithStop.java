package com.efounder.util;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.efounder.ospmobilelib.R;
import com.efounder.widget.NumberProgressBar;

public class WithProgressLoadingWithStop {

	/*
	 * 定义loadingDialog
	 */
	private static Dialog loadingDialog;

	/*
	 * 定义Appcontext全局变量
	 */
	//public static AppContext appContext = AppContext.getInstance();

	/*
	 * 加载显示的文字
	 */
	static NumberProgressBar bar;
	static ImageButton imageButton;
	static boolean isloading = true;
	
	public static CancelProgressListener cancelProgressListener;

	/*
	 * 重载方法
	 */
	public static void show(Context context) {
		Resources res = context.getResources();
		String loading = res.getString(R.string.common_text_loading);

		show(context,loading);
	}

	/*
	 * 显示dialog
	 */
	public static void show(Context context ,String msg) {

		// 用来载入LinearLayout
		LayoutInflater layoutInflater = LayoutInflater.from(context);

		// 创建视图
		View v = layoutInflater.inflate(R.layout.withwaitingstop, null);
		
		imageButton = (ImageButton) v.findViewById(R.id.withwaitingstopbutton);
		imageButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				isloading= !isloading;
				if(isloading){
					imageButton.setBackgroundResource(R.drawable.stop_download);
					cancelProgressListener.startProgress();
					
				}else{
					imageButton.setBackgroundResource(R.drawable.start_download);
					cancelProgressListener.cancelProgress();
				}
				
				
			}
		});
		
		
		
		bar = (NumberProgressBar) v.findViewById(R.id.numberbar1);

		// 加载布局
		LinearLayout layout = (LinearLayout) v.findViewById(R.id.dialog_view);

		/*// 加载imageView

		ImageView imageView = (ImageView) v.findViewById(R.id.loading_img);
       
		// 设置textVeiw
		TextView textView = (TextView) v.findViewById(R.id.loading_textView);
		textView.setText(msg + "...");
		Resources res = appContext.getResources(); 
		float fontsize = res.getDimension(R.dimen.activity_login_button_fontsize);
		textView.setTextSize(fontsize);

		//获取Imageview设置图片的宽度和高度
		Bitmap bitmap = ((BitmapDrawable)imageView.getBackground()).getBitmap();
		
		// 加载动画
		Animation loadingAnimation = AnimationUtils.loadAnimation(appContext,
				R.anim.loading_animation);

		// 使用ImageView显示动画
		imageView.startAnimation(loadingAnimation);
		*/
		loadingDialog = new Dialog(context, R.style.loadingData_dialog);
		// 不可以用“返回键”取消
		 loadingDialog.setCancelable(true);
		//loadingDialog.setCancelable(true);
		loadingDialog.setContentView(layout);
		loadingDialog.setCanceledOnTouchOutside(false);
		loadingDialog.setOnCancelListener(new 
	      DialogInterface.OnCancelListener(){
			public void onCancel(DialogInterface dialog){
				if(null!= cancelProgressListener )
				cancelProgressListener.cancelProgress();
			        }
		          });
		

		//设置dialog的大小
		 Window dialogWindow = loadingDialog.getWindow();
		 WindowManager.LayoutParams lp = dialogWindow.getAttributes();
		/* lp.width = Detail_Util.dip2px(appContext, bar.getWidth())+450;
		 lp.height =Detail_Util.dip2px(appContext,  bar.getHeight())+350;*/
		 DisplayMetrics dm = new DisplayMetrics(); ;//屏幕分辨率容器
		    WindowManager windowManager = (WindowManager) AppContext.getInstance()
	                .getSystemService(Context.WINDOW_SERVICE);
		    windowManager.getDefaultDisplay().getMetrics(dm);
	        int  screenWidth = dm.widthPixels;

	        int screenHeight = dm.heightPixels;
	        lp.width = (int) (screenWidth/1.5);
	      //  lp.height = screenHeight/3;
	        lp.height = 150;
		 dialogWindow.setAttributes(lp);

		/*loadingDialog.getWindow().setType(
				WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);*/
		loadingDialog.show();
	}

	/*
	 * 取消dialog
	 */

	public static void dismiss() {
		if (loadingDialog != null)
			loadingDialog.dismiss();
	}
	public static Dialog getDialog(){
		return loadingDialog;
	}
	public static void setProgress(int i ){
	  bar.setProgress(i);
	}
	public interface CancelProgressListener{
		public void cancelProgress();
		public void startProgress();
		
	}
	public static void setCancelProgressListener(CancelProgressListener cpl){
		cancelProgressListener = cpl;
	}
	public static void setPause(){
		imageButton.setBackgroundResource(R.drawable.start_download);
	}
}
