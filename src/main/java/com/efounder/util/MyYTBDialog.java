package com.efounder.util;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.efounder.ospmobilelib.R;
import com.efounder.builder.base.data.EFRowSet;
import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;
import com.pansoft.espmodel.FormModel;

public class MyYTBDialog extends Dialog implements View.OnClickListener {
    /**
     * 提交审核
     */
    public static final int DIALOG_SHEN_HE = 0x0000;
    /**
     * 取回动作
     */
    public static final int DIALOG_TAKE_BACK = 0x0001;
    /**
     * 退回动作
     */
    private static final int DIALOG_ROLL_BACK = 0x0002;

    // private EditText prePassword;
    private EditText shenhe;
    private TextView title;
    private TextView submit;
    private TextView cancel;
    Context context;
    private int viewId;
    private FormModel formModel;
    private EFRowSet efRowSet;
    private String shenheText;
    private String quhuiText;
    private String tuihuiText;
    private String userMame;

    public MyYTBDialog(Context context) {
        super(context);
        this.context = context;
    }

    public MyYTBDialog(Context context, int theme, int id, EFRowSet efRowSet) {
        super(context, theme);
        this.context = context;
        this.viewId = id;
        this.efRowSet = efRowSet;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //this.setContentView(R.layout.shen_he_dialog);
        //this.setContentView(R.layout.new_shen_he_dialog);
        this.setContentView(R.layout.shen_he_dialog_new);
        // prePassword = (EditText) findViewById(R.id.prePassword);
        title = (TextView) findViewById(R.id.title);
        if (viewId == DIALOG_SHEN_HE) {
            title.setText("流程任务处理");
        }//else if(viewId==DIALOG_TAKE_BACK){
        else if (viewId == DIALOG_TAKE_BACK) {
            title.setText("流程取回处理");
        } else if (viewId == DIALOG_ROLL_BACK) {
            title.setText("流程退回处理");
        }
        shenhe = (EditText) findViewById(R.id.shenhe);
        submit = (TextView) findViewById(R.id.dialog_button_ok);
        cancel = (TextView) findViewById(R.id.dialog_button_cancel);
        submit.setOnClickListener(this);
        cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        //TODO
        if (viewId == DIALOG_SHEN_HE) {
            if (v.equals(submit)) {
                shenheText = shenhe.getText().toString();
                new AsyncTask<String, Integer, String>() {
                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        LoadingDataUtilBlack.show(context, "正在提交...");
                    }

                    @Override
                    protected String doInBackground(String... params) {
                        String result = submitOperation();
                        return result;
                    }

                    protected void onPostExecute(String result) {
                        super.onPostExecute(result);
                        LoadingDataUtilBlack.dismiss();
                        if (result == null) {
                            ToastUtil.showToast(context, "提交失败！");
                        } else {
                            ToastUtil.showToast(context, result);
                        }
                        dismiss();
                    }
                }.execute();

            } else if (v.equals(cancel)) {
                this.dismiss();
            }
        } else if (viewId == DIALOG_TAKE_BACK) {
            if (v.equals(submit)) {
                shenheText = shenhe.getText().toString();
                new AsyncTask<String, Integer, String>() {
                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        LoadingDataUtilBlack.show(context, "正在提交...");
                    }

                    @Override
                    protected String doInBackground(String... params) {
//                        FlowRetakeAction flowRetakeAction = new FlowRetakeAction();
//                        flowRetakeAction.efRowSet = formModel;
//
//                        return flowRetakeAction.doAction();
                        String result = takeBackOperation();
                        return result;
                    }

                    protected void onPostExecute(String result) {
                        super.onPostExecute(result);
                        LoadingDataUtilBlack.dismiss();
                        if (result == null) {
                            ToastUtil.showToast(context, "提交失败！");
                        } else {
                            ToastUtil.showToast(context, result);
                        }
                        dismiss();
                    }
                }.execute();
            } else if (v.equals(cancel)) {
                this.dismiss();
            }
        } else if (viewId == DIALOG_ROLL_BACK) {
            if (v.equals(submit)) {
                shenheText = shenhe.getText().toString();
                new AsyncTask<String, Integer, String>() {
                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        LoadingDataUtilBlack.show(context, "正在提交...");
                    }

                    @Override
                    protected String doInBackground(String... params) {
                        //FlowRollbackAction frba = new FlowRollbackAction();
                        //frba.efRowSet = formModel;
                        String result = rollBackOperation();

                        return result;
                    }

                    protected void onPostExecute(String result) {
                        super.onPostExecute(result);
                        LoadingDataUtilBlack.dismiss();
                        if (result == null) {
                            ToastUtil.showToast(context, "提交失败！");
                        } else {
                            ToastUtil.showToast(context, result);
                        }
                        dismiss();
                    }
                }.execute();
            } else if (v.equals(cancel)) {
                this.dismiss();
            }
        }
    }

    // TODO: 2017/1/10  提交操作
    private String submitOperation() {
        JParamObject PO = JParamObject.Create();
        JResponseObject RO = null;

        PO.SetValueByParamName("NODE_ID", efRowSet.getString("NODE_TAG", ""));
        PO.SetValueByParamName("FLOW_ID", efRowSet.getString("FLOW_ID", ""));
        PO.SetValueByParamName("OBJ_GUID", efRowSet.getString("OBJ_GUID", ""));
        PO.SetValueByParamName("BIZ_MDL", efRowSet.getString("MDL_ID", ""));//传跟mdl——id一样
        PO.SetValueByParamName("MDL_ID", efRowSet.getString("MDL_ID", ""));
        PO.SetValueByParamName("BIZ_UNIT", efRowSet.getString("BIZ_UNIT", ""));
        PO.SetValueByParamName("BIZ_DATE", efRowSet.getString("BIZ_DATE", ""));
        PO.SetValueByParamName("BIZ_DJBH", efRowSet.getString("BIZ_DJBH", ""));
        PO.SetValueByParamName("OP_LEVEL", efRowSet.getString("OP_LEVEL", ""));
        PO.SetValueByParamName("OP_PROC_NOTE", shenheText);
        PO.SetValueByParamName("OP_ID", efRowSet.getString("OP_ID", ""));
        PO.SetValueByParamName("TASK_UNIT", efRowSet.getString("TASK_UNIT", ""));
        PO.SetValueByParamName("BIZ_LOGIN_UNIT", efRowSet.getString("BIZ_UNIT", ""));
        // PO.SetValueByParamName("OP_SUBMIT_NAME", efRowSet.getString("OP_SUBMIT_NAME", ""));

       // PO.SetValueByEnvName("UserName", "szfeng");
        String userName = (String) PO.getEnvValue("UserName","");
        userName = userName.replace(".zyyt","");
        System.out.printf(userName);

        PO.SetValueByEnvName("UserName", userName);
       // PO.SetValueByEnvName("UserCaption", "孙泽峰");
        PO.SetValueByEnvName("UserCaption", AndroidEnvironmentVariable.getUserName());

        PO.SetValueByParamName("serviceName", "FlowTaskService");
        PO.SetValueByParamName("serviceMethod", "submitTask");
        try {
            RO = EAI.DAL.SVR("YTBService", PO);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (RO == null) {
            return "提交失败";
        }
        JResponseObject responseObject = (JResponseObject) RO.getResponseObject();
        if (responseObject == null) {
            return "提交失败";
        }
        if (responseObject.getErrorCode() == -1) {
            return "提交失败";
        } else {
            return "提交成功";
        }


    }

    // TODO: 2017/1/10 取回操作
    private String takeBackOperation() {
        JParamObject PO = JParamObject.Create();
        JResponseObject RO = null;
        PO.SetValueByParamName("OP_LEVEL", efRowSet.getString("OP_LEVEL", ""));
        PO.SetValueByParamName("OP_PROC_NOTE", shenheText);
        PO.SetValueByParamName("FLOW_ID", efRowSet.getString("FLOW_ID", ""));
        PO.SetValueByParamName("NODE_ID", efRowSet.getString("NODE_TAG", ""));
        PO.SetValueByParamName("OBJ_GUID", efRowSet.getString("OBJ_GUID", ""));
        PO.SetValueByParamName("BIZ_UNIT", efRowSet.getString("BIZ_UNIT", ""));
        PO.SetValueByParamName("BIZ_DATE", efRowSet.getString("BIZ_DATE", ""));
        PO.SetValueByParamName("BIZ_DJBH", efRowSet.getString("BIZ_DJBH", ""));
        PO.SetValueByParamName("BIZ_MDL", efRowSet.getString("MDL_ID", ""));
        PO.SetValueByParamName("MDL_ID", efRowSet.getString("MDL_ID", ""));
        PO.SetValueByParamName("LOOP_ID", efRowSet.getString("LOOP_ID", ""));
        PO.SetValueByParamName("OP_ID", efRowSet.getString("OP_ID", ""));
        //PO.SetValueByParamName("POP_ID", efRowSet.getString("POP_ID", ""));
        PO.SetValueByParamName("TASK_UNIT", efRowSet.getString("TASK_UNIT", ""));
        PO.SetValueByParamName("BIZ_LOGIN_UNIT", efRowSet.getString("BIZ_UNIT", ""));

        String PFLOW_ID = efRowSet.getString("PFLOW_ID", "");
        if ("".equals(PFLOW_ID)) {
            PFLOW_ID = efRowSet.getString("TOFLOW_ID", "");
        }
        PO.SetValueByParamName("PFLOW_ID", PFLOW_ID);
        PO.SetValueByParamName("PFLSPLIT_JE_COLOW_ID", efRowSet.getString("PFLSPLIT_JE_COLOW_ID", ""));

        //PO.SetValueByEnvName("UserName", "szfeng");
        String userName = (String) PO.getEnvValue("UserName","");
        userName = userName.replace(".zyyt","");
        System.out.printf(userName);

        PO.SetValueByEnvName("UserName", userName);

        //PO.SetValueByEnvName("UserCaption", "孙泽峰");
        PO.SetValueByEnvName("UserCaption", AndroidEnvironmentVariable.getUserName());

        PO.SetValueByParamName("serviceName", "FlowTaskService");
        PO.SetValueByParamName("serviceMethod", "retakeTask");
        try {
            RO = EAI.DAL.SVR("YTBService", PO);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (RO == null) {
            return "取回失败";
        }
        JResponseObject responseObject = (JResponseObject) RO.getResponseObject();
        if (responseObject == null) {
            return "取回失败";
        }
        if (responseObject.getErrorCode() == -1) {
            return "取回失败";
        } else {
            return "取回成功";
        }


    }

    // TODO: 2017/1/10  退回操作
    private String rollBackOperation() {
        JParamObject PO = JParamObject.Create();
        JResponseObject RO = null;
        PO.SetValueByParamName("OP_LEVEL", efRowSet.getString("OP_LEVEL", ""));
        PO.SetValueByParamName("OP_PROC_NOTE", shenheText);
        PO.SetValueByParamName("FLOW_ID", efRowSet.getString("FLOW_ID", ""));
        PO.SetValueByParamName("NODE_ID", efRowSet.getString("NODE_TAG", ""));
        PO.SetValueByParamName("OBJ_GUID", efRowSet.getString("OBJ_GUID", ""));
        PO.SetValueByParamName("BIZ_UNIT", efRowSet.getString("BIZ_UNIT", ""));
        PO.SetValueByParamName("BIZ_DATE", efRowSet.getString("BIZ_DATE", ""));
        PO.SetValueByParamName("BIZ_DJBH", efRowSet.getString("BIZ_DJBH", ""));
        PO.SetValueByParamName("BIZ_MDL", efRowSet.getString("MDL_ID", ""));
        PO.SetValueByParamName("MDL_ID", efRowSet.getString("MDL_ID", ""));
        PO.SetValueByParamName("LOOP_ID", efRowSet.getString("LOOP_ID", ""));//退回独有
        PO.SetValueByParamName("OP_ID", efRowSet.getString("OP_ID", ""));
        PO.SetValueByParamName("POP_ID", efRowSet.getString("POP_ID", ""));//退回独有
        PO.SetValueByParamName("TASK_UNIT", efRowSet.getString("TASK_UNIT", ""));
        PO.SetValueByParamName("BIZ_LOGIN_UNIT", efRowSet.getString("BIZ_UNIT", ""));

        String PFLOW_ID = efRowSet.getString("PFLOW_ID", "");
        if ("".equals(PFLOW_ID)) {
            PFLOW_ID = efRowSet.getString("TOFLOW_ID", "");
        }
        PO.SetValueByParamName("PFLOW_ID", PFLOW_ID);//退回独有
        // PO.SetValueByParamName("PFLOW_ID", efRowSet.getString("PFLOW_ID", ""));


        //PO.SetValueByEnvName("UserName", "szfeng");
        String userName = (String) PO.getEnvValue("UserName","");
        userName = userName.replace(".zyyt","");
        System.out.printf(userName);

        PO.SetValueByEnvName("UserName", userName);
       // PO.SetValueByEnvName("UserCaption", "孙泽峰");
        PO.SetValueByEnvName("UserCaption", AndroidEnvironmentVariable.getUserName());
        PO.SetValueByParamName("serviceName", "FlowTaskService");
        PO.SetValueByParamName("serviceMethod", "rollBackTask");
        try {
            RO = EAI.DAL.SVR("YTBService", PO);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (RO == null) {
            return "退回失败,请在PC端处理";
        }
        JResponseObject responseObject = (JResponseObject) RO.getResponseObject();
        if (responseObject == null) {
            return "退回失败,请在PC端处理";
        }
        if (responseObject.getErrorCode() == -1) {
            return "退回失败,请在PC端处理";
        } else {
            return "退回成功";
        }


    }
}