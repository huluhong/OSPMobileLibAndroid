/**
 * auto lch 3-08
 */
package com.efounder.util;

import java.util.Hashtable;
import java.util.Map;

import android.content.Context;

import com.core.xml.StubObject;
import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;
import com.efounder.forwechat.BaseApp;
import com.efounder.service.security.SecurityContext;

public class SecurityManager{
	
	/**
	 * 根据appcontext中的权限map判断是否可见
	 * @param gnbh
	 * @return
	 */
	
	private static boolean checkGNQXWithGNBH(Context context ,String gnbh){
		//Map qxMap = AndroidEnvironmentVariable.getPowerMap();
		BaseApp baseApp = (BaseApp) context.getApplicationContext();
		Map qxMap = baseApp.getQxMap();
		if(qxMap==null||qxMap.isEmpty()) return false;
		String gnqx = (String) qxMap.get(gnbh);
		
		return gnqx!=null;
	}
	/**
	 * 根据菜单StubObject检查权限 
	 * @return
	 */
	public static boolean checkGNQXWithStub(Context context,StubObject so){
		String gnbhString = so.getString("GNBH", "");
		if(!gnbhString.equals(""))
			return checkGNQXWithGNBH(context, gnbhString);	
		return true;
	}
	/**
	 * 获取权限列表，并保存到APPcontext
	 * @param context
	 * @param username
	 */
	public static void getUserLimitWithUserName(final Context context,final String username){
		    
		
		/*	new AsyncTask<Void, Void, JResponseObject>() {
				@Override
				protected JResponseObject doInBackground(Void... params) {*/
					
					JParamObject poForPower = JParamObject.Create();
					//String userName = AndroidEnvironmentVariable.getUserName();
					poForPower.SetValueByParamName("UserName",username);
					
					// 连接服务器
					JResponseObject roForPower = null;
					try {
						roForPower = EAI.DAL.IOM("SecurityObject", "getUserFunctionLimitMap", poForPower, null,
								null);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				/*	return roForPower;
				}

				@SuppressWarnings("unchecked")
				protected void onPostExecute(JResponseObject result) {*/
					
					if(roForPower==null||roForPower.getErrorCode()==-1){
						return;
					}
					SecurityContext securityContext = (SecurityContext) roForPower.getResponseObject();
					//securityContext.getStubTable();
					//MySecurityContext msecurityContext = new MySecurityContext();
				    Hashtable< String, Object> hashTable = securityContext.getStubTable();
				    if(hashTable!=null){
				    	//Map<String, Object> qxmap = securityContext.getStubTable(username+"_Map");
				    	Map<String, Object> qxmap =  (Map<String, Object>) hashTable.get(username+"_Map");
						BaseApp baseApp = (BaseApp) context.getApplicationContext();
						baseApp.setQxMap(qxmap);
				    }
					
				}

			//}.execute();
	//}
		
}