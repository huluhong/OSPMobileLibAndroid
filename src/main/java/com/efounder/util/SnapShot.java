package com.efounder.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

public class SnapShot {

	/**
	 * 快照
	 * @param view 需要快照的view
	 * @param directoryPath 文件夹路径，例如：  Environment.getExternalStorageDirectory() +File.separator+ "snapShot"
	 * @param fileName 文件名，以.png结尾，例如：System.currentTimeMillis() + ".png"
	 */
	public Bitmap shot(View view, final String directoryPath,final String fileName) {
		// 1.
		// 在调用getDrawingCache()方法从ImageView对象获取图像之前，一定要调用setDrawingCacheEnabled(true)方法,
		// 否则，无法从ImageView对象iv_photo中获取图像；
	
		view.setDrawingCacheEnabled(true);
		// 2.获取快照
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.buildDrawingCache();
//        Bitmap bitmap = view.getDrawingCache();
        final Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        if (bitmap == null) {
			Log.e(this.getClass().toString(), "获取快照为null，此时view尚未绘制");
			return null;
		}
        new AsyncTask<String, Integer, Bitmap>() {

			@Override
			protected Bitmap doInBackground(String... params) {
				//快照
				saveBitmap(bitmap, directoryPath, fileName);
				return bitmap;
			}
			
		}.execute();
		
		// 3.在调用getDrawingCache()方法从ImageView对象获取图像之后，一定要调用setDrawingCacheEnabled(false)方法,
		// 以清空画图缓冲区，否则，下一次从ImageView对象iv_photo中获取的图像，还是原来的图像。
		view.setDrawingCacheEnabled(false);
		return bitmap;
	}

	/** 保存方法 */
	private void saveBitmap(Bitmap bitmap, String directoryPath,String fileName) {
		Log.i("", "保存图片");
		File dir = new File(directoryPath);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		File file = new File(directoryPath, fileName);
		try {
			FileOutputStream out = new FileOutputStream(file);
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
			out.flush();
			out.close();
			Log.i("", "已经保存");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
