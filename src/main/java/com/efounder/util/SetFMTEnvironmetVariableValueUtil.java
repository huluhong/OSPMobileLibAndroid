package com.efounder.util;

import android.content.Context;
import android.widget.Toast;

import com.efounder.constant.EnvironmentVariable;
import com.pansoft.xmlparse.FormatCol;
import com.pansoft.xmlparse.FormatColTab;
import com.pansoft.xmlparse.FormatRow;
import com.pansoft.xmlparse.FormatRowTab;
import com.pansoft.xmlparse.FormatSet;
import com.pansoft.xmlparse.FormatTable;
import com.pansoft.xmlparse.FormatTableTab;
import com.pansoft.xmlparse.MobileFormatUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangshunyun on 2017/9/11.
 * <p>
 * 将hfmt ffmt 和tfmt放入环境变量里的PowerMap中
 * 取值的时候EnvironmentVariable.getPowerMap().get("hfmt")...以此类推
 */

public class SetFMTEnvironmetVariableValueUtil {
    public static FormatSet formatSet;

    /**
     * @param context
     * @param flowId  task_flowId
     */
    public static Map<String, Object> setFmtEnvironmentValue(Context context, String flowId) {

        Map<String, Object> map = new HashMap<>();
        formatSet = MobileFormatUtil.getInstance().getFormatSet();
//        Map<String, Object> map = new HashMap<String, Object>();
        FormatTable hfmt = formatSet.getFormatTableById(flowId);
        FormatTableTab tfmt = formatSet.getFormatTableTabById(flowId + "_Tab");
        //由于ffmt显示哪个最终由tfmt确定，ffmt有几个是由tfmt中的cols中的个数确定的,ffmt的内容是根据
        //tfmt里rows里DataSetColID的值决定的，通过String dataSetColID = list.get(0).getDataSetColID();
        //获得到dataSetColID，然后通过ffmt = formatSet.getFormatTableById(flowId + "_FL_" + index);
        //就可以得到ffmt
        if (null != tfmt) {
            List<FormatRowTab> formatRowList = tfmt.getFormatRowList();
            int size = formatRowList.size();
            if (size > 0) {
                //由于key 为ffmt的数据结构是个数组，遍历formatRowList
                int flag = 0;
                Object[] ffmtArray = new Object[size];
                for (int i = 0; i < size; i++) {
                    String index = "";
                    if ((i + 1) < 10) {
                        index = "0" + (i + 1);
                    } else {
                        index = (i + 1) + "";
                    }
                    List<FormatColTab> list = formatRowList.get(i).getList();
                    String dataSetColID = list.get(0).getDataSetColID();
                    FormatTable ffmt;
                    if (null == dataSetColID) {
                        ffmt = formatSet.getFormatTableById(flowId + "_FL_" + index);
                    } else {
                        ffmt = formatSet.getFormatTableById(dataSetColID);
                    }
//                    FormatTable ffmt = formatSet.getFormatTableById(flowId + "_FL_" + index);
                    if (null != ffmt) {
                        Map fmt = getFMT(ffmt);
                        ffmtArray[i] = fmt;
                    } else {
                        flag = flag + 1;
                        setMapValue(map, flowId);
                        break;
                    }
                }
                if (ffmtArray != null) {
                    map.put("ffmt", ffmtArray);
                } else {
                    map.put("ffmt", null);
                }
                if(flag == size)
                    map.put("ffmt", null);
            } else {
                setMapValue(map, flowId);
            }
        } else {
            setMapValue(map, flowId);
        }
        map.put("ImgPath", EnvironmentVariable.getProperty("ImgPath", "file:///sdcard/ZSYGXOSPMobile/res/unzip_res/Image/"));
        if (hfmt != null) {
            map.put("hfmt", getFMT(hfmt));
        } else {
          map.put("hfmt", null);
//            Toast.makeText(context, "业务模型未加载，请更新资源文件", Toast.LENGTH_SHORT).show();
//            return false;
        }
        if (tfmt != null) {
            map.put("tfmt", getFMT_Tab(tfmt));
        } else {
            map.put("tfmt", null);
        }
//        EnvironmentVariable.setPowerMap(map);

        return map;
    }

    private static void setMapValue(Map<String, Object> map, String flowId) {
        FormatTable ffmt = formatSet.getFormatTableById(flowId + "_FL");
        if (ffmt != null) {
            map.put("ffmt", getFMT(ffmt));
        } else {
            map.put("ffmt", null);
        }
    }

    /**
     * @param fmt
     * @return
     */
    public static Map getFMT(FormatTable fmt) {

        final Map<String, Object> fmtMap = new HashMap<>();

        List<FormatRow> rows = fmt.getFormatRowList();
        List<List> colsList = new ArrayList<List>();
        for (int i = 0; i < rows.size(); i++) {
            List<FormatCol> cols = rows.get(i).getList();
            List<Map> colArray = new ArrayList<Map>();

            for (int j = 0; j < cols.size(); j++) {
                Map<String, Object> colum = new HashMap<String, Object>();
                colum.put("caption", cols.get(j).getCaption());
                colum.put("dataSetColID", cols.get(j).getDataSetColID());
                colum.put("columnType", cols.get(j).getColumnType());
                colum.put("numberPrecision", cols.get(j).getNumberPrecision());
                colum.put("textAlign", cols.get(j).getTextAlign());
                colum.put("textFormat", cols.get(j).getTextFormat());
                colum.put("dateFormat", cols.get(j).getDateFormat());
                colum.put("mask", cols.get(j).getMask());

                colArray.add(colum);
            }
            colsList.add(colArray);
        }

        fmtMap.put("detailCol", fmt.getDetailCol());
        fmtMap.put("detailName", fmt.getDetailName());
        fmtMap.put("rows", rows.size());
        fmtMap.put("cols", colsList);
        fmtMap.put("tableName", fmt.getId());
        fmtMap.put("title", fmt.getCaption());
        return fmtMap;
    }

    public static Map getFMT_Tab(FormatTableTab fmt) {
        final Map<String, Object> hfmt = new HashMap<>();

        List<FormatRowTab> rows = fmt.getFormatRowList();
        List<List> colsList = new ArrayList<List>();
        for (int i = 0; i < rows.size(); i++) {
            List<FormatColTab> cols = rows.get(i).getList();
            List<Map> colArray = new ArrayList<Map>();

            for (int j = 0; j < cols.size(); j++) {
                Map<String, Object> colum = new HashMap<String, Object>();
                colum.put("tabName", cols.get(j).getTabName());
                colum.put("icon", cols.get(j).getIcon());
                colum.put("selectIcon", cols.get(j).getSelectIcon());
                colum.put("dataSetColID", cols.get(j).getDataSetColID());
                colArray.add(colum);
            }
            colsList.add(colArray);
        }

        hfmt.put("detailCol", fmt.getDetailCol());
        hfmt.put("detailName", fmt.getDetailName());
        hfmt.put("rows", rows.size());
        hfmt.put("cols", colsList);
        hfmt.put("tableName", fmt.getId());
        hfmt.put("title", fmt.getCaption());
        return hfmt;
    }
}
