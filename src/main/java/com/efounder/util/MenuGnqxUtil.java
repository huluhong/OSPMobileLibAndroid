package com.efounder.util;

import com.core.xml.StubObject;
import com.efounder.constant.EnvironmentVariable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 菜单功能权限过滤
 * Created by yqs on 2017/9/7.
 */

public class MenuGnqxUtil {

    /**
     * 过滤功能权限
     *
     * @param mainMenus 要过滤的菜单
     * @return 过滤后的菜单
     */
    public static List<StubObject> handleGNQX(List<StubObject> mainMenus) {
        if (mainMenus == null) {
            return new ArrayList<StubObject>();
        }
        Iterator<StubObject> iterator = mainMenus.iterator();
        String gnqxString = EnvironmentVariable.getProperty("gnqx", "");
        if (gnqxString != null) {//==null表示没有开启权限
            String[] gnqxStrings = gnqxString.split(";");
            while (iterator.hasNext()) {
                StubObject stubObject = iterator.next();
                String permission = stubObject.getString("permission", "");
                if (!permission.equals("")) {
                    for (int j = 0; j < gnqxStrings.length; j++) {
                        String gnqx = gnqxStrings[j];
                        if (gnqx.equalsIgnoreCase(permission)) {
                            break;
                        } else {
                            if (j == gnqxStrings.length - 1) {
                                iterator.remove();
                            }
                        }
                    }
                }
            }
        }

        return mainMenus;
    }
}
