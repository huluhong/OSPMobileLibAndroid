package com.efounder.util;

import com.efounder.builder.base.data.EFRowSet;
import com.efounder.constant.EnvironmentVariable;

import static com.efounder.frame.utils.Constants.KEY_F_ZGBH;
import static com.efounder.frame.utils.Constants.KEY_SETTING_APPID;
import static com.efounder.frame.utils.Constants.KEY_TANGZU_JSRQ;

/**
 * Created by YQS on 2017/12/20.
 */

public class TangZuUtil {
    public static void saveTangZuInfo(EFRowSet weChatRowSet) {
        if (weChatRowSet == null) {
            return;
        }
        if (!"OSPMobileTZu".equals(EnvironmentVariable.getProperty(KEY_SETTING_APPID))) {
            return;
        }
        String jsrq = weChatRowSet.getString("F_JSRQ", "");//取角色
        String tzF_name = weChatRowSet.getString("F_NAME", "");
        String f_zgbh = weChatRowSet.getString("F_ZGBH", "");
        String f_yxqytx = weChatRowSet.getString("F_YXQYTX", "");//保存是否允许亲友填写

        EnvironmentVariable.setProperty(KEY_TANGZU_JSRQ, jsrq);
        EnvironmentVariable.setProperty("tzF_name", tzF_name);
        EnvironmentVariable.setProperty(KEY_F_ZGBH, f_zgbh);
        EnvironmentVariable.setProperty("F_YXQYTX", f_yxqytx);
    }

}
