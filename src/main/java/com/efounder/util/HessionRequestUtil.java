//package com.efounder.util;
//
//import android.content.Context;
//import android.util.Log;
//
//import com.efounder.eai.EAI;
//import com.efounder.eai.data.JParamObject;
//import com.efounder.eai.data.JResponseObject;
//
//public class HessionRequestUtil {
//
//	private static final String TAG = "HessionRequestUtil";
//	/**
//	 *
//	 * @param context
//	 * @param mdm_id  HY_HYSZD
//	 * @param hysmc
//	 * @return
//	 */
//	public static JResponseObject getHYSZD(Context context,String mdm_id,String hysmc){
//		JResponseObject RO = null;
//		JParamObject PO = CommonPo.getPo(context);
//		Log.i(TAG, "Po.getEnvRoot:"+PO.getEnvRoot());
//		PO.SetValueByParamName("DCT_ID", mdm_id);
//		//PO.SetValueByParamName(mdm_id+"_MDMSelfWhere", "f_syzt = '1' and f_hysddmc like \'"+hysmc+"%\'");
//		PO.SetValueByParamName(mdm_id+"_MDMSelfWhere", "f_syzt = '1'");
//		try {
//			RO = EAI.DAL.SVR("LargeDICTService", PO);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		if(RO==null){
//			Log.i(TAG,"----RO is null");
//		}else{
//			Log.i(TAG,"----RO:"+RO.toString());
//		}
//		return RO;
//	}
//
//	/**
//	 * 获取会议字典数据
//	 * @param context
//	 * @param hyrq 会议日期 20151216
//	 * @return
//	 */
//	public static JResponseObject getHyzdByDay(Context context,String hyrq ){
//		JResponseObject RO = null;
//		JParamObject PO = CommonPo.getPo(context);
//		Log.i(TAG, "Po.getEnvRoot:"+PO.getEnvRoot());
//
//		PO.SetValueByParamName("DCT_ID", "HY_HYZD");
//		//FIXME 获取会议数据的过滤条件，不知道是否正确 （F_SPZT审批状态  是什么意思 ）
////		PO.SetValueByParamName("HY_HYZD_MDMSelfWhere", "f_sdate like \'"+hyrq +"%\' ");
//		PO.SetValueByParamName("HY_HYZD_MDMSelfWhere", "f_sdate like \'"+hyrq +"%\' and F_SPZT=2");
//		PO.SetValueByParamName("HY_HYZD_MDMSelfOrder", "f_hylx asc");
//		try {
//			RO = EAI.DAL.SVR("LargeDICTService", PO);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		if(RO==null){
//			Log.i(TAG,"----RO1 is null");
//		}else{
//			Log.i(TAG,"----RO1:"+RO.toString());
//		}
//		return RO;
//	}
//	/**
//	 * 会议资料字典
//	 * @param context
//	 * @param hybh
//	 * @return
//	 */
//	public static JResponseObject getHYZLZD(Context context,String hybh ){
//		JResponseObject RO = null;
//		JParamObject PO = CommonPo.getPo(context);
//		Log.i(TAG, "Po.getEnvRoot:"+PO.getEnvRoot());
//		//原来是HY_HYZLZD,2015-02-06 调整成 附件字典 HY_HYZLZD_AFFIX
//		PO.SetValueByParamName("DCT_ID", "HY_HYZLZD_AFFIX");
//		PO.SetValueByParamName("HY_HYZLZD_AFFIX_MDMSelfWhere", "exists(select 1 from HY_HYZLZD where f_hybh =\'"+hybh+"\' and HY_HYZLZD_AFFIX.F_GUID = HY_HYZLZD.F_GUID)");
//		try {
//			RO = EAI.DAL.SVR("LargeDICTService", PO);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		if(RO==null){
//			Log.i(TAG,"----RO is null");
//		}else{
//			Log.i(TAG,"----RO:"+RO.toString());
//		}
//		return RO;
//	}
//	/**
//	 * 获取会议人员
//	 * @param context
//	 * @param hybh
//	 * @return
//	 */
//	public static JResponseObject getHYRY(Context context,String hybh){
//		JResponseObject RO = null;
//		JParamObject PO = CommonPo.getPo(context);
//		Log.i(TAG, "Po.getEnvRoot:"+PO.getEnvRoot());
//		PO.SetValueByParamName("DCT_ID", "HY_HYRY");
//		PO.SetValueByParamName("HY_HYRY_MDMSelfWhere", "f_hybh =\'"+hybh+"\'");
//		PO.SetValueByParamName("HY_HYRY_MDMSelfOrder", "F_XH ASC");
//		try {
//			RO = EAI.DAL.SVR("LargeDICTService", PO);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		if(RO==null){
//			Log.i(TAG,"----RO is null");
//		}else{
//			Log.i(TAG,"----RO:"+RO.toString());
//		}
//		return RO;
//	}
//}
