package com.efounder.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import com.efounder.ospmobilelib.R;

public class CustomProgressDialog extends ProgressDialog{

	public CustomProgressDialog(Context context) {
		super(context);
	}
	
	public CustomProgressDialog(Context context, int theme) {
		super(context, theme);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.progressbar);
	}
	
//	public  CustomProgressDialog show(Context ctx,String s){
//		CustomProgressDialog d = new CustomProgressDialog(ctx);
//		d.show();
//		return d;
//	}
}
