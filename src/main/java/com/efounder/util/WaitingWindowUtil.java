package com.efounder.util;

import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.efounder.ospmobilelib.R;

public class WaitingWindowUtil {

	/*
	 * 瀹氢箟loadingDialog
	 */
	public  static Dialog loadingDialog;

	/*
	 * 瀹氢箟Appcontext鍏ㄥ眬鍙橀噺
	 */
	public static AppContext appContext = AppContext.getInstance();

	/*
	 * 锷犺浇鏄剧ず镄勬枃瀛?
	 */
	public static String stringShow;

	/*
	 * 閲嶈浇鏂规硶
	 */
	public static void show() {

		show("锷犺浇涓?..");
	}

	/*
	 * 鏄剧ずdialog
	 */
	public static void show(String msg) {

		// 鐢ㄦ潵杞藉叆LinearLayout
		LayoutInflater layoutInflater = LayoutInflater.from(appContext);

		// 鍒涘缓瑙嗗浘
		View v = layoutInflater.inflate(R.layout.login_dialog, null);

		// 锷犺浇甯冨眬鏂囦欢
		LinearLayout layout = (LinearLayout) v.findViewById(R.id.dialog_view);

		// 锷犺浇imageView
		ImageView imageView = (ImageView) v.findViewById(R.id.loading_img);

		// 璁剧疆textVeiw
		TextView textView = (TextView) v.findViewById(R.id.loading_textView);
		textView.setText(msg + "......");

		// 锷犺浇锷ㄧ敾
		Animation loadingAnimation = AnimationUtils.loadAnimation(appContext,
				R.anim.loading_animation);

		// 浣跨敤ImageView鏄剧ず锷ㄧ敾
		imageView.startAnimation(loadingAnimation);
		//鍒濆鍖杁ialog
		loadingDialog = new Dialog(appContext, R.style.loading_dialog);
		
		// 鏄惁鐢ㄢ€滆繑锲为敭钬濆彇娑?
		loadingDialog.setCancelable(false);
		
		loadingDialog.setContentView(layout);
		loadingDialog.getWindow().setType(
				WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
		loadingDialog.show();
	}

	/*
	 * 鍙栨秷dialog
	 */
	public static void dismiss() {
		if (loadingDialog != null) {
			loadingDialog.dismiss();
		}
	}

}
