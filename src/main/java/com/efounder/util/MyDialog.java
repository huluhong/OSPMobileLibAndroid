package com.efounder.util;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.efounder.ospmobilelib.R;
import com.pansoft.espaction.FlowRetakeAction;
import com.pansoft.espaction.FlowRollbackAction;
import com.pansoft.espaction.FlowSubmitAction;
import com.pansoft.espmodel.FormModel;
import com.pansoft.utils.CommonPo;
public class MyDialog extends Dialog implements View.OnClickListener {
	/**提交审核*/
	public static final int DIALOG_SHEN_HE = 0x0000;
	/**取回动作*/
	public static final int DIALOG_TAKE_BACK = 0x0001;
	/**退回动作*/
	private static final int DIALOG_ROLL_BACK = 0x0002;
	
			// private EditText prePassword;
			private EditText shenhe;
			private TextView title;
			private TextView submit;
			private TextView cancel;
			Context context;
			private int viewId;
			private FormModel formModel;

			public MyDialog(Context context) {
				super(context);
				this.context = context;
			}

			public MyDialog(Context context, int theme,int id,FormModel formModel) {
				super(context, theme);
				this.context = context;
				this.viewId = id;
				this.formModel = formModel;
			}

			@Override
			protected void onCreate(Bundle savedInstanceState) {
				super.onCreate(savedInstanceState);
				//this.setContentView(R.layout.shen_he_dialog);
				//this.setContentView(R.layout.new_shen_he_dialog);
				this.setContentView(R.layout.shen_he_dialog_new);
				// prePassword = (EditText) findViewById(R.id.prePassword);
				title = (TextView) findViewById(R.id.title);
				if(viewId==DIALOG_SHEN_HE){
					title.setText("流程任务处理");
				}//else if(viewId==DIALOG_TAKE_BACK){
				else if(viewId==DIALOG_TAKE_BACK){
					title.setText("流程取回处理");
				}
				else if(viewId==DIALOG_ROLL_BACK){
					title.setText("流程退回处理");
				}
				shenhe = (EditText) findViewById(R.id.shenhe);
				submit = (TextView) findViewById(R.id.dialog_button_ok);
				cancel = (TextView) findViewById(R.id.dialog_button_cancel);
				submit.setOnClickListener(this);
				cancel.setOnClickListener(this);
			}

			@Override
			public void onClick(View v) {
				//TODO 
				if(viewId==DIALOG_SHEN_HE){
					if (v.equals(submit)) {
						new AsyncTask<String, Integer, String>() {
							@Override
							protected void onPreExecute() {
								super.onPreExecute();
								LoadingDataUtil.show("正在提交...");
							}
							@Override
							protected String doInBackground(String... params) {
								FlowSubmitAction fsa = new  FlowSubmitAction();
								if(fsa.PO ==null)
									fsa.PO= CommonPo.getPo();
								fsa.PO.SetValueByParamName("OP_PROC_NOTE",shenhe.getText().toString());
								fsa.flowDriveDataObject = formModel;
								
								return fsa.doAction();
							}
							protected void onPostExecute(String result) {
								super.onPostExecute(result);
								LoadingDataUtil.dismiss();
								if (result == null) {
									ToastUtil.showToast(context, "提交失败！");
								}else {
									ToastUtil.showToast(context, result);
								}
								dismiss();
							}
						}.execute();
						
					} else if (v.equals(cancel)) {
						this.dismiss();
					}
				}else if(viewId==DIALOG_TAKE_BACK){
					if (v.equals(submit)) {

						new AsyncTask<String, Integer, String>() {
							@Override
							protected void onPreExecute() {
								super.onPreExecute();
								LoadingDataUtil.show("正在提交，请稍候...");
							}
							@Override
							protected String doInBackground(String... params) {
								FlowRetakeAction flowRetakeAction = new  FlowRetakeAction();
								flowRetakeAction.flowDriveDataObject = formModel;
								
								return flowRetakeAction.doAction();
							}
							protected void onPostExecute(String result) {
								super.onPostExecute(result);
								LoadingDataUtil.dismiss();
								if (result == null) {
									ToastUtil.showToast(context, "提交失败！");
								}else {
									ToastUtil.showToast(context, result);
								}
								dismiss();
							}
						}.execute();
					} else if (v.equals(cancel)) {
						this.dismiss();
					}
				}
				
				else if(viewId==DIALOG_ROLL_BACK){
					if (v.equals(submit)) {

						new AsyncTask<String, Integer, String>() {
							@Override
							protected void onPreExecute() {
								super.onPreExecute();
								LoadingDataUtil.show("正在提交，请稍候...");
							}
							@Override
							protected String doInBackground(String... params) {
								FlowRollbackAction frba = new  FlowRollbackAction();
								frba.flowDriveDataObject = formModel;
								
								return frba.doAction();
							}
							protected void onPostExecute(String result) {
								super.onPostExecute(result);
								LoadingDataUtil.dismiss();
								if (result == null) {
									ToastUtil.showToast(context, "提交失败！");
								}else {
									ToastUtil.showToast(context, result);
								}
								dismiss();
							}
						}.execute();
					} else if (v.equals(cancel)) {
						this.dismiss();
					}
				}
			}

		}