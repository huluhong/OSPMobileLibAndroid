package com.efounder.util;

import android.util.Log;

/**
 * Created by lch on 2016/8/27 0027.
 */

public class LogHelp {
    private static String sRootTag = "";
    private static boolean isPrint = true;

    public static void setRootTag(String rootTag) {
        sRootTag = rootTag;
    }

    /**
     * 打印log详细信息
     */
    public static void d(String tag, String content) {
        if(isPrint)
        Log.d(sRootTag + "_" + tag, content);
    }
    /**
     * 打印log详细信息
     */
    public static void i(String tag, String content) {
        if(isPrint)
        Log.d(sRootTag + "_" + tag, content);
    }

    /**
     * 得到调用此方法的线程的线程名
     *
     * @return
     */
    public static String getThreadName() {
        StringBuffer sb = new StringBuffer();
        sb.append(Thread.currentThread().getName());
        sb.append("-> ");
        sb.append(Thread.currentThread().getStackTrace()[3].getMethodName());
        sb.append("()");
        sb.append(" ");
        return sb.toString();
    }
}
