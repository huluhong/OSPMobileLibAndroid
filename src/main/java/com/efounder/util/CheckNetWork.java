package com.efounder.util;

import java.io.IOException;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
/**
 * {@检测判断网络状况，包括当前有无链接，wifi是否打开，数据流量是否打开
 *  当前网络链接的状态}
 * @author long
 *
 */
public class CheckNetWork {
	/**
	 * 判断是否有网络连接
	 * @param context
	 * @return   true 有连接
	 * @return   false无连接
	 */
	public  static boolean isNetworkConnected(Context context) {  
	    if (context != null) {  
	        ConnectivityManager mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);  
	        NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();  
	        if (mNetworkInfo != null) {  
	            return mNetworkInfo.isAvailable();  
	        }  
	    }  
	    return false;  
	}
	/**
	 * 判断WiFi是否可用
	 * @param context
	 * @return true wifi已链接
	 * @return flase wifi未链接
	 */
	public boolean isWifiConnected(Context context) {  
	    if (context != null) {  
	        ConnectivityManager mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);  
	        NetworkInfo mWiFiNetworkInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);  
	        if (mWiFiNetworkInfo != null) {
	        	if(mWiFiNetworkInfo.isAvailable()){
	        		return true;
	        	}
	            return false;  
	        }  
	    }  
	    return false;  
	}
	/**
	 * 判断mobile网络是否可用
	 * @param context
	 * @return
	 */
	public boolean isMobileConnected(Context context) {  
	    if (context != null) {  
	        ConnectivityManager mConnectivityManager = (ConnectivityManager) context  
	                .getSystemService(Context.CONNECTIVITY_SERVICE);  
	        NetworkInfo mMobileNetworkInfo = mConnectivityManager  
	                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);  
	        if (mMobileNetworkInfo != null) {  
	        		if(mMobileNetworkInfo.isAvailable()){
	        			return true;
	        		}else{
	        			return false;
	        		}
	        }  
	    }  
	    return false;  
	}
	
	/**
	 * 获取网络连接类型
	 * @param context
	 * @return
	 */
	public static int getConnectedType(Context context) {  
	    if (context != null) {  
	        ConnectivityManager mConnectivityManager = (ConnectivityManager) context  
	                .getSystemService(Context.CONNECTIVITY_SERVICE);  
	        NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();  
	        if (mNetworkInfo != null && mNetworkInfo.isAvailable()) {  
	            return mNetworkInfo.getType();  
	        }  
	    }  
	    return -1;  
	}
	/**
	 * 
	 * @param IP
	 * @return true 服务器可以ping通; return false 服务器ping不通
	 * 该功能主要测试是否可以链接指定IP 可测试是否连接VPN
	 */
	public Boolean PingIp(String IP){
		Boolean result = null;
		Process p;
		try {
			p = Runtime.getRuntime().exec("/system/bin/ping -c "+ "1" + " " + IP);
	        int status = p.waitFor(); 
	        if (status == 0) {  
	            result=true; 
	        }    
	        else 
	        { 
	            result=false; 
	        } 
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return result;
	}
	
	
	/**
	 * 
	 * @return true 有连接
	 * @return false无连接
	 * 			
	 */
	public Boolean docheckNetwork(Context context) {
		
		ConnectivityManager connectivityManager=(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo net=connectivityManager.getActiveNetworkInfo();
		if(net==null){
		return false;
		}else{
			return true;
			}
		}
}