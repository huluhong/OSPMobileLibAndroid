package com.efounder.util;

import android.content.Context;
import android.util.Log;

import com.efounder.chat.model.Constant;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;

import java.io.File;

import static com.efounder.frame.utils.Constants.KEY_SETTING_APPID;
import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_ADDRESS;
import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_HTTP_TYPE;
import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_PATH;
import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_PORT;

public class CommonPo{
	private static final String TAG = "CommonPo";
	private static JParamObject PO;
	/*
	 * 设置信息初始化
	 */
	private static String port, service, address, path, name, sign;
	private static Boolean isSafe;
	private static String userName, passWord;
	public static void setPoToNull(){
		PO = null;
	}
	public static JParamObject getPo(Context context){
		
//		storageUtil = new StorageUtil(context, "storage");
//		port = storageUtil.getString("port");
//		address = storageUtil.getString("address");
//		service = storageUtil.getString("service");
//		name = storageUtil.getString("name");
//		path = storageUtil.getString("path");
//		sign = storageUtil.getString("sign");
//		isSafe = storageUtil.getBoolean("isSecurity", false);
//		userName = storageUtil.getString("userName");
//		passWord = storageUtil.getString("passWord");
		port = EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_PORT);
		address =EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_ADDRESS);
		service	= EnvironmentVariable.getProperty("service");
		name = EnvironmentVariable.getProperty(KEY_SETTING_APPID);
		path = EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_PATH);
		sign = EnvironmentVariable.getProperty("sign");
		userName = EnvironmentVariable.getUserName();
		passWord = EnvironmentVariable.getPassword();

		if(PO==null){
			
		PO = JParamObject.Create();
		
		String protocol = EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_HTTP_TYPE);

		String ServerURL = protocol + "://" + address + ":" + port + "/"
				+ path;
		String productPath = ServerURL + File.separator + "Android";
		
//		PO.setEnvValue("licenseID", "753B7C4C94467F1C31BC20CB3E913E6D08F81F676704C4F7");
		PO.setEnvValue("UserName",userName);
        PO.setEnvValue("UserPass", passWord);
		PO.setEnvValue("ServerURL",ServerURL);
		PO.setEnvValue("productPath",productPath);
		Log.i(TAG, "ServerURL："+ServerURL);
		Log.i(TAG, "productPath："+productPath);
//		PO.setEnvValue("ServerURL","http://10.75.131.128:8080/EnterpriseServer");
//		PO.setEnvValue("productPath","http://10.75.131.128:8080/EnterpriseServer/Android");
		PO.setEnvValue("DataBaseName", service); // 数据服务
		PO.setEnvValue("DBNO", sign);// 数据标示
		PO.setEnvValue("Password",passWord);
		PO.setEnvValue("Product", name);// 服务名称
		Log.i(TAG, "ServerURL="+ServerURL);
		Log.i(TAG, "productPath="+productPath);
//		Log.i(TAG, "ServerURL="+"http://"+address+":"+port+"/EnterpriseServer");
//		Log.i(TAG, "productPath="+"http://"+address+":"+port+"/EnterpriseServer/Android");
		Log.i(TAG, "DataBaseName="+service);
		Log.i(TAG, "DBNO="+sign);
		Log.i(TAG, "Product="+name);
			EAI.Protocol = EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_HTTP_TYPE);
		EAI.Server = address;
		EAI.Port = port;
		EAI.Path = path;
		EAI.Service = "Android";
	
		}
		return PO;
	}
}