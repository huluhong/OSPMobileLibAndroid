package com.efounder.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


/**
 * 监听网络状态变化，发送toast通知
 *
 * @author yqs
 */
public class NetWorkStateReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        //Toast.makeText(context, intent.getAction(), 1).show();
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobileInfo = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifiInfo = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo activeInfo = manager.getActiveNetworkInfo();
        if (mobileInfo == null || wifiInfo == null) {
            return;
        }
        if (!mobileInfo.isConnected() && !wifiInfo.isConnected()) {
            //Toast.makeText(context,"网络连接断开，请检查网络！",Toast.LENGTH_SHORT).show();
        } else {

            //Toast.makeText(context,"网络连接已恢复！",Toast.LENGTH_SHORT).show();
        }
    }

}
