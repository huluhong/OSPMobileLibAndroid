package com.efounder.agency.activity;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.adapter.AgentFgAdapter;
import com.efounder.chat.event.PendingTaskListSizeEvent;
import com.efounder.chat.event.ProcessedTaskListSizeEvent;
import com.efounder.chat.model.Task;
import com.efounder.frame.baseui.BaseFragment;
import com.efounder.ospmobilelib.R;
import com.efounder.view.LinePagerIndicatorCustom;
import com.utilcode.util.SizeUtils;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ColorTransitionPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.badge.BadgePagerTitleView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AgentFgWithIndicator extends BaseFragment {

    private static final String[] CHANNELS = new String[]{"待我审批", "我已审批"};
    private static StubObject stubObject;

    private RelativeLayout include;

    private List<String> mDataList = Arrays.asList(CHANNELS);
    private ArrayList<Task> mAllTaskDataList = null;
    private ViewPager mViewPager;
    int pendingSize = 0;
    int processedSize = 0;
    private AgentFgAdapter mAdapter;
    private TaskCommonNavigatorAdapter taskCommonNavigatorAdapter;
    private LinearLayout searchButton;
    private ArrayList<Task> dataProcessedList;
    private ArrayList<Task> dataPendingList;

    //    private ExamplePagerAdapter mExamplePagerAdapter = new ExamplePagerAdapter(mDataList);
    public AgentFgWithIndicator() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public AgentFgWithIndicator(StubObject object) {
        this.stubObject = object;
    }

    public static AgentFgWithIndicator newInstance(boolean b) {
        AgentFgWithIndicator agentFgNew = new AgentFgWithIndicator();
        Bundle bundle = new Bundle();
        bundle.putBoolean("IS_ALONE", b);
        agentFgNew.setArguments(bundle);
        return agentFgNew;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        mDataList = new ArrayList<>();
//        mDataList.add("待我审批");
//        mDataList.add("我已审批");
        // Inflate the layout for this fragment
        EventBus.getDefault().register(this);
        mAllTaskDataList = new ArrayList<>();
        View rootView = inflater.inflate(R.layout.fragment_agent_fg_with_indicator, container, false);

        include = (RelativeLayout) rootView.findViewById(R.id.include);
        // 判断fragment是否是单独的
        if (isAlone()) {
            include.setVisibility(View.VISIBLE);
        } else {
            include.setVisibility(View.GONE);
        }

        //todo 20181023 从stubobject 取出是否显隐藏titlebar
        if (stubObject != null) {
            String hileTitleBar = stubObject.getString("hileTitleBar", "0");
            if ("1".equals(hileTitleBar)) {
                include.setVisibility(View.GONE);

            }
        }


        TextView title = (TextView) rootView.findViewById(R.id.fragmenttitle);
        title.setText("任务中心");
        LinearLayout leftbacklayout = (LinearLayout) rootView
                .findViewById(R.id.leftbacklayout);
        leftbacklayout.setVisibility(View.VISIBLE);
        leftbacklayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        mViewPager = (ViewPager) rootView.findViewById(R.id.agent_viewpager);
        searchButton = (LinearLayout) rootView.findViewById(R.id.search_button);
//        mViewPager.setAdapter(mExamplePagerAdapter);
        mAdapter = new AgentFgAdapter(getChildFragmentManager(), mDataList, stubObject);
        mViewPager.setAdapter(mAdapter);
        initMagicIndicator4(rootView);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentItem = mViewPager.getCurrentItem();
                Intent intent = new Intent(getContext(), TaskSearchActivity.class);
                intent.putParcelableArrayListExtra("taskDataList", currentItem == 0 ? dataPendingList : dataProcessedList);
//                intent.putParcelableArrayListExtra("taskDataList", mAllTaskDataList);
                startActivity(intent);
            }
        });
        return rootView;
    }

    class TaskCommonNavigatorAdapter extends CommonNavigatorAdapter {

        @Override
        public int getCount() {
            return mDataList.size();
        }

        @Override
        public IPagerTitleView getTitleView(Context context, final int index) {
            BadgePagerTitleView badgePagerTitleView = new BadgePagerTitleView(context);

            SimplePagerTitleView simplePagerTitleView = new ColorTransitionPagerTitleView(context);
            simplePagerTitleView.setNormalColor(Color.BLACK);
            simplePagerTitleView.setTextSize(16);
            simplePagerTitleView.setSelectedColor(getResources().getColor(R.color.chat_red));
            if (index == 0) {
                simplePagerTitleView.setText(mDataList.get(index) + "(" + pendingSize + ")");
            } else {
                simplePagerTitleView.setText(mDataList.get(index) + "(" + processedSize + ")");

            }
            simplePagerTitleView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mViewPager.setCurrentItem(index);
                }
            });

            badgePagerTitleView.setInnerPagerTitleView(simplePagerTitleView);
            return badgePagerTitleView;
        }

        @Override
        public IPagerIndicator getIndicator(Context context) {
            LinePagerIndicatorCustom linePagerIndicatorCustom = new LinePagerIndicatorCustom(context);
            linePagerIndicatorCustom.setLineHeight(SizeUtils.dp2px(2));
            linePagerIndicatorCustom.setMode(LinePagerIndicator.MODE_EXACTLY);
            linePagerIndicatorCustom.setColors(getResources().getColor(R.color.chat_red));

            LinePagerIndicator linePagerIndicator = new LinePagerIndicator(context);
            linePagerIndicator.setLineHeight(SizeUtils.dp2px(2));
            linePagerIndicator.setMode(LinePagerIndicator.MODE_MATCH_EDGE);

            linePagerIndicator.setColors(getResources().getColor(R.color.chat_red));
            return linePagerIndicatorCustom;
        }

        @Override
        public float getTitleWeight(Context context, int index) {
//                return super.getTitleWeight(context, index);
            if (index == 0) {
                return 2.0f;
            } else if (index == 1) {
                return 2.0f;
            } else {
                return 1.0f;
            }
        }

    }

    private void initMagicIndicator4(View rootView) {
        MagicIndicator magicIndicator = (MagicIndicator) rootView.findViewById(R.id.magic_indicator4);

        CommonNavigator commonNavigator = new CommonNavigator(getActivity());
        commonNavigator.setAdjustMode(true);//自动适应模式
        taskCommonNavigatorAdapter = new TaskCommonNavigatorAdapter();
        commonNavigator.setAdapter(taskCommonNavigatorAdapter);
        magicIndicator.setNavigator(commonNavigator);
        LinearLayout titleContainer = commonNavigator.getTitleContainer(); // must after setNavigator
        titleContainer.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        titleContainer.setDividerDrawable(new ColorDrawable() {
            @Override
            public int getIntrinsicWidth() {
                return SizeUtils.px2dp(15);

            }
        });
        ViewPagerHelper.bind(magicIndicator, mViewPager);
    }

    @Subscribe
    public void pendingSize(PendingTaskListSizeEvent event) {
        pendingSize = event.getSize();
        List<Task> dataList = event.getDataList();
        if (dataPendingList != null) {
            mAllTaskDataList.removeAll(dataPendingList);
            dataPendingList.clear();
        }
        if (dataPendingList == null)
            dataPendingList = new ArrayList<>();
        dataPendingList.addAll(dataList);
        mAllTaskDataList.addAll(dataList);
        taskCommonNavigatorAdapter.notifyDataSetChanged();
    }

    @Subscribe
    public void processedSize(ProcessedTaskListSizeEvent event) {
        processedSize = event.getSize();
        List<Task> dataList = event.getDataList();
        if (dataProcessedList != null) {
            mAllTaskDataList.removeAll(dataProcessedList);
            dataProcessedList.clear();
        }
        if (dataProcessedList == null)
            dataProcessedList = new ArrayList<>();
        dataProcessedList.addAll(dataList);
        mAllTaskDataList.addAll(dataList);
        taskCommonNavigatorAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        //EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }
}
