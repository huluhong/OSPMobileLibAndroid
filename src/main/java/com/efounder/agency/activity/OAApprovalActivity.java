//package com.efounder.agency.activity;
//
//import java.util.List;
//import java.util.Map;
//
//import android.graphics.Color;
//import android.os.Bundle;
//import android.view.Gravity;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup.LayoutParams;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.efounder.ospmobilelib.R;
//import com.efounder.activity.AbActivity;
//import com.efounder.agency.utils.TaskDataParser;
//import com.efounder.builder.base.data.EFDataSet;
//import com.efounder.forwechat.BaseApp;
//import com.efounder.view.titlebar.AbTitleBar;
//import com.zcw.togglebutton.ToggleButton;
//import com.zcw.togglebutton.ToggleButton.OnToggleChanged;
//
///**
// * 公文审批的 审批页面
// *
// * @author cherise
// *
// */
//public class OAApprovalActivity extends AbActivity implements OnClickListener {
//
//	private static final String TAG = "AboutActivity";
//	private LinearLayout oaLayout;
//	AbTitleBar mTitleBar;
//	/**
//	 * 传阅 的按钮
//	 */
//	RelativeLayout cyreLayout;
//	LinearLayout hidecyLayout;
//	ToggleButton chuanyueButton;
//	LinearLayout dynamicLayout;
//	ImageView cyArrowImageView;
//	ImageView cyUserImgView;
//	/**
//	 * 环节内提交 的按钮
//	 */
//	RelativeLayout hjntjreLayout;
//	LinearLayout hidehjntjLayout;
//	ToggleButton hjntjButton;
//	LinearLayout hjntjDynamicLayout;
//	ImageView hjntjArrowImageView;
//	ImageView hjntjUserImgView;
//	ImageView hjntjAddView;
//	/**
//	 * 转办 的按钮
//	 */
//	RelativeLayout zbreLayout;
//	LinearLayout hidezbLayout;
//	ToggleButton zbButton;
//	ToggleButton zbMessageButton;
//	LinearLayout zbDynamicLayout;
//	ImageView zbArrowImageView;
//	ImageView zbUserImgView;
//	ImageView zbAddView;
//	/**
//	 * common 的按钮
//	 */
//	RelativeLayout commonreLayout;
//	LinearLayout hidecomnonLayout;
//	ToggleButton commonButton;
//	ToggleButton commonMessageButton;
//	LinearLayout commonDynamicLayout;
//	ImageView commonArrowImageView;
//	ImageView commonUserImgView;
//	ImageView commonAddView;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		BaseApp.actManager.putActivity(TAG, this);
//
//		setAbContentView(R.layout.oa_approval_act);
//
//		initView();
//
//		initData();
//
//	}
//
//	private void initView() {
//
//		mTitleBar = this.getTitleBar();
//		mTitleBar.setTitleText("公文审批");
//		mTitleBar.setTitleTextColor(Color.WHITE);
//		mTitleBar.setLogo(R.drawable.ef_title_view_back);
//		mTitleBar.setTitleBarBackground(R.color.red_ios);
//
//		Button button = new Button(this);
//
//		button.setWidth(LayoutParams.WRAP_CONTENT);
//		button.setHeight(LayoutParams.WRAP_CONTENT);
//		mTitleBar.getRightLayout().addView(button);
//		// mTitleBar.addRightView(button);
//
//		mTitleBar.setTitleBarGravity(Gravity.CENTER, Gravity.CENTER);
//		// mTitleBar.setTitleTextMargin(20, 0, 0, 0);
//		mTitleBar.setLogoOnClickListener(this);
//
//		oaLayout = (LinearLayout) findViewById(R.id.oa_approval_lay);
//		addChuanyueView();
//		addhjntjView();
//
//		addzbView();
//	}
//
//	private void initData(){
//		TaskDataParser taskDataParser = new TaskDataParser();
//		EFDataSet rootDataSet = (EFDataSet) getIntent().getSerializableExtra("rootDataSet");
//		List<Map<String, Object>> operationData = taskDataParser.parseOperation(rootDataSet);
//	}
//
//
//	@Override
//	public void onClick(View v) {
//
//		int id = v.getId();
//		if (v == mTitleBar.getLogoView()) {
//			BaseApp.actManager.removeActivity(TAG);
//
//		} else if (id == R.id.selected_chuanyuan_people_lay) {// 传阅
//			hideOrShow(dynamicLayout, cyArrowImageView);
//
//		} else if (id == R.id.hjntj_relalay) {// 环节内提交那一行
//			if (hidehjntjLayout.getVisibility() == View.VISIBLE) {
//				hidehjntjLayout.setVisibility(View.GONE);
//				hjntjButton.toggleOff();
//			} else {
//				hidehjntjLayout.setVisibility(View.VISIBLE);
//				hjntjButton.toggleOn();
//			}
//		} else if (id == R.id.chuanyue_relalay) {// 传阅那一行
//			if (hidecyLayout.getVisibility() == View.VISIBLE) {
//				hidecyLayout.setVisibility(View.GONE);
//				chuanyueButton.toggleOff();
//			} else {
//				hidecyLayout.setVisibility(View.VISIBLE);
//				chuanyueButton.toggleOn();
//			}
//		} else if (id == R.id.hjntj_xyhjclr_lay) {// 环节内提交
//			hideOrShow(hjntjDynamicLayout, hjntjArrowImageView);
//		} else if (id == R.id.zhuanban_xyhjclr) {// 转办
//			hideOrShow(zbDynamicLayout, zbArrowImageView);
//		} else if (id == R.id.hjntj_add) {// 环节内提交 审批意见 add按钮
//			Toast.makeText(OAApprovalActivity.this, "点击add按钮", Toast.LENGTH_SHORT)
//					.show();
//		} else if (id == R.id.zb_add) {// 转办审批意见 add按钮
//			Toast.makeText(OAApprovalActivity.this, "点击转办add按钮", Toast.LENGTH_SHORT)
//					.show();
//		}
//
//		else if (id == R.id.chuanyue_selectpeople) {// 传阅头像
//			Toast.makeText(OAApprovalActivity.this, "点击传阅头像", Toast.LENGTH_SHORT)
//					.show();
//		}
//
//		else if (id == R.id.zhuanban_select_user_img) {// 转办头像
//			Toast.makeText(OAApprovalActivity.this, "点击转办头像", Toast.LENGTH_SHORT)
//					.show();
//		}
//
//		else if (id == R.id.zhuanban_relalay) {// 专版那一行
//			if (hidezbLayout.getVisibility() == View.VISIBLE) {
//				hidezbLayout.setVisibility(View.GONE);
//				zbButton.toggleOff();
//			} else {
//				hidezbLayout.setVisibility(View.VISIBLE);
//				zbButton.toggleOn();
//			}
//		}
//	}
//
//	/**
//	 * 得到公用的文书管理 组件
//	 */
//	private View addcommomView(String title) {
//		View  commonView = getLayoutInflater().inflate(R.layout.oa_wenshubanli_layout,
//				null);
//		commonreLayout = (RelativeLayout) commonView
//				.findViewById(R.id.common_relalay);
//		TextView titleTextView = (TextView) commonView.findViewById(R.id.oa_parent_title);
//		titleTextView.setText(title);
//		commonreLayout.setOnClickListener(this);
//		commonButton = (ToggleButton) commonView.findViewById(R.id.oa_commomoggbut);
//
//		hidecomnonLayout = (LinearLayout) commonView.findViewById(R.id.hide_common_lay);
//		LinearLayout selectcommonLayout = (LinearLayout) commonView
//				.findViewById(R.id.common_xyhjclr);// 下一环节处理人
//		selectcommonLayout.setOnClickListener(this);
//		commonDynamicLayout = (LinearLayout) commonView
//				.findViewById(R.id.dynamic_common_lay1);
//		commonArrowImageView = (ImageView) commonView.findViewById(R.id.commom_arrow);
//		commonUserImgView = (ImageView) commonView
//				.findViewById(R.id.common_select_user_img);// 传阅选择人头像
//		commonUserImgView.setOnClickListener(this);
//		commonAddView = (ImageView) commonView.findViewById(R.id.common_add);
//		commonAddView.setOnClickListener(this);
//
//		commonButton.toggleOff();
//		commonButton.setOnToggleChanged(new OnToggleChanged() {
//
//			@Override
//			public void onToggle(boolean on) {
//				if (on) {
//					hidecomnonLayout.setVisibility(View.VISIBLE);
//				} else {
//					hidecomnonLayout.setVisibility(View.GONE);
//				}
//
//			}
//		});
//		//oaLayout.addView(commonView);
//		return commonView;
//
//	}
//
//
//	/**
//	 * 得到转办的 组件
//	 */
//	private void addzbView() {
//		View zbView = getLayoutInflater().inflate(R.layout.oa_zhuanban_layout,
//				null);
//		zbreLayout = (RelativeLayout) zbView.findViewById(R.id.zhuanban_relalay);
//		zbreLayout.setOnClickListener(this);
//		zbButton = (ToggleButton) zbView.findViewById(R.id.oa_zbtoggbut);
//		zbMessageButton = (ToggleButton) zbView
//				.findViewById(R.id.message_notice_togglebutton);
//		hidezbLayout = (LinearLayout) zbView.findViewById(R.id.hide_zb_lay);
//		LinearLayout selectzbLayout = (LinearLayout) zbView
//				.findViewById(R.id.zhuanban_xyhjclr);// 选择穿越人员lay
//		selectzbLayout.setOnClickListener(this);
//		zbDynamicLayout = (LinearLayout) zbView
//				.findViewById(R.id.dynamic_zhuanban_lay1);
//		zbArrowImageView = (ImageView) zbView.findViewById(R.id.zhuanban_arrow);
//		zbUserImgView = (ImageView) zbView
//				.findViewById(R.id.zhuanban_select_user_img);// 传阅选择人头像
//		zbUserImgView.setOnClickListener(this);
//		zbAddView = (ImageView) zbView.findViewById(R.id.zb_add);
//		zbAddView.setOnClickListener(this);
//
//		zbButton.toggleOff();
//		zbButton.setOnToggleChanged(new OnToggleChanged() {
//
//			@Override
//			public void onToggle(boolean on) {
//				if (on) {
//					hidezbLayout.setVisibility(View.VISIBLE);
//				} else {
//					hidezbLayout.setVisibility(View.GONE);
//				}
//
//			}
//		});
//		oaLayout.addView(zbView);
//
//	}
//
//	/**
//	 * 得到传阅的 组件
//	 */
//	private void addChuanyueView() {
//		View chuanyueView = getLayoutInflater().inflate(
//				R.layout.oa_chuanyue_layout, null);
//		cyreLayout = (RelativeLayout) chuanyueView
//				.findViewById(R.id.chuanyue_relalay);
//		cyreLayout.setOnClickListener(this);
//		chuanyueButton = (ToggleButton) chuanyueView
//				.findViewById(R.id.oa_checkbutton1);
//		hidecyLayout = (LinearLayout) chuanyueView
//				.findViewById(R.id.hide_chuanyue);
//		LinearLayout selectCyLayout = (LinearLayout) chuanyueView
//				.findViewById(R.id.selected_chuanyuan_people_lay);// 选择穿越人员lay
//		selectCyLayout.setOnClickListener(this);
//		dynamicLayout = (LinearLayout) chuanyueView
//				.findViewById(R.id._dynamic_chuanyue_lay);
//		cyArrowImageView = (ImageView) chuanyueView
//				.findViewById(R.id.chuanyue_arrow);
//		cyUserImgView = (ImageView) chuanyueView
//				.findViewById(R.id.chuanyue_selectpeople);// 传阅选择人头像
//		cyUserImgView.setOnClickListener(this);
//		chuanyueButton.toggleOff();
//		chuanyueButton.setOnToggleChanged(new OnToggleChanged() {
//
//			@Override
//			public void onToggle(boolean on) {
//				if (on) {
//					hidecyLayout.setVisibility(View.VISIBLE);
//				} else {
//					hidecyLayout.setVisibility(View.GONE);
//				}
//
//			}
//		});
//		oaLayout.addView(chuanyueView);
//
//	}
//
//	/**
//	 * 得到环节内提交的 组件
//	 */
//	private void addhjntjView() {
//		View hjntjView = getLayoutInflater().inflate(R.layout.oa_hjntj_layout,
//				null);
//		hjntjButton = (ToggleButton) hjntjView
//				.findViewById(R.id.oa_checkbutton2);
//		hjntjreLayout = (RelativeLayout) hjntjView
//				.findViewById(R.id.hjntj_relalay);
//		hjntjreLayout.setOnClickListener(this);
//		hidehjntjLayout = (LinearLayout) hjntjView
//				.findViewById(R.id.hjntj_hide_lay);
//		LinearLayout selecthjntjLayout = (LinearLayout) hjntjView
//				.findViewById(R.id.hjntj_xyhjclr_lay);// 选择穿越人员lay
//		selecthjntjLayout.setOnClickListener(this);
//		hjntjDynamicLayout = (LinearLayout) hjntjView
//				.findViewById(R.id.dynamic_hjntj_lay1);
//		hjntjArrowImageView = (ImageView) hjntjView
//				.findViewById(R.id.hjntj_arrow);
//		hjntjUserImgView = (ImageView) hjntjView
//				.findViewById(R.id.hjntj_select_user_img);// 传阅选择人头像
//		hjntjAddView = (ImageView) hjntjView.findViewById(R.id.hjntj_add);
//		hjntjAddView.setOnClickListener(this);
//
//		hjntjButton.toggleOff();
//		hjntjButton.setOnToggleChanged(new OnToggleChanged() {
//
//			@Override
//			public void onToggle(boolean on) {
//				if (on) {
//					hidehjntjLayout.setVisibility(View.VISIBLE);
//				} else {
//					hidehjntjLayout.setVisibility(View.GONE);
//				}
//
//			}
//		});
//		oaLayout.addView(hjntjView);
//
//	}
//
//	/**
//	 * 隐藏或者显示 更改箭头方向
//	 *
//	 * @param dynamicLayout2
//	 * @param cyArrowImageView2
//	 */
//	private void hideOrShow(LinearLayout dynamicLayout2,
//			ImageView cyArrowImageView2) {
//		if (dynamicLayout2.getVisibility() == View.GONE) {
//			dynamicLayout2.setVisibility(View.VISIBLE);
//			cyArrowImageView2.setImageDrawable(this.getResources().getDrawable(
//					R.drawable.oa_arrow_top_blue));
//		} else {
//			dynamicLayout2.setVisibility(View.GONE);
//			cyArrowImageView2.setImageDrawable(this.getResources().getDrawable(
//					R.drawable.oa_arrow_down_blue));
//		}
//
//	}
//
//}
