//package com.efounder.agency.activity;
//
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentActivity;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentTransaction;
//import android.view.Gravity;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.Button;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.efounder.agency.utils.TokenHelper;
//import com.efounder.frame.baseui.BaseFragment;
//import com.efounder.model.AgentTaskModel;
//import com.efounder.ospmobilelib.R;
//import com.efounder.util.StorageUtil;
//import com.jauker.widget.BadgeView;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.concurrent.Executors;
//
///**
// * 包含OA，合同，费用报销的的fragment的activity
// *
// * @author yqs
// */
//public class TaskFragmentContainActivity extends FragmentActivity implements
//        OnClickListener {
//
//    public static final String TAG = "TaskFragmentContainActivity";
//    BaseFragment baseFragment = null;
//    private FragmentManager fm;
//    private RelativeLayout includeLayout;
//    private AgentTaskModel model;
//    private String dbCount, dyCount;
//    private Button textView1, textView2, textView3, textView4;
//    private int currentIndex = 1;// 当前所在的fragment
//    private BadgeView daibanView, daiyueView;
//    private CountAsyncTask countAsyncTask;
//    private StorageUtil storageUtil;
//
//    protected void onCreate(Bundle savedInstanceState) {
//
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.agentfragment_contains);
//
//        StorageUtil storageUtil1 = new StorageUtil(this, "storage");
//        String systemUserName = storageUtil1.getString("userName");
//        storageUtil = new StorageUtil(this, systemUserName);
//
//        model = (AgentTaskModel) getIntent().getSerializableExtra("model");
//        daibanView = new BadgeView(this);
//        daiyueView = new BadgeView(this);
//
//        includeLayout = (RelativeLayout) findViewById(R.id.include);
//
//        TextView titleTextView = (TextView) this
//                .findViewById(R.id.fragmenttitle);
//        if ("CONTRACT".equals(model.getType())) {
//            titleTextView.setText("合同审批");
//
//        } else if ("FFS".equals(model.getType())) {
//            titleTextView.setText("费用报销");
//        } else {
//            titleTextView.setText("公文审批");
//        }
//
//        textView1 = (Button) findViewById(R.id.textview1);
//        textView2 = (Button) findViewById(R.id.textview2);
//        textView3 = (Button) findViewById(R.id.textview3);
//        textView4 = (Button) findViewById(R.id.textview4);
//        textView1.setOnClickListener(this);
//        textView2.setOnClickListener(this);
//        textView3.setOnClickListener(this);
//        textView4.setOnClickListener(this);
//        if (model.getType().equals("CONTRACT")) {// 合同没有代悦已阅
//            textView3.setVisibility(View.INVISIBLE);
//            textView4.setVisibility(View.INVISIBLE);
//
//        }
//        if (model.getType().equals("FFS")) {// 费用报销隐藏最后一个按钮
//            // textView2.setText("保留");
//            // textView3.setText("已办");
//            textView3.setVisibility(View.INVISIBLE);
//            textView4.setVisibility(View.INVISIBLE);
//
//        }
//
//        daibanView.setBadgeGravity(Gravity.TOP | Gravity.RIGHT);
//        daibanView.setTextSize(14);
//        daibanView.setTargetView(textView1);
//        dbCount = String.valueOf(model.getDbCount());
//        daibanView.setBadgeCount(model.getDbCount());
//
//        if (!model.getType().equals("CONTRACT")
//                && !model.getType().equals("FFS")) {
//
//            daiyueView.setBadgeGravity(Gravity.TOP | Gravity.RIGHT);
//            daiyueView.setTargetView(textView3);
//            daiyueView.setTextSize(14);
//            dyCount = String.valueOf(model.getDyCount());
//            daiyueView.setBadgeCount(model.getDyCount());
//
//        }
//
//        if (model.getDbCount() != 0) {
//            daibanView.setVisibility(View.VISIBLE);
//        }
//        if (model.getDyCount() != 0) {
//            daiyueView.setVisibility(View.VISIBLE);
//        }
//
//        LinearLayout leftbacklayout = (LinearLayout) this
//                .findViewById(R.id.leftbacklayout);
//        leftbacklayout.setVisibility(View.VISIBLE);
//        leftbacklayout.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//
//        showFragemt("db");
//
//    }
//
//    public void jumpAndStoreStack(Fragment frag) {
//        if (frag == null) {
//            return;
//        }
//        FragmentTransaction trasaction = fm.beginTransaction();
//        trasaction.add(R.id.fragment_contains, frag);
//        trasaction.addToBackStack("a");
//        trasaction.commit();
//    }
//
//    @Override
//    protected void onResume() {
//
//        super.onResume();
//        if (countAsyncTask != null) {
//            countAsyncTask.cancel(true);
//        }
//        countAsyncTask = new CountAsyncTask();
//        countAsyncTask.executeOnExecutor(Executors.newCachedThreadPool());
//
//    }
//
//    @Override
//    protected void onStop() {
//
//        super.onStop();
//        if (countAsyncTask != null) {
//            countAsyncTask.cancel(true);
//        }
//    }
//
//    @Override
//    public void onClick(View v) {
//        int id = v.getId();
//
//        if (id == R.id.textview1) {// 待办
//            if (currentIndex == 1) {
//                return;
//            }
//            currentIndex = 1;
//            changeButtonShape();
//            textView1.setBackgroundDrawable(getResources().getDrawable(
//                    R.drawable.button_shape_noselect));
//            showFragemt("db");
//        } else if (id == R.id.textview2) {// 已办或者保留
//            if (currentIndex == 2) {
//                return;
//            }
//            currentIndex = 2;
//            changeButtonShape();
//            textView2.setBackgroundDrawable(getResources().getDrawable(
//                    R.drawable.button_shape_noselect));
//            // if (model.getType().equals("FFS")) {
//            // showFragemt("bl");
//            // } else {
//            showFragemt("yb");
//            // }
//
//        } else if (id == R.id.textview3) {// 待阅或者已办
//            if (currentIndex == 3) {
//                return;
//            }
//            currentIndex = 3;
//            changeButtonShape();
//            textView3.setBackgroundDrawable(getResources().getDrawable(
//                    R.drawable.button_shape_noselect));
//            if (model.getType().equals("FFS")) {
//                showFragemt("yb");
//            } else {
//                showFragemt("dy");
//            }
//
//        } else if (id == R.id.textview4) {// 已阅
//            if (currentIndex == 4) {
//                return;
//            }
//            currentIndex = 4;
//            changeButtonShape();
//            textView4.setBackgroundDrawable(getResources().getDrawable(
//                    R.drawable.button_shape_noselect));
//            showFragemt("yy");
//        }
//
//    }
//
//    private void showFragemt(String type) {
//        baseFragment = new TaskListDataFragment("", type);
//        fm = getSupportFragmentManager();
//        if (!baseFragment.isAlone()) {
//            includeLayout.setVisibility(View.GONE);
//        }
//        FragmentTransaction fragmentTransaction = fm.beginTransaction();
//        if (baseFragment != null) {
//            fragmentTransaction.replace(R.id.fragment_contains, baseFragment);
//        }
//        fragmentTransaction.commit();
//
//    }
//
//    public void changeButtonShape() {
//        textView1.setBackgroundDrawable(getResources().getDrawable(
//                R.drawable.button_shape));
//        textView2.setBackgroundDrawable(getResources().getDrawable(
//                R.drawable.button_shape));
//        textView3.setBackgroundDrawable(getResources().getDrawable(
//                R.drawable.button_shape));
//        textView4.setBackgroundDrawable(getResources().getDrawable(
//                R.drawable.button_shape));
//
//    }
//
//    /**
//     * 加载shuliang的线程
//     */
//    public class CountAsyncTask extends AsyncTask<Void, Void, Void> {
//
//        @Override
//        protected Void doInBackground(Void... params) {
//
//            List<AgentTaskModel> list = new ArrayList<AgentTaskModel>();
//            list.add(model);
//            TokenHelper.getTokenAndUnreadCount(list,
//                    TaskFragmentContainActivity.this);
//
//
//            //OldTokenHelper.getOneUnreadCount(TaskFragmentContainActivity.this, model);
//
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void result) {
//
//            super.onPostExecute(result);
//            if (model.getDbCount() != 0 && daibanView != null) {
//                daibanView.setBadgeGravity(Gravity.TOP | Gravity.RIGHT);
//                daibanView.setTextSize(14);
//                dbCount = String.valueOf(model.getDbCount());
//                daibanView.setBadgeCount(model.getDbCount());
//                daibanView.setVisibility(View.VISIBLE);
//                System.out.println(String.valueOf(daibanView.getBadgeCount()));
//
//            } else if (daibanView != null) {
//                daibanView.setVisibility(View.GONE);
//            }
//
//            if (!model.getType().equals("CONTRACT") && model.getDyCount() != 0) {
//                daiyueView.setBadgeGravity(Gravity.TOP | Gravity.RIGHT);
//                daiyueView.setTextSize(14);
//                dyCount = String.valueOf(model.getDyCount());
//                daiyueView.setBadgeCount(model.getDyCount());
//                daiyueView.setVisibility(View.VISIBLE);
//            } else if (daiyueView != null) {
//                daiyueView.setVisibility(View.GONE);
//            }
//
//        }
//
//        @Override
//        protected void onCancelled() {
//            super.onCancelled();
//
//        }
//
//    }
//
//
//}
