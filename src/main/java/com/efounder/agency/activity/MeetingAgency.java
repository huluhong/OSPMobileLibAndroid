//package com.efounder.agency.activity;
//
//import android.annotation.SuppressLint;
//import android.content.Intent;
//import android.graphics.Color;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.ExpandableListView;
//import android.widget.LinearLayout;
//import android.widget.ListAdapter;
//import android.widget.RelativeLayout;
//import android.widget.ScrollView;
//import android.widget.TextView;
//
//import com.efounder.ospmobilelib.R;
//import com.efounder.activity.AgentDetailYTBAct;
//import com.efounder.adapter.MyExpandListviewAdapterNew;
//import com.efounder.builder.base.data.EFDataSet;
//import com.efounder.builder.base.data.EFRowSet;
//import com.efounder.eai.EAI;
//import com.efounder.eai.data.JParamObject;
//import com.efounder.eai.data.JResponseObject;
//import com.efounder.frame.baseui.BaseFragment;
//import com.efounder.util.CommonPo;
//import com.efounder.util.LoadingDataUtilBlack;
//import com.efounder.util.ToastUtil;
//import com.efounder.widget.MyExpandlistView;
//import com.handmark.pulltorefresh.library.PullToRefreshBase;
//import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
//import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
//import com.pansoft.espflow.util.FlowTaskUtil;
//import com.pansoft.espflow.util.FlowYTBTaskUtil;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.Executors;
//
///**
// * 会议待办 2016.11.22 整理代码
// */
//public class MeetingAgency extends BaseFragment implements
//        OnRefreshListener<ScrollView> {
//
//    String titleStr;
//    private static final int UPDATED = 1;
//    private MyExpandlistView mExpandlistView;
//    private MyExpandListviewAdapterNew mNewAdapter;
//    // private MyExpandListviewAdapterSlide mNewAdapter;
//    private boolean hasLoadData = false;
//    private NewLoadAsyncTask loadAsyncTask;
//    private String systemType="";//系统类型：风险管控，硬件维修
//    RelativeLayout include;
//    LinearLayout linearLayout;
//    private PullToRefreshScrollView pullToRefreshScrollView;
//
//    public MeetingAgency(String str) {
//        super();
//        this.titleStr = str;
//    }
//
//    public MeetingAgency() {
//        super();
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        if (isAlone() && !hasLoadData) {
//            if (loadAsyncTask != null) {
//                loadAsyncTask.cancel(true);
//            }
//            loadAsyncTask = new NewLoadAsyncTask();
//            loadAsyncTask.executeOnExecutor(Executors.newCachedThreadPool());
//
//        }
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        View root = inflater.inflate(R.layout.fg_new_agent_ui_withtilte, null);
//
//        pullToRefreshScrollView = (PullToRefreshScrollView) root
//                .findViewById(R.id.pullToRefreshScrollView);
//        pullToRefreshScrollView.setOnRefreshListener(this);
//        include = (RelativeLayout) root.findViewById(R.id.include);
//        TextView titleTextView = (TextView) root.findViewById(R.id.fragmenttitle);
//        titleTextView.setText(titleStr);
//        // 判断fragment是否是单独的
//        if (isAlone()) {
//            include.setVisibility(View.GONE);
//        } else {
//            include.setVisibility(View.VISIBLE);
//        }
//
////        include.setBackgroundResource(R.color.red);
//        TextView title = (TextView) root.findViewById(R.id.fragmenttitle);
//        title.setText("任务管理");
//        title.setTextColor(Color.WHITE);
//        LinearLayout leftbacklayout = (LinearLayout) root
//                .findViewById(R.id.leftbacklayout);
//        leftbacklayout.setVisibility(View.VISIBLE);
//
//        leftbacklayout.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                MeetingAgency.this.getFragmentManager().popBackStack();
//            }
//        });
//
//        Button buttonRight = (Button) root.findViewById(R.id.closeButton);
//        buttonRight.setVisibility(View.INVISIBLE);
//        buttonRight.setBackgroundResource(R.drawable.rightmenu);
//        buttonRight.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
//        linearLayout = (LinearLayout) root
//                .findViewById(R.id.fg_new_agent_linwarlayout);
//
//        mExpandlistView = (MyExpandlistView) root
//                .findViewById(R.id.expandlistview);
//        mExpandlistView.setGroupIndicator(null);
//        mExpandlistView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
//            @Override
//            public boolean onChildClick(ExpandableListView parent, View v,
//                                        int groupPosition, int childPosition, long id) {
//                EFRowSet child = (EFRowSet) mNewAdapter.getChild(groupPosition,
//                        childPosition);
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("data", child);
//                Intent intent = new Intent(getActivity(), AgentDetailYTBAct.class);
//                intent.putExtra("dataSource", bundle);
//                getActivity().startActivity(intent);
//                return false;
//            }
//        });
//        super.onCreateView(inflater, container, savedInstanceState);
//        return root;
//
//    }
//
//    // ----------------------------
//    class NewLoadAsyncTask extends AsyncTask<Void, Integer, EFDataSet> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            // 显示加载进度对话框
//            if (pullToRefreshScrollView != null
//                    && pullToRefreshScrollView.isRefreshing()) {
//            } else {
//                LoadingDataUtilBlack.show(MeetingAgency.this.getActivity());
//            }
//        }
//
//        @Override
//        protected EFDataSet doInBackground(Void... params) {
//            Map<String, List<Map<String, String>>> map = null;
//            EFDataSet flowTaskDataSet = null;
//            try {
//                // 在这里添加调用接口获取数据的代码
//                flowTaskDataSet = getYouTianBuNetTaskDate();
//                // getTest();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return flowTaskDataSet;
//        }
//
//        @SuppressWarnings("deprecation")
//        @Override
//        protected void onPostExecute(EFDataSet result) {
//            if (result != null) {
//                linearLayout.setBackgroundDrawable(getResources()
//                        .getDrawable(R.drawable.write));
//                Map<String, List<EFRowSet>> map = FlowTaskUtil
//                        .openTaskFromEFDataSetToMap(result);
//                if (map != null) {
//                    Message msg = new Message();
//                    msg.obj = map;
//                    msg.what = UPDATED;
//                    myHandler.sendMessage(msg);
//
//                } else {
//                    Message msg = new Message();
//                    Map<String, List<EFRowSet>> map1 = new HashMap<String, List<EFRowSet>>();
//                    msg.obj = map1;
//                    msg.what = UPDATED;
//                    myHandler.sendMessage(msg);
//                    ToastUtil.showToast(getActivity(), "未获取到数据");
//                    hasLoadData = false;
//                    linearLayout.setBackgroundDrawable(getResources()
//                            .getDrawable(R.drawable.renwuno));
//                }
//            } else {
//                Message msg = new Message();
//                Map<String, List<EFRowSet>> map = new HashMap<String, List<EFRowSet>>();
//                msg.obj = map;
//                msg.what = UPDATED;
//                myHandler.sendMessage(msg);
//                ToastUtil.showToast(getActivity(), "未获取到数据");
//                linearLayout.setBackgroundDrawable(getResources().getDrawable(
//                        R.drawable.renwuno));
//            }
//
//            // 关闭对话框
//            LoadingDataUtilBlack.dismiss();
//            if (pullToRefreshScrollView != null
//                    && pullToRefreshScrollView.isRefreshing()) {
//                pullToRefreshScrollView.onRefreshComplete();
//            }
//        }
//    }
//
//    Handler myHandler = new Handler() {
//        public void handleMessage(Message msg) {
//            switch (msg.what) {
//                case UPDATED:
//                /*
//                 * mNewAdapter = new MyExpandListviewAdapterNew(
//				 * AgentFgNew.this.getActivity(), (Map<String, List<EFRowSet>>)
//				 * msg.obj);
//				 */
//                    mNewAdapter = new MyExpandListviewAdapterNew(
//                            MeetingAgency.this.getActivity(),
//                            (Map<String, List<EFRowSet>>) msg.obj);
//
//                    mExpandlistView.setAdapter(mNewAdapter);
//                    //setListViewHeight(mExpandlistView);
//
//
//                    // 设置expendlistview默认展开（下四行代码）
//                    int groupCount = mExpandlistView.getCount();
//                    for (int i = 0; i < groupCount; i++) {
//                        mExpandlistView.expandGroup(i);
//                    }
//                    setListViewHeight(mExpandlistView);
//                    hasLoadData = false;
//                    break;
//            }
//            super.handleMessage(msg);
//        }
//    };
//
//    private EFDataSet getNetTaskDate() {
//        // 创建PO
//        EFDataSet flowTaskDataSet = null;
//        JParamObject PO = JParamObject.Create();
//        Log.i("Popopoppopop", PO.getEnvValue("DataBaseName", "") + "....");
//        // PO.SetValueByParamName("MDL_ID", "HY_HYSQModel");
//        // PO.SetValueByParamName("HY_HYSQModel", "MDL_ID");
//        PO.SetValueByParamName("QXBZW", "2");
//        PO.SetValueByParamName("SELF_SUBMIT_TASK", "1");
//        // PO.SetValueByParamName("MDL_UNIT_DCTIDS", "BZZZJGZD");
//        PO.SetValueByParamName("MDL_UNIT_DCTIDS", "YHZZJG");
//        PO.SetValueByParamName("_CHILD_FLOW_DISP_TYPE_", "1");
//        PO.SetValueByParamName("taskDataSetID", "TASKPendingDataSet");
//        // PO.SetValueByParamName("TASK_TYPE", "pending");
//        try {
//            Log.i("xxxxxxxxxxx", EAI.Path);
//            flowTaskDataSet = FlowTaskUtil.getFlowTaskData(PO, "pending");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        CommonPo.setPoToNull();
//        return flowTaskDataSet;
//    }
//
//    // ToDo 油田部获取待办数据
//    private EFDataSet getYouTianBuNetTaskDate() {
//
////        EAI.Protocol = "http";
////        EAI.Server = "192.168.0.109";
////        EAI.Port = "8080";
////        EAI.Path = "ESTMobile";
////        EAI.Service = "Android";
//        // 创建PO
//        EFDataSet flowTaskDataSet = null;
//        JParamObject PO = JParamObject.Create();
//        JResponseObject RO = null;
//        String userName = (String) PO.getEnvValue("UserName", "");
//        userName = userName.replace(".zyyt", "");
//        System.out.println("风险管控打印用户名:" + userName);
//        PO.SetValueByEnvName("UserName", userName);
//
//        PO.SetValueByParamName("QXBZW", "2");
//        PO.SetValueByParamName("SELF_SUBMIT_TASK", "1");
//        PO.SetValueByParamName("MDL_UNIT_DCTIDS", "YHZZJG");
//        PO.SetValueByParamName("_CHILD_FLOW_DISP_TYPE_", "1");
//        PO.SetValueByParamName("taskDataSetID", "TASKPendingDataSet");
//        PO.SetValueByParamName("TASK_TYPE", "pending");
//
//
//        try {
//            flowTaskDataSet = FlowYTBTaskUtil.getFlowTaskData(PO, "pending",systemType);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        CommonPo.setPoToNull();
//        return flowTaskDataSet;
//    }
//
//    @SuppressLint("NewApi")
//    @Override
//    public void onVisible() {
//        if (!hasLoadData) {
//            //  linearLayout.setBackground(getResources().getDrawable(R.drawable.renwuno));
//            linearLayout.setBackground(getResources().getDrawable(
//                    R.drawable.write));
//
//        }
//
//    }
//
//    /**
//     * 下拉刷新
//     */
//    @Override
//    public void onRefresh(PullToRefreshBase<ScrollView> refreshView) {
//        if (loadAsyncTask != null) {
//            loadAsyncTask.cancel(true);
//        }
//        loadAsyncTask = new NewLoadAsyncTask();
//        loadAsyncTask.executeOnExecutor(Executors.newCachedThreadPool());
//    }
//
//    private void setListViewHeight(ExpandableListView listView) {
//        ListAdapter listAdapter = listView.getAdapter();
//        int totalHeight = 0;
//        int count = listAdapter.getCount();
//        for (int i = 0; i < listAdapter.getCount(); i++) {
//            View listItem = listAdapter.getView(i, null, listView);
//            listItem.measure(0, 0);
//            totalHeight += listItem.getMeasuredHeight();
//        }
//
//        ViewGroup.LayoutParams params = listView.getLayoutParams();
//        params.height = totalHeight
//                + (listView.getDividerHeight() * (listAdapter.getCount() - 1)) + 100;
//        listView.setLayoutParams(params);
//        listView.requestLayout();
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        if (loadAsyncTask != null) {
//            loadAsyncTask.cancel(true);
//        }
//    }
//}
