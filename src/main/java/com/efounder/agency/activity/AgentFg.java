package com.efounder.agency.activity;

/**
 * zjl 2015年3月12日11:28:46
 * 代办主页
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;

import com.core.xml.StubObject;
import com.efounder.ospmobilelib.R;
import com.efounder.activity.AgentDetailAct;
import com.efounder.activity.TabBottomActivity;
import com.efounder.adapter.AgentExpandableListAdapter;
import com.efounder.builder.base.data.EFDataSet;
import com.efounder.builder.base.data.EFRowSet;
import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;
import com.efounder.util.CommonPo;
import com.efounder.util.LoadingDataUtilBlack;
import com.efounder.util.ToastUtil;
import com.efounder.view.titlebar.AbTitleBar;
import com.efounder.widget.AbPullToRefreshView;
import com.efounder.widget.AbPullToRefreshView.OnFooterLoadListener;
import com.efounder.widget.AbPullToRefreshView.OnHeaderRefreshListener;
import com.efounder.widget.MyExpandableListView;
import com.pansoft.espflow.util.FlowTaskUtil;

public class AgentFg extends Fragment implements OnHeaderRefreshListener,OnFooterLoadListener{
	private static final String TAG = "AgentFg";
	private static final int UPDATED = 1;
	private JParamObject PO;
	private JResponseObject RO;
	
	AbTitleBar title;
	String titleStr;
	private StubObject mParentMenuItem;
	private ArrayList<Object> mMenuItems;
//	----------------------------------
	private EFDataSet flowTaskDataSet;
	
	/** 变量声明区 */
	private View view;
	private AbPullToRefreshView expandableViewContainer;
	private MyExpandableListView expandableListView;
	private AgentExpandableListAdapter adapter;
	
	public AgentFg(StubObject parentMenuItem, ArrayList<Object> menuItems,
			String titleStr) {
		setAttachedData(parentMenuItem, menuItems,titleStr);
	}
	public AgentFg(){
		super();
	}
	@Override
	public void onStart() {
		title = ((TabBottomActivity)getActivity()).getTitleBar();
		title.setTitleText(titleStr);
		super.onStart();
	}
	public void setAttachedData(StubObject parentMenuItem, ArrayList<Object> menuItems,
			String titleStr) {
		this.mParentMenuItem = parentMenuItem;
		this.mMenuItems = menuItems;
		this.titleStr = titleStr;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// 载入总布局容器
		view = inflater.inflate(R.layout.agent, container, false);
		expandableViewContainer = (AbPullToRefreshView)view.findViewById(R.id.list);
		expandableListView = (MyExpandableListView) view.findViewById(R.id.expandablelist);
		expandableListView.setGroupIndicator(null);
		new NewLoadAsyncTask().execute();
		initListener();
        
		return view;
	}
	
	private void initListener(){
		expandableViewContainer.setOnHeaderRefreshListener(this);
		expandableViewContainer.setOnFooterLoadListener(this);
		expandableViewContainer.getHeaderView().setHeaderProgressBarDrawable(this.getResources().getDrawable(R.drawable.progress_circular));
		expandableViewContainer.getFooterView().setFooterProgressBarDrawable(this.getResources().getDrawable(R.drawable.progress_circular));

		expandableListView.setOnChildClickListener(new OnChildClickListener() {
			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				Log.i(TAG, "groupPosition="+groupPosition+",childPosition="+childPosition);
				EFRowSet child = (EFRowSet) adapter.getChild(groupPosition, childPosition);
				Bundle bundle = new Bundle();
				bundle.putSerializable("data", child);
				Intent intent = new Intent(getActivity(),AgentDetailAct.class);
				intent.putExtra("dataSource", bundle);
				getActivity().startActivity(intent);
				return false;
			}
		});
	}

	
//	---------------------------------------------------
	
	class NewLoadAsyncTask extends AsyncTask<Void, Integer, EFDataSet>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //显示加载进度对话框
            LoadingDataUtilBlack.show(AgentFg.this.getActivity());
        }

        @Override
        protected EFDataSet  doInBackground(Void... params) {
        	Map<String ,List<Map<String,String>>> map = null;
        	EFDataSet flowTaskDataSet = null;
            try {
                //在这里添加调用接口获取数据的代码
            	flowTaskDataSet =   getNetTaskDate();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return flowTaskDataSet;
        }

     

        @Override
        protected void onPostExecute(EFDataSet result) {
			if(result!=null){
				Log.i(TAG, "result.getRowCount():"+result.getRowCount());
				Map<String, List<EFRowSet>> map  = FlowTaskUtil.openTaskFromEFDataSetToMap(result);
				if(map!=null){
					Message msg = new Message();
					msg.obj = map;
					msg.what = UPDATED;
					myHandler.sendMessage(msg);
				}else{
					ToastUtil.showToast(getActivity(), "未获取到数据");
				}
    		 }
            //关闭对话框
            LoadingDataUtilBlack.dismiss();
        }
    
		
	}
	
	Handler myHandler = new Handler() {  
        public void handleMessage(Message msg) {   
             switch (msg.what) {
	             case UPDATED:
	            	adapter = new AgentExpandableListAdapter(AgentFg.this.getActivity(),(Map<String, List<EFRowSet>>)msg.obj);
	     			expandableListView.setAdapter(adapter);
	     			break;
             }   
             super.handleMessage(msg);   
        }   
   };
	private EFDataSet getNetTaskDate() {
		//创建PO	
    	JParamObject PO = CommonPo.getPo(this.getActivity());
    	Log.i("Popopoppopop",PO.getEnvValue("DataBaseName", "") +"....");
      //  PO.SetValueByParamName("MDL_ID", "HY_HYSQModel");
    	//PO.SetValueByParamName("HY_HYSQModel", "MDL_ID");
    	PO.SetValueByParamName("QXBZW", "2");
    	PO.SetValueByParamName("SELF_SUBMIT_TASK", "1");
    	//PO.SetValueByParamName("MDL_UNIT_DCTIDS", "BZZZJGZD");
    	PO.SetValueByParamName("MDL_UNIT_DCTIDS", "YHZZJG");
    	PO.SetValueByParamName("_CHILD_FLOW_DISP_TYPE_", "1");
    	PO.SetValueByParamName("taskDataSetID", "TASKPendingDataSet");
    	//PO.SetValueByParamName("TASK_TYPE", "pending");
    	try {
    		Log.i("xxxxxxxxxxx",EAI.Path);
    		flowTaskDataSet = FlowTaskUtil.getFlowTaskData(PO, "pending");
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return flowTaskDataSet;
	}
	@Override
	public void onHeaderRefresh(AbPullToRefreshView view) {
		// TODO Auto-generated method stub
		new NewLoadAsyncTask().execute();
		expandableViewContainer.onHeaderRefreshFinish();
	}
	@Override
	public void onFooterLoad(AbPullToRefreshView view) {
		// TODO Auto-generated method stub
		expandableViewContainer.onFooterLoadFinish();
	}
}
