//package com.efounder.agency.activity;
//
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.Color;
//import android.graphics.drawable.BitmapDrawable;
//import android.graphics.drawable.Drawable;
//import android.net.Uri;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.provider.MediaStore;
//import android.util.DisplayMetrics;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.Window;
//import android.view.inputmethod.EditorInfo;
//import android.widget.AdapterView;
//import android.widget.AdapterView.OnItemClickListener;
//import android.widget.AdapterView.OnItemSelectedListener;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.GridView;
//import android.widget.LinearLayout;
//import android.widget.RadioButton;
//import android.widget.RelativeLayout;
//import android.widget.Spinner;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.efounder.ospmobilelib.R;
//import com.efounder.adapter.LeaveFileGridAdapter;
//import com.efounder.builder.base.data.EFDataSet;
//import com.efounder.builder.base.data.EFRowSet;
//import com.efounder.eai.EAI;
//import com.efounder.eai.data.JParamObject;
//import com.efounder.eai.data.JResponseObject;
//import com.efounder.email.model.AttachFile;
//import com.efounder.email.util.AttaachDialog;
//import com.efounder.email.util.FileSizeUtil;
//import com.efounder.email.util.FileUtil;
//import com.efounder.forwechat.BaseApp;
//import com.efounder.util.AbStrUtil;
//import com.efounder.util.CommonPo;
//import com.efounder.util.LoadingDataUtilBlack;
//import com.efounder.util.StorageUtil;
//import com.efounder.util.ToastUtil;
//import com.pansoft.appcontext.AppConstant;
//import com.pansoft.espcomp.LeaveDateWindow;
//import com.pansoft.espcomp.LeaveDateWindow.DateOnclickListener;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.Executors;
//
///**
// * 发起请假，填写请假单activity
// *
// * @author yqs
// *
// */
//public class StartLeaveActivity extends Activity implements OnClickListener,
//		OnItemClickListener {
//	public static final String TAG = "StartLeaveActivity";
//	private static final int PHOTO_REQUEST_TAKEPHOTO = 1;// 拍照
//	final String fileDir = AppConstant.APP_ROOT + "/file";// 拍照文件的路径
//
//	private Spinner qjTypeSpinner;
//	private ArrayAdapter adapter;
//	private TextView tv_startTime, tv_endTime;// 开始时间结束时间
//	private TextView tv_nameView, tv_sexView, tv_gangweiView;// 姓名 性别 岗位
//	private TextView tv_ldhthView, tv_workUnitView, tv_workTimeView;// 劳动合同号，工作单位，参加工作时间
//	private RadioButton isMarryButton;// 是否已婚
//	private EditText et_destination;// 目的地
//	private String qjType;// 请假类型
//	private String qjTypeCode = "a01";// 请假类型的代码,a01默认为出差
//	private EditText et_qjTianShu;// 请假天数
//	private EditText et_qjReason;// 请假事由
//	private GridView gridView;// 显示附件
//	private LinearLayout attachLayout;// 添加附件的layout
//	private Button button;// 提交按钮
//	LeaveFileGridAdapter gridAdapter;
//	String imageName;// 拍照图片路径
//
//	private CreateLeaveBillsTask createLeaveBillsTask;// 创建请假单
//	private SubmitLeaveBillsTask submitLeaveBillsTask;// 提交请假单
//	private String LDHTHId;
//
//	private ArrayList<AttachFile> files;
//	List<Map> fileListMap;// 附件map
//	private static final String[] m = { "出差", "培训", "轮休", "事假", "病假", "工伤",
//			"婚假", "丧假", "产假", "计生假", "哺乳假", "探亲假", "带薪年休" };
//
//	Map<String, String> submitMap;// 提交请假单需要的数据
//
//	protected void onCreate(Bundle savedInstanceState) {
//
//		super.onCreate(savedInstanceState);
//		BaseApp.actManager.putActivity(TAG, this);
//		setContentView(R.layout.activity_startleave);
//		files = new ArrayList<AttachFile>();
//		submitMap = new HashMap<String, String>();
//		fileListMap = new ArrayList<Map>();
//		LDHTHId = getIntent().getStringExtra("LDHTHId");
//
//		initView();
//		if (createLeaveBillsTask != null) {
//			createLeaveBillsTask.cancel(true);
//		}
//		createLeaveBillsTask = new CreateLeaveBillsTask(LDHTHId);
//		createLeaveBillsTask.executeOnExecutor(Executors.newCachedThreadPool());
//
//	}
//
//	private void initData() {
//		tv_nameView.setText(submitMap.get("xm"));
//		tv_sexView.setText(submitMap.get("xb"));
//		tv_gangweiView.setText(submitMap.get("gw"));
//		tv_ldhthView.setText(submitMap.get("ldhth"));
//		tv_workUnitView.setText(submitMap.get("depname"));
//		tv_workTimeView.setText(submitMap.get("cjgzsj"));
//
//	}
//
//	@SuppressWarnings("unchecked")
//	private void initView() {
//		RelativeLayout include = (RelativeLayout) findViewById(R.id.include);
//		include.setBackgroundResource(R.color.chat_red);
//		LinearLayout leftbacklayout = (LinearLayout) findViewById(R.id.leftbacklayout);
//		leftbacklayout.setVisibility(View.VISIBLE);
//		TextView titleTextView = (TextView) findViewById(R.id.fragmenttitle);// 标题
//		titleTextView.setTextColor(Color.WHITE);
//		titleTextView.setText("请假单");
//
//		// 左上方返回按钮
//		leftbacklayout.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				BaseApp.actManager.removeActivity(TAG);
//			}
//		});
//
//		tv_nameView = (TextView) findViewById(R.id.ll1_tv_name);// 姓名
//		tv_sexView = (TextView) findViewById(R.id.ll1_sex);// 性别
//		tv_gangweiView = (TextView) findViewById(R.id.ll1_gangwei);// 岗位
//		et_destination = (EditText) findViewById(R.id.ll_3_edittext);
//		tv_ldhthView = (TextView) findViewById(R.id.ll_4_tv);// 劳动合同号
//		tv_workUnitView = (TextView) findViewById(R.id.ll_5_tv);// 工作单位
//		tv_workTimeView = (TextView) findViewById(R.id.ll_6_tv);// 参加工作时间
//		tv_startTime = (TextView) findViewById(R.id.ll_8_tv1);// 开始时间
//		tv_endTime = (TextView) findViewById(R.id.ll_8_tv2);// 结束时间
//		isMarryButton = (RadioButton) findViewById(R.id.radiobtn_yihun);// 已婚
//		et_qjTianShu = (EditText) findViewById(R.id.ll_7_edittianshu);// 请假天数
//		et_qjTianShu.setInputType(EditorInfo.TYPE_CLASS_PHONE);//设置弹出数字输入法
//		et_qjReason = (EditText) findViewById(R.id.ll_9_edittext);// 请假事由
//		attachLayout = (LinearLayout) findViewById(R.id.ll_10);
//		button = (Button) findViewById(R.id.myleaveButton);// 提交按钮
//		button.setOnClickListener(this);
//		gridView = (GridView) findViewById(R.id.grid);
//		gridView.setOnItemClickListener(this);
//		setGridView(files);
//		tv_startTime.setOnClickListener(this);
//		tv_endTime.setOnClickListener(this);
//
//		qjTypeSpinner = (Spinner) findViewById(R.id.ll_7_spinner1);// 出差类型spinner
//		adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item,
//				m);
//		// 设置下拉列表的风格
//		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//		// 将adapter 添加到spinner中
//		qjTypeSpinner.setAdapter(adapter);
//		// 添加事件Spinner事件监听
//		qjTypeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
//
//			@Override
//			public void onItemSelected(AdapterView<?> parent, View view,
//					int position, long id) {
//				qjType = qjTypeSpinner.getSelectedItem().toString();
//				if (qjType.equals("出差") || qjType.equals("培训")
//						|| qjType.equals("病假")) {
//					attachLayout.setVisibility(View.VISIBLE);
//				} else {
//					attachLayout.setVisibility(View.GONE);
//				}
//				getQJTypeCode(qjType);
//				Log.i(TAG, qjType);
//
//			}
//
//			@Override
//			public void onNothingSelected(AdapterView<?> parent) {
//
//			}
//		});
//
//		// 设置默认值
//		qjTypeSpinner.setVisibility(View.VISIBLE);
//
//	}
//
//	@Override
//	protected void onPause() {
//		super.onPause();
//
//	}
//
//	@Override
//	public void onClick(final View v) {
//		int id = v.getId();
//		if (id == R.id.ll_8_tv1 || id == R.id.ll_8_tv2) {
//			final LeaveDateWindow dateWindows = new LeaveDateWindow(this, v);
//			// dateWindows.showAsDropDown(v, 0, -300);
//			dateWindows.showAtLocation(et_destination, Gravity.CENTER_VERTICAL,
//					0, 0);
//			dateWindows.setDateOnclickListener(new DateOnclickListener() {
//
//				@Override
//				public void ondateCalendarClick(String date) {
//					dateWindows.dismiss();
//					((TextView) v).setText(date);
//
//				}
//			});
//		} else if (id == R.id.myleaveButton) {
//			// 提交按钮
//			getSubmitData();
//
//		}
//
//	}
//
//	/**
//	 * 整合提交信息
//	 */
//	@SuppressWarnings("unchecked")
//	private void getSubmitData() {
//		String ksrq = tv_startTime.getText().toString();// 开始时间
//		String jsrq = tv_endTime.getText().toString();// 结束日期
//		String qjsy = et_qjReason.getText().toString();// 请假事由
//		String xjd = et_destination.getText().toString();// 目的地
//		String qjts = et_qjTianShu.getText().toString();// 请假天数
//		if (AbStrUtil.isEmpty(xjd)) {
//			ToastUtil.showToast(StartLeaveActivity.this, "目的地不能为空");
//			return;
//		}
//		if (AbStrUtil.isEmpty(qjts)) {
//			ToastUtil.showToast(StartLeaveActivity.this, "请假天数不能为空");
//			return;
//		}
//		if (AbStrUtil.isEmpty(ksrq)) {
//			ToastUtil.showToast(StartLeaveActivity.this, "开始日期不能为空");
//			return;
//		}
//		if (AbStrUtil.isEmpty(jsrq)) {
//			ToastUtil.showToast(StartLeaveActivity.this, "结束日期不能为空");
//			return;
//		}
//		if (AbStrUtil.isEmpty(qjsy)) {
//			ToastUtil.showToast(StartLeaveActivity.this, "请假事由不能为空");
//			return;
//		}
//
//		submitMap.put("ksrq", ksrq);
//		submitMap.put("jsrq", jsrq);
//		submitMap.put("qjsy", qjsy);
//		submitMap.put("xjd", xjd);
//		submitMap.put(qjTypeCode, qjts);// 请假类型及天数
//		// 附件
//		if (qjTypeCode.equals("a01") || qjTypeCode.equals("a02")
//				|| qjTypeCode.equals("a05")) {
//			if (files.size() == 0) {
//				ToastUtil.showToast(StartLeaveActivity.this, "附件不能为空，请添加附件");
//				return;
//			}
//		}
//		fileListMap.clear();
//		for (int i = 0; i < files.size(); i++) {
//			AttachFile attachFile = files.get(i);
//			File file = new File(attachFile.getAttachPath());
//			if (!file.exists()) {
//				continue;
//			}
//			String fileName = file.getName();
//			Map map = new HashMap<String, Object>();
//			map.put("filename", fileName);
//			try {
//				FileInputStream in = new FileInputStream(
//						attachFile.getAttachPath());
//				int length = in.available();
//				byte[] buffer = new byte[length];
//				in.read(buffer);
//				map.put("filelen", length);
//				map.put("filedata", buffer);
//				// map.put("filedata", new File(attachFile.getAttachPath()));
//			} catch (Exception e1) {
//
//				e1.printStackTrace();
//			}
//			fileListMap.add(map);
//
//		}
//		if (submitLeaveBillsTask != null) {
//			submitLeaveBillsTask.cancel(true);
//		}
//		submitLeaveBillsTask = new SubmitLeaveBillsTask(submitMap);
//		submitLeaveBillsTask.executeOnExecutor(Executors.newCachedThreadPool());
//	}
//
//	/**
//	 * 创建请假单
//	 *
//	 * @author yqs
//	 *
//	 */
//	private class CreateLeaveBillsTask extends
//			AsyncTask<Void, Integer, JResponseObject> {
//		private String id;
//
//		public CreateLeaveBillsTask(String LDHTHId) {
//			this.id = LDHTHId;
//		}
//
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			LoadingDataUtilBlack.show(StartLeaveActivity.this);
//		}
//
//		@Override
//		protected JResponseObject doInBackground(Void... params) {
//			JParamObject PO = JParamObject.Create();
//			CommonPo.setPoToNull();
//			JResponseObject RO = null;
//			if (LDHTHId.equals("")) {
//				getLeaveLDHTHId();// 如果劳动合同号是空，从服务器请求
//			}
//			if (LDHTHId.equals("")) {
//				return null;
//			}
//			PO.SetValueByParamName("sys_ldhth", LDHTHId);
//			// PO.SetValueByParamName("sys_ldhth", "043039");
//			PO.SetValueByParamName("method", "createNewQJD");
//			try {
//				// 连接服务器
//				RO = EAI.DAL.SVR("ExternalQXJService", PO);
//			} catch (Exception e) {
//				e.printStackTrace();
//				Log.i(TAG, "RO请求异常！！！");
//			}
//			return RO;
//		}
//
//		@Override
//		protected void onPostExecute(JResponseObject result) {
//			super.onPostExecute(result);
//			LoadingDataUtilBlack.dismiss();
//			if (result != null) {
//				EFDataSet efDataSet = (EFDataSet) result.getResponseObject();
//				if (efDataSet != null) {
//					List<EFRowSet> list = efDataSet.getRowSetArray();
//					if (list != null) {
//						EFRowSet ef = list.get(0);
//						submitMap.put("xm", ef.getString("xm", ""));
//						submitMap.put("xb", ef.getString("xb", ""));
//						submitMap.put("ldhth", ef.getString("ldhth", ""));
//						submitMap.put("hf", ef.getString("hf", ""));
//						submitMap.put("depname", ef.getString("depname", ""));
//						submitMap.put("depcode", ef.getString("depcode", ""));
//						submitMap.put("gw", ef.getString("gw", ""));
//						submitMap.put("cjgzsj", ef.getString("cjgzsj", ""));
//						submitMap.put("myid", ef.getString("myid", ""));
//
//						initData();
//					} else {
//						showTipsDialog(true,"获取请假单数据失败，请返回重试");
//					}
//
//				} else {
//					showTipsDialog(true,"获取请假单数据失败，请返回重试");
//				}
//
//			} else {
//				showTipsDialog(true,"获取请假单数据失败，请返回重试");
//			}
//		}
//	}
//
//	/**
//	 * 提交请假单
//	 *
//	 * @author yqs
//	 *
//	 */
//	private class SubmitLeaveBillsTask extends
//			AsyncTask<Void, Integer, JResponseObject> {
//		private Map<String, String> submitMap;
//
//		public SubmitLeaveBillsTask(Map<String, String> submitMap) {
//			this.submitMap = submitMap;
//		}
//
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			LoadingDataUtilBlack.show(StartLeaveActivity.this, "提交中...");
//		}
//
//		@Override
//		protected JResponseObject doInBackground(Void... params) {
//			JParamObject PO = JParamObject.Create();
//			CommonPo.setPoToNull();
//			JResponseObject RO = null;
//			PO.SetValueByParamName("method", "doQJCommit");
//			PO.SetValueByParamName("myid", submitMap.get("myid"));
//			PO.SetValueByParamName("xm", submitMap.get("xm"));
//			PO.SetValueByParamName("xb", submitMap.get("xb"));
//			PO.SetValueByParamName("ldhth", submitMap.get("ldhth"));
//			PO.SetValueByParamName("hf", submitMap.get("hf"));
//			PO.SetValueByParamName("depname", submitMap.get("depname"));
//			PO.SetValueByParamName("depcode", submitMap.get("depcode"));
//			PO.SetValueByParamName("gw", submitMap.get("gw"));
//			PO.SetValueByParamName("cjgzsj", submitMap.get("cjgzsj"));
//			PO.SetValueByParamName("xjd", submitMap.get("xjd"));// 目的地
//			PO.SetValueByParamName("ksrq", submitMap.get("ksrq"));
//			PO.SetValueByParamName("jsrq", submitMap.get("jsrq"));
//			PO.SetValueByParamName("qjsy", submitMap.get("qjsy"));
//			PO.SetValueByParamName(qjTypeCode, submitMap.get(qjTypeCode));
//			if (qjTypeCode.equals("a01") || qjTypeCode.equals("a02")
//					|| qjTypeCode.equals("a05")) {
//				PO.setValue("affixs", fileListMap);
//				// PO.setObject("affixs", fileListMap);
//			}
//
//			try {
//				// 连接服务器
//				RO = EAI.DAL.SVR("ExternalQXJService", PO);
//			} catch (Exception e) {
//				e.printStackTrace();
//				Log.i(TAG, "RO请求异常");
//			}
//			return RO;
//		}
//
//		@Override
//		protected void onPostExecute(JResponseObject result) {
//			super.onPostExecute(result);
//			LoadingDataUtilBlack.dismiss();
//			if (result != null) {
//				EFDataSet efDataSet = (EFDataSet) result.getResponseObject();
//				if (efDataSet != null) {
//					List<EFRowSet> list = efDataSet.getRowSetArray();
//					if (list != null) {
//						EFRowSet efRowSet = list.get(0);
//						// msg=保存成功！
//						String msg = efRowSet.getString("msg", "");
//						String info = efRowSet.getString("result", "0");
//						// if (msg.equals("保存成功！")) {
//						if (info.equals("1")) {
//							showTipsDialog(true,"提交成功");
//						} else {
//							if (msg.contains("距离上次轮休假")){
//								showTipsDialog(false,"提交失败，距离上次轮休假不超过两个月");
//								return;
//							}
//							showTipsDialog(false,"提交失败");
//						}
//					} else {
//						showTipsDialog(false,"提交失败");
//					}
//
//				} else {
//					showTipsDialog(false,"提交失败");
//				}
//			} else {
//				showTipsDialog(false,"提交失败");
//			}
//		}
//	}
//
//	/**
//	 * 审批结束后提示是否成功
//	 *
//	 * @param
//	 */
//	private void showTipsDialog(boolean isSuccess,String tips) {
//		AlertDialog.Builder builder = new AlertDialog.Builder(
//				StartLeaveActivity.this);
//
//		if (isSuccess) {
//			builder.setTitle("提示")
//					.setMessage(tips)
//					.setPositiveButton("确定",
//							new DialogInterface.OnClickListener() {
//
//								@Override
//								public void onClick(DialogInterface dialog,
//										int which) {
//									dialog.dismiss();
//									StartLeaveActivity.this.finish();
//
//								}
//							}).show();
//		} else {
//			builder.setTitle("提示")
//					.setMessage(tips)
//					.setPositiveButton("确定",
//							new DialogInterface.OnClickListener() {
//
//								@Override
//								public void onClick(DialogInterface dialog,
//										int which) {
//									dialog.dismiss();
//								}
//							}).show();
//		}
//
//	}
//
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		super.onActivityResult(requestCode, resultCode, data);
//		String path = "";
//		String fileName = "";
//		if (resultCode == Activity.RESULT_OK) {
//			if (requestCode == PHOTO_REQUEST_TAKEPHOTO) {
//				// 拍照
//				path = fileDir + "/" + imageName;
//				fileName = path.substring(path.lastIndexOf("/") + 1);
//				System.out.println("拍照文件名称：" + fileName);
//
//			} else {
//
//				// 根据选择的文件得到uri
//				Uri uri = data.getData();
//
//				Log.i(TAG, "uri：" + uri);
//				// 从uri中得到文件路径
//				path = FileUtil.getFilePath(this, uri);
//
//				Log.i(TAG, "文件url:" + path);
//				if (path == null) {
//					ToastUtil.showToast(StartLeaveActivity.this, "该文件不支持选择");
//					return;
//				}
//				fileName = path.substring(path.lastIndexOf("/") + 1);
//				System.out.println("文件名称" + fileName);
//			}
//			AttachFile file = new AttachFile();
//			file.setAttachName(fileName);
//			file.setAttachPath(path);
//
//			String filesize;
//			try {
//				filesize = FileSizeUtil.getFileSize(new File(path)) + "";
//				file.setAttachSize(filesize);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//
//			// 设置文件缩略图
//			Bitmap bmp = FileUtil.getImageFrompath(StartLeaveActivity.this,
//					path);
//			Drawable drawable = new BitmapDrawable(bmp);
//			file.setAttachImage(drawable);
//
//			// 显示到gridview中
//			showGrid(file);
//
//		}
//	}
//
//	/**
//	 * 将选择的附件显示到gridview界面中
//	 *
//	 * @param file
//	 */
//	private void showGrid(AttachFile file) {
//
//		files.add(file);
//		setGridView(files);
//
//	}
//
//	/**
//	 * 设置GirdView参数，绑定数据
//	 *
//	 * 文件list集合
//	 */
//	private void setGridView(ArrayList<AttachFile> files2) {
//		int size = files2.size() + 1;
//		int length = 100;
//
//		DisplayMetrics dm = new DisplayMetrics();
//		getWindowManager().getDefaultDisplay().getMetrics(dm);
//		float density = dm.density;
//		int gridviewWidth = (int) (size * (length + 4) * density);
//		int itemWidth = (int) (length * density);
//
//		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
//				gridviewWidth, LinearLayout.LayoutParams.FILL_PARENT);
//		gridView.setLayoutParams(params); // 设置GirdView布局参数,横向布局的关键
//		gridView.setColumnWidth(itemWidth); // 设置列表项宽
//		gridView.setHorizontalSpacing(10); // 设置列表项水平间距
//		gridView.setStretchMode(GridView.NO_STRETCH);
//		gridView.setNumColumns(size); // 设置列数量=列表集合数
//
//		gridAdapter = new LeaveFileGridAdapter(files, StartLeaveActivity.this);
//		gridView.setAdapter(gridAdapter);
//	}
//
//	@Override
//	public void onItemClick(AdapterView<?> parent, View view, int position,
//			long id) {
//		if (position == files.size()) {
//			showDialog();
//		} else {
//			AttaachDialog dialog = new AttaachDialog(StartLeaveActivity.this,
//					R.style.dialog, position, files, gridAdapter);
//
//			dialog.show();
//		}
//
//	}
//
//	private void showDialog() {
//
//		File file = new File(fileDir);
//		if (!file.exists()) {
//			file.mkdirs();
//		}
//		final AlertDialog dlg = new AlertDialog.Builder(this).create();
//		dlg.show();
//		Window window = dlg.getWindow();
//		// *** 主要就是在这里实现这种效果的.
//		// 设置窗口的内容页面,shrew_exit_dialog.xml文件中定义view内容
//		window.setContentView(R.layout.alertdialog);
//		// 为确认按钮添加事件,执行退出应用操作
//		TextView tv_paizhao = (TextView) window.findViewById(R.id.tv_content1);
//		tv_paizhao.setText("拍照");
//		tv_paizhao.setOnClickListener(new OnClickListener() {
//			@SuppressLint("SdCardPath")
//			public void onClick(View v) {
//
//				imageName = getNowTime() + ".png";
//				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//				// 指定调用相机拍照后照片的储存路径
//				intent.putExtra(MediaStore.EXTRA_OUTPUT,
//						Uri.fromFile(new File(fileDir, imageName)));
//				startActivityForResult(intent, PHOTO_REQUEST_TAKEPHOTO);
//				dlg.cancel();
//			}
//		});
//		TextView tv_xiangce = (TextView) window.findViewById(R.id.tv_content2);
//		tv_xiangce.setText("文件");
//		tv_xiangce.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				showFileChooser();
//
//				dlg.cancel();
//			}
//		});
//
//	}
//
//	/**
//	 * 得到 请销假用户合同号id
//	 *
//	 * @return
//	 */
//	private String getLeaveLDHTHId() {
//		if (LDHTHId.equals("")) {
//
//			JParamObject PO = JParamObject.Create();
//			CommonPo.setPoToNull();
//			JResponseObject RO = null;
//			StorageUtil storageUtil = new StorageUtil(this, "storage");
//			String systemUserName = storageUtil.getString("userName");
//			storageUtil = new StorageUtil(this, systemUserName);
//			String userId = storageUtil.getString("LEAVE" + "Id", "");
//			String passWord = storageUtil.getString("LEAVE" + "Pwd", "");
//			if (userId.equals("") || passWord.equals("")) {
//				return "";
//			}
//			PO.SetValueByParamName("UserCode", userId);// 用户名（名字或劳动合同号）
//			PO.SetValueByParamName("PassWord", passWord);// 密码
//			PO.SetValueByParamName("method", "doLoginAction");
//
//			try {
//				// 连接服务器
//				RO = EAI.DAL.SVR("ExternalQXJService", PO);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			if (RO != null) {
//
//				EFDataSet efDataSet = (EFDataSet) RO.getResponseObject();
//				if (efDataSet != null) {
//					List<EFRowSet> list = efDataSet.getRowSetArray();
//					if (list != null
//							&& !list.get(0).getString("result", "")
//									.equals("-1")) {
//						LDHTHId = list.get(0).getString("sys_ldhth", "");
//						Log.i(TAG, "创建请假单请求合同号:" + LDHTHId);
//					}
//				}
//
//			}
//		}
//		return LDHTHId;
//
//	}
//
//	/** 调用文件选择软件来选择文件 **/
//	private void showFileChooser() {
//		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//		intent.setType("*/*");
//
//		intent.addCategory(Intent.CATEGORY_OPENABLE);
//		try {
//			startActivityForResult(Intent.createChooser(intent, "请选择一个要上传的文件"),
//					2);
//		} catch (android.content.ActivityNotFoundException ex) {
//
//			Toast.makeText(this, "请安装文件管理器", Toast.LENGTH_SHORT).show();
//		}
//	}
//
//	private String getNowTime() {
//		Date date = new Date(System.currentTimeMillis());
//		SimpleDateFormat dateFormat = new SimpleDateFormat("MMddHHmmssSS");
//		return dateFormat.format(date);
//	}
//
//	/**
//	 * 根据请假的类型得到请假类型的 代号
//	 *
//	 * @param qjType2
//	 *            "出差", "培训", "轮休", "事假", "病假", "工伤", "婚假", "丧假", "产假", "计生假",
//	 *            "哺乳假", "探亲假", "带薪年休"
//	 */
//	protected void getQJTypeCode(String qjType2) {
//		if ("出差".equals(qjType2)) {
//			qjTypeCode = "a01";
//		} else if ("培训".equals(qjType2)) {
//			qjTypeCode = "a02";
//		} else if ("轮休".equals(qjType2)) {
//			qjTypeCode = "a03";
//		} else if ("事假".equals(qjType2)) {
//			qjTypeCode = "a04";
//		} else if ("病假".equals(qjType2)) {
//			qjTypeCode = "a05";
//		} else if ("工伤".equals(qjType2)) {
//			qjTypeCode = "a06";
//		} else if ("婚假".equals(qjType2)) {
//			qjTypeCode = "a07";
//		} else if ("丧假".equals(qjType2)) {
//			qjTypeCode = "a08";
//		} else if ("产假".equals(qjType2)) {
//			qjTypeCode = "a09";
//		} else if ("计生假".equals(qjType2)) {
//			qjTypeCode = "a10";
//		} else if ("哺乳假".equals(qjType2)) {
//			qjTypeCode = "a11";
//		} else if ("探亲假".equals(qjType2)) {
//			qjTypeCode = "a13";
//		} else if ("带薪年休".equals(qjType2)) {
//			qjTypeCode = "a14";
//		}
//
//	}
//}
