package com.efounder.agency;

import java.util.ArrayList;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class MyFragmentPageAadpter extends FragmentPagerAdapter {
		
	private ArrayList<Fragment> fragmentsList;
	public MyFragmentPageAadpter(FragmentManager fm) {super(fm);}
	public MyFragmentPageAadpter(FragmentManager fm, ArrayList<Fragment> fragments) {
        super(fm);
        this.fragmentsList = fragments;
    }
	
	
	@Override
	public Fragment getItem(int index) {
		return fragmentsList.get(index);
	}

	@Override
	public int getCount() {
		return fragmentsList.size();
	}

	
}
