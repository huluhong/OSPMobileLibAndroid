package com.efounder.agency;
import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class MyViewPager extends ViewPager {

        private Context context;
        private boolean willIntercept = true;
        
        public MyViewPager(Context context) {
                super(context);
                this.context = context;
        }
        
        public MyViewPager(Context context, AttributeSet attrs) {
                super(context, attrs);
                this.context = context;
        }

        
        
        @Override
        public boolean onInterceptTouchEvent(MotionEvent arg0) {
               
                        return false;
                
        }

       

}