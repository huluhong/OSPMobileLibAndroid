package com.efounder.email.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.efounder.ospmobilelib.R;
import com.efounder.email.util.SDCardImageLoader;
import com.efounder.email.util.ScreenUtils;

/**
 * PhotoWall涓璆ridView镄勶拷?锟介厤锟?
 *
 * @author hanj
 */

public class PhotoWallAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<String> imagePathList = null;

    private SDCardImageLoader loader;

    //璁板綍鏄惁琚拷?锟芥嫨
    private SparseBooleanArray selectionMap;

    public PhotoWallAdapter(Context context, ArrayList<String> imagePathList) {
        this.context = context;
        this.imagePathList = imagePathList;
   System.out.println(ScreenUtils.getScreenH());
   WindowManager wm = (WindowManager) context
           .getSystemService(Context.WINDOW_SERVICE);

int width = wm.getDefaultDisplay().getWidth();
int height = wm.getDefaultDisplay().getHeight();
       // loader = new SDCardImageLoader(ScreenUtils.getScreenW(), ScreenUtils.getScreenH());
loader = new SDCardImageLoader(width,height);
        selectionMap = new SparseBooleanArray();
    }

    @Override
    public int getCount() {
        return imagePathList == null ? 0 : imagePathList.size();
    }

    @Override
    public Object getItem(int position) {
        return imagePathList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        String filePath = (String) getItem(position);

        final ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.photo_wall_item, null);
            holder = new ViewHolder();

            holder.imageView = (ImageView) convertView.findViewById(R.id.photo_wall_item_photo);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.photo_wall_item_cb);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        //tag镄刱ey蹇呴』浣跨敤id镄勬柟寮忓畾涔変互淇濊瘉鍞竴锛屽惁鍒欎细鍑虹幇IllegalArgumentException.
        holder.checkBox.setTag(R.id.tag_first, position);
        holder.checkBox.setTag(R.id.tag_second, holder.imageView);
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Integer position = (Integer) buttonView.getTag(R.id.tag_first);
                ImageView image = (ImageView) buttonView.getTag(R.id.tag_second);

                selectionMap.put(position, isChecked);
                if (isChecked) {
                    image.setColorFilter(context.getResources().getColor(R.color.image_checked_bg));
                } else {
                    image.setColorFilter(null);
                }
            }
        });
        
        //yqs澧炲姞  鍙互镣瑰向锲剧墖杩涜阃変腑
        holder.imageView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Boolean boolean1 =  selectionMap.get(position);
				 if (boolean1) {
					 holder.imageView.setColorFilter(context.getResources().getColor(R.color.image_checked_bg));
					 selectionMap.put(position, !boolean1);
					 holder.checkBox.setChecked(!boolean1);
	                } else {
	                	 holder.imageView.setColorFilter(null);
	                	 selectionMap.put(position, !boolean1);
	                	 holder.checkBox.setChecked(!boolean1);
	                }
				
			}
		});

        holder.checkBox.setChecked(selectionMap.get(position));
        holder.imageView.setTag(filePath);
        loader.loadImage(4, filePath, holder.imageView);
        return convertView;
    }

    private class ViewHolder {
        ImageView imageView;
        CheckBox checkBox;
    }

    public SparseBooleanArray getSelectionMap() {
        return selectionMap;
    }

    public void clearSelectionMap() {
        selectionMap.clear();
    }
}
