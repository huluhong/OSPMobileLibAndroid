package com.efounder.email.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.efounder.ospmobilelib.R;
import com.efounder.email.model.AttachFile;

/**
 * 鍙戦?侀偖浠? 娣诲姞闄勪欢 gridview阃傞厤鍣?
 * @author yqs
 *
 */
public class MyGridAdapter extends BaseAdapter {

	ArrayList<AttachFile> files = new ArrayList<AttachFile>();
	Context context;

	

	public MyGridAdapter(ArrayList<AttachFile> files, Context context) {
		super();
		this.files = files;
		this.context = context;
	}

	@Override
	public int getCount() {
		// TODO 镊姩鐢熸垚镄勬柟娉曞瓨镙?
		return files.size();
	}

	@Override
	public AttachFile getItem(int position) {
		// TODO 镊姩鐢熸垚镄勬柟娉曞瓨镙?
		return files.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO 镊姩鐢熸垚镄勬柟娉曞瓨镙?
		return position;
	}

	@SuppressWarnings("deprecation")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = View.inflate(context, R.layout.grid_items, null);
			new ViewHolder(convertView);
		}
		ViewHolder holder = (ViewHolder) convertView.getTag();
		AttachFile file = getItem(position);
		holder.image.setImageDrawable(file.getAttachImage());
		holder.fileName.setText(file.getAttachName());
		holder.fileSize.setText(file.getAttachSize());

		return convertView;
	}

	class ViewHolder {
		/**闄勪欢锲剧墖*/
		public ImageView image;
		/**闄勪欢鍚岖О*/
		public TextView fileName;
		/**闄勪欢澶у皬*/
		public TextView fileSize;

		public ViewHolder(View view) {
			image = (ImageView) view.findViewById(R.id.itemImage);
			fileName = (TextView) view.findViewById(R.id.email_attachmentname);
			fileSize =(TextView) view.findViewById(R.id.email_attachsize);
			view.setTag(this);
		}
	}

}
