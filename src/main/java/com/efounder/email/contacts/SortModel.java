package com.efounder.email.contacts;

import java.io.Serializable;

import android.R.integer;

public class SortModel extends Contact implements Serializable {

	public SortModel(int contactId,String name, String number, String sortKey) {
		super(contactId, number, sortKey,name);
	}
	public SortModel(int contactId,String name, String number, String sortKey,String sortLetters) {
		super(contactId,name, number, sortKey);
		this.sortLetters = sortLetters;
	}

	public String sortLetters; // 显示数据拼音的首字母

	public SortToken sortToken = new SortToken();

	public SortModel() {
		
	}

}
