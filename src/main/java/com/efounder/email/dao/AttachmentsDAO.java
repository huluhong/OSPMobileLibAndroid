package com.efounder.email.dao;

import java.util.ArrayList;
import java.util.Arrays;

import android.R.integer;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.efounder.email.contacts.SortModel;
import com.efounder.email.db.DBManager;
import com.efounder.email.db.EmailsDbHelper;
import com.efounder.email.model.AttachFile;
import com.efounder.email.model.Email;
import com.efounder.email.util.AbStrUtil;

/**
 * 邮件数据库操作
 * 
 * @author yqs
 * 
 */

public class AttachmentsDAO {

	private EmailsDbHelper attachmentDbHelper;
	private SQLiteDatabase db;// 创建SQLiteDatabase对象

	public AttachmentsDAO(Context context) {
		attachmentDbHelper = DBManager.getEmailsDbHelper(context);// 初始化DBgetOpenHelper对象
	}

	/**
	 * 保存附件（单个）
	 * 
	 * @param email
	 */

	public void saveSinglefile(AttachFile file) {
		db = attachmentDbHelper.getWritableDatabase();// 初始化SQLiteDatabase对象

		db.execSQL(
				"insert into emailattachment(_id,name,path,boxtype,emailtype)  values (?,?,?,?,?)",
				new Object[] { file.getId(), file.getAttachName(),
						file.getAttachPath(), file.getBoxType(),
						file.getEmailType() });

	}

	/**
	 * 保存附件（集合）
	 * 
	 * @param ArrayList
	 *            <Email> emails 邮件list集合
	 */

	public void saveSetFiles(ArrayList<AttachFile> files) {
		db = attachmentDbHelper.getWritableDatabase();// 初始化SQLiteDatabase对象
		AttachFile file = new AttachFile();
		for (int i = 0; i < files.size(); i++) {
			file = files.get(i);

			db.execSQL(
					"insert into emailattachment(_id,name,path,boxtype,emailtype)  values (?,?,?,?,?)",
					new Object[] { file.getId(), file.getAttachName(),
							file.getAttachPath(), file.getBoxType(),
							file.getEmailType() });

		}

	}

	/**
	 * 查找所有附件
	 * 
	 * @return
	 */

	public ArrayList<AttachFile> queryFiles(String id) {
		db = attachmentDbHelper.getWritableDatabase();// 初始化SQLiteDatabase对象
		// 查找并存储到Cursor类中
		Cursor cursor = db.rawQuery(
				"select * from emailattachment where _id = ?",
				new String[] { id });
		ArrayList<AttachFile> allList = new ArrayList<AttachFile>();
		// 遍历查询数据库
		for (int i = 0; i < cursor.getCount(); i++) {
			cursor.moveToPosition(i);

			AttachFile file = new AttachFile();
			file.setId(id);
			file.setAttachPath(cursor.getString(cursor.getColumnIndex("path")));
			file.setAttachName(cursor.getString(cursor.getColumnIndex("name")));
			file.setBoxType(cursor.getString(cursor.getColumnIndex("boxtype")));
			file.setEmailType(cursor.getString(cursor
					.getColumnIndex("emailtype")));
			allList.add(file);
		}
		System.out.println("allList.size()：" + allList.size());
		return allList;
	}

	/**
	 * 获取所有附件的数量
	 * 
	 * @return
	 */
	public long getAttachmentFileCount() {
		db = attachmentDbHelper.getWritableDatabase();// 初始化SQLiteDatabase对象
		Cursor cursor = db.rawQuery("select count(_id) from emailattachment",
				null);// 获取id的记录数
		if (cursor.moveToNext())// 判断Cursor中是否有数据
		{
			return cursor.getLong(0);// 返回总记录数
		}
		return 0;

	}

	/**
	 * 清除邮箱数据库表的全部数据
	 */
	public void deleteTableData() {
		db = attachmentDbHelper.getWritableDatabase();
		db.execSQL("delete from emailattachment");

	}

/**
 * 清楚保存的附件信息  
 * @param emailType 邮箱类型
 * @param boxType  收件箱 或者发件箱
 */
	public void deleteTableData(String emailType, String boxType) {
		db = attachmentDbHelper.getWritableDatabase();
		db.execSQL(
				"delete from emailattachment where emailtype =? and boxtype = ?",
				new String[] { emailType, boxType });

	}
}
