package com.efounder.email.dao;

import java.util.ArrayList;
import java.util.Arrays;

import android.R.integer;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.efounder.email.contacts.SortModel;
import com.efounder.email.db.DBManager;
import com.efounder.email.db.EmailsDbHelper;
import com.efounder.email.model.Email;
import com.efounder.email.util.AbStrUtil;

/**
 * 邮件数据库操作
 * 
 * @author yqs
 * 
 */

public class InboxEmailsDAO {

	private EmailsDbHelper emailsDbHelper;
	private SQLiteDatabase db;// 创建SQLiteDatabase对象

	public InboxEmailsDAO(Context context) {
		emailsDbHelper = DBManager.getEmailsDbHelper(context);// 初始化DBgetOpenHelper对象
	}

	/**
	 * 保存邮件（单个）
	 * 
	 * @param email
	 */

	public void saveSingleEmail(Email email) {
		db = emailsDbHelper.getWritableDatabase();// 初始化SQLiteDatabase对象
		if (isExist(email.getMessageID())) {
			return;
		} else {
			db.execSQL(
					"insert into inboxemails(_id,sender,ishavefile,isread,subject,sendtime,image,userid,type)  values (?,?,?,?,?,?,?,?,?)",
					new Object[] { email.getMessageID(), email.getFrom(),
							email.getIsHaveFile().toString(),
							email.getIsRead().toString(), email.getSubject(),
							email.getSentdata(), email.getImage(),
							email.getUserId(), email.getEmailType() });
		}

	}

	/**
	 * 保存邮件（集合）
	 * 
	 * @param ArrayList
	 *            <Email> emails 邮件list集合
	 */

	public void saveSetEmail(ArrayList<Email> emails) {
		db = emailsDbHelper.getWritableDatabase();// 初始化SQLiteDatabase对象
		Email email = new Email();
		for (int i = 0; i < emails.size(); i++) {
			email = emails.get(i);
			if (isExist(email.getMessageID())) {
				continue;
			} else {
				db.execSQL(
						"insert into inboxemails(_id,sender,ishavefile,isread,subject,sendtime,image,userid,type)  values (?,?,?,?,?,?,?,?,?)",
						new Object[] { email.getMessageID(), email.getFrom(),
								email.getIsHaveFile().toString(),
								email.getIsRead().toString(),
								email.getSubject(), email.getSentdata(),
								email.getImage(), email.getUserId(),
								email.getEmailType() });
			}
		}

	}

	/**
	 * 查找所有邮件
	 * 
	 * @return
	 */

	public ArrayList<Email> queryEmails(String type, String userId) {
		db = emailsDbHelper.getWritableDatabase();// 初始化SQLiteDatabase对象
		// 查找并存储到Cursor类中
		Cursor cursor = db.rawQuery(
				"select * from inboxemails where type = ? and userid = ?",
				new String[] { type, userId });
		ArrayList<Email> allList = new ArrayList<Email>();
		// 遍历查询数据库
		for (int i = 0; i < cursor.getCount(); i++) {
			cursor.moveToPosition(i);

			Email email = new Email();
			email.setMessageID(cursor.getString(cursor.getColumnIndex("_id")));
			email.setSubject(cursor.getString(cursor.getColumnIndex("subject")));
			email.setSentdata(cursor.getString(cursor
					.getColumnIndex("sendtime")));
			email.setFrom(AbStrUtil.getNameFromEmail(cursor.getString(cursor.getColumnIndex("sender"))));
			email.setImage(cursor.getInt(cursor.getColumnIndex("image")));
			email.setIsHaveFile((cursor.getString(cursor
					.getColumnIndex("ishavefile"))).equals("true") ? true
					: false);
			email.setIsRead((cursor.getString(cursor.getColumnIndex("isread")))
					.equals("true") ? true : false);
			email.setEmailType(cursor.getString(cursor.getColumnIndex("type")));
			email.setUserId(cursor.getString(cursor.getColumnIndex("userid")));
			allList.add(email);
		}
		System.out.println("allList.size()：" + allList.size());
		return allList;
	}

	/**
	 * 根据id查找邮件
	 * 
	 * @return
	 */

	public Email queryEmailById(String id) {
		db = emailsDbHelper.getWritableDatabase();// 初始化SQLiteDatabase对象
		// 查找并存储到Cursor类中
		Cursor cursor = db.rawQuery("select * from inboxemails where _id = ?",
				new String[] { id });
		if (cursor.moveToNext()) {
			Email email = new Email();
			email.setMessageID(cursor.getString(cursor.getColumnIndex("_id")));
			email.setSubject(cursor.getString(cursor.getColumnIndex("subject")));
			email.setSentdata(cursor.getString(cursor
					.getColumnIndex("sendtime")));
			email.setFrom(cursor.getString(cursor.getColumnIndex("sender")));
			email.setImage(cursor.getInt(cursor.getColumnIndex("image")));
			email.setIsHaveFile((cursor.getString(cursor
					.getColumnIndex("ishavefile"))).equals("true") ? true
					: false);
			//email.setIsHaveFile(false);
			email.setIsRead((cursor.getString(cursor.getColumnIndex("isread")))
					.equals("true") ? true : false);
			email.setEmailType(cursor.getString(cursor.getColumnIndex("type")));
			email.setUserId(cursor.getString(cursor.getColumnIndex("userid")));
			email.setCopyTo(cursor.getString(cursor.getColumnIndex("chaosong")));
			email.setSecretTo(cursor.getString(cursor.getColumnIndex("misong")));
			email.setTo(cursor.getString(cursor.getColumnIndex("receiver")));
			email.setContent(cursor.getString(cursor.getColumnIndex("content")));
			return email;
		}

		return null;
	}

	/**
	 * 根据id查找邮件是否存在
	 * 
	 * @return
	 */

	public Boolean isExist(String id) {
		db = emailsDbHelper.getWritableDatabase();// 初始化SQLiteDatabase对象
		// 查找并存储到Cursor类中
		Cursor cursor = db.rawQuery("select * from inboxemails where _id = ?",
				new String[] { id });
		if (cursor.moveToNext()) {

			String itemId = cursor.getString(cursor.getColumnIndex("_id"));

			if (itemId != null && !itemId.equals("")) {
				return true;
			}

		}

		return false;
	}

	/**
	 * 根据id将数据库中的邮件设置为已读
	 * 
	 * @param id
	 */
	public void setIsRead(String id) {
		db = emailsDbHelper.getWritableDatabase();// 初始化SQLiteDatabase对象
		if (isExist(id)) {
			db.execSQL("update inboxemails set isread=? where _id=?",new Object[]{"true", id});		
		}
	}

	/**
	 * 更新邮件
	 * 
	 * @param
	 */
	public void updateEmail(Email email) {
		db = emailsDbHelper.getWritableDatabase();
		if (isExist(email.getMessageID())) {
			db.execSQL(
					"update  inboxemails  set sender=?,chaosong=?,misong =?,receiver = ?,content =?  where _id = ?",
					new Object[] { email.getFrom(), email.getCopyTo(),
							email.getSecretTo(), email.getTo(),
							email.getContent(), email.getMessageID()

					});

		} else {
			System.out.println("更新失败！邮件不存在");
		}
	}

	/**
	 * 获取所有邮件的数量
	 * 
	 * @return
	 */
	public long getEmailCount() {
		db = emailsDbHelper.getWritableDatabase();// 初始化SQLiteDatabase对象
		Cursor cursor = db.rawQuery("select count(_id) from inboxemails", null);// 获取id的记录数
		if (cursor.moveToNext())// 判断Cursor中是否有数据
		{
			return cursor.getLong(0);// 返回总记录数
		}
		return 0;

	}

	/**
	 * 获取所需类型邮件的数量 dpemail email
	 * 
	 * @return
	 */
	public long getEmailTypeCount(String type, String userId) {
		db = emailsDbHelper.getWritableDatabase();// 初始化SQLiteDatabase对象
		Cursor cursor = db
				.rawQuery(
						"select count(_id) from inboxemails where type =? and userid = ?",
						new String[] { type, userId });// 获取id的记录数
		if (cursor.moveToNext())// 判断Cursor中是否有数据
		{
			return cursor.getLong(0);// 返回总记录数
		}
		return 0;

	}

	/**
	 * 删除邮件
	 * 
	 * @param ids
	 */
	public void deleteEmail(Integer... ids) {
		if (ids.length > 0)// 判断是否存在要删除的id
		{
			StringBuffer sb = new StringBuffer();// 创建StringBuffer对象
			for (int i = 0; i < ids.length; i++)// 遍历要删除的id集合
			{
				sb.append('?').append(',');// 将删除条件添加到StringBuffer对象中
			}
			sb.deleteCharAt(sb.length() - 1);// 去掉最后一个“,“字符
			db = emailsDbHelper.getWritableDatabase();// 初始化SQLiteDatabase对象
			// 执行删除联系人操作
			db.execSQL("delete from inboxemails where _id in (" + sb + ")",
					(Object[]) ids);
		}

	}

	/**
	 * 删除邮件单个
	 * 
	 * @param ids
	 */
	public void deleteEmailById(String id) {

		db = emailsDbHelper.getWritableDatabase();// 初始化SQLiteDatabase对象
		// 执行删除联系人操作
		db.execSQL("delete from inboxemails where _id = ?", new String[] { id });

	}

	/**
	 * 通过类型删除邮件
	 * 
	 * @param type
	 *            邮件类型
	 */
	public void deleteDataByType(String type) {
		db = emailsDbHelper.getWritableDatabase();
		db.execSQL("delete from inboxemails where type = ?",
				new String[] { type });
	}

	/**
	 * 清除邮箱数据库表的全部数据
	 */
	public void deleteTableData() {
		db = emailsDbHelper.getWritableDatabase();
		db.execSQL("delete from inboxemails");

	}
}
