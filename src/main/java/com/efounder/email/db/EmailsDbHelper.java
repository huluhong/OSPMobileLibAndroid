package com.efounder.email.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class EmailsDbHelper extends SQLiteOpenHelper {

	// 数据库名
	private static final String DBNAME = "emails.db";
	// 当前数据库的版本
	private static final int DBVERSION =10;
	// 收件箱数据库
	final String CREATE_INBOXEMAILS_TABLE = "create table if not exists inboxemails ("
			+ "_id varchar(400) not null primary key,"
			+ "sender varchar(200),"
			+ "ishavefile varchar(30),"
			+ "isread varchar(30),"
			+ "subject varchar(400),"
			+ "sendtime varchar(100),"
			+ "image integer, "
			+ "userid varchar(100),"
			+ "type varchar(20),"
			+ "chaosong text,"
			+ "misong text,"
			+ "receiver text,"
			+ " content text)";
	// 发件箱数据库
	final String CREATE_OUTBOXEMAILS_TABLE = "create table if not exists outboxemails ("
			+ "_id varchar(400) not null primary key,"
			+ "receiver text,"
			+ "ishavefile boolean,"
			+ "subject varchar(400),"
			+ "sendtime varchar(100),"
			+ "image integer," 
			+ "userid varchar(100)," 
			+ "type varchar(20),"
			+ "chaosong text,"
			+ "misong text,"
			+ "sender varchar(200),"
			+ " content text)";
	
	// 邮箱附件数据库
		final String CREATE_EMAILS_ATTACHMENT_TABLE = "create table if not exists emailattachment ("
				+ "_id varchar(400) not null,"
				+ "name varchar(200),"				
				+ "path varchar(500),"
				+ "boxtype varchar(20),"
				+ "emailtype varchar(20))";
	// 联系人数据库
	final String CREATE_CONTACTS_TABLE = "create table if not exists contacts (_id integer ,"
			+ "name varchar(100), number varchar(100),sortkey varchar(20),sortletters varchar(20))";
	
	
	
	final String DROP_TABLE_INBOX ="drop table if exists inboxemails ";
	final String DROP_TABLE_OUTBOX ="drop table if exists outboxemails";
	final String DROP_TABLE_ATTACHMENT ="drop table if exists emailattachment";
	final String DROP_TABLE_CONTACTS ="drop table  if exists contacts";

	public EmailsDbHelper(Context context) {
		super(context, DBNAME, null, DBVERSION);

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		 Log.i("emaildb", "#############创建数据库##############:" + DBVERSION);
		db.execSQL(CREATE_INBOXEMAILS_TABLE);
		db.execSQL(CREATE_OUTBOXEMAILS_TABLE);
		db.execSQL(CREATE_EMAILS_ATTACHMENT_TABLE);
		db.execSQL(CREATE_CONTACTS_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		 Log.i("emaildb", "#############数据库升级了##############:" + DBVERSION);
		db.execSQL(DROP_TABLE_INBOX);
		db.execSQL(DROP_TABLE_OUTBOX);
		db.execSQL(DROP_TABLE_ATTACHMENT);
		db.execSQL(DROP_TABLE_CONTACTS);
		
		
		db.execSQL(CREATE_INBOXEMAILS_TABLE);
		db.execSQL(CREATE_OUTBOXEMAILS_TABLE);
		db.execSQL(CREATE_EMAILS_ATTACHMENT_TABLE);
		db.execSQL(CREATE_CONTACTS_TABLE);


	}

	public boolean deleteDatabase(Context context) {
		return context.deleteDatabase(DBNAME);
	}

}
