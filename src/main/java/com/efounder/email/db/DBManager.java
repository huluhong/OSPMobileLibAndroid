package com.efounder.email.db;

import android.content.Context;

import com.efounder.util.AppContext;

public class DBManager {

private static EmailsDbHelper emailsDbHelper;


public  static EmailsDbHelper getEmailsDbHelper(Context context ){
	if (emailsDbHelper == null) {
		emailsDbHelper = new EmailsDbHelper(AppContext.getInstance());
		return emailsDbHelper;
	}else {
		return emailsDbHelper;
	}
}
}
