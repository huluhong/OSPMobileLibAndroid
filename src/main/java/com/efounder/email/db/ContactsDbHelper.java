package com.efounder.email.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ContactsDbHelper extends SQLiteOpenHelper {

	// 鏁版嵁搴撳悕
	private static final String DBNAME = "contacts.db";
	// 褰撳墠鏁版嵁搴撶殑鐗堟湰
	private static final int DBVERSION = 1;
	final String CREATE_CONTACTS_TABLE = "create table if not exists contacts (_id integer ,"
			+ "name varchar(100), number varchar(100),sortkey varchar(20),sortletters varchar(20))";

	public ContactsDbHelper(Context context) {
		super(context, DBNAME, null, DBVERSION);

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_CONTACTS_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO 鑷姩鐢熸垚鐨勬柟娉曞瓨鏍?

	}
	
	public boolean deleteDatabase(Context context) { 
		return context.deleteDatabase(DBNAME); 
		} 

}
