package com.efounder.email.util;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.efounder.email.model.AttachFile;
import com.efounder.ospmobilelib.R;

import java.util.ArrayList;


public class AttaachDialog extends Dialog implements OnClickListener {
	Context context;
	int position;
	ArrayList<AttachFile> files;
	TextView title, delete, display;
	BaseAdapter gridAdapter;

	public AttaachDialog(Context context, int theme) {
		super(context, theme);

	}

	public AttaachDialog(Context context, int theme, int position,
			ArrayList<AttachFile> files, BaseAdapter gridAdapter) {
		super(context, theme);
		this.context = context;
		this.files = files;
		this.position = position;
		this.gridAdapter = gridAdapter;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.attachment_diolog);
		title = (TextView) findViewById(R.id.dialog_title);
		delete = (TextView) findViewById(R.id.dialog_delete);
		display = (TextView) findViewById(R.id.dialog_display);
		delete.setOnClickListener(this);
		display.setOnClickListener(this);

		title.setText(files.get(position).getAttachName());

	}

	@Override
	public void onClick(View v) {
		// 点击删除
		if (v.getId() == R.id.dialog_delete) {
			files.remove(position);
			gridAdapter.notifyDataSetChanged();
			dismiss();

		}
		// 点击预览
		if (v.getId() == R.id.dialog_display)
			try {
				Intent intent = FileUtil.openFile(files.get(position)
						.getAttachPath());
				context.startActivity(intent);

			} catch (Exception e) {
				Toast.makeText(context, "打开失败,没有找到此文件", Toast.LENGTH_SHORT)
						.show();
			} finally {
				dismiss();
			}

	}

}
