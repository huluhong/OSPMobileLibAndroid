package com.efounder.email.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.efounder.builder.base.data.EFDataSet;
import com.efounder.builder.base.data.EFRowSet;
import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;
import com.efounder.email.dao.AttachmentsDAO;
import com.efounder.email.dao.InboxEmailsDAO;
import com.efounder.email.db.EmailsDbHelper;
import com.efounder.email.model.Email;
import com.efounder.ospmobilelib.R;
import com.efounder.util.AndroidEnvironmentVariable;
import com.efounder.util.GetTokenDataByPO;
import com.efounder.util.GlobalMap;
import com.efounder.util.LoadingDataUtilBlack;
import com.efounder.utils.ResStringUtil;
import com.efounder.widget.PullToRefreshSwipeMenuListView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;

/**
 * 收件箱Activity
 *
 * @author yqs
 */
public class EmailInBoxAct extends Activity implements OnClickListener,
        OnItemClickListener {
    private static String TAG = "EmailInBoxAct";
    private int page = 1;// 邮箱第一页20条数据
    // private ImageView inboxWrite;// 写邮件
    private SwipeMenuItem readItem;// 侧滑已读按钮
    private EditText etSearch;// 搜索
    private ImageView ivClearText;// 搜索清除按钮
    // private ImageView backImageView;// 返回按钮
    private RelativeLayout shoujianRelativeLayout;
    private FrameLayout frameLayout;
    private SwipeMenuCreator creator;
    private PullToRefreshSwipeMenuListView listView;
    private SmartRefreshLayout mSrl;
    private ArrayList<Email> listems;
    private ListAdapter adapter;
    private int deleteTag = -1;// 删除位置标记
    private Handler mHandler;

    private Map<String, EFDataSet> map;
    private EFDataSet emailEFDataSet;
    String usereId = AndroidEnvironmentVariable.getUserID();
    InboxEmailsDAO inboxEmailsDAO;// 收件箱数据库操作类
    AttachmentsDAO attachmentsDAO;
    EmailsDbHelper dbHelper;
    private String emailType = null;
    private String boxType = null;
    // tokenid
    private String tokenId;
    private AsyncTask<Void, Void, JResponseObject> inboxTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.shoujianxiang_layout);
        etSearch = (EditText) findViewById(R.id.et_search);// 搜索文字编辑框
        ivClearText = (ImageView) findViewById(R.id.ivClearText);// 搜索框删除按钮
        frameLayout = (FrameLayout) findViewById(R.id.layoutContainer);
        listView = (PullToRefreshSwipeMenuListView) findViewById(R.id.inbox_listview);
        frameLayout.setOnClickListener(this);

        RelativeLayout include = (RelativeLayout) findViewById(R.id.include);
        include.setBackgroundResource(R.color.red_ios);
        LinearLayout leftbacklayout = (LinearLayout) findViewById(R.id.leftbacklayout);
        leftbacklayout.setVisibility(View.VISIBLE);
        TextView title = (TextView) findViewById(R.id.fragmenttitle);
        leftbacklayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dbHelper.close();
                finish();

            }
        });
        // 邮件类型 dpemail或者email
        emailType = getIntent().getStringExtra("emailtype");
        boxType = getIntent().getStringExtra("boxtype");
        if ("DPEMAIL".equals(emailType)) {
            title.setText(ResStringUtil.getString(R.string.mobile_lib_email_outbox_headquarters));
        } else if ("EMAIL".equals(emailType)) {
            title.setText(ResStringUtil.getString(R.string.mobile_lib_email_outbox_puguang));
        } else {
            title.setText(ResStringUtil.getString(R.string.mobile_lib_email_outbox));
        }
        tokenId = GlobalMap.getProperty(emailType + "TokenId", "");

        title.setTextColor(Color.WHITE);
        Button closeButton = (Button) findViewById(R.id.closeButton);
        closeButton.setVisibility(View.VISIBLE);
        closeButton.setBackgroundResource(R.drawable.inbox_write_white);
        closeButton.setOnClickListener(this);

        Log.i(TAG, "emailType:" + emailType);
        listems = new ArrayList<Email>();

        adapter = new ListAdapter(listems);
        listView.setAdapter(adapter);
        mSrl = (SmartRefreshLayout) findViewById(R.id.srl);
        mSrl.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                page = page + 1;
                Log.i(TAG, "page:" + page);

                final int lastItemid = listView.getLastVisiblePosition(); // 获取当前屏幕最后Item的ID
                loadInboxDataByNet(page);
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                inboxEmailsDAO.deleteDataByType(emailType);// 清空表中最近的20条邮件
                attachmentsDAO.deleteTableData(emailType, boxType);
                page = 1;
                loadInboxDataByNet(page);

                adapter.updateListView(listems);
            }
        });
        mHandler = new Handler();
        listView.setOnItemClickListener(this);
        // 左滑动
        leftSlide();
        initListener();

        // 加载总部收件箱数据
        dbHelper = new EmailsDbHelper(this);
        // dbHelper.deleteDatabase(this);//删除数据库
        inboxEmailsDAO = new InboxEmailsDAO(this);// 收件箱数据库操作类
        attachmentsDAO = new AttachmentsDAO(this);
        // if (inboxEmailsDAO.getEmailTypeCount(emailType, usereId) < 1) {

        loadInboxDataByNet(page);
        // } else {
        // System.out.println(inboxEmailsDAO.getEmailTypeCount(emailType,
        // usereId));
        // listems = inboxEmailsDAO.queryEmails(emailType, usereId);
        // adapter.updateListView(listems);
        //
        // }

    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        dbHelper.close();// 关闭数据库
        if (inboxTask != null) {
            inboxTask.cancel(true);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.closeButton) {// 写邮件
            Intent intent = new Intent(EmailInBoxAct.this, EmailWriteAct.class);
            intent.putExtra("emailtype", emailType);
            intent.putExtra("boxtype", boxType);
            startActivity(intent);
        } else if (id == R.id.inbox_back) {// 返回按钮
            dbHelper.close();// 关闭数据库
            finish();
        }
//        switch (id) {
//            // 写邮件
//
//            case R.id.closeButton:
//                Intent intent = new Intent(EmailInBoxAct.this, EmailWriteAct.class);
//                intent.putExtra("emailtype", emailType);
//                intent.putExtra("boxtype", boxType);
//                startActivity(intent);
//
//                break;
//            // 搜索框layout
//            case R.id.layoutContainer:
//
//                break;
//
//            // 返回按钮
//            case R.id.inbox_back:
//                dbHelper.close();// 关闭数据库
//                finish();
//                break;
//        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position1,
                            long id) {
        System.out.println(position1);
        deleteTag = position1;
        Email emaildata = adapter.getItem(position1);
        // Email emaildata = listems.get(position1);// 得到当前位置的邮件
        emaildata.setIsRead(true); // 邮件列表中position位置的邮件编辑为已读

        // listems.remove(position1);// 数组中移除当前邮件
        // listems.add(position1, emaildata);// 将设为已读的邮件插入当前位置

        inboxEmailsDAO.setIsRead(emaildata.getMessageID());
        adapter.notifyDataSetChanged();
        // 传递数据跳转到邮件详情界面
        Intent intent = new Intent(EmailInBoxAct.this, EmailDetailsAct.class);
        intent.putExtra("emailmap", emaildata);
        intent.putExtra("boxtype", boxType);
        intent.putExtra("emailtype", emailType);
        startActivityForResult(intent, 2);

    }

    /**
     * 邮件 listview适配器
     *
     * @author yqs
     */
    class ListAdapter extends BaseAdapter {
        List<Email> listems = new ArrayList<Email>();

        public ListAdapter(List<Email> listems) {
            this.listems = listems;
        }

        /**
         * 当ListView数据发生变化时,调用此方法来更新ListView
         *
         * @param list
         */
        public void updateListView(ArrayList<Email> list) {
            if (list == null) {
                this.listems = new ArrayList<Email>();
            } else {
                this.listems = list;
            }
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return listems.size();
        }

        @Override
        public Email getItem(int position) {
            return listems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(EmailInBoxAct.this,
                        R.layout.inbox_listitems, null);
                new ViewHolder(convertView);
            }
            ViewHolder holder = (ViewHolder) convertView.getTag();
            Email item = getItem(position);
            // holder.inboxImage.setImageDrawable(getResources().getDrawable(
            // item.getImage()));
            holder.inboxImage.setImageDrawable(getResources().getDrawable(
                    R.drawable.inbox_image));
            holder.inboxAddresser.setText(item.getFrom());
            holder.emailTheme.setText(item.getSubject());
            holder.emailtime.setText(item.getSentdata());

            if (item.getIsRead()) {
                holder.isReadImageView.setVisibility(View.INVISIBLE);
                holder.inboxAddresser.setTextColor(getResources().getColor(
                        R.color.black));
                // holder.emailTheme.setTextColor(getResources().getColor(
                // R.color.gray1));
            } else {
                holder.isReadImageView.setVisibility(View.VISIBLE);
                holder.inboxAddresser.setTextColor(getResources().getColor(
                        R.color.blue));
                // holder.emailTheme.setTextColor(getResources().getColor(
                // R.color.blue));

            }
            return convertView;
        }

        class ViewHolder {
            public ImageView inboxImage; // 收件箱用户头像
            public TextView inboxAddresser;// 发件人姓名
            public TextView emailTheme;// 邮件主题
            public TextView emailtime;// 发送时间
            public TextView emailContent;// 邮件内容
            private ImageView isReadImageView;// 是否已读

            public ViewHolder(View view) {
                inboxImage = (ImageView) view.findViewById(R.id.inboximage);
                inboxAddresser = (TextView) view
                        .findViewById(R.id.inbox_addresser);
                emailTheme = (TextView) view
                        .findViewById(R.id.inbox_emailtheme);

                emailtime = (TextView) view.findViewById(R.id.inbox_emailtime);
                emailContent = (TextView) view
                        .findViewById(R.id.inbox_emailcontent);
                isReadImageView = (ImageView) view.findViewById(R.id.isread);
                view.setTag(this);
            }
        }
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    /**
     * 左滑显示删除和标记未读
     */
    private void leftSlide() {
        creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                // 删除按钮
                SwipeMenuItem deleteItem = new SwipeMenuItem(EmailInBoxAct.this);
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                deleteItem.setWidth(dp2px(90));
                deleteItem.setTitle(ResStringUtil.getString(R.string.common_text_delete));
                deleteItem.setTitleSize(18);
                deleteItem.setTitleColor(Color.WHITE);
                menu.addMenuItem(deleteItem);
            }
        };

        // set creator
        listView.setMenuCreator(creator);

        // step 2. listener item click event

        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu,
                                           int index) {
                menu.removeMenuItem(readItem);
                switch (index) {
                    // case 0:
                    // // 如果已读
                    // if (listems.get(position).getIsRead()) {
                    // Email email = listems.get(position);
                    // email.setIsRead(false);
                    // listView.smoothOpenMenu(3);
                    // listems.remove(position);
                    // listems.add(position, email);
                    //
                    // // inboxEmailsDAO.deleteTableData();
                    // // inboxEmailsDAO.add(listems);
                    //
                    // adapter.notifyDataSetChanged();
                    //
                    // } else {
                    // Email email = listems.get(position);
                    // email.setIsRead(true);
                    //
                    // listems.remove(position);
                    // listems.add(position, email);
                    //
                    // // inboxEmailsDAO.deleteTableData();
                    // // inboxEmailsDAO.add(listems);
                    //
                    // adapter.notifyDataSetChanged();
                    // }
                    //
                    // break;
                    case 0:

                        // inboxEmailsDAO.deleteEmailFromTheme(listems.get(position)
                        // .getSubject());
                        showAlertDialog(position);
                        // Email deleteEmail = listems.get(position);
                        // deleteHeadEmail(deleteEmail.getMessageID());
                        // listems.remove(position);
                        // adapter.notifyDataSetChanged();
                        break;
                }
                return true;

            }
        });

    }

    private void showAlertDialog(final int position) {
        final Email deleteEmail = listems.get(position);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(ResStringUtil.getString(R.string.common_text_hint)).setMessage(ResStringUtil.getString(R.string.mobile_lib_email_delete))
                .setPositiveButton(ResStringUtil.getString(R.string.common_text_confirm), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        deleteHeadEmail(deleteEmail.getMessageID());
                        listems.remove(position);
                        adapter.notifyDataSetChanged();
                        dialog.dismiss();

                    }
                }).setNegativeButton(R.string.common_text_cancel, null).show();

    }

    /**
     * 搜索框监听器
     */
    private void initListener() {

        /** 清除输入字符 **/
        ivClearText.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                etSearch.setText("");
            }
        });
        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable e) {

                String content = etSearch.getText().toString();
                if ("".equals(content)) {
                    ivClearText.setVisibility(View.INVISIBLE);
                } else {
                    ivClearText.setVisibility(View.VISIBLE);
                }
                if (content.length() > 0) {

                    ArrayList<Email> fileterList = (ArrayList<Email>) search(content);
                    adapter.updateListView(fileterList);

                } else {

                    adapter.updateListView(listems);
                }
                listView.setSelection(0);

            }

        });

    }

    /**
     * 模糊查询
     *
     * @param str 搜索输入的字符
     * @return
     */
    private ArrayList<Email> search(String str) {
        ArrayList<Email> filterList = new ArrayList<Email>();// 过滤后的list

        for (Email email : listems) {
            if (email.getFrom() != null && email.getSubject() != null) {
                if (email.getFrom().contains(str)
                        || email.getSubject().contains(str)
                        || email.getFrom().contains(str)
                        || email.getSentdata().contains(str)) {
                    filterList.add(email);

                }
            }

        }

        return filterList;
    }

    /**
     * 根据得到的返回值删除邮件
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2 && resultCode == 2) {
            Boolean bool = false;
            bool = (Boolean) data.getSerializableExtra("deleteTag");
            if (bool) {

                deleteHeadEmail(listems.get(deleteTag).getMessageID());
                listems.remove(deleteTag);// deleteTag 是点击跳转的position
                adapter.notifyDataSetChanged();
            }

        }
    }

    /**
     * 从数据库中查询数据
     */
    public void getDataFromDb() {
        listems.removeAll(listems);
        if (listems.size() > 0) {
            System.out.println(listems.get(0).getToAdress());
            System.out.println(listems.get(0).getTo());
        }

    }

    /**
     * 加载收件箱数据
     */
    private void loadInboxDataByNet(final int pages) {
//		Log.i(TAG, "加在收件箱数据");
        if (inboxTask != null) {
            inboxTask.cancel(true);
        }
        inboxTask = new AsyncTask<Void, Void, JResponseObject>() {

            @Override
            protected void onPreExecute() {
                // TODO Auto-generated method stub
                super.onPreExecute();
                // 加载dialog显示
                LoadingDataUtilBlack.show(EmailInBoxAct.this);
            }

            @Override
            protected JResponseObject doInBackground(Void... params) {
                // 创建PO

                JParamObject PO = JParamObject.Create();
                JResponseObject RO = null;

                // tokenid
                tokenId = GlobalMap.getProperty(
                        emailType + "TokenId", "");
                if ("".equals(tokenId)) {
//					Log.i(TAG, "token不存在");
                    tokenId = GetTokenDataByPO.getEmailTokenid(emailType);
                }

                PO.SetValueByParamName("WB_SYS_KEY", emailType);
                PO.SetValueByParamName(emailType + "_systemmethod", boxType);
                PO.SetValueByParamName(emailType + "_params_PageIndex",
                        String.valueOf(pages));
                PO.SetValueByParamName(emailType + "_params_tokenIDFromMoblie",
                        tokenId);

                try {
                    // 连接服务器
                    RO = EAI.DAL.SVR("ExternalInterfaceDataService", PO);
                    System.out.println("..........");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return RO;
            }

            @SuppressWarnings({"unused", "unchecked"})
            @Override
            protected void onPostExecute(JResponseObject result) {
                super.onPostExecute(result);
                String usereId = AndroidEnvironmentVariable.getUserID();
                String userName = AndroidEnvironmentVariable.getUserID();
                String password = AndroidEnvironmentVariable.getPassword();

                if (pages == 1) {
                    listems.removeAll(listems);
                }
                if (result != null) {

                    map = result.getResponseMap();
                    if (map.containsKey(emailType)) {

                        emailEFDataSet = map.get(emailType);
                        if (emailEFDataSet != null) {
                            List<EFRowSet> list = emailEFDataSet
                                    .getRowSetArray();
                            EFDataSet oneDataSet = null;
                            // EFRowSet oneInfoRowSet = null;//一封邮件的
                            // 其中一条信息，比如邮件id
                            Map<String, Object> emailMap = new HashMap<String, Object>();

                            if (list != null) {

                                for (EFRowSet oneRowSet : list) {
                                    Email email = new Email();

                                    String emailId = (String) oneRowSet
                                            .getString("ID", "");
                                    String title = (String) oneRowSet
                                            .getString("Subject", "");
                                    String sender = (String) oneRowSet
                                            .getString("Sender", "");
                                    String sendTime = (String) oneRowSet
                                            .getString("DeleiverDate", "");

                                    Boolean isHaveAttach = (oneRowSet
                                            .getString("IsAttachment", ""))
                                            .equals("true") ? true : false;
                                    Boolean isRead = (oneRowSet.getString(
                                            "IsRead", "")).equals("true") ? true
                                            : false;
                                    String toAddress = usereId + "@sinopec.com";

                                    email.setMessageID(emailId);
                                    email.setSubject(title);
                                    email.setFrom(sender);
                                    email.setImage(R.drawable.inbox_image);
                                    email.setSentdata(sendTime);
                                    email.setIsRead(isRead);
                                    email.setIsHaveFile(isHaveAttach);
                                    email.setEmailType(emailType);
                                    email.setUserId(usereId);

                                    // 这几条数据不是必须的
                                    // email.setFromAddress(sender);
                                    // email.setTo(userName);
                                    // email.setToAdress(toAddress);
                                    // email.setContent(title);

                                    listems.add(email);

                                }
                            }

                        } else {
                            Toast.makeText(EmailInBoxAct.this, ResStringUtil.getString(R.string.common_text_no_more_data),
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(EmailInBoxAct.this, ResStringUtil.getString(R.string.common_text_no_data),
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(EmailInBoxAct.this, ResStringUtil.getString(R.string.common_text_no_more_data),
                            Toast.LENGTH_SHORT).show();
                }

                adapter.notifyDataSetChanged();
                mSrl.finishRefresh();
                mSrl.finishLoadMore();
                LoadingDataUtilBlack.dismiss();
                if (pages == 1) {
                    inboxEmailsDAO.saveSetEmail(listems);
                }

            }

            protected void onCancelled() {
                mSrl.finishRefresh();
                mSrl.finishLoadMore();
            }

        };
        inboxTask.executeOnExecutor(Executors.newCachedThreadPool());

    }

    /**
     * 根据id删除邮件
     *
     * @param messageID
     */
    private void deleteHeadEmail(final String messageID) {
//		Log.i(TAG, "删除邮件");
        new AsyncTask<Void, Void, JResponseObject>() {

            @Override
            protected void onPreExecute() {

                super.onPreExecute();

            }

            @Override
            protected JResponseObject doInBackground(Void... params) {
                // 创建PO

                JParamObject PO = JParamObject.Create();
                JResponseObject RO = null;

                tokenId = GlobalMap.getProperty(
                        emailType + "TokenId", "");
                if ("".equals(tokenId)) {
//					Log.i(TAG, "token不存在");
                    tokenId = GetTokenDataByPO.getEmailTokenid(emailType);
//					Log.i(TAG, "重新请求的token： " + tokenId);
                }

                PO.SetValueByParamName("WB_SYS_KEY", emailType);
                PO.SetValueByParamName(emailType + "_systemmethod",
                        "EmailDelete");
                PO.SetValueByParamName(emailType + "_params_ItemID", messageID);
                PO.SetValueByParamName(emailType + "_params_tokenIDFromMoblie",
                        tokenId);
                PO.SetValueByParamName(emailType + "_params_boxflag", "inbox");

                try {
                    // 连接服务器
                    RO = EAI.DAL.SVR("ExternalInterfaceDataService", PO);
                    System.out.println("..........");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return RO;
            }

            @Override
            protected void onPostExecute(JResponseObject result) {
                super.onPostExecute(result);

                if (result != null) {
                    inboxEmailsDAO.deleteEmailById(messageID);
                    Toast.makeText(EmailInBoxAct.this, ResStringUtil.getString(R.string.mobile_lib_email_delete_success),
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(EmailInBoxAct.this, ResStringUtil.getString(R.string.mobile_lib_email_delete_fails),
                            Toast.LENGTH_SHORT).show();
                }

                adapter.notifyDataSetChanged();

                LoadingDataUtilBlack.dismiss();

            }

            protected void onCancelled() {
                LoadingDataUtilBlack.dismiss();

            }

            ;

        }.executeOnExecutor(Executors.newCachedThreadPool());

    }

}
