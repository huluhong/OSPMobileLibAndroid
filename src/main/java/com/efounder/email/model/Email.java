
/**
 * @项目 Android_Demo_Mail
 * @包名 com.android.demo.entries
 * @文件 Email.java
 * @描述
 * @日期 2013-4-15
 * @版本 1.0
 */

package com.efounder.email.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @类名 Email
 * @作者 YULIANGMAX
 * @日期 2013-4-15
 * @版本 1.0
 */
public class Email implements Serializable {

	private static final long serialVersionUID = 1L;
	/**邮件id*/
	private String messageID;
	/** 发件人名字 */
	private String from;
	/** 发件人邮箱 */
	private String fromAddress;
	/** 收件人名字 */
	private String to;
	/** 收件人 */
	private String toAdress;
	
	/** 抄送人名字 */
	private String copyTo;
	/** 抄送人 */
	private String copyToAdress;
	
	/** 秘送人名字 */
	private String secretTo;
	/** 秘送人 */
	private String secretToAdress;
	/** 是否已读 */
	private Boolean isRead;
	/** 是否含有附件 */
	private Boolean isHaveFile;
	/** 邮件主题 */
	private String subject;
	/** 邮件发送时间 */
	private String sentdata;
	/** 邮件内容 */
	private String content;
	/** 附件信息 */
	private AttachFile[] attachFiles;
	/** 邮件类型 dpemail email */
	private String emailType;
	/** 用户id */
	private String userId;
	private List<AttachFile> fileList;
	private EmailUser fromUser;
	private List<EmailUser> toUsers;
	private List<EmailUser> copyToUsers;
	private List<EmailUser> secretToUsers;

	public EmailUser getFromUser() {
		return fromUser;
	}

	public void setFromUser(EmailUser fromUser) {
		this.fromUser = fromUser;
	}

	public List<EmailUser> getToUsers() {
		return toUsers;
	}

	public void setToUsers(List<EmailUser> toUsers) {
		this.toUsers = toUsers;
	}

	public List<EmailUser> getCopyToUsers() {
		return copyToUsers;
	}

	public void setCopyToUsers(List<EmailUser> copyToUsers) {
		this.copyToUsers = copyToUsers;
	}

	public List<EmailUser> getSecretToUsers() {
		return secretToUsers;
	}

	public void setSecretToUsers(List<EmailUser> secretToUsers) {
		this.secretToUsers = secretToUsers;
	}

	public String getEmailType() {
		return emailType;
	}

	public List<AttachFile> getFileList() {
		return fileList;
	}

	public void setFileList(List<AttachFile> fileList) {
		this.fileList = fileList;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}

	private String cc;
	private String bcc;
	
	
	private boolean replysign;
	private boolean html;
	private boolean news;
	private ArrayList<String> attachments;// 附件
	private String charset;

	private int image;//头像

	
	public Email() {
		super();
	}


	public Email(String messageId,String from, String fromAddress,String to,String toAdress,
			Boolean isRead, Boolean isHaveFile, String subject,
			String sentdata, String content,int image) {
		super();
		this.messageID = messageId;
		this.from = from;
		this.fromAddress = fromAddress;
		this.to = to;
		this.toAdress = toAdress;
		this.isRead = isRead;
		this.isHaveFile = isHaveFile;
		this.subject = subject;
		this.sentdata = sentdata;
		this.content = content;
		this.image = image;
	}


	public String getCopyTo() {
		return copyTo;
	}


	public void setCopyTo( String copyTo) {
		this.copyTo = copyTo;
	}


	public String getSecretTo() {
		return secretTo;
	}


	public void setSecretTo( String secretTo) {
		this.secretTo = secretTo;
	}


	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getToAdress() {
		return toAdress;
	}

	public void setToAdress( String toAdress) {
		this.toAdress = toAdress;
	}

	public Boolean getIsHaveFile() {
		return isHaveFile;
	}

	public void setIsHaveFile(Boolean isHaveFile) {
		this.isHaveFile = isHaveFile;
	}

	

	public int getImage() {
		return image;
	}

	public void setImage(int image) {
		this.image = image;
	}

	

	public String getMessageID() {
		return messageID;
	}

	public void setMessageID(String messageID) {
		this.messageID = messageID;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo( String to) {
		this.to = to;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSentdata() {
		return sentdata;
	}

	public void setSentdata(String sentdata) {
		this.sentdata = sentdata;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isReplysign() {
		return replysign;
	}

	public void setReplysign(boolean replysign) {
		this.replysign = replysign;
	}

	public boolean isHtml() {
		return html;
	}

	public void setHtml(boolean html) {
		this.html = html;
	}

	public boolean isNews() {
		return news;
	}

	public void setNews(boolean news) {
		this.news = news;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public ArrayList<String> getAttachments() {
		return attachments;
	}

	public void setAttachments(ArrayList<String> attachments) {
		this.attachments = attachments;
	}

	public Boolean getIsRead() {
		return isRead;
	}

	public void setIsRead(Boolean isRead) {
		this.isRead = isRead;
	}

	@Override
	public String toString() {
		// TODO 自动生成的方法存根
		return super.toString();
	}

}
