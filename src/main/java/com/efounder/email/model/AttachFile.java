package com.efounder.email.model;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

/**
 * 邮件附件 model
 * 
 * @author yqs
 *
 */
public class AttachFile implements Serializable {
	/** 附件名称 */
	private String attachName;
	/** 附件图片 */
	private Drawable attachImage;
	/** 附件大小 */
	private String attachSize;
	/** 附件路径 */
	private String attachPath;
	
	/** 附件id */
	private String id;
	/**邮箱类型 总部 普光 */
	private String emailType;
	/** 收件箱或者发件箱 */
	private String boxType;
	private String emailId;//邮件的id

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Drawable getAttachImage() {
		return attachImage;
	}

	public void setAttachImage(Drawable attachImage) {
		this.attachImage = attachImage;
	}

	public AttachFile() {
		super();
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getAttachName() {
		return attachName;
	}

	public void setAttachName(String attachName) {
		this.attachName = attachName;
	}

	public String getAttachSize() {
		return attachSize;
	}

	public void setAttachSize(String attachSize) {
		this.attachSize = attachSize;
	}

	public String getAttachPath() {
		return attachPath;
	}

	public void setAttachPath(String attachPath) {
		this.attachPath = attachPath;
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

	public String getBoxType() {
		return boxType;
	}

	public void setBoxType(String boxType) {
		this.boxType = boxType;
	}
	

}
