package com.efounder.email.model;

import java.io.Serializable;

/**
 * 邮件联系人
 *
 * @author yqs
 */
public class EmailUser implements Serializable {
    private int id;
    private String name;
    private String address;//邮箱地址

    public EmailUser(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public EmailUser(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public EmailUser() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
