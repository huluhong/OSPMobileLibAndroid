package com.efounder.bean;

import com.core.xml.StubObject;

import java.util.List;

/**
 * @author cimu
 * @date 2018/10/19
 * 配置文件配置菜单通用的bean
 */

public class CurrencyGroupBean {
    //三级菜单标题
    private String title;
    /**
     * 四级菜单StubObject的集合
     */
    private List<StubObject> gridView;
    /**
     * 菜单得图标
     */
    private String menuIcon;

    public String getMenuIcon() {
        return menuIcon;
    }

    public CurrencyGroupBean setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<StubObject> getGridView() {
        return gridView;
    }

    public void setGridView(List<StubObject> gridView) {
        this.gridView = gridView;
    }

    public static class gridViewBean {
        private String text;
        private int image;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public int getImage() {
            return image;
        }

        public void setImage(int image) {
            this.image = image;
        }
    }
}
