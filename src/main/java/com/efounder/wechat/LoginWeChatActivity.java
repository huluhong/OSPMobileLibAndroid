//package com.efounder.wechat;
//
//import android.support.v7.app.AlertDialog;
//import android.content.Intent;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.WindowManager;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageButton;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.efounder.activity.AbActivity;
//import com.efounder.activity.SettingActivity;
//import com.efounder.eai.data.JParamObject;
//import com.efounder.eai.data.JResponseObject;
//import com.efounder.forwechat.BaseApp;
//import com.efounder.ospmobilelib.R;
//import com.efounder.util.LoadingDataUtilBlack;
//import com.efounder.util.LoginMethod;
//import com.efounder.util.StorageUtil;
//import com.efounder.view.titlebar.AbTitleBar;
//import com.zcw.togglebutton.ToggleButton.OnToggleChanged;
//
//import java.util.ArrayList;
//
///**
// *
// * @author zhenglaikun 2014.11.26 登陆界面 初始化设置信息 后台连接 250,447行 调试处
// */
//public class LoginWeChatActivity extends AbActivity implements OnClickListener {
//	public static final String TAG = "LoginActivity";
//	// 下载资源文件时所使用的判断
//	private static final String KEY_INIT = "init";
//	private static final String KEY_UPDATE = "updateAndroid";
//	private static final String KEY_MOBILE_RESOURCE = "mobile_resource";
//
//	// private static final String KEY_MOBILE_RESOURCE = "tablet_resource";
//	private static final String KEY_VERSION = "version";
//	private static final String KEY_PATH = "path";
//
//	private static final int MSG_INIT_SHOW = 0x0001;
//	private static final int MSG_UPDATE_SHOW = 0x0002;
//	private static final int MSG_INIT_CLOSE = 0x0004;
//	private static final int MSG_UPDATE_CLOSE = 0x0008;
//	private static final int MSG_OPEN = 0x000c;
//	private boolean isForeground;
//
//	/**
//	 * 服务器端版本号
//	 *
//	 */
//	private String serverVersion;
//	/**
//	 * 数据下载地址
//	 */
//	private static String ServerURL;
//
//	/*
//	 * 得到预存的用户密码
//	 */
//	private String userName, passWord;
//	/*
//	 * 是否保存用户信息
//	 */
//	private boolean isSave = false;
//	/*
//	 * 是否自动登录
//	 */
//	private boolean isAuto = false;
//	/*
//	 * 保存按钮
//	 */
//	private Button login_Button;
//	/*
//	 * 设置信息初始化
//	 */
//	private String port, service, address, path, APPID, sign;
//	// private Boolean isADuser;
//	private boolean isSafe = false;
//	/*
//	 * 输入框
//	 */
//	private EditText editText_userName, editText_password;
//	private StorageUtil storageUtil;
//
//	private com.zcw.togglebutton.ToggleButton toggleButton_rememberPassword_newButton;
//	private com.zcw.togglebutton.ToggleButton toggleButton_autoButton;
//	private JParamObject PO;
//	private JResponseObject RO;
//
//	private LinearLayout loginLayout, loginLayoutHolder, loginSettingLayout;
//	private EditText userNameEdit, userPwdEdit;
//	private LinearLayout pwdRel;
//	private Button loginButton;
//	private ImageButton settingButton;
//	private TextView remberPwdText, autoLoginText, findbackPwd;
//
//	AbTitleBar mTitleBar;
//	//LoginAsyncTask loginAsyncTask;
//	String versionFromServer;
//	// 资源文件地址
//	String pathurl;
//	// 判断广播是否注册
//	boolean isBroadRegist = false;
//
//	/*将首页的界面元素传到下个界面*/
//	private ArrayList<Object> mainMenuList; // 主菜单
//	private ArrayList<Object> defaultMenus;
//	public static final String KEY_MENU_ROOT = "menuRoot";
//
//
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		// 自定义标题栏，requestWindowFeature必须在setContent之前
//
//		setContentView(R.layout.activity_login);
//
//		BaseApp.actManager.putActivity(TAG, this);
//
//		// 注册加载完表单网页的广播
//		// registerBoradcastReceiver();
//
//		loginLayout = (LinearLayout) findViewById(R.id.login_userandpass);
//		loginLayoutHolder = (LinearLayout) findViewById(R.id.login_userandpass_holder);
//		userNameEdit = (EditText) findViewById(R.id.login_usernameEdit);
//		userPwdEdit = (EditText) findViewById(R.id.login_passwordEdit);
//		pwdRel = (LinearLayout) findViewById(R.id.pwd_RelLayout);
//		loginSettingLayout = (LinearLayout) findViewById(R.id.bottom_setting);
//		// remberPwdText = (TextView)
//		// findViewById(R.id.textveiw_remberPassword);
//		// autoLoginText = (TextView) findViewById(R.id.textveiw_autoLogin);
//		findbackPwd = (TextView) findViewById(R.id.login_findBackPassword);
//
//		// 登陆按钮监听事件
//		login_Button = (Button) findViewById(R.id.login_loginButton);
//		login_Button.setOnClickListener(this);
//		// 设置按钮
//		settingButton = (ImageButton) findViewById(R.id.image_settingbutton);
//		settingButton.setOnClickListener(this);
//
//		WindowManager windowManager = getWindowManager();
//
//		// 初始化PO
//		// PO = new JParamObject();
//
//		storageUtil = new StorageUtil(getApplicationContext(), "storage");
//
//		// 用户名，密码
//		editText_userName = (EditText) findViewById(R.id.login_usernameEdit);
//		editText_password = (EditText) findViewById(R.id.login_passwordEdit);
//
//		// 记住用户名
//		// toggleButton_rememberPassword = (CheckBox)
//		// findViewById(R.id.login_toggleButton);
//		toggleButton_rememberPassword_newButton = (com.zcw.togglebutton.ToggleButton) findViewById(R.id.login_toggleButton_new);
//		// 自动登录
//		toggleButton_autoButton = (com.zcw.togglebutton.ToggleButton) findViewById(R.id.login_autoLogin);
//		//toggleButton_autoButton.setAnimate(false);
//		storageUtil.putBoolean("isSave", true);
//		/*
//		 * toggleButton_rememberPassword .setOnCheckedChangeListener(new
//		 * OnCheckedChangeListener() {
//		 *
//		 * @Override public void onCheckedChanged(CompoundButton buttonView,
//		 * boolean isChecked) { storageUtil.putBoolean("isSave", isChecked); }
//		 * });
//		 */
//
//		// 读取storageUtil内容
//		userName = storageUtil.getString("userName");
//		passWord = storageUtil.getString("passWord");
//		isSave = storageUtil.getBoolean("isSave", isSave);
//		isAuto = storageUtil.getBoolean("isAuto", isAuto);
//		// 如果选择记住密码，显示密码
//		if (isSave) {
//			toggleButton_rememberPassword_newButton.toggleOn();
//		}
//
//
//		// 初始化设置信息
//		initEnvironmenmt();
//
//		// 显示用户名，密码
//		if (userName != null) {
//			editText_userName.setText(userName);
//			// editText_password.setText(passWord);
//		}
//		if (isSave) {
//			// editText_userName.setText(userName);
//			editText_password.setText(passWord);
//		}
//
//		// 记住密码
//		toggleButton_rememberPassword_newButton
//				.setOnToggleChanged(new OnToggleChanged() {
//
//					@Override
//					public void onToggle(boolean on) {
//						//toggleButton_rememberPassword_newButton.setAnimate(true);
//						storageUtil.putBoolean("isSave", on);
//
//						if(!on)
//						storageUtil.putString("passWord", "");
//						storageUtil.commit();
//
//					}
//				});
//
//		toggleButton_autoButton.setOnToggleChanged(new OnToggleChanged() {
//
//			@Override
//			public void onToggle(boolean on) {
//				storageUtil.putBoolean("isAuto", on);
//				storageUtil.commit();
//
//			}
//		});
//
//		// 如果选择自动登录，按钮选中
//		if (isAuto) {
//			toggleButton_autoButton.toggleOn();
//
//			if (userName != null && !userName.equals("") && passWord != null
//					&& !passWord.equals("")) {
//				PO = JParamObject.Create();
//				PO.clear();
//				//LoginMethod.loginBegin(this, userName, passWord);
//				/*loginAsyncTask = new LoginAsyncTask();
//				loginAsyncTask.execute();*/
//			}
//		}
//
//	}
//
//	/*
//	 * 设置标题返回键，登陆按钮监听事件
//	 */
//	public void onClick(View v) {
//
//		int id = v.getId();
//
//		// 点击返回按钮，登陆按钮
//		/* if(v==mTitleBar.getLogoView()){ */
//		if (false) {
//			BaseApp.actManager.removeActivity(TAG);
//		} else if (id == R.id.login_loginButton) {
//
//			// 获取输入的用户名 密码
//			userName = editText_userName.getText().toString();
//			passWord = editText_password.getText().toString();
//
//			// 用户名为空
//			if (userName == null || userName.equals("")) {
//				AlertDialog.Builder builder = new AlertDialog.Builder(this);
//				builder.setTitle("确认");
//				builder.setPositiveButton("确认", null);
//				builder.setMessage("用户名为空");
//				builder.show();
//				return;
//			}
//
//			// 登录成功，保存 用户名、密码、保存用户按钮（true/false）
//			else {
//
//				// 初始化设置信息
//				// getSettingInfo();
//				// starAnActivity(LoginActivity.this, MainFrameActivity.class);
//				// 异步登陆操作
//			/*	if (loginAsyncTask != null) {
//					loginAsyncTask.cancel(true);
//					loginAsyncTask = null;
//					LoginAsyncTask loginAsyncTask = new LoginAsyncTask();
//					loginAsyncTask.execute();
//					return;
//				}*/
//				PO = JParamObject.Create();
//				PO.clear();
//				LoginMethod loginMethod = new LoginMethod(this);
//				loginMethod.loginBegin(LoginWeChatActivity.this, userName, passWord);
//				//LoginMethod.loginBegin(Login_withTitle.this, userName, passWord);
//				/*loginAsyncTask = new LoginAsyncTask();
//				loginAsyncTask.execute();*/
//			}
//		}
//
//		else if (id == R.id.image_settingbutton) {
//			Intent intent = new Intent(LoginWeChatActivity.this,
//					SettingActivity.class);
//			startActivity(intent);
//
//		}
//	}
//
//	@Override
//	protected void onPause() {
//		super.onPause();
//		// LoadingDataUtil.dismiss();
//		LoadingDataUtilBlack.dismiss();
//
//		// if (isBroadRegist)
//		// unregisterReceiver(mBroadcastReceiver);
//		isBroadRegist = false;
//		// this.finish();
//	}
//
//	@Override
//	protected void onResume() {
//		isForeground = true;
//		Log.i("test broadcast.......", isBroadRegist + "");
//		super.onResume();
//
//		// LoadingDataUtil.dismiss();
//	}
//
//	/*
//	 * 初始化设置信息
//	 */
//	public void initEnvironmenmt() {
//		// 设置存储的信息
//		getSettingInfo();
//		/*
//		 * if (port == null || port.equals("") || service == null ||
//		 * service.equals("") || address == null || address.equals("") || APPID
//		 * == null || APPID.equals("") || sign == null || sign.equals("") ||
//		 * path == null || path.equals("")) {
//		 */
//		// Toast.makeText(this, "请检查设置中的配置信息是否正确", 0).show();
//		// return;
//		// 服务地址
//		/*
//		 * storageUtil.putString("address", "zyesp.zyof.com.cn"); address =
//		 * "zyesp.zyof.com.cn";
//		 */
//		// XXX 1.服务地址 -- https://pgyd.zyof.com.cn --端口：80 443
//		address = "pgyd.zyof.com.cn";
//		// address = "10.80.0.104";
//		storageUtil.putString("address", address);
//		// XXX 2.服务端口
//		port = "443";
//		// port = "8080";
//		storageUtil.putString("port", port);
//		// 服务路径
//		storageUtil.putString("path", "EnterpriseServer");
//		path = "EnterpriseServer";
//		// 服务名称
//		storageUtil.putString(KEY_SETTING_APPID, "PGYDYYM");
//		APPID = "PGYDYYM";
//
//		// 数据服务
//		storageUtil.putString("service", "ZYYT_DB01");
//		service = "ZYYT_DB01";
//		// 数据标示
//		// storageUtil.putString("sign", "ZYHYGL01");
//		storageUtil.putString("sign", "ZYHYGL01");
//		sign = "ZYHYGL01";
//		// XXX 3.是否安全登录
//		isSafe = true;
//		// isSafe = false;
//		storageUtil.putBoolean("isSafe", isSafe);
//		storageUtil.commit();
//
//		// }
//	}
//
//	/*
//	 * 得到设置存储的信息
//	 */
//	public void getSettingInfo() {
//		port = storageUtil.getString("port");
//		address = storageUtil.getString("address");
//		service = storageUtil.getString("service");
//		APPID = storageUtil.getString(KEY_SETTING_APPID);
//		path = storageUtil.getString("path");
//		sign = storageUtil.getString("sign");
//		isSafe = storageUtil.getBoolean("isSafe", true);
//	}
//
//
//}
