package com.efounder.forwechat;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.multidex.MultiDex;
import android.util.Log;

import com.efounder.chat.service.OSPService;
import com.efounder.chat.service.WakeReceiver;
import com.efounder.chat.service.WatchReceiver;
import com.efounder.chat.service.WatchService;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.utils.EFFrameUtils;
import com.efounder.http.EFHttpRequest;
import com.efounder.util.ActivityTaskManager;
import com.marswin89.marsdaemon.DaemonClient;
import com.marswin89.marsdaemon.DaemonConfigurations;

import java.io.File;
import java.util.Map;

import static com.efounder.frame.utils.Constants.CHAT_PASSWORD;
import static com.efounder.frame.utils.Constants.CHAT_USER_ID;
import static com.efounder.frame.utils.Constants.KEY_SETTING_APPID;
import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_ADDRESS;
import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_HTTP_TYPE;
import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_PATH;
import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_PORT;


public class BaseAppdaqing extends com.efounder.util.AppContext{

	private static final String TAG = "BaseApp"; /** 当前Activity **/

	/**
	 * activity管理类
	 */
	public static ActivityTaskManager actManager = ActivityTaskManager.getInstance(); // Activity管理类
	public static Context context;

	public static String UserInfo = "userinfo";
	public static String UserInfo_ID = "userinfo_id";
	public static String UserInfo_PWD = "userinfo_pwd";
	public static String UserInfo_EMAIL = "userinfo_email";
	public static String UserInfo_PHONENUMBER = "userinfo_phoneNumber";
	public static String UserInfo_NAME = "userinfo_name";
	public static String UserInfo_LOGINNAME = "userinfo_loginname";
	public static String ISOpen_PWD = "userinfo_PWDS";
	public static String PassState = "PassState";
	public static String LoginState = "LoginState";
	private  Map<String, Object> qxMap;

	/**
	 * 移除相对应的activity
	 * @param curActivityTag
	 */
	public static void exitActivity(String curActivityTag) {
		if (BaseApp.actManager.containsName(curActivityTag)) {
			BaseApp.actManager.removeActivity(curActivityTag);
		}
	}

	private DaemonClient mDaemonClient;

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		mDaemonClient = new DaemonClient(createDaemonConfigurations());
		mDaemonClient.onAttachBaseContext(base);

		Log.d( "loadDex", "App attachBaseContext ");
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {//>=5.0的系统默认对dex进行oat优化
			MultiDex.install (this );
		} else {
			return;
		}
	}

	private DaemonConfigurations createDaemonConfigurations(){
		DaemonConfigurations.DaemonConfiguration configuration1 = new DaemonConfigurations.DaemonConfiguration(
			"com.efounder.liveapp:gray", OSPService.class.getCanonicalName(), WakeReceiver.class.getCanonicalName()
		);

		DaemonConfigurations.DaemonConfiguration configuration2 = new DaemonConfigurations.DaemonConfiguration(
				"com.efounder.liveappDQ.Remote", WatchService.class.getCanonicalName(), WatchReceiver.class.getCanonicalName()
		);

		DaemonConfigurations.DaemonListener listener = new MyDaemonListener();

		return new DaemonConfigurations(configuration1, configuration2, listener);
	}

	class MyDaemonListener implements DaemonConfigurations.DaemonListener{
		@Override
		public void onPersistentStart(Context context) {
		}

		@Override
		public void onDaemonAssistantStart(Context context) {
		}

		@Override
		public void onWatchDaemonDaed() {
		}
	}


	@Override
	public void onCreate() {
		super.onCreate();
		Log.i(TAG, "--------onCreate");
		String userId = EnvironmentVariable.getProperty(CHAT_USER_ID);
		String password = EnvironmentVariable.getProperty(CHAT_PASSWORD);
		Log.e("--",TAG + "-----userId:" + userId + ",password:" + password);
		EnvironmentVariable.setProperty(KEY_SETTING_APPID,"OSPMobileLiveAppDQ");
		EnvironmentVariable.setProperty("PublicNumberUrl","http://47.89.191.21:8080");
		EnvironmentVariable.setProperty(KEY_SETTING_DEFAULT_SERVER_HTTP_TYPE,"http");
		EnvironmentVariable.setProperty(KEY_SETTING_DEFAULT_SERVER_ADDRESS,"47.89.191.21");
		EnvironmentVariable.setProperty(KEY_SETTING_DEFAULT_SERVER_PORT,"8080");
		EnvironmentVariable.setProperty(KEY_SETTING_DEFAULT_SERVER_PATH,"ESTMobile");

		/*MCRSDK.init();
        RtspClient.initLib();
        MCRSDK.setPrint(1, null);*/
		initVolley();
		//UtilAudioPlay.loadCameraInfo(this);
		initPDFView();

//		this.startService(new Intent(this, OSPService.class));

		initFloder();
//		initImageLoader(getApplicationContext());
		//记录错误信息
//		CrashHandler crashHandler = CrashHandler.getInstance();
//		crashHandler.init(this);

		registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
			@Override
			public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
				EFFrameUtils.setCurrentActivity(activity);
			}

			@Override
			public void onActivityStarted(Activity activity) {

			}

			@Override
			public void onActivityResumed(Activity activity) {
				if (!activity.equals(EFFrameUtils.getCurrentActivity())){
					EFFrameUtils.setCurrentActivity(activity);
				}
			}

			@Override
			public void onActivityPaused(Activity activity) {

			}

			@Override
			public void onActivityStopped(Activity activity) {

			}

			@Override
			public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

			}

			@Override
			public void onActivityDestroyed(Activity activity) {

			}
		});

	}
	/**
	 * 初始化网络访问类 操作
	 */
	private void initVolley(){
		EFHttpRequest.init(this);
	}

	/**
	 * pdf控件的相关初始化
	 */
	private void initPDFView(){
		context = getApplicationContext();
       /* EBookDroidApp.context = getApplicationContext();

        EmergencyHandler.init(this);
        LogContext.init(this);
        SettingsManager.init(this);
        CacheManager.init(this);*/
	}


	public  Map<String, Object> getQxMap() {
		return qxMap;
	}
	public  void setQxMap(Map<String, Object> qxMap) {
		this.qxMap = qxMap;
	}
//	private static void initImageLoader(Context context) {
		// This configuration tuning is custom. You can tune every option, you may tune some of them,
		// or you can create default configuration by
		//  ImageLoaderConfiguration.createDefault(this);
		// method.
//		ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
//		config.threadPriority(Thread.NORM_PRIORITY - 2);
//		config.denyCacheImageMultipleSizesInMemory();
//		config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
//		config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
//		config.discCacheFileCount(60);// 缓存文件的最大个数
//		config.tasksProcessingOrder(QueueProcessingType.LIFO);
//		config.writeDebugLogs(); // Remove for release app
//
//		// Initialize ImageLoader with configuration.
//		ImageLoader.getInstance().init(config.build());
//	}

	private void initFloder() {
		String filePath = Environment.getExternalStorageDirectory().getPath()
				+ "/ESPMobile/";
		File file = new File(filePath);
		if (!file.exists()) {
			file.mkdirs();
		}

	}

	@Override
	public void onTerminate() {
		// 程序终止的时候执行
		Log.i(TAG, "--------onTerminate");
		super.onTerminate();
	}
	@Override
	public void onLowMemory() {
		// 低内存的时候执行
		Log.i(TAG, "--------onLowMemory");
		super.onLowMemory();
	}
	@Override
	public void onTrimMemory(int level) {
		// 程序在内存清理的时候执行
		Log.i(TAG, "--------onTrimMemory");
		super.onTrimMemory(level);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		Log.i(TAG, "--------onConfigurationChanged");
		super.onConfigurationChanged(newConfig);
	}

}
