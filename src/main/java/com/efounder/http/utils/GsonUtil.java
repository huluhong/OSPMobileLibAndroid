package com.efounder.http.utils;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
/**
 * Gson解析工具类
 * @author long
 *2015年8月4日10:36:25
 */
public class GsonUtil {
	private static String TAG = "GsonUtil";
	private static Gson gson = new GsonBuilder()
			.excludeFieldsWithoutExposeAnnotation().create();



	public static String str2Json(Object obj){
		return gson.toJson(obj);
	}
	
	public static  Object json2Object(String jsonStr, Class<?> cl) {
		Object obj = null;
		try {
			obj = gson.fromJson(jsonStr, cl);
		} catch (JsonSyntaxException e) {
			Log.e(TAG, "------json  is   wrong----------");
		}
		
		return obj;
	}
	
	public static <T> T fromJson(String json,TypeToken<T> token) {
		try {
			return gson.fromJson(json, token.getType());
		} catch (Exception ex) {
			Log.e(TAG, "------json  is   wrong----------");
			return null;
		}
	}
	
}