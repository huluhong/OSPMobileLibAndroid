package com.efounder.http.utils;

import com.android.volley.DefaultRetryPolicy;
/**
 * 设置超时时间
 * @author long
 *
 */
public class RetryPolicyFactory {

	public static final int GET = 0;
	public static final int POST = 1;
	public static final int PUT = 2;
	public static final int DELETE = 3;

	public static DefaultRetryPolicy create(int type) {
		if (type == POST) {
			return getPubRetryPolicy();
		}
		return getPubRetryPolicy();
	}

	private static DefaultRetryPolicy getPubRetryPolicy() {
		//延时15秒
		return new DefaultRetryPolicy(15000,0,0);
	}
}