package com.efounder.http.utils;

import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;
import com.efounder.util.GlobalMap;

public class GetContractData {
	static String usereId;// 用户id
	static String userName;// 用户名
	static String password;// 用户密码


	/**
	 * 获取合同详情二级数据
	 *
	 * @return
	 */
	public  static JResponseObject getSecondDate(String value) {
		// 创建PO
		JParamObject PO = JParamObject.Create();
		JResponseObject RO = null;
		String tokenConId = GlobalMap.getProperty("CONTRACTId", "");

		PO.SetValueByParamName("WB_SYS_KEY", "CONTRACT");

		PO.SetValueByParamName("CONTRACT_params_searchResult", value);
		PO.SetValueByParamName("CONTRACT_params_tokenIDFromMoblie",tokenConId);
		PO.SetValueByParamName("CONTRACT_systemmethod", "GetFormDetailHandler");


		try {
			// 连接服务器
			RO = EAI.DAL.SVR("ExternalInterfaceDataService", PO);
			System.out.println("..........");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return RO;
	}
	/**
	 * 获取合同html二级数据
	 *
	 * @return
	 */
	public static JResponseObject gethtmlDate(String value) {
		// 创建PO
		JParamObject PO = JParamObject.Create();
		JResponseObject RO = null;
		String tokenConId = GlobalMap.getProperty("CONTRACTId", "");

		PO.SetValueByParamName("WB_SYS_KEY", "CONTRACT");
		PO.SetValueByParamName("CONTRACT_params_name", "FLOW");
		PO.SetValueByParamName("CONTRACT_params_value",value);
		PO.SetValueByParamName("CONTRACT_params_tokenIDFromMoblie",tokenConId);
		PO.SetValueByParamName("CONTRACT_systemmethod", "GetHtmlDetailHandler");


		try {
			// 连接服务器
			RO = EAI.DAL.SVR("ExternalInterfaceDataService", PO);
			System.out.println("..........");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return RO;
	}



}
