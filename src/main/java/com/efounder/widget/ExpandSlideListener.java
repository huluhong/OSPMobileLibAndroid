package com.efounder.widget;

import static com.nineoldandroid.view.ViewHelper.setTranslationX;

import com.nineoldandroid.animation.Animator;
import com.nineoldandroid.view.ViewHelper;
import com.nineoldandroid.view.ViewPropertyAnimator;

import androidx.core.view.MotionEventCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.view.View.OnTouchListener;
import android.widget.AbsListView;
/**
 * listtener 
 * @author cyy
 *
 */
public class ExpandSlideListener implements OnTouchListener{

	private static final String TAG = "ExpandSlideListener";
	
	private static final int animateTime =  200; //锷ㄧ敾镞堕棿
	
	private static final int INVALID_POINTER = -1;
	
	private static final int SLIDING_STATE_NONE = 0;// no sliding
	private static final int SLIDING_STATE_MANUAL = 1;// manual sliding
	private static final int SLIDING_STATE_AUTO = 2;// auto sliding
 
	private int mTouchSlop;
	private int mScrollState;
	private int mDownPosition ; //鎸変笅镞剁殑item浣岖疆
	private int mDownMotionX; 
	
	private MyExpandlistView expandSlideListView;
	
	private int mActivePointerId;
	private int delatX = 0;
	private int downPositionX; //镣瑰向鏄殑x浣岖疆
	
	private SlideItem mSlideItem;
	
	private VelocityTracker mVelocityTracker;
	
	public ExpandSlideListener(MyExpandlistView expandSlideListView){
		this.expandSlideListView = expandSlideListView;
		ViewConfiguration configuration = ViewConfiguration.get(expandSlideListView.getContext());
		mTouchSlop = configuration.getScaledTouchSlop();
	}
	
	//镊姩鍏抽棴镓揿紑镄刬tem
	public void closeOpenedItem() {
		if (isOpend()) {
			autoScroll(mSlideItem.offset, false);
		}
	}
	
	void reset() {
		mSlideItem = null;
		mScrollState = SLIDING_STATE_NONE;
	}
	
	//寰楀埌镓揿紑镄刬tem
	public int getOpendPosition() {
		if (isOpend()) {
			return mSlideItem.position;
		}
		return AbsListView.INVALID_POSITION;
	}
	//鏄惁镓揿紑钟舵??
	public boolean isOpend() {
		return mSlideItem != null && mSlideItem.isOpen();
	}
	
	/**
	 * 镊姩婊氩姩  true> open  false > close
	 * @param offset
	 * @param toOpen
	 */
	private void autoScroll(final int offset, final boolean toOpen) {
		mScrollState = SLIDING_STATE_AUTO;
		
		int moveTo = 0;
		
		moveTo = toOpen ? mSlideItem.MaxOffsetX : 0;
		
		if(mSlideItem.leftView !=  null){
//			//宸︿晶婊氩姩
			 ViewPropertyAnimator.animate(mSlideItem.leftView).translationX(-moveTo).setDuration(animateTime);
		}
		//鍓嶆櫙婊氩姩
		ViewPropertyAnimator.animate(mSlideItem.frontView).translationX(-moveTo).setDuration(animateTime).setListener(new Animator.AnimatorListener() {
			
			@Override
			public void onAnimationStart(Animator arg0) {
				// TODO Auto-generated method stub
				
				
			}
			
			@Override
			public void onAnimationRepeat(Animator arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animator arg0) {
				// TODO Auto-generated method stub
				if(mSlideItem == null){
					throw new NullPointerException("SlideItem === null");
				}
				
				if(toOpen){
					mSlideItem.offset = -mSlideItem.MaxOffsetX;
				}else{
					mSlideItem.offset = 0;
				}
				
				finishScroll();
			}
			
			@Override
			public void onAnimationCancel(Animator arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	/**
	 * 瀹屾垚婊氩姩
	 */
	private void finishScroll(){
		mScrollState = SLIDING_STATE_NONE;
		if(mSlideItem.preOffsetX != mSlideItem.offset){
			if(mSlideItem.preOffsetX != 0){
				boolean left = mSlideItem.preOffsetX > 0 && mSlideItem.preOffsetX <= mSlideItem.MaxOffsetX;
				
				//鍏抽棴锲炶皟鏂规硶
				//expandSlideListView.onClosed(mSlideItem.position, left);
			}
			if (mSlideItem.offset != 0) {// Current sliding has open left or
				// right back view.So wo should
				// norify opend
				boolean left = mSlideItem.offset > 0 && mSlideItem.offset <= mSlideItem.MaxOffsetX;
				//镓揿紑锲炶皟
				//expandSlideListView.onOpend(mSlideItem.position, left);
			}
		}
		
		if (mSlideItem.offset != 0) {
			//mSlideItem.frontView.setOpend(true);
			mSlideItem.preOffsetX = mSlideItem.offset;
			mSlideItem.preDelatX = 0;
		} else {
			//mSlideItem.frontView.setOpend(false);
//			mSlideItem.child.setLeftBackViewShow(false);
//			mSlideItem.child.setRightBackViewShow(false);
			mSlideItem = null;
		}
		
	}
	
	boolean onInterceptTouchEvent(MotionEvent ev){
		int action = MotionEventCompat.getActionMasked(ev);
		
		switch (action) {
		case MotionEvent.ACTION_DOWN:
			if(isInSliding()){
				return true;
			}
			mDownPosition = AbsListView.INVALID_POSITION;
			mDownMotionX = 0;
			mActivePointerId = INVALID_POINTER;
			int position = expandSlideListView.pointToPosition((int) ev.getX(), (int) ev.getY());
			
			if (position == AbsListView.INVALID_POSITION) {
				break;
			}
			boolean allowSlide = expandSlideListView.getAdapter().isEnabled(position)
					&& expandSlideListView.getAdapter().getItemViewType(position) >= 0;
			if (allowSlide) {
				mDownPosition = position;
				mActivePointerId = ev.getPointerId(0);
				mDownMotionX = (int) ev.getX();
				initOrResetVelocityTracker();
				mVelocityTracker.addMovement(ev);
			}
			
			break;

		case MotionEvent.ACTION_MOVE:
			if (mDownPosition == AbsListView.INVALID_POSITION) {
				break;
			}
//			if (expandSlideListView.isInScroll()) {
//				break;
//			}
			int pointerIndex = getPointerIndex(ev);
			// get scroll speed
			initVelocityTrackerIfNotExists();
			mVelocityTracker.addMovement(ev);
			mVelocityTracker.computeCurrentVelocity(1000);
			float velocityX = Math.abs(mVelocityTracker.getXVelocity(mActivePointerId));
			float velocityY = Math.abs(mVelocityTracker.getYVelocity(mActivePointerId));
			// whether is scroll on x axis
			boolean isScrollX = velocityX > velocityY;
			
			int distance = Math.abs((int) ev.getX(pointerIndex) - mDownMotionX);

			if (isScrollX && distance > mTouchSlop) {
				ViewParent parent = expandSlideListView.getParent();
				if (parent != null) {
					parent.requestDisallowInterceptTouchEvent(true);
				}
				mScrollState = SLIDING_STATE_MANUAL;
				return true;
			}
			
			break;
		default:
			break;
		}
		return false;
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		
		if (!expandSlideListView.isEnabled()) {
			return false;
		}
		// TODO Auto-generated method stub
		int action = MotionEventCompat.getActionMasked(event);
		switch (action) {
		case MotionEvent.ACTION_DOWN:
			
			if(isInSliding()){
				return true;
			}	
			
			break;
	
		case MotionEvent.ACTION_MOVE:
			Log.d(TAG, "ontouch move");
		//	if()
			Log.d(TAG, ">>>>position ====="+ mDownPosition);
			if(mDownPosition == AbsListView.INVALID_POSITION){
				break;
			}
			
			View view = expandSlideListView.getChildAt(mDownPosition - expandSlideListView.getFirstVisiblePosition());
			if  ( !(view instanceof CoustomerItemView)) {
				break;
			}
			
			int pointerIndex = getPointerIndex(event);
			if (mScrollState == SLIDING_STATE_MANUAL) {
				if (mSlideItem == null) {
					
					mSlideItem = new SlideItem(mDownPosition);
				}
				
				int deltaX = (int) event.getX(pointerIndex) - mDownMotionX;
				int nextOffset = deltaX - mSlideItem.preDelatX + mSlideItem.offset;
				mSlideItem.preDelatX = deltaX;
				if (nextOffset > mSlideItem.minOffsetX) {
					nextOffset = mSlideItem.minOffsetX;
				}
				//纭缭鏄线宸﹀垝链夋晥
				if (Math.abs(nextOffset) > mSlideItem.MaxOffsetX&&nextOffset <= mSlideItem.minOffsetX) {
					nextOffset = -mSlideItem.MaxOffsetX;
				}
				if (mSlideItem.offset != nextOffset) {
					mSlideItem.offset = nextOffset;
					move(nextOffset);
				}
				return true;
				
			}else{
				initVelocityTrackerIfNotExists();
				mVelocityTracker.addMovement(event);
				mVelocityTracker.computeCurrentVelocity(1000);
				float velocityX = Math.abs(mVelocityTracker.getXVelocity(mActivePointerId));
				float velocityY = Math.abs(mVelocityTracker.getYVelocity(mActivePointerId));
				// whether is scroll on x axis
				boolean isScrollX = velocityX > velocityY;
				// get scroll distance
				int distance = Math.abs((int) event.getX(pointerIndex) - mDownMotionX);

				if (isScrollX && distance > mTouchSlop) {
					ViewParent parent = expandSlideListView.getParent();
					if (parent != null) {
						parent.requestDisallowInterceptTouchEvent(true);
					}
					mScrollState = SLIDING_STATE_MANUAL;
					return true;
				}
			}
			
			break;
			
		case MotionEvent.ACTION_UP:
			
			int offsetX = (int) (event.getX() - delatX);
			Log.d(TAG, "ontouch up"+ offsetX +" "+delatX);
			
			if (mDownPosition == AbsListView.INVALID_POSITION) {
				break;
			}
			if (mSlideItem == null) {
				break;
			}
			
			if (mScrollState == SLIDING_STATE_MANUAL) {
				int pointIndex = getPointerIndex(event);

				int deltaX = (int) event.getX(pointIndex) - mDownMotionX;
				if (deltaX == 0) {// sliding distance equals 0
					reset();
					return true;
				}
				
				if (mSlideItem.offset == 0 || mSlideItem.offset == -mSlideItem.minOffsetX || mSlideItem.offset == -mSlideItem.MaxOffsetX) {
					finishScroll();
					return true;
				}
				
				boolean doOpen = false;// 寮?杩樻槸鍏?
				
				boolean distanceGreater = Math.abs(mSlideItem.offset - mSlideItem.preOffsetX) > Math.abs(mSlideItem.MaxOffsetX)
						/ (float) 4;
				if (mSlideItem.offset - mSlideItem.preOffsetX > 0) {
					doOpen = distanceGreater;
				} else {
					doOpen = !distanceGreater;
				}
					
				autoScroll(mSlideItem.offset, doOpen);
				return true;
			}else{
				//if (mSlideListView.isInScrolling()) {
				//	closeOpenedItem();
				//}
			}
			
			break;
			
			
		default:
			mScrollState = SLIDING_STATE_NONE;
			break;
		}
		
		return false;
	}
	
	// 浣岖Щ鍒跺畾镄勮窛绂?
	private void move(int offset){
		ViewHelper.setTranslationX(mSlideItem.frontView, offset);
		if (mSlideItem.leftView != null) {
			//mSlideItem.child.setLeftBackViewShow(true);
			//SlideAction leftAction = mSlideListView.getSlideLeftAction();
			//if (leftAction == SlideAction.SCROLL) {
				setTranslationX(mSlideItem.leftView,  offset);
		//	}
		}
//		if (mSlideItem.rightBackView != null) {
//			mSlideItem.child.setRightBackViewShow(false);
//		}
	}
	
	//鍒ゆ柇鏄惁鍦ㄦ粴锷?
	boolean isInSliding() {
		return mScrollState != SLIDING_STATE_NONE;
	}
	
	private void initOrResetVelocityTracker() {
		if (mVelocityTracker == null) {
			mVelocityTracker = VelocityTracker.obtain();
		} else {
			mVelocityTracker.clear();
		}
	}

	private void initVelocityTrackerIfNotExists() {
		if (mVelocityTracker == null) {
			mVelocityTracker = VelocityTracker.obtain();
		}
	}
	
	private int getPointerIndex(MotionEvent event) {
		int pointerIndex = event.findPointerIndex(mActivePointerId);
		if (pointerIndex == INVALID_POINTER) {
			pointerIndex = 0;
			mActivePointerId = event.getPointerId(pointerIndex);
		}
		return pointerIndex;
	}

	
	private class SlideItem{
		
		private final int position; // 婊戝姩item镄勪綅缃?
		
		private CoustomerItemView mSlideItem;
		
		private int preDelatX;
		
		private int offset; //璁板綍链?缁堢殑浣岖Щ
		
		private int preOffsetX;//涓娄竴娆＄殑浣岖Щ 
		
		private int nextOffsetX; //涓嬩竴娆＄殑浣岖Щ
		
		private final int minOffsetX;
		
		private final int MaxOffsetX;
		
		private View leftView;
		
		private View frontView;
		
		
		public SlideItem(int position){
			this.position = position;
			
			mSlideItem = (CoustomerItemView) expandSlideListView.getChildAt(position - expandSlideListView.getFirstVisiblePosition());
			if(mSlideItem == null){
				throw new NullPointerException("mSlideitem == null");
			}
			leftView = mSlideItem.leftView;
			frontView = mSlideItem.frontView;
			minOffsetX = 0;
			MaxOffsetX = mSlideItem.leftView.getWidth();
		}
		
		private Boolean isOpen(){
			return offset != 0;
		}
	}

}
