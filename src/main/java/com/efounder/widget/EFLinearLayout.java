package com.efounder.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.efounder.chat.widget.BadgeView;
import com.efounder.interfaces.Ibadge;
import com.efounder.ospmobilelib.R;

/**
 * Created by lch on 2017/6/5.
 */

public class EFLinearLayout extends LinearLayout implements Ibadge {
    private int chatId;
    private BadgeView badgeView;
    private Context context;
    private int badgeCount;

    public EFLinearLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EFLinearLayout(Context context) {
        this(context, null);
    }

    /**
     * 获得我自定义的样式属性
     *
     * @param context
     * @param attrs
     * @param defStyle
     */
    public EFLinearLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;

    }

    @Override
    public void setChatId(int chatId) {
        this.chatId = chatId;
    }

    @Override
    public void initBadgeView(BadgeView badgeView) {
        this.badgeView = badgeView;

    }

    @Override
    public BadgeView getBadgeView() {
        if (this.badgeView == null) {
            badgeView = new BadgeView(context, this);
            badgeView.setBadgeMargin(0, 0);
            badgeView.setTextSize(13);
        }
        return this.badgeView;
    }

    @Override
    public void setBadgeCount(int badgeCount) {
        this.badgeCount = badgeCount;
        badgeView.setBadgeMargin(0, 0);
        badgeView.setTextSize(13);
        if (badgeCount > 99) {
            badgeView.setText("99+");
        } else {
            badgeView.setText(badgeCount + "");
        }
        //ShortcutBadger.applyCount(context, badgeCount);
        badgeView.setBadgeBackgroundColor(getResources().getColor(
                R.color.chat_red));
        if (badgeCount <= 0 && badgeCount != -1) {
            badgeView.setVisibility(View.INVISIBLE);
        } else {
            badgeView.show();
        }
    }

    @Override
    public int getBadgeCount() {
        return badgeCount;
    }
}
