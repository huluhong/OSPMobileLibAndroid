package com.efounder.widget;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.efounder.activity.NoticeDetailActivity;
import com.efounder.builder.base.data.EFRowSet;
import com.efounder.imageloader.GlideImageLoader;
import com.efounder.ospmobilelib.R;

import java.util.List;
import java.util.Map;


/**
 * 
 * @author lch
 *  公众号item
 * 
 */
public class PublicNumberMessageItem extends LinearLayout {

	private Context mContext;
	private OnPublicNumberItemClickListener onPublicNumberItemClickListener;
	private ViewGroup chat_item_publicnumber_frame;

	public PublicNumberMessageItem(Context context) {
		super(context);
		mContext = context;
		this.setBackgroundResource(R.drawable.radius_bg);
		LayoutParams mLineaLayoutParams = new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		this.setLayoutParams(mLineaLayoutParams);
		LayoutInflater inflater = LayoutInflater.from(mContext);
		chat_item_publicnumber_frame = (ViewGroup) inflater.inflate(
				R.layout.chat_item_publicnumber_frame, this);
	}
	
	public PublicNumberMessageItem(Context context,AttributeSet attrs) {
		super(context,attrs);
		mContext = context;
		this.setBackgroundResource(R.drawable.radius_bg);
		LayoutParams mLineaLayoutParams = new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		this.setLayoutParams(mLineaLayoutParams);
		LayoutInflater inflater = LayoutInflater.from(mContext);
		chat_item_publicnumber_frame = (ViewGroup) inflater.inflate(
				R.layout.chat_item_publicnumber_frame, this);
	}

	// 设置数据 card 是一个包含头图片，头标题，还有子card的模型
	public void setCard(final List<Map<String,Object>> content) {

//		// 初始化异步加载图片的类
//		ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton
//																// instance
//
//		// 设置异步加载图片的配置信息
//		DisplayImageOptions options = new DisplayImageOptions.Builder()
//				//.showStubImage(R.drawable.loading_image_background) // image在加载过程中，显示的图片
//				.showImageForEmptyUri(R.drawable.loading_image_background) // empty URI时显示的图片
//				.showImageOnFail(R.drawable.loading_image_background) // 不是图片文件
//																		// 显示图片
//				.resetViewBeforeLoading(false) // default
//				.delayBeforeLoading(1000).cacheInMemory(true) // default 不缓存至内存
//				.cacheOnDisc(true) // default 不缓存至手机SDCard
//				// .preProcessor(...)
//				// .postProcessor(...)
//				// .extraForDownloader(...)
//				.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)// default
//				// .bitmapConfig(Bitmap.Config.ARGB_8888) // default
//				// .decodingOptions(...)
//				// .displayer(new SimpleBitmapDisplayer()) // default
//				// 可以设置动画，比如圆角或者渐变
//				// .handler(new Handler()) // default
//				.build();

		// 开始初始化top层的图片和标题
		//Map bodyMap = iMStruct002.bodyToMap();// 格式为：：{"content":[{"title":"","image":"","url":""},{"title":"","image":"","url":""}]}
		//List<Map> content = (List<Map>) bodyMap.get("content");
		Map<String, Object> topMap = content.get(0);

		// 顶部图片
		//String topimage = ((JsonPrimitive) topMap.get("image")).getAsString();
		String topimage = (String) topMap.get("image");
		// 顶部标题
		String toptitle =  (String) topMap.get("title");
		//url
		final String topurl = (String) topMap.get("url");

		

		RelativeLayout chat_item_publicnumber_top = (RelativeLayout) chat_item_publicnumber_frame
				.findViewById(R.id.chat_item_publicnumber_top);
		chat_item_publicnumber_top.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//Toast.makeText(mContext, "chat_item_publicnumber_top click", 500).show();
				/*
				Intent intent = new Intent(mContext, TransformFragmentActivity.class);
				intent.putExtra(TransformFragmentActivity.EXTRA_FRAGMENT_NAME, Public_Number_MessageContent_Fragment.class.getName());
				intent.putExtra("url", topurl);
				mContext.startActivity(intent);
				
				if (onPublicNumberItemClickListener != null) {
					onPublicNumberItemClickListener.onClick(0);
				
				}*/
				Intent intent = new Intent(mContext,
						NoticeDetailActivity.class);
				EFRowSet efRowSet = new EFRowSet();
				efRowSet.putString("F_URL", topurl);
				intent.putExtra("rowset",
						efRowSet);
				mContext.startActivity(intent);
			}
		});
		// top图片
		ImageView chat_item_publicnumber_top_image = (ImageView) chat_item_publicnumber_frame
				.findViewById(R.id.chat_item_publicnumber_top_image);
		GlideImageLoader.getInstance().displayImage(mContext,chat_item_publicnumber_top_image,topimage,R.drawable.loading_image_background,R.drawable.loading_image_background);
//		imageLoader.displayImage(topimage, chat_item_publicnumber_top_image,
//				options);

		// top title
		TextView chat_item_publicnumber_top_title = (TextView) chat_item_publicnumber_frame
				.findViewById(R.id.chat_item_publicnumber_top_title);
		// chat_item_publicnumber_top_title.setText(Description);
		chat_item_publicnumber_top_title.setText(toptitle);

		LinearLayout chat_item_publicnumber_below = (LinearLayout) chat_item_publicnumber_frame
				.findViewById(R.id.chat_item_publicnumber_below);
		//先删除子view（setIMStruct002 会调用多次）
		chat_item_publicnumber_below.removeAllViews();

		// 遍历子subcard，动态添加子布局
		// List<PublicNumberSubCard> baseCardList = card.getSubCards();\
		String suburl  = "";
		for (int i = 1; i < content.size(); i++) {
			final int position = i;
			// PublicNumberSubCard subCard = content.get(i);
			Map<String, Object> submap = content.get(i);
			String subimage = (String) topMap.get("image");
			String subtitle =  (String) topMap.get("title");
			suburl =  (String) topMap.get("url");

			RelativeLayout chat_item_publicnumber_below_item = (RelativeLayout) LayoutInflater.from(mContext)
					.inflate(R.layout.chat_item_publicnumber_below_item, null);
			ImageView chat_item_publicnumber_below_image = (ImageView) chat_item_publicnumber_below_item
					.findViewById(R.id.chat_item_publicnumber_below_image);

//			ImageAware chat_item_publicnumber_below_imageaware = new ImageViewAware(chat_item_publicnumber_below_image, false);
//			imageLoader.displayImage(subimage,
//					chat_item_publicnumber_below_imageaware, options);
			GlideImageLoader.getInstance().displayImage(mContext,chat_item_publicnumber_below_image,subimage,R.drawable.loading_image_background,R.drawable.loading_image_background);

			TextView chat_item_publicnumber_below_textview = (TextView) chat_item_publicnumber_below_item
					.findViewById(R.id.chat_item_publicnumber_below_textview);
			chat_item_publicnumber_below_textview.setText(subtitle);

			chat_item_publicnumber_below
					.addView(chat_item_publicnumber_below_item);
			
			chat_item_publicnumber_below_item.setTag(position);

			chat_item_publicnumber_below_item
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							//Toast.makeText(mContext, "chat_item_publicnumber_below_item click", 500).show();

							/*Intent intent = new Intent(mContext, TransformFragmentActivity.class);
							intent.putExtra(TransformFragmentActivity.EXTRA_FRAGMENT_NAME, Public_Number_MessageContent_Fragment.class.getName());
							intent.putExtra("url", suburl);
							mContext.startActivity(intent);
							
							if (onPublicNumberItemClickListener != null) {
								onPublicNumberItemClickListener
										.onClick(position);
							}*/
							
							Intent intent = new Intent(mContext,
									NoticeDetailActivity.class);
							
							int  i = (Integer) v.getTag();
							String xurl = (String) content.get(i).get("url");
							EFRowSet efRowSet = new EFRowSet();
							efRowSet.putString("F_URL", xurl);
							intent.putExtra("rowset",
									efRowSet);
							mContext.startActivity(intent);
							
						}
					});
		}

	}

	public interface OnPublicNumberItemClickListener {
		public void onClick(int position);

	}

	
}
