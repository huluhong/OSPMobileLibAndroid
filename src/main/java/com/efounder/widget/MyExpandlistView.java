package com.efounder.widget;	


import android.content.Context;
import android.database.DataSetObserver;
import androidx.core.view.MotionEventCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

public class MyExpandlistView extends ExpandableListView {
 
	private static final String TAG = "MyExpandlistView";
	private ExpandSlideListener expandSlideListener;
	//private ExpandSlideListener1 expandSlideListener;
    public MyExpandlistView  (Context context, AttributeSet attrs) {

 

        super(context, attrs);
        init();

    }

 

    public MyExpandlistView  (Context context) {

 

        super(context);

        init();

    }

 

    public  MyExpandlistView  (Context context, AttributeSet attrs, int defStyle) {

 

        super(context, attrs, defStyle);

        init();
    }

 

    @Override

 

    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

 

        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,

 

                MeasureSpec.AT_MOST);

 

        super.onMeasure(widthMeasureSpec, expandSpec);

 

    }

    private void init() {
		//expandSlideListener = new ExpandSlideListener1(this);
    	expandSlideListener = new ExpandSlideListener(this);
		this.setOnTouchListener(expandSlideListener);
	}
    @Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		
		if(isEnabled()){
			int action = MotionEventCompat.getActionMasked(ev);
			if(action == MotionEvent.ACTION_DOWN){
				int downPosition = pointToPosition((int)ev.getX(), (int)ev.getY());
				Log.d(TAG, downPosition+"<<<<<<<<<<<按下的位置 down");
				int opendPosition = expandSlideListener.getOpendPosition();
				
				if (opendPosition != INVALID_POSITION) {
					if (expandSlideListener.isInSliding()) {
						return false;
					}
					// if down position not equals the opend position,drop this
					// motion event and close the opend item
					if (downPosition != opendPosition) {
						expandSlideListener.closeOpenedItem();
						return false;
					}
				}
			}
			if(action == MotionEvent.ACTION_MOVE){
			}
		}
		
		return super.dispatchTouchEvent(ev);
	}
	
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		Log.d(TAG, "on interceptTouchEvent");
		//return true;
		if(isEnabled()){
			if(expandSlideListener.onInterceptTouchEvent(ev)){
				return true;
			}
		}
		return super.onInterceptTouchEvent(ev);
	}
	
	private class InnerDataSetObserver extends DataSetObserver{
		@Override
		public void onChanged() {
			super.onChanged();
			closeDirect();
		}

		
	}
	
	private void closeDirect() {
		if (expandSlideListener.isOpend()) {
			postDelayed(new Runnable() {
				@Override
				public void run() {
					expandSlideListener.closeOpenedItem();
				}
			}, 100);
		}else{
			expandSlideListener.reset();
		}
	}
	
	private InnerDataSetObserver mInnerDataSetObserver;
	
	@Override
	public void setAdapter(ExpandableListAdapter adapter) {
		// TODO Auto-generated method stub
		super.setAdapter(adapter);
		if (adapter!=null) {
			mInnerDataSetObserver = new InnerDataSetObserver();
			adapter.registerDataSetObserver(mInnerDataSetObserver);
		}
	}

}