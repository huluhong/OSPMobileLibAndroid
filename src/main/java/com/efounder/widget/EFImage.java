package com.efounder.widget;

import android.content.Context;
import androidx.appcompat.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.View;

import com.efounder.interfaces.IBGABadge;
import com.efounder.model.ClearBadgeEvent;
import com.efounder.ospmobilelib.R;
import com.efounder.util.EnvSupportManager;
import com.utilcode.util.SizeUtils;

import org.greenrobot.eventbus.EventBus;

import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;


/**
 * Created by lch on 2017/6/5.
 */

public class EFImage extends AppCompatImageView implements IBGABadge {
    private String nodeId;//角标所在tab的id，如"xiaoxi",-1,CHAT_ID_MESSAGE
    private QBadgeView badgeView;
    private Context context;
    private int badgeCount;
    private boolean draggable;//角标是否可拖拽

    public EFImage(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EFImage(Context context) {
        this(context, null);
    }

    /**
     * 获得我自定义的样式属性
     *
     * @param context
     * @param attrs
     * @param defStyle
     */
    public EFImage(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;

    }

    @Override
    public void setNodeId(String nodeId, boolean draggable) {
        this.nodeId = nodeId;
        this.draggable = draggable;
    }

    @Override
    public void initBadgeView(QBadgeView badgeView) {
        if (this.badgeView == null)
            this.badgeView = badgeView;
    }

    @Override
    public QBadgeView getBadgeView() {
        if (this.badgeView == null) {
            badgeView = new QBadgeView(context);
            badgeView.bindTarget(this);
        }
        return this.badgeView;
    }

    @Override
    public void setBadgeCount(int badgeCount) {
        this.badgeCount = badgeCount;
        //设置角标数
        badgeView.setBadgeNumber(badgeCount);
        badgeView.setBadgeBackgroundColor(getResources().getColor(R.color.chat_badge_color));
        //角标设置较大的padding，更容易拖动
        badgeView.setPadding(SizeUtils.dp2px(10), SizeUtils.dp2px(10),
                SizeUtils.dp2px(10), SizeUtils.dp2px(10));
        //是否可以拖拽角标
        if (EnvSupportManager.isSupportBadgeDrag() && draggable) {
            badgeView.setOnDragStateChangedListener(new Badge.OnDragStateChangedListener() {
                @Override
                public void onDragStateChanged(int dragState, Badge badge, View targetView) {
                    if (dragState == STATE_SUCCEED) {
                        EventBus.getDefault().post(new ClearBadgeEvent(nodeId));
                    }
                }
            });
        }
//        badgeView.setBadgeMargin(0, 0);
//        if (badgeCount == -1) {
//            badgeView.setTextSize(8);
//            badgeView.setText(".");
//            badgeView.setPadding(SizeUtils.dp2px(4), 0, SizeUtils.dp2px(4), 0);
//            badgeView.setTextColor(Color.RED);
//        }else {
//            badgeView.setTextSize(13);
//            if (badgeCount > 99) {
//                badgeView.setText("99+");
//            } else {
//                badgeView.setText(badgeCount + "");
//            }
//        }
//        //ShortcutBadger.applyCount(context, badgeCount);
//        badgeView.setBadgeBackgroundColor(getResources().getColor(
//                R.color.chat_badge_color));
//        if (badgeCount <= 0 && badgeCount != -1) {
//            badgeView.setVisibility(View.INVISIBLE);
//        } else {
//            badgeView.show();
//        }
    }

    @Override
    public int getBadgeCount() {
        return badgeCount;
    }
}
