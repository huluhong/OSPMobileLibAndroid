package com.efounder.widget;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.ospmobilelib.R;
import com.efounder.widget.QuickActionWidget.OnQuickActionClickListener;
import com.pansoft.resmanager.ResFileManager;
/**
 * 扩展底部菜单栏   超过5项时 自动扩展弹出框  预留备用
 * @author long
 *2015-1-7 09:14:06
 */
public class TabBar extends LinearLayout {
	
	private static final String TAG = "TabBar";
	private static final String CAPTION="caption";
	private static final String SELECTEDICON="selectedIcon";
	private static final String NORMALICON="normalIcon";
	private static final int MORE_BTN = 1000;/**更多按钮ID 标示号*/
	/**
	 * 底部菜单超过6项时的默认值  超过6项时 第五项为弹出框按钮
	 */
	private int BOTTOM_ITEM = 4;
	/**
	 * 底部菜单最多
	 */
	private int MAX_BOTTOM_ITEM = 5;
	/**
	 * 底部菜单最多数5 +1用于限制底部展出菜单数
	 */
	private int MAX_BOTTOM_ITEMS = MAX_BOTTOM_ITEM +1;
	private final Context mContext;
	private int mCurrentTabIndex;
	
	private QuickActionWidget mGrid;// 快捷栏控件
	
	/**
	 * 记录菜单个数
	 */
	private int mTabCount;
	/**
	 * 底部菜单宽度
	 */
	private int mTabWidth;
	private OnCurrentTabChangedListener mTabChangedListener;
	private List<ImageView> imageList=new ArrayList<ImageView>();
	private List<TextView> textList=new ArrayList<TextView>();
	private List<Object> espMenuList;
	private List<Bitmap> bitmapList;
	private List<Bitmap> bitmap_pressList;
	public TabBar(Context context, ArrayList<Object> espMenuList) {
		super(context);
		// TODO Auto-generated constructor stub
		this.espMenuList=espMenuList;
		bitmapList=new ArrayList<Bitmap>();
		bitmap_pressList=new ArrayList<Bitmap>();
		this.mContext=context;
		setWillNotDraw(false); // 重要!!!
		mCurrentTabIndex = -1;
		mTabCount = 0;
		init();
	}
	 public void init() {
		this.setOrientation(LinearLayout.VERTICAL);
		this.setPadding(0, 0, 0, 0);
		//读取sd卡上的图片
		for(int i=0; i<espMenuList.size(); i++){
			String normalIcon=((StubObject)espMenuList.get(i)).getObject(NORMALICON, "").toString();
			normalIcon = ResFileManager.IMAGE_DIR + "/" + normalIcon;
			File file = new File(normalIcon); 
            if (file.exists()) { 
            	Bitmap bm = BitmapFactory.decodeFile(normalIcon); 
            	bitmapList.add(bm);  
            }else{
            	Bitmap bm = BitmapFactory.decodeResource(mContext.getResources(),R.drawable.ic_launcher);
            	bitmapList.add(bm);
            }
            String selectdeIcon=((StubObject)espMenuList.get(i)).getObject(SELECTEDICON, "").toString();
            selectdeIcon = ResFileManager.IMAGE_DIR + "/" + selectdeIcon;
            File file_press=new File(selectdeIcon);
            if (file_press.exists()) { 
            	Bitmap bm = BitmapFactory.decodeFile(selectdeIcon); 
            	bitmap_pressList.add(bm);  
            }else{
            	Bitmap bm = BitmapFactory.decodeResource(mContext.getResources(),R.drawable.ic_launcher);
            	bitmap_pressList.add(bm);
            }
            
		}
		WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
		Display dp = wm.getDefaultDisplay();
		mTabWidth = dp.getWidth();
		mTabCount = espMenuList.size();
		Log.i(TAG, "espMenuList.size()"+espMenuList.size());
		if (mTabCount > 0&&mTabCount < MAX_BOTTOM_ITEMS) {
			BOTTOM_ITEM = mTabCount;
			mTabWidth = mTabWidth / mTabCount; // 计算每个tab的宽度
			mCurrentTabIndex = 0;
		}else{
			mTabWidth = mTabWidth / MAX_BOTTOM_ITEM; // 计算每个tab的宽度
			mCurrentTabIndex = 0;
		}
		LayoutParams outParams = new LayoutParams(mTabWidth,
				LayoutParams.WRAP_CONTENT);
		outParams.gravity=Gravity.CENTER;
		View.OnClickListener clkListener = new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				int index = (Integer) v.getTag();
				Log.v("index", "index:" +index);
				if(index == MORE_BTN){
					mGrid.show(v);
					return;
				}
				if (index != mCurrentTabIndex) {
					if (mTabChangedListener != null)
						mTabChangedListener.onCurrentTabChanged(index);
					mCurrentTabIndex = index;
					setCurrentTabColor(index);
				}
			}
		};
		//添加一个线条
		ImageView line = new ImageView(mContext);
		line.setBackgroundColor(getResources().getColor(R.color.tabline));
		LayoutParams lineParams = new LayoutParams(LayoutParams.FILL_PARENT,
				1);
		this.addView(line, lineParams);
		LinearLayout tabcontent = new LinearLayout(mContext);
		tabcontent.setOrientation(LinearLayout.HORIZONTAL);
		LayoutParams tabcontentParams = new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT);
		tabcontent.setLayoutParams(tabcontentParams);
		this.setBackgroundResource(R.color.title_bar_bg);
		this.addView(tabcontent);
		//逐个添加Tab
		if(mTabCount<MAX_BOTTOM_ITEMS){
			addItemTab(outParams, clkListener, tabcontent,mTabCount);
		}else{
			addItemTab(outParams, clkListener, tabcontent,BOTTOM_ITEM);
			LinearLayout tab = new LinearLayout(mContext);
			tab.setGravity(Gravity.CENTER);
			tab.setOrientation(LinearLayout.VERTICAL);
			tab.setPadding(0, 5, 0, 0);
			tab.setTag(MORE_BTN); // 设置内部标号
			tab.setClickable(true);
			ImageView imv = new ImageView(mContext);
			imv.setScaleType(ScaleType.FIT_XY);
			imv.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_menu_more_n));
			imageList.add(imv);
			TextView tv = new TextView(mContext);
			tv.setGravity(Gravity.CENTER_HORIZONTAL);
			tv.setText("更多");
			tv.setTextColor(Color.BLACK);
			textList.add(tv);
			tab.addView(imv, 60, 60);//目前标签的宽和高设置为60*60，图片全部拉伸填充
			tab.addView(tv);
			tab.setOnClickListener(clkListener);
			tabcontent.addView(tab, outParams);
			initQuickActionGrid();
//			initQuickActionGrid(mTabCount-BOTTOM_ITEM);
			
		}
		
	}
	 /**
	 * 初始化快捷栏  list剩余菜单选项  底部菜单超过6项时调用该方法
	 */
		private void initQuickActionGrid() {
			mGrid = new QuickActionGrid(mContext);
			for(int i=BOTTOM_ITEM;i<this.mTabCount;i++){
				mGrid.addQuickAction(new QuickAction(new BitmapDrawable(mContext.getResources(),bitmapList.get(i)), ((StubObject)espMenuList.get(i)).getObject(CAPTION, "未知").toString()));
			}
			mGrid.setOnQuickActionClickListener(mActionListener);
		}
		
		/**
		 * 快捷栏item点击事件
		 */
		private OnQuickActionClickListener mActionListener = new OnQuickActionClickListener() {
			public void onQuickActionClicked(QuickActionWidget widget, int position) {
				if (mTabChangedListener != null){
					mTabChangedListener.onCurrentTabChanged(BOTTOM_ITEM+position);
//					AbLogUtil.i(TAG, "BOTTOM_ITEM+position:"+(BOTTOM_ITEM+position));
					mCurrentTabIndex = (BOTTOM_ITEM+position);
					setCurrentTabColor(BOTTOM_ITEM+position);
				}
			}
		};
	 /**
	  * 添加底部ItemTab选项
	  * @param outParams
	  * @param clkListener
	  * @param tabcontent
	  * @param mTabCount
	  */
	private void addItemTab(LayoutParams outParams,
			View.OnClickListener clkListener, LinearLayout tabcontent,int mTabCount) {
		for (int i = 0; i < mTabCount; ++i) {
			LinearLayout tab = new LinearLayout(mContext);
			tab.setGravity(Gravity.CENTER);
			tab.setOrientation(LinearLayout.VERTICAL);
			tab.setPadding(0, 5, 0, 0);
			tab.setTag(i); // 设置内部标号
			tab.setClickable(true);
			ImageView imv = new ImageView(mContext);
			imv.setScaleType(ScaleType.FIT_XY);
			imv.setImageBitmap(bitmapList.get(i));
			if(0 == i){
				imv.setImageBitmap(bitmap_pressList.get(0));
			}
			imageList.add(imv);
			TextView tv = new TextView(mContext);
			tv.setGravity(Gravity.CENTER_HORIZONTAL);
			String caption=((StubObject)espMenuList.get(i)).getObject(CAPTION, "").toString();
			tv.setText(caption);
			if(0 == i){
				tv.setTextColor(getResources().getColor(R.color.textcolor));
			}else{
				tv.setTextColor(Color.BLACK);
			}
			textList.add(tv);
			tab.addView(imv, 60, 60);//目前标签的宽和高设置为60*60，图片全部拉伸填充
			tab.addView(tv);
			tab.setOnClickListener(clkListener);
			tabcontent.addView(tab, outParams);
		}
	}
	/**
	 * 点击标签后更改所有的标签状态
	 * @param index
	 */
	public void setCurrentTabColor(int index){
		for(int i=0; i<BOTTOM_ITEM; i++){
			if(index == i){
				imageList.get(i).setImageBitmap(bitmap_pressList.get(i));
				textList.get(i).setTextColor(getResources().getColor(R.color.textcolor));
			}else{
				imageList.get(i).setImageBitmap(bitmapList.get(i));
				textList.get(i).setTextColor(Color.BLACK);
			}
		}
	}
	/**
	 * 设置当前Tab的序号
	 * @param index
	 */
	public void setCurrentTab(int index) {
		if (index > -1 && index < mTabCount&&index!=mCurrentTabIndex) {
			mCurrentTabIndex = index;
			this.invalidate();
			if (mTabChangedListener != null)
				mTabChangedListener.onCurrentTabChanged(mCurrentTabIndex);
		}
	}

	public void setOnCurrentTabChangedListener(
			OnCurrentTabChangedListener listener) {
		mTabChangedListener = listener;
	}
	/**
	 * 获取Tab个数
	 * 
	 * @return Tab个数
	 */
	public int getTabCount() {
		return mTabCount;
	}

	public interface OnCurrentTabChangedListener {
		public void onCurrentTabChanged(int index);
	}

}