package com.efounder.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ScrollView;

public class AbBaseScrollView extends ScrollView{
    public AbBaseScrollView(Context context) {
        super(context);
    }

    public AbBaseScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
    	// TODO Auto-generated method stub
    	return super.dispatchTouchEvent(ev);
    }
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
    	Log.i("", "getPointerCount------------"+ev.getPointerCount()+",getAction:"+ev.getAction());
    	Log.i("", "isFillViewport------------"+this.isFillViewport());
    	
    	if (ev.getPointerCount() ==2) {
    		return true;
		}
    	
    	switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			return super.onInterceptTouchEvent(ev);
		case MotionEvent.ACTION_MOVE:
			int x=(int) ev.getX();
	    	WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
	    	DisplayMetrics metrics = new DisplayMetrics();
	    	wm.getDefaultDisplay().getMetrics(metrics);
	    	int width = metrics.widthPixels;
	    	//最右边1/4区域可以自由滚动  && 界面需要滚动
	    	if (x > (width/4*3) && this.getHeight()>=((ViewGroup)this.getParent().getParent()).getHeight()) {
//	    	if (x > (width/4*3) && this.isFillViewport()) {//最右边1/4区域可以自由滚动
	    		Log.i("", "onInterceptTouchEvent------------true");
				return true;
			}
			break;
		case MotionEvent.ACTION_UP:
			break;

		default:
			break;
		}
    	
        return false;
    }
}