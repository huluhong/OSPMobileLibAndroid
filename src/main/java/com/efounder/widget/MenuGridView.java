package com.efounder.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.ospmobilelib.R;

import com.efounder.utils.FilesOperationDataUtil;
import com.pansoft.resmanager.ResFileManager;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;



public class MenuGridView extends GridView {
    private Context context;
  
    private ArrayList<Object> data_list;
   
    
    ExtEdgeListLayoutBroadcast extEdgeListLayoutBroadcast;
    
	private GridAdapter gridAdapter;
	
	ArrayList<HashMap<String,Object>> listmap;
	
	ArrayList<Object> extEdgeMenus;
    
    
	public MenuGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		
		
	}
	public MenuGridView(Context context) {
		super(context);
		this.context = context;
		
	/*	  //注册广播类
        extEdgeListLayoutBroadcast = new ExtEdgeListLayoutBroadcast();
   		// 注册intentFilter
   		IntentFilter filter = new IntentFilter();
   		filter.addAction("uiChange");
   		BroadCastutil.getinstance().addbroadCast(extEdgeListLayoutBroadcast);
   		// 注册filter，连接broadcastSaveFile
   		this.context.registerReceiver(extEdgeListLayoutBroadcast, filter);*/
	}
	public void setList(ArrayList<Object> extEdgeMenus){
		if(extEdgeMenus!=null){
		this.extEdgeMenus = extEdgeMenus;
		gridAdapter = new GridAdapter(context, this.extEdgeMenus);
		this.setAdapter(gridAdapter);
		}
	}
	public class GridAdapter extends BaseAdapter {  
        private LayoutInflater mInflater;  
        private Context mContext;  
        LinearLayout.LayoutParams params;  
        ArrayList<Object> listmap;
      
        public GridAdapter(Context context, ArrayList<Object> listmap) { 
        	this.listmap = listmap;
            mContext = context;  
            mInflater = LayoutInflater.from(context);  
              
            params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);  
            params.gravity = Gravity.CENTER;  
        }  
      
        public int getCount() {  
            return listmap.size()-1;  
        }  
      
        public Object getItem(int position) {  
            return listmap.get(position);  
        }  
      
        public long getItemId(int position) {  
            return position;  
        }  
      
        public View getView(final int position, View convertView, ViewGroup parent) {  
            ItemViewTag viewTag;  
              
            if (convertView == null)  
            {  
                convertView = mInflater.inflate(R.layout.menugriditem, null);  
                  
                // construct an item tag  
                viewTag = new ItemViewTag((ImageView) convertView.findViewById(R.id.image), (TextView) convertView.findViewById(R.id.text),(Button)convertView.findViewById(R.id.addbutton));  
                convertView.setTag(viewTag);  
            } else  
            {  
                viewTag = (ItemViewTag) convertView.getTag();  
            }  
            
            // set name  
            viewTag.mName.setText(((StubObject)listmap.get(position)).getString("caption","").toString());  
              
         
        	ApplicationInfo appInfo = context.getApplicationInfo();
        
        	
        	String menuIcon=(String)((StubObject)listmap.get(position)).getString("menuIcon","");
			menuIcon = ResFileManager.IMAGE_DIR + "/" + menuIcon;
			File file = new File(menuIcon); 
			Bitmap bm = null;
			if (file.exists()) { 
            	bm = BitmapFactory.decodeFile(menuIcon); 
            }
			
        	
			else if(bm==null){
			
				int resID = context.getResources().getIdentifier("iframe_icon_refresh", "drawable", appInfo.packageName);
				bm = BitmapFactory.decodeResource(context.getResources(), resID);
			}
			viewTag.mIcon.setImageBitmap(bm);
			
            if((Boolean) ((StubObject)listmap.get(position)).getBoolean("ischecked",false)){
            	viewTag.button.setText(R.string.common_text_cancel);
            	viewTag.button.setBackgroundResource(R.drawable.bg_buttoncancle);
            }
            viewTag.button.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(((Button)v).getText().equals("添加")){
						((Button)v).setText(R.string.common_text_cancel);
						((Button)v).setBackgroundResource(R.drawable.bg_buttoncancle);
						((StubObject)listmap.get(position)).setBoolean("ischecked", true);
						((StubObject)extEdgeMenus.get(position)).setBoolean("ischecked",true );
					
					}else{
						((Button)v).setText("添加");
						((StubObject)listmap.get(position)).setBoolean("ischecked", false);
						((StubObject)extEdgeMenus.get(position)).setBoolean("ischecked",false );
						((Button)v).setBackgroundResource(R.drawable.bg_buttonadd);
					}
					FilesOperationDataUtil.clearFileData(mContext);
				//	FilesOperationDataUtil.createFile(mContext, extEdgeMenus);
					//sendBroadCast();
					
				}
			});
            return convertView;  
        }  
          
        class ItemViewTag  
        {  
            protected ImageView mIcon;  
            protected TextView mName;  
            protected Button button;
              
            /** 
             * The constructor to construct a navigation view tag 
             *  
             * @param name 
             *            the name view of the item 

             * @param icon 
             *            the icon view of the item 
             */  
            public ItemViewTag(ImageView icon, TextView name,Button button )  
            {  
                this.mName = name;  
                this.mIcon = icon;  
                this.button = button;
            }  
        } 
    }
	public void refresh(){
	//	data_list= FilesOperationDtaUtil.readFile(context);
		
		gridAdapter.notifyDataSetChanged();
	}
	/*
	 * 文件保存成功后发送广播
	 */
	public void sendBroadCast() {

		String ACTION = "CommonGridViewuiChange";
		Intent intent = new Intent();
		intent.putExtra("name", "buttonclick");
		intent.setAction(ACTION);
		// 普通广播
		context.sendBroadcast(intent);
	}
	public class ExtEdgeListLayoutBroadcast extends BroadcastReceiver{
	    @Override
	    public void onReceive(Context context, Intent intent) {
	        String action = intent.getAction();
	        String name = intent.getStringExtra("name");
	  
	        if("del".equals(name)){
	        	refresh();
	        }
	    }
	}

}
