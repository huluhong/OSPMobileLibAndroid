package com.efounder.widget;

import android.content.Context;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.efounder.ospmobilelib.R;

import java.io.IOException;

/**
 * Created by meijun on 17-9-18.
 */

public class CameraView  extends FrameLayout implements SurfaceHolder.Callback, View.OnClickListener {

    private SurfaceView mSurfaceView;
    private SurfaceHolder mSurfaceHolder;
    private Camera mCamera;

    public CameraView(Context context) {
        super(context);
        initView();
    }



    public CameraView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public CameraView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }
    private void initView() {


    }



    public void onActivityCreated() {

        mSurfaceView = new SurfaceView(this.getContext());
        this.addView(mSurfaceView);
        ImageView imageView = new ImageView(this.getContext());
        imageView.setImageResource(R.drawable.icon_camera);
        LayoutParams params = new LayoutParams(20, 20);
        params.gravity = Gravity.BOTTOM| Gravity.CENTER_HORIZONTAL;
        this.addView(imageView, params);

    }

    public void onActivityStarted( ) {

    }


    public void onActivityResumed( ) {


        if (mCamera == null) {
            mCamera = getCameraInstance();
        }
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(this);
        this.setOnClickListener(this);
    }


    public void onActivityPaused( ) {
        mCamera.release();
        mCamera = null;
        mSurfaceHolder.removeCallback(this);
    }


    public void onActivityStopped( ) {

    }



    public void onActivityDestroyed( ) {

    }

    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @Override
    public void onClick(View v) {



    }

    private void startPreview(SurfaceHolder holder) {

        try {
            mCamera.setPreviewDisplay(holder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mCamera.startPreview();

        mCamera.setDisplayOrientation(90);
        // get Camera parameters
        Camera.Parameters params = mCamera.getParameters();
// set the focus mode
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
// set Camera parameters
        mCamera.setParameters(params);
        mCamera.autoFocus(new Camera.AutoFocusCallback() {
            @Override
            public void onAutoFocus(boolean success, Camera camera) {

            }
        });
    }
}
