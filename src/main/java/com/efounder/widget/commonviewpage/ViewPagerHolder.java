package com.efounder.widget.commonviewpage;

import android.content.Context;
import android.view.View;

/**
 * @author : zzj
 * @e-mail : zhangzhijun@pansoft.com
 * @date : 2018/10/1310:56
 * @desc :
 * @version: 1.0
 */
public interface ViewPagerHolder<T> {

    /**
     *  创建View
     * @param context
     * @return
     */
    View createView(Context context);

    /**
     * 绑定数据
     * @param context
     * @param position
     * @param data
     */
    void onBind(Context context, int position, T data);
}
