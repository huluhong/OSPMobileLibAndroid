package com.efounder.widget.commonviewpage;

/**
 * @author : zzj
 * @e-mail : zhangzhijun@pansoft.com
 * @date : 2018/10/1313:06
 * @desc :
 * @version: 1.0
 */
public class LaunchBean {
    private String title;
    private int picTitle;
    private int picContent;

    public LaunchBean(String title, int picTitle, int picContent) {
        this.title = title;
        this.picTitle = picTitle;
        this.picContent = picContent;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPicTitle() {
        return picTitle;
    }

    public void setPicTitle(int picTitle) {
        this.picTitle = picTitle;
    }

    public int getPicContent() {
        return picContent;
    }

    public void setPicContent(int picContent) {
        this.picContent = picContent;
    }
}
