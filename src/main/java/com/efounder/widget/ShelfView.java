package com.efounder.widget;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.ospmobilelib.R;
import com.efounder.activity.MainFrameActivity;
import com.pansoft.resmanager.ResFileManager;

public class ShelfView extends GridView{
	
	//集成到工程里时需要解开的部分
	private static StubObject mMmainMenuItem; // 对应的某主菜单项
	private static ArrayList<Object> mSecondLevelMenus; // 二级菜单
	private static final String CAPTION="caption";
	private static final String MENUICON="menuIcon";
	private ArrayList<HashMap<String,Object>> shelfList;
	public ShelfView(Context context){
		super(context);
		init();
	}

	private void init() {
		// TODO Auto-generated method stub
		setVerticalSpacing(10); 
		setNumColumns(3);
		setOnItemClickListener(new OnGridItemClickListener());
		setSelector(new ColorDrawable(Color.TRANSPARENT));
	}

	public ShelfView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	class ShelfViewAdapter extends BaseAdapter{
		
		ArrayList<HashMap<String,Object>> adapterList;
		LayoutInflater inflater;
		public ShelfViewAdapter(ArrayList<HashMap<String,Object>> list){
			adapterList = list;
			inflater = LayoutInflater.from(getContext());
		}

		@Override
		public int getCount() {
			
			return adapterList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return adapterList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder viewHolder = new ViewHolder();
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.shelfview_item2,
						null);
				viewHolder.imageView = (ImageView) convertView
						.findViewById(R.id.shelfView_Item_Img);
				viewHolder.imageView.setImageResource(R.drawable.reportforms_1);
				viewHolder.textView = (TextView) convertView
						.findViewById(R.id.shelfView_Item_Text);
				viewHolder.textView.setText((String)(adapterList.get(position).get("caption")));
				
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			
			return convertView;
		}
		
	}
	
	 class ViewHolder {
		ImageView imageView;
		TextView textView;
	}
	
	public void setShelfViewData(ArrayList<HashMap<String,Object>> list){
		shelfList = list;
	}
	
	/**
	 * 为书架设置数据的方法，同时设置需要显示几列
	 * @param list
	 */
	//集成到工程里时需要解开的部分
	public void fillGridData(ArrayList<Object> list) {
//		mSecondLevelMenus = list;
//		initShelfView();
		mSecondLevelMenus = list;
		ArrayList<HashMap<String, Object>> mainMenuList = new ArrayList<HashMap<String,Object>>();
		for(int i=0; i<mSecondLevelMenus.size(); i++){
			HashMap<String, Object> mapgrid = new HashMap<String, Object>();
			
			String menuIcon=((StubObject)mSecondLevelMenus.get(i)).getObject(MENUICON, "").toString();
			menuIcon = ResFileManager.IMAGE_DIR + "/" + menuIcon;
//			normalIcon = ResFileManager.IMAGE_DIR  + normalIcon;
			File file = new File(menuIcon); 
            if (file.exists()) { 
            	
            	mapgrid.put("icon",menuIcon);
            }else{
            	mapgrid.put("icon","");
            }
            String caption=((StubObject)mSecondLevelMenus.get(i)).getObject(CAPTION, "").toString();
            
        	
    		mapgrid.put("caption",caption);
            mainMenuList.add(mapgrid);
		}
		
		shelfList = mainMenuList;
		ShelfViewAdapter adapter = new ShelfViewAdapter(shelfList);
		setAdapter(adapter);
	}
	
	class OnGridItemClickListener implements OnItemClickListener{

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// TODO Auto-generated method stub
			MainFrameActivity menuFrag = MainFrameActivity.getInstance();
		/*	if (menuFrag != null) {
				menuFrag.showForms((StubObject)mSecondLevelMenus.get(position));
			}*/
		}
		
	}

}
