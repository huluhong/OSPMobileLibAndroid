package com.efounder.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

import com.efounder.util.MyStaticWebView;

public class ParentViewOnViewPager extends FrameLayout {
	private ESPWebView mDispatchWebView;
    public ParentViewOnViewPager(Context context) {
		super(context);
		mDispatchWebView = MyStaticWebView.getWv();
		// TODO Auto-generated constructor stub
	}
	public ParentViewOnViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mDispatchWebView = MyStaticWebView.getWv();
	}


	/*private MineWebView mDispatchWebView;

    public void preventParentTouchEvent (WebView view) {
        mDispatchWebView = (MineWebView)view;
    }*/

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_MOVE && mDispatchWebView != null) {
            mDispatchWebView.ignoreTouchCancel(true);
            return true;
        }
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (mDispatchWebView != null){
            switch (ev.getAction()) {
                case MotionEvent.ACTION_MOVE:
                    mDispatchWebView.onTouchEvent(ev);
                    break;
                default:
                    mDispatchWebView.ignoreTouchCancel(false);
                    mDispatchWebView.onTouchEvent(ev);
                    mDispatchWebView = null;
                    break;
            }
            return true;
        }
        return super.onTouchEvent(ev);
    }
}