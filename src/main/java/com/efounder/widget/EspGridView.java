package com.efounder.widget;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.fragment.app.Fragment;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.ospmobilelib.R;
import com.efounder.activity.TabBottomActivity;
import com.efounder.util.AbFragmentManager;
import com.pansoft.resmanager.ResFileManager;

public class EspGridView extends GridView {

	// 集成到工程里时需要解开的部分
	private static StubObject mMmainMenuItem; // 对应的某主菜单项
	private static List<StubObject> mSecondLevelMenus; // 二级菜单
	private static final String CAPTION = "caption";
	private static final String MENUICON = "menuIcon";
	private ArrayList<HashMap<String, Object>> shelfList;
	private List<Bitmap> bitmapList = new ArrayList<Bitmap>();
	Context context;

	public EspGridView(Context context) {
		super(context);
		this.context = context;
		init();
	}

	private void init() {
		// TODO Auto-generated method stub
		setOnItemClickListener(new OnGridItemClickListener());
		setSelector(new ColorDrawable(Color.TRANSPARENT));
	}

	public EspGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		init();
	}

	class ShelfViewAdapter extends BaseAdapter {

		ArrayList<HashMap<String, Object>> adapterList;
		LayoutInflater inflater;

		public ShelfViewAdapter(ArrayList<HashMap<String, Object>> list) {
			adapterList = list;
			inflater = LayoutInflater.from(getContext());
		}

		@Override
		public int getCount() {

			return adapterList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return adapterList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder viewHolder = new ViewHolder();
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.second_grid, null);
				viewHolder.imageView = (ImageView) convertView
						.findViewById(R.id.image);
				viewHolder.imageView.setImageBitmap(bitmapList.get(position));
				viewHolder.textView = (TextView) convertView
						.findViewById(R.id.text);
				viewHolder.textView.setText((String) (adapterList.get(position)
						.get("caption")));

				viewHolder.cross_tv = (TextView) convertView
						.findViewById(R.id.cross_text);
				viewHolder.cross_tv.setText(position + "");

				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}

			return convertView;
		}

	}

	class ViewHolder {
		ImageView imageView;
		TextView textView;
		TextView cross_tv;
	}

	public void setShelfViewData(ArrayList<HashMap<String, Object>> list) {
		shelfList = list;
	}

	/**
	 * 为书架设置数据的方法，同时设置需要显示几列
	 * 
	 * @param list
	 */
	// 集成到工程里时需要解开的部分
	public void fillGridData(List<StubObject> list) {
		// mSecondLevelMenus = list;
		// initShelfView();
		mSecondLevelMenus = list;
		ArrayList<HashMap<String, Object>> mainMenuList = new ArrayList<HashMap<String, Object>>();
		for (int i = 0; i < mSecondLevelMenus.size(); i++) {
			HashMap<String, Object> mapgrid = new HashMap<String, Object>();

			String menuIcon = ((StubObject) mSecondLevelMenus.get(i))
					.getObject(MENUICON, "").toString();
			menuIcon = ResFileManager.IMAGE_DIR + "/" + menuIcon;
			// normalIcon = ResFileManager.IMAGE_DIR + normalIcon;
			File file = new File(menuIcon);
			if (file.exists()) {
				Bitmap bm = BitmapFactory.decodeFile(menuIcon);
				bitmapList.add(bm);
			} else {
				Bitmap bm = BitmapFactory.decodeResource(
						context.getResources(), R.drawable.ic_launcher);
				bitmapList.add(bm);
			}
			String caption = ((StubObject) mSecondLevelMenus.get(i)).getObject(
					CAPTION, "").toString();

			mapgrid.put("caption", caption);
			mainMenuList.add(mapgrid);
		}

		shelfList = mainMenuList;
		ShelfViewAdapter adapter = new ShelfViewAdapter(shelfList);
		setAdapter(adapter);
	}

	class OnGridItemClickListener implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// TODO Auto-generated method stub

			TabBottomActivity menuFrag = TabBottomActivity.getInstance();
			Fragment frag = null;
			StubObject mainMenuItem = (StubObject) mSecondLevelMenus
					.get(position);

			AbFragmentManager ab = new AbFragmentManager(context);

			frag = ab.getFragment(mainMenuItem);
			menuFrag.jumpAndStoreStack(frag);
		}

	}

}
