package com.efounder.fragment;

import java.util.List;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.core.xml.StubObject;
import com.efounder.activity.MainFrameActivity;
import com.efounder.adapter.TypedListAdapter;
import com.efounder.ospmobilelib.R;
import com.efounder.service.Registry;
import com.efounder.util.AbLogUtil;
import com.efounder.view.titlebar.AbTitleBar;
//import com.pansoft.xmlparse.MenuList;

/**
 * 普通菜单选项 listview
 *
 */
public class LevelMenuFragment extends Fragment implements OnItemClickListener, OnClickListener {

	private static final String TAG = "LevelMenuFragment";
	private StubObject mParentMenuItem;
	private List<StubObject> mMenuItems;
	private int mContainerId;
	// Views
	private boolean mBackBtnDisplayed;
	private View mBackBtn;
	private ListView mListView;
	
	AbTitleBar title;
	String titleStr;

	public LevelMenuFragment() {
	}

	public LevelMenuFragment(StubObject parentMenuItem, List<StubObject> menuItems,
			int containerId, boolean displayBackBtn,String titleStr) {
		setAttachedData(parentMenuItem, menuItems, containerId, displayBackBtn,titleStr);
	}

	public void setAttachedData(StubObject parentMenuItem, List<StubObject> menuItems,
			int containerId, boolean displayBackBtn,String titleStr) {
		this.mParentMenuItem = parentMenuItem;
		this.mMenuItems = menuItems;
		this.mContainerId = containerId;
		this.mBackBtnDisplayed = displayBackBtn;
		this.titleStr = titleStr;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.layout_frag_next_level, null);
		if (mBackBtnDisplayed) {
//			ViewUtils.setViewVisibility(mBackBtnContainer, View.VISIBLE);
			mBackBtn.setOnClickListener(this);
		}
		mListView = (ListView) rootView.findViewById(R.id.lv_list);
		mListView.setAdapter(new TypedListAdapter(getActivity(), mParentMenuItem.getString(
				"viewType", null), mMenuItems));
		mListView.setOnItemClickListener(this);
		return rootView;
	}

	@Override
	public void onClick(View v) {
		// 按back键，返回上一层Fragment
		MainFrameActivity menuFrag = MainFrameActivity.getInstance();
		if (menuFrag != null) {
			menuFrag.popBackStack();
		}
		FragmentManager fm = getFragmentManager();
		fm.popBackStack();
	}
	@Override
	public void onStart() {
		title = ((MainFrameActivity)getActivity()).getTitleBar();
		title.setTitleText(titleStr);
		super.onStart();
	}
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		StubObject menuItem = (StubObject) mMenuItems.get(position);
		String title = menuItem.getObject("caption", "未知").toString();
//		ArrayList<Object> subMenu = MenuList.getMenuList((String) menuItem.getID());
		List<StubObject> subMenu = Registry.getRegEntryList((String) menuItem.getID());
		AbLogUtil.i(TAG, "Level: subMenu.size() = " + (subMenu == null ? 0 : subMenu.size()));
		AbLogUtil.i(TAG, "----1----"+(String) menuItem.getID());
		if (subMenu != null && subMenu.size() > 0) {
			AbLogUtil.i(TAG, "----2----");
			// 有下一级菜单，需要进行跳转
			MainFrameActivity menuFrag = MainFrameActivity.getInstance();
			if (menuFrag != null) {
				AbLogUtil.i(TAG, "----3----");
				LevelMenuFragment nextLevelFragment = new LevelMenuFragment(menuItem, subMenu,
						mContainerId, true,title);
				menuFrag.jumpAndStoreStack(nextLevelFragment);
			}
		} else if (!TextUtils.isEmpty(menuItem.getString("forms", null))) {
			/*AbLogUtil.i(TAG, "----4----"+menuItem.getString("forms", null));
			// forms不为空，去显示对应的forms
			MainFrameActivity menuFrag = MainFrameActivity.getInstance();
			if (menuFrag != null) {
				AbLogUtil.i(TAG, "----5----");
				menuFrag.showForms(menuItem);
			}*/
		} else {
			Toast.makeText(getActivity(), menuItem.getCaption() + " clicked!", Toast.LENGTH_SHORT)
					.show();
		}
	}
}
