//package com.efounder.fragment;
//
//import android.annotation.SuppressLint;
//import android.content.Intent;
//import android.graphics.Color;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.view.Display;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.view.ViewGroup.LayoutParams;
//import android.widget.AbsListView;
//import android.widget.Button;
//import android.widget.ExpandableListView;
//import android.widget.ExpandableListView.OnChildClickListener;
//import android.widget.ImageView;
//import android.widget.ImageView.ScaleType;
//import android.widget.LinearLayout;
//import android.widget.ListAdapter;
//import android.widget.ListView;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.core.xml.StubObject;
//import com.efounder.activity.NoticeDetailActivity;
//import com.efounder.adapter.MyExpandListviewAdapter;
//import com.efounder.frame.baseui.BaseFragment;
//import com.efounder.builder.base.data.EFDataSet;
//import com.efounder.eai.EAI;
//import com.efounder.eai.data.JParamObject;
//import com.efounder.eai.data.JResponseObject;
//import com.efounder.ospmobilelib.R;
//import com.efounder.util.LoadingDataUtilBlack;
//import com.efounder.view.titlebar.AbTitleBar;
//import com.efounder.widget.AbSlidingPlayView;
//import com.efounder.widget.CommonGridView;
//import com.efounder.widget.MyExpandlistView;
//import com.efounder.widget.ViewPagerwithGridView;
//import com.pansoft.espmodel.NewsAndNotice;
//import com.pansoft.xmlparse.NewsWithNoticeParseUtil;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@SuppressLint("NewApi")
//public class NoticeFragmentBack extends BaseFragment {
//
//	private static final String TAG = "MainFragment";
//	private AbSlidingPlayView mSlidingPlayView = null;
//	private LinearLayout topContentLl;
//
//	AbTitleBar mTitleBar;
//	String titleStr;
//
//	String app_add;
//	// 底部gridview
//	ViewPagerwithGridView vpv;
//	CommonGridView cgv;
//	ArrayList<StubObject> bottomGridList;
//
//	private List<Map<String, Object>> mData;
//
//	private List<HashMap<String, String>> group;
//	private List<List<HashMap<String, String>>> child;
//	private MyExpandListviewAdapter adapter;
//
//	private JParamObject PO;
//	private JResponseObject RO;
//	private String gridTitle;
//
//	MyExpandlistView expandableLayoutListView;
//
//	NewsAndNotice nwn;
//
//	public NoticeFragmentBack(String str) {
//		super();
//		titleStr = str;
//	}
//
//	public NoticeFragmentBack() {
//		super();
//	}
//
//	public NoticeFragmentBack(StubObject mainMenuItem,
//			String title) {
//		// TODO Auto-generated constructor stub
//	}
//
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//			Bundle savedInstanceState) {
//		View rootView = inflater.inflate(R.layout.notice, container, false);
//		/* 1 滚动图片 */
//		topContentLl = (LinearLayout) rootView.findViewById(R.id.topContent);
//		// 组和个AbSlidingPlayView
//		mSlidingPlayView = new AbSlidingPlayView(getActivity());
//
//		ImageView iv1 = new ImageView(this.getActivity());
//		iv1.setImageResource(R.drawable.mainscrollpic1);
//		iv1.setScaleType(ScaleType.FIT_XY);
//
//		ImageView iv2 = new ImageView(this.getActivity());
//		iv2.setImageResource(R.drawable.mainscrollpic2);
//		iv2.setScaleType(ScaleType.FIT_XY);
//
//		ImageView iv3 = new ImageView(this.getActivity());
//		iv3.setImageResource(R.drawable.mainscrollpic3);
//		iv3.setScaleType(ScaleType.FIT_XY);
//
//		mSlidingPlayView.setNavHorizontalGravity(Gravity.CENTER);
//		mSlidingPlayView.addView(iv1);
//		mSlidingPlayView.addView(iv2);
//		mSlidingPlayView.addView(iv3);
//		mSlidingPlayView.startPlay();
//		// 设置高度
//		Display d = this.getActivity().getWindowManager().getDefaultDisplay();
//		mSlidingPlayView.setLayoutParams(new AbsListView.LayoutParams(
//				LayoutParams.FILL_PARENT, (int) (d.getWidth() / 1.8)));
//		mSlidingPlayView.setPadding(0, 0, 0, 10);
//		topContentLl.addView(mSlidingPlayView);
//		// 解决冲突问题
//		mSlidingPlayView
//				.setOnItemClickListener(new AbSlidingPlayView.AbOnItemClickListener() {
//
//					@Override
//					public void onClick(int position) {
//
//					}
//				});
//
//		mSlidingPlayView
//				.setOnPageChangeListener(new AbSlidingPlayView.AbOnChangeListener() {
//
//					@Override
//					public void onChange(int position) {
//					}
//				});
//
//		app_add = "自定义";
//
//		/* 2 grid菜单 */
//		/*
//		 * RelativeLayout bottomGridContain = (RelativeLayout) rootView
//		 * .findViewById(R.id.rightbottomgrid); vpv = new
//		 * ViewPagerwithGridView(this.getActivity(), null); cgv = new
//		 * CommonGridView(this.getActivity(), null); middleGridList =
//		 * setBottomGrid(); cgv.setArrayList(middleGridList);
//		 * bottomGridContain.addView(cgv);
//		 */
//
//		expandableLayoutListView = (MyExpandlistView) rootView
//				.findViewById(R.id.expandlistview);
//		expandableLayoutListView.setFocusable(false);
//
//		RelativeLayout include = (RelativeLayout) rootView
//				.findViewById(R.id.include);
//		include.setBackgroundResource(R.color.red_ios);
//	//	include.setVisibility(View.GONE);
//		LinearLayout leftbacklayout = (LinearLayout) rootView
//				.findViewById(R.id.leftbacklayout);
//		leftbacklayout.setVisibility(View.VISIBLE);
//		TextView title = (TextView) rootView.findViewById(R.id.fragmenttitle);
//		leftbacklayout.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//
//				// NoticeFragment.this.getFragmentManager().popBackStack();
//				getActivity().finish();
//			}
//		});
//		title.setText("新闻公告");
//		title.setTextColor(Color.WHITE);
//
//		if (isAlone()) {
//			include.setVisibility(View.VISIBLE);
//		} else {
//			include.setVisibility(View.GONE);
//		}
//		Button closeButton = (Button) rootView.findViewById(R.id.closeButton);
//
//		closeButton.setVisibility(View.INVISIBLE);
//		closeButton.setBackgroundResource(R.drawable.rightmenu);
//		closeButton.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				// Intent intent = new
//				// Intent(MyFragment.this.getActivity(),SettingActivity.class);
//				// startActivity(intent);
//			}
//		});
//
//		// include.setVisibility(View.GONE);
//		GetDataAsyncTask getDataAsyncTask = new GetDataAsyncTask();
//		getDataAsyncTask.execute();
//
//		return rootView;
//
//	}
//
//	public void onResume() {
//		// TODO Auto-generated method stub
//
//		super.onResume();
//	}
//
//	@Override
//	public void onPause() {
//		// TODO Auto-generated method stub
//		super.onPause();
//
//	}
//
//
//
//	public void setListViewHeightBasedOnChildren(ListView listView) {
//		// 获取ListView对应的Adapter
//		ListAdapter listAdapter = listView.getAdapter();
//		if (listAdapter == null) {
//			return;
//		}
//		int totalHeight = 0;
//		for (int i = 0; i < listAdapter.getCount(); i++) { // listAdapter.getCount()返回数据项的数目
//			View listItem = listAdapter.getView(i, null, listView);
//			listItem.measure(0, 0); // 计算子项View 的宽高
//			totalHeight += listItem.getMeasuredHeight(); // 统计所有子项的总高度
//		}
//		LayoutParams params = listView.getLayoutParams();
//		params.height = totalHeight
//				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
//		// listView.getDividerHeight()获取子项间分隔符占用的高度
//		// params.height最后得到整个ListView完整显示需要的高度
//		listView.setLayoutParams(params);
//	}
//
//	/**
//	 * 添加数据信息
//
//	 */
//
//	/*
//	 * 内部AsyncTask类
//	 */
//	class GetDataAsyncTask extends AsyncTask<Void, Integer, JResponseObject> {
//
//		// 进度显示
//		protected void onPreExecute() {
//
//			// 进度显示
//			super.onPreExecute();
//
//			// 加载dialog显示
//			// if(LoadingDataUtilBlack.getDialog()==null)
//			LoadingDataUtilBlack.show(NoticeFragmentBack.this.getActivity());
//		}
//
//		@Override
//		protected JResponseObject doInBackground(Void... params) {
//
//			nwn = NewsWithNoticeParseUtil.getInstance("option.xml").getNWN();
//			String F_TYPE = null;
//			F_TYPE = nwn.getType();
//			String service = nwn.getServiceKey();
//			String method = nwn.getMethod();
//			gridTitle = nwn.getCaption();
//			PO = JParamObject.Create();
//			// PO.setValue("type", "tz");
//			PO.SetValueByParamName("F_TYPE", F_TYPE);
//			// PO.setValue("title", "F_XWBT");
//			try {
//				// 连接服务器
//				RO = EAI.DAL.IOM(service, method, PO, null, null);
//
//				return RO;
//				// RO.ErrorCode = 0成功 -1失败
//			} catch (Exception e) {
//				e.printStackTrace();
//				return null;
//			}
//		}
//
//		// 当后台操作结束时，此方法将会被调用，计算结果将做为参数传递到此方法中，直接将结果显示到UI组件上
//		protected void onPostExecute(JResponseObject result) {
//			// Dialog dia = LoadingDataUtilBlack.getDialog();
//			LoadingDataUtilBlack.dismiss();
//			if (result == null)
//				return;
//			final EFDataSet efDataSet;
//			try {
//				efDataSet = (EFDataSet) result.getResponseObject();
//			} catch (Exception e) {
//				return;
//			}
//			adapter = new MyExpandListviewAdapter(
//					NoticeFragmentBack.this.getActivity(), efDataSet, gridTitle);
//			expandableLayoutListView.setAdapter(adapter);
//			expandableLayoutListView.setGroupIndicator(null);
//			expandableLayoutListView.expandGroup(0);
//			expandableLayoutListView
//					.setOnChildClickListener(new OnChildClickListener() {
//
//						@Override
//						public boolean onChildClick(ExpandableListView parent,
//								View v, int groupPosition, int childPosition,
//								long id) {
//							//
//							// TabBottomActivity menuFrag =
//							// TabBottomActivity.getInstance();
//							// Fragment frag = new
//							// SimpleWebFragmentWithEFrowset(efDataSet.getRowSet(childPosition),"新闻详情");
//							// ((FragmentContainActivity)
//							// getActivity()).jumpAndStoreStack(frag);
//
//							Intent intent = new Intent(getActivity(),
//									NoticeDetailActivity.class);
//							intent.putExtra("rowset",
//									efDataSet.getRowSet(childPosition));
//							getActivity().startActivity(intent);
//							return false;
//						}
//					});
//			LoadingDataUtilBlack.dismiss();
//		}
//
//	}
//}
