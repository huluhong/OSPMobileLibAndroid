package com.efounder.fragment;

import java.io.File;
import com.efounder.frame.baseui.BaseFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.core.xml.StubObject;
import com.efounder.ospmobilelib.R;
import com.efounder.activity.TabBottomActivity;
import com.efounder.view.titlebar.AbTitleBar;
import com.efounder.widget.EspGridView;
import com.pansoft.resmanager.ResFileManager;
public class ShelfMenuFragment extends BaseFragment {
	public static final String TAG = "ShelfMenuFragment";
	
	ArrayList<HashMap<String, Object>> shelfViewList;
	private EspGridView shelfView;
	RelativeLayout rootRel;
	/**
	 * 对应的某主菜单项
	 */
	private  StubObject mMmainMenuItem; 
	/**
	 * 二级菜单
	 */
	private  List<StubObject> mSecondLevelMenus; // 二级菜单
	/**
	 * 展示容器
	 */
	private static int mContainerId;
	
	private static final String CAPTION="caption";
	private static final String MENUICON="menuIcon";
	
	ShelfMenuFragment subMenuFragment;
	
	// Views
	AbTitleBar title ;//标题栏
	private String titleStr;//标题

	public ShelfMenuFragment(StubObject mainMenuItem, List<StubObject> subMenus, int containerId, String title) {
		mMmainMenuItem = mainMenuItem;
		mSecondLevelMenus = subMenus;
		this.titleStr = title;
		mContainerId = containerId;
		this.titleStr = title;
	}


	@Override
	public void onStart() {
		title = ((TabBottomActivity)getActivity()).getTitleBar();
		title.setTitleText(titleStr);
		super.onStart();
	}
	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {

		View  view =  inflater.inflate(R.layout.submenufrag, container, false);

	

		ArrayList<HashMap<String, Object>> mainMenuList = new ArrayList<HashMap<String,Object>>();
		for(int i=0; i<mSecondLevelMenus.size(); i++){
			HashMap<String, Object> mapgrid = new HashMap<String, Object>();
			String menuIcon=((StubObject)mSecondLevelMenus.get(i)).getObject(MENUICON, "").toString();
			menuIcon = ResFileManager.IMAGE_DIR + "/" + menuIcon;
			Log.i(TAG, i+"menuIcon:"+menuIcon);
			File file = new File(menuIcon); 
            if (file.exists()) { 
            	mapgrid.put("icon",menuIcon);
            }else{
            	mapgrid.put("icon","");
            }
            String caption=((StubObject)mSecondLevelMenus.get(i)).getObject(CAPTION, "").toString();
    		mapgrid.put("caption",caption);
            mainMenuList.add(mapgrid);
		}
		
		rootRel = (RelativeLayout) view.findViewById(R.id.rootRel);
		shelfView = new EspGridView(this.getActivity());
//		shelfView.setGridHDColumns(7);
//		shelfView.setGridMobileColumns(3);
//		shelfView.fillGridData(mainMenuList);
		shelfView.fillGridData(mSecondLevelMenus);
		
		rootRel.addView(shelfView);
		return view;
	}

}
