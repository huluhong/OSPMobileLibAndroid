/*package com.efounder.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.ospmobilelib.R;
import com.pansoft.espcomp.draggrid.adapter.DragAdapter;
import com.pansoft.espcomp.draggrid.adapter.OtherAdapter;
import com.pansoft.espcomp.draggrid.beans.ChannelItem;
import com.pansoft.espcomp.draggrid.db.DatabaseHelper;
import com.pansoft.espcomp.draggrid.servicemodel.ChannelManage;
import com.pansoft.espcomp.draggrid.view.DragGrid;
import com.pansoft.espcomp.draggrid.view.OtherGridView;
import com.pansoft.xmlparse.MenuList;

*//**
 * 频道管理
 *//*
public class ChannelFragment extends Fragment implements OnItemClickListener,OnClickListener {
	public static String TAG = "ChannelFragment";
	private FragmentActivity activity;
	
	private OnOrderChangedListener onOrderChangedListener;
	private  ArrayList<Object> stubObjectDatas; //  XXX
	private PopupWindow popupWindow;
	
	private View view;
	
	*//** 用户栏目的GRIDVIEW *//*
	private DragGrid userGridView;
	*//** 其它栏目的GRIDVIEW *//*
	private OtherGridView otherGridView;
	*//** 用户栏目对应的适配器，可以拖动 *//*
	DragAdapter userAdapter;
	*//** 其它栏目对应的适配器 *//*
	OtherAdapter otherAdapter;
	*//** 其它栏目列表 *//*
	ArrayList<ChannelItem> otherChannelList = new ArrayList<ChannelItem>();
	*//** 用户栏目列表 *//*
	ArrayList<ChannelItem> userChannelList = new ArrayList<ChannelItem>();
	*//** 是否在移动，由于这边是动画结束后才进行的数据更替，设置这个限制为了避免操作太频繁造成的数据错乱。 *//*	
	boolean isMove = false;
	
	DatabaseHelper dbHelper;
	
	public ChannelFragment() {
		super();
	}
	
	public ChannelFragment(ArrayList<Object> stubObjectDatas) {
		super();
		this.stubObjectDatas = stubObjectDatas;
	}

	public void setOnOrderChangedListener(
			OnOrderChangedListener onOrderChangedListener) {
		this.onOrderChangedListener = onOrderChangedListener;
	}
	@Override
	public void onAttach(Activity activity) {
		this.activity = (FragmentActivity)activity;
		this.dbHelper = new DatabaseHelper(activity);
		
		super.onAttach(activity);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.channel, container, false);
		initView();
		initData();
		return view;
	}
	
	*//** 初始化数据*//*
	private void initData() {
		//--1
		ChannelManage.getManage(dbHelper).initDefaultChannel(getRawChannelItems());
	    userChannelList = ((ArrayList<ChannelItem>)ChannelManage.getManage(dbHelper).getUserChannel());
	    otherChannelList = ((ArrayList<ChannelItem>)ChannelManage.getManage(dbHelper).getOtherChannel());
	    userAdapter = new DragAdapter(activity, userChannelList);
	    userGridView.setAdapter(userAdapter);
	    otherAdapter = new OtherAdapter(activity, otherChannelList);
	    otherGridView.setAdapter(otherAdapter);
	    //设置GRIDVIEW的ITEM的点击监听
	    otherGridView.setOnItemClickListener(this);
	    userGridView.setOnItemClickListener(this);
	}
	private List<ChannelItem> getRawChannelItems(){
		//TODO 获取数据
		stubObjectDatas = MenuList.getMenuList("home");
		List<ChannelItem> rawChannelItems = new ArrayList<ChannelItem>();
		if (stubObjectDatas != null) {
			for (int i = 0; i < stubObjectDatas.size(); i++) {
				StubObject stubObject = (StubObject) stubObjectDatas.get(i);
				ChannelItem item = new ChannelItem();
				item.setId(String.valueOf(stubObject.getID()));
				item.setName(stubObject.getCaption());
				rawChannelItems.add(item);
			}
			
		}
		return rawChannelItems;
	}
	*//** 初始化布局*//*
	private void initView() {
		userGridView = (DragGrid) view.findViewById(R.id.userGridView);
		otherGridView = (OtherGridView) view.findViewById(R.id.otherGridView);
		view.findViewById(R.id.back).setOnClickListener(this);//返回
		view.findViewById(R.id.right_text).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.back) {
			if (popupWindow != null)  
				popupWindow.dismiss();
		} else if (id == R.id.right_text) {
			saveChannel();
			if(userAdapter.isListChanged()){
				// 完成界面排序
				if (onOrderChangedListener != null) {
					onOrderChangedListener.onOrderChanged(userChannelList);
				}
				Log.d(TAG, "数据发生改变");
			}else{
			}
			//TODO 动画
//			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
			if (popupWindow != null)  
				popupWindow.dismiss();
		} else {
		}
	}
	
	*//** GRIDVIEW对应的ITEM点击监听接口  *//*
	@Override
	public void onItemClick(AdapterView<?> parent, final View view, final int position,long id) {
		//如果点击的时候，之前动画还没结束，那么就让点击事件无效
		if(isMove){
			return;
		}
		int pareid = parent.getId();
		if (pareid == R.id.userGridView) {
			// position为 0，1 的不可以进行任何操作
//			if (position != 0 && position != 1) {
			if (position >= DragGrid.DRAG_FIX_COUNT) {
				final ImageView moveImageView = getView(view);
				if (moveImageView != null) {
					TextView newTextView = (TextView) view.findViewById(R.id.text_item);
					final int[] startLocation = new int[2];
					newTextView.getLocationInWindow(startLocation);
					final ChannelItem channel = ((DragAdapter) parent.getAdapter()).getItem(position);//获取点击的频道内容
					otherAdapter.setVisible(false);
					//添加到最后一个
					otherAdapter.addItem(channel);
					new Handler().postDelayed(new Runnable() {
						public void run() {
							try {
								int[] endLocation = new int[2];
								//获取终点的坐标
								otherGridView.getChildAt(otherGridView.getLastVisiblePosition()).getLocationInWindow(endLocation);
								MoveAnim(moveImageView, startLocation , endLocation, channel,userGridView);
								userAdapter.setRemove(position);
							} catch (Exception localException) {
							}
						}
					}, 50L);
				}
			}
		} else if (pareid == R.id.otherGridView) {
			final ImageView moveImageView = getView(view);
			if (moveImageView != null){
				TextView newTextView = (TextView) view.findViewById(R.id.text_item);
				final int[] startLocation = new int[2];
				newTextView.getLocationInWindow(startLocation);
				final ChannelItem channel = ((OtherAdapter) parent.getAdapter()).getItem(position);
				userAdapter.setVisible(false);
				//添加到最后一个
				userAdapter.addItem(channel);
				new Handler().postDelayed(new Runnable() {
					public void run() {
						try {
							int[] endLocation = new int[2];
							//获取终点的坐标
							userGridView.getChildAt(userGridView.getLastVisiblePosition()).getLocationInWindow(endLocation);
							MoveAnim(moveImageView, startLocation , endLocation, channel,otherGridView);
							otherAdapter.setRemove(position);
						} catch (Exception localException) {
						}
					}
				}, 50L);
			}
		} else {
		}
	}
	*//**
	 * 点击ITEM移动动画
	 * @param moveView
	 * @param startLocation
	 * @param endLocation
	 * @param moveChannel
	 * @param clickGridView
	 *//*
	private void MoveAnim(View moveView, int[] startLocation,int[] endLocation, final ChannelItem moveChannel,
			final GridView clickGridView) {
		int[] initLocation = new int[2];
		//获取传递过来的VIEW的坐标
		moveView.getLocationInWindow(initLocation);
		//得到要移动的VIEW,并放入对应的容器中
		final ViewGroup moveViewGroup = getMoveViewGroup();
		final View mMoveView = getMoveView(moveViewGroup, moveView, initLocation);
		//创建移动动画
		TranslateAnimation moveAnimation = new TranslateAnimation(
				startLocation[0], endLocation[0], startLocation[1],
				endLocation[1]);
		moveAnimation.setDuration(300L);//动画时间
		//动画配置
		AnimationSet moveAnimationSet = new AnimationSet(true);
		moveAnimationSet.setFillAfter(false);//动画效果执行完毕后，View对象不保留在终止的位置
		moveAnimationSet.addAnimation(moveAnimation);
		mMoveView.startAnimation(moveAnimationSet);
		moveAnimationSet.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				isMove = true;
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				moveViewGroup.removeView(mMoveView);
				// instanceof 方法判断2边实例是不是一样，判断点击的是DragGrid还是OtherGridView
				if (clickGridView instanceof DragGrid) {
					otherAdapter.setVisible(true);
					otherAdapter.notifyDataSetChanged();
					userAdapter.remove();
				}else{
					userAdapter.setVisible(true);
					userAdapter.notifyDataSetChanged();
					otherAdapter.remove();
				}
				isMove = false;
			}
		});
	}
	
	*//**
	 * 获取移动的VIEW，放入对应ViewGroup布局容器
	 * @param viewGroup
	 * @param view
	 * @param initLocation
	 * @return
	 *//*
	private View getMoveView(ViewGroup viewGroup, View view, int[] initLocation) {
		int x = initLocation[0];
		int y = initLocation[1];
		viewGroup.addView(view);
		LinearLayout.LayoutParams mLayoutParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		mLayoutParams.leftMargin = x;
		mLayoutParams.topMargin = y;
		view.setLayoutParams(mLayoutParams);
		return view;
	}
	
	*//**
	 * 创建移动的ITEM对应的ViewGroup布局容器
	 *//*
	private ViewGroup getMoveViewGroup() {
		ViewGroup moveViewGroup = (ViewGroup) activity.getWindow().getDecorView();
		LinearLayout moveLinearLayout = new LinearLayout(activity);
		moveLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		moveViewGroup.addView(moveLinearLayout);
		return moveLinearLayout;
	}
	
	*//**
	 * 获取点击的Item的对应View，
	 * @param view
	 * @return
	 *//*
	private ImageView getView(View view) {
		view.destroyDrawingCache();
		view.setDrawingCacheEnabled(true);
		Bitmap cache = Bitmap.createBitmap(view.getDrawingCache());
		view.setDrawingCacheEnabled(false);
		ImageView iv = new ImageView(activity);
		iv.setImageBitmap(cache);
		return iv;
	}
	
	*//** 退出时候保存选择后数据库的设置  *//*
	private void saveChannel() {
		ChannelManage.getManage(dbHelper).deleteAllChannel();
		ChannelManage.getManage(dbHelper).saveUserChannel(userAdapter.getChannnelLst());
		ChannelManage.getManage(dbHelper).saveOtherChannel(otherAdapter.getChannnelLst());
	}
	
	@Override
	public void onDestroy() {
		if (dbHelper != null) {
			dbHelper.close();
		}
		super.onDestroy();
	}
	
	
	public void setStubObjectDatas(ArrayList<Object> stubObjectDatas) {
		this.stubObjectDatas = stubObjectDatas;
	}

	public void setPopupWindow(PopupWindow popupWindow) {
		this.popupWindow = popupWindow;
	}


	*//**
	 * 排序发生改变的监听
	 * @author hudq
	 *
	 *//*
	public interface OnOrderChangedListener{
		*//**
		 * 当排序发生改变
		 * @param userChannelList
		 *//*
		public void onOrderChanged(ArrayList<ChannelItem> userChannelList);
	}
}
*/