/*package com.efounder.fragment;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.core.xml.StubObject;
import com.efounder.MainFrameActivity;
import com.efounder.ospmobilelib.R;
import com.efounder.activity.AbActivity;
import com.efounder.fragment.ChannelFragment.OnOrderChangedListener;
import com.efounder.util.AbFragmentManager;
import com.efounder.util.AbLogUtil;
import com.pansoft.espcomp.PopMenu;
import com.pansoft.espcomp.draggrid.beans.ChannelItem;
import com.pansoft.espcomp.draggrid.db.DatabaseHelper;
import com.pansoft.espcomp.draggrid.servicemodel.ChannelManage;
import com.pansoft.resmanager.ResFileManager;
import com.pansoft.xmlparse.DomXmlParse;
import com.pansoft.xmlparse.MenuList;

public class ConfigurableFragment extends Fragment implements OnClickListener,OnOrderChangedListener{
	public static final String TAG = "ConfigurableFragment";
	private AbActivity abActivity;
	*//**
	 * 对应的某主菜单项
	 *//*
	private  StubObject mMmainMenuItem;
	*//**
	 * 二级菜单
	 *//*
	private ArrayList<Object> mSecondLevelMenus; //  
	private String titleStr;//标题
	private View rootView;
	private PopMenu popMenu;
	配置模块弹出窗
	private PopupWindow popupWindow;// 
	@Override
	public void onAttach(Activity activity) {
		this.abActivity = (AbActivity) activity;
		super.onAttach(activity);
	}
	
	public ConfigurableFragment(StubObject mMmainMenuItem, String titleStr) {
		super();
		this.mMmainMenuItem = mMmainMenuItem;
		this.mSecondLevelMenus =  MenuList.getMenuList((String) mMmainMenuItem.getID());
		this.titleStr = titleStr;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_configurable, container, false);
		initPopupWindow(rootView);
		initView(rootView);
		return rootView;
	}

	private void initView(final View rootView){
		ViewTreeObserver observer = rootView.getViewTreeObserver();
//		observer.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
//			
//			@Override
//			public void onGlobalLayout() {
////				rootView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//				rootView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
//				Log.i(TAG, "OnGlobalLayoutListener-----------"+titleStr);
//				abActivity.getTitleBar().setTitleText(titleStr);//设置标题
//			}
//		});
		observer.addOnPreDrawListener(new OnPreDrawListener() {
			
			@Override
			public boolean onPreDraw() {
				rootView.getViewTreeObserver().removeOnPreDrawListener(this);
				Log.i(TAG, "OnPreDrawListener-----------"+titleStr);
				abActivity.getTitleBar().setTitleText(titleStr);//设置标题
				return true;
			}
		});
		//显示右上角菜单按钮
		LinearLayout menuLL = abActivity.getTitleBar().getRightLayout();
		menuLL.setVisibility(View.VISIBLE);
		ImageView menuIV = new ImageView(abActivity);
		menuIV.setImageResource(R.drawable.button_selector_menu_more);
		menuIV.setOnClickListener(this);
		//menuIV.setId(R.id.menu_more);
		menuLL.addView(menuIV);
		
		//初始化弹出菜单
		popMenu = new PopMenu(abActivity);
		popMenu.addItem("编辑");
		popMenu.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				switch (position) {
				case 0:
					popupWindow.setFocusable(true);
					popupWindow.showAtLocation(rootView, Gravity.CENTER, 0, 0);
					popMenu.dismiss();
					break;

				default:
					break;
				}
			}
		});
		//XXX 尝试解决滑动冲突(不起作用)
//		rootView.findViewById(R.id.scroll_ll).setOnTouchListener(new OnTouchListener() {
//			
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//				v.getParent().requestDisallowInterceptTouchEvent(true);
//				Log.i("","event.getAction:"+ event.getAction());
//				return false;
//			}
//		});
		List<StubObject> sortedMenus= sortMenus(ChannelManage.getManage(new DatabaseHelper(abActivity)).getUserChannel());
		createConfiguredView(rootView,sortedMenus);
	}
	@SuppressWarnings("deprecation")
	private void initPopupWindow(View rootView){
		View contentView = getActivity().getLayoutInflater().inflate(R.layout.popupwindow_deploy_module, null, false);
		
		contentView.setFocusable(true);
		contentView.setFocusableInTouchMode(true); 
		contentView.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				switch (keyCode) {
				case KeyEvent.KEYCODE_BACK:
					popupWindow.dismiss();  
					break;
				default:
					break;
				}
				return true;
			}
		});
		
		int width = (int) (getActivity().getWindowManager().getDefaultDisplay().getWidth() *0.9);
		int height = (int) (getActivity().getWindowManager().getDefaultDisplay().getHeight() *0.9);
		
		popupWindow = new PopupWindow(contentView, width, height);
//		popupWindow.setBackgroundDrawable(new ColorDrawable(0x00ffffff));
		//TODO 添加动画
//		popupWindow.setAnimationStyle(R.style.);
		FragmentManager manager =  abActivity.getSupportFragmentManager();
		ChannelFragment channelFragment = (ChannelFragment)manager.findFragmentById(R.id.ChannelFragment);
//		manager.beginTransaction().replace(R.id.fragment_container, new ChannelFragment(mSecondLevelMenus)).commit();
		//1 设置需要排序的数据  XXX
//		channelFragment.setStubObjectDatas(mSecondLevelMenus);
		//2 popupWindow
		channelFragment.setPopupWindow(popupWindow);
		//3 设置顺序发生改变时的监听
		channelFragment.setOnOrderChangedListener(this);
	}
	*//**
	 * 
	 * @param rootView
	 * @param sortedMenus
	 *//*
	private void createConfiguredView(View rootView,List<? extends Object> sortedMenus){
		LinearLayout parent = (LinearLayout) rootView.findViewById(R.id.root);//parent
		parent.removeAllViews();//先删除子View
		for (int i = 0; i < sortedMenus.size(); i++) {
			StubObject stubObject = (StubObject) sortedMenus.get(i);//数据
			
			//子界面标题
			TextView subTitle = new TextView(abActivity);//标题
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, abActivity.getResources().getDisplayMetrics()));
			subTitle.setLayoutParams(params);
			subTitle.setBackgroundColor(Color.GRAY);
			subTitle.setGravity(Gravity.CENTER_VERTICAL);
			subTitle.setTextColor(Color.WHITE);
			subTitle.setText(stubObject.getCaption());
			//子界面
			LinearLayout child = new LinearLayout(abActivity);
			child.setOrientation(LinearLayout.VERTICAL);
			//设置宽高
			String width = stubObject.getString("width", "MATCH_PARENT");//默认宽度 MATCH_PARENT
			String height = stubObject.getString("height", "WRAP_CONTENT");//默认高度 WRAP_CONTENT
			LinearLayout.LayoutParams paramsChild = new LinearLayout.LayoutParams(computeWinthHeight(width), computeWinthHeight(height));
			//bottomMargin=5dp
			paramsChild.bottomMargin = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, abActivity.getResources().getDisplayMetrics());
			child.setLayoutParams(paramsChild);
			
			
			child.addView(subTitle);
			parent.addView(child);
			
			
			
			String connID = stubObject.getString("connID", "");
			if (!"".equals(connID)) {//connID 存在
				StubObject mainMenuItem = getMainMenuItem(connID);
				if (mainMenuItem != null) {
					child.setId(i+10);
					//Fragment fragment = AbFragmentManager.getFragment(mainMenuItem);
					AbFragmentManager ab = new AbFragmentManager(this.getActivity());
					
					Fragment	fragment = ab.getFragment(mainMenuItem);
					FragmentManager manager = abActivity.getSupportFragmentManager();
					FragmentTransaction transaction = manager.beginTransaction();
					transaction.add(i+10, fragment);
					transaction.commit();
				}
			}else {
				child.addView(getFormView(stubObject));
			}
			
		}
	}
	
	*//**
	 * 计算宽高数值
	 * @param value
	 * @return
	 *//*
	private int computeWinthHeight(String value){
		int intValue = -2;
		if ("MATCH_PARENT".equals(value)) {
			intValue = -1;
		}else if ("WRAP_CONTENT".equals(value)) {
			intValue = -2;
		}else {
			try {
				int i = Integer.parseInt(value);
				intValue = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, i, getResources().getDisplayMetrics());
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		return intValue;
	}
	
	private StubObject getMainMenuItem(String connID){
		ArrayList<Object> mainMenuList = MenuList.getMenuList(MainFrameActivity.KEY_MENU_ROOT);
		for (int i = 0; i < mainMenuList.size(); i++) {
			StubObject stubObject = (StubObject) mainMenuList.get(i);
			if (connID.equals(String.valueOf(stubObject.getID()))) {
				return stubObject;
			}
		}	
		return null;
	}
	private View getFormView(StubObject mData){
		View view = null;

		// 取"forms"属性
		String formName = mData.getString("forms", null);
		
		if (TextUtils.isEmpty(formName)) {
			TextView textView = new TextView(getActivity());
			textView.setText("StubObject 中 forms 为 null");
			view = textView;
		}
		AbLogUtil.i(TAG, "formName:"+formName);
		File f = new File(ResFileManager.FORM_DIR + "/" + formName + ".xml");
		AbLogUtil.i(TAG, "file:"+f);
		if (!f.exists()) {
			Toast.makeText(getActivity(), "对应表单文件不存在！", Toast.LENGTH_SHORT).show();
		}

		Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
			}
		};

		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(f);
			DomXmlParse dom = new DomXmlParse(getActivity(), handler);
			dom.readXML(inputStream);
			if(dom.getFormList().size()>0)
				view = dom.getFormList().get(0);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					// ignore
				}
			}
		}
	
		return view;
	}
	@Override
	public void onStart() {
		//显示右上角菜单按钮
		LinearLayout menuLL = abActivity.getTitleBar().getRightLayout();
		menuLL.setVisibility(View.VISIBLE);
		abActivity.getTitleBar().setTitleText(titleStr);//设置标题
		super.onStart();
	}
	@Override
	public void onPause() {
		//隐藏右上角菜单按钮
		LinearLayout menuLL = abActivity.getTitleBar().getRightLayout();
		menuLL.setVisibility(View.GONE);
		super.onPause();
	}
	
	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.menu_more) {
			popMenu.show(v);
		} else {
		}
	}

	@Override
	public void onOrderChanged(ArrayList<ChannelItem> userChannelList) {
		createConfiguredView(rootView, sortMenus(userChannelList));
	}
	
	*//**
	 * 将mSecondLevelMenus排序、筛选
	 * @param userChannelList
	 * @return
	 *//*
	public List<StubObject> sortMenus(List<ChannelItem> userChannelList) {

		List<StubObject> sortedList = new ArrayList<StubObject>();
		outer:for (ChannelItem channelItem : userChannelList) {

			for (Object object : mSecondLevelMenus) {
				StubObject stubObject = (StubObject) object;
				if (channelItem.getId().equals(String.valueOf(stubObject.getID()))) {
					sortedList.add(stubObject);
					continue outer;
				}
			}
		}

		return sortedList;
	}
}
*/