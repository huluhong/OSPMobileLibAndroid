//package com.efounder.fragment;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.List;
//import java.util.Locale;
//
//import android.annotation.TargetApi;
//import android.app.Activity;
//import android.content.ContentResolver;
//import android.content.Intent;
//import android.database.Cursor;
//import android.graphics.Color;
//import android.os.Build;
//import android.os.Bundle;
//import android.provider.ContactsContract.CommonDataKinds.Phone;
//import android.support.v4.app.Fragment;
//import android.text.Editable;
//import android.text.TextUtils;
//import android.text.TextWatcher;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.core.xml.StubObject;
//import com.efounder.ospmobilelib.R;
//import com.efounder.activity.ContactsDetailsActivity;
//import com.efounder.thirdpartycomps.stickylistheaders.StickyListHeadersListView;
//import com.efounder.util.SideBar;
//import com.efounder.util.SideBar.OnTouchingLetterChangedListener;
//import com.pansoft.espcomp.sortlistview.ClearEditText;
//import com.pansoft.espcomp.sortlistview.HanyuParser;
//import com.pansoft.espcomp.sortlistview.PinyinComparator;
//import com.pansoft.espcomp.sortlistview.SortAdapter;
//import com.pansoft.espcomp.sortlistview.SortModel;
////import com.pansoft.espcomp.sortlistview.SideBar;
////import com.pansoft.espcomp.sortlistview.SideBar.OnTouchingLetterChangedListener;
//
///**
// * 通讯录
// *
// * @author hudq
// *
// */
//public class ContactsFragment extends Fragment implements
//		AdapterView.OnItemClickListener,
//		StickyListHeadersListView.OnHeaderClickListener,
//		StickyListHeadersListView.OnStickyHeaderOffsetChangedListener,
//		StickyListHeadersListView.OnStickyHeaderChangedListener,
//		OnClickListener {
//	private StubObject mainMenuItem;
//	// AbActivity abActivity;
//	Activity abActivity;
//	private StickyListHeadersListView stickyList;
//	private SideBar sideBar;
//	private TextView dialog;
//	private SortAdapter adapter;
//	private ClearEditText mClearEditText;
//	private View view;
//	private boolean fadeHeader = true;
//	private List<SortModel> SourceDateList;
//	private List<SortModel> contactList = null;
//
//	/**
//	 * 根据拼音来排列ListView里面的数据类
//	 */
//	private PinyinComparator pinyinComparator;
//
//	public ContactsFragment(StubObject mainMenuItem) {
//		super();
//		this.mainMenuItem = mainMenuItem;
//	}
//
//	public ContactsFragment() {
//	}
//
//	/*
//	 * @Override public void onAttach(Activity activity) { this.abActivity =
//	 * (AbActivity) activity; super.onAttach(activity); }
//	 */
//
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//			Bundle savedInstanceState) {
//		View view = inflater.inflate(R.layout.fragment_contacts, container,
//				false);
//
//		initViews(view);
//
//		return view;
//	}
//
//	private void initViews(View view) {
//		// 实例化汉字转拼音类
//		pinyinComparator = new PinyinComparator();
//		mClearEditText = (ClearEditText) view.findViewById(R.id.filter_edit);
//		mClearEditText.clearFocus();
//		sideBar = (SideBar) view.findViewById(R.id.sidrbar);
//		dialog = (TextView) view.findViewById(R.id.dialog);
//		sideBar.setTextView(dialog);
//
//		RelativeLayout include = (RelativeLayout) view
//				.findViewById(R.id.include);
//		include.setBackgroundResource(R.color.red_ios);
//		LinearLayout leftbacklayout = (LinearLayout) view
//				.findViewById(R.id.leftbacklayout);
//		leftbacklayout.setVisibility(View.VISIBLE);
//		leftbacklayout.setOnClickListener(this);
//		TextView title = (TextView) view.findViewById(R.id.fragmenttitle);
//		title.setText("通讯录");
//		title.setTextColor(Color.WHITE);
//		include.setVisibility(View.VISIBLE);
//
//		// 设置右侧触摸监听
//		/*
//		 * sideBar.setOnTouchingLetterChangedListener(new
//		 * OnTouchingLetterChangedListener() {
//		 *
//		 * @Override public void onTouchingLetterChanged(String s) { //
//		 * 该字母首次出现的位置 int position = adapter.getPositionForSection(s.charAt(0));
//		 * if (position != -1) { stickyList.setSelection(position); }
//		 *
//		 * } });
//		 */
//		// 设置右侧触摸监听
//		sideBar.setOnTouchingLetterChangedListener(new OnTouchingLetterChangedListener() {
//
//			@Override
//			public void onTouchingLetterChanged(String s) {
//				int position = adapter.getPositionForSection(s.charAt(0));
//				if (position != -1) {
//					stickyList.setSelection(position);
//				}
//
//			}
//		});
//
//		stickyList = (StickyListHeadersListView) view
//				.findViewById(R.id.country_lvcountry);
//		stickyList.setOnItemClickListener(this);
//		stickyList.setOnHeaderClickListener(this);
//		stickyList.setOnStickyHeaderChangedListener(this);
//		stickyList.setOnStickyHeaderOffsetChangedListener(this);
//		stickyList.setDrawingListUnderStickyHeader(true);
//		stickyList.setAreHeadersSticky(true);
//		stickyList.setStickyHeaderTopOffset(-10);
//		stickyList.setFastScrollEnabled(false);
//		stickyList.setFastScrollAlwaysVisible(false);
//		stickyList.setSelector(R.color.transparent);
//
//		// 加载本地联系人
//		loadContacts();
//
//		/*
//		 * SourceDateList =
//		 * filledData(getResources().getStringArray(R.array.date));
//		 *
//		 * // 根据a-z进行排序源数据 Collections.sort(SourceDateList, pinyinComparator);
//		 * adapter = new SortAdapter(getActivity(), SourceDateList);
//		 * stickyList.setAdapter(adapter);
//		 */
//
//		// mClearEditText.setInputType(InputType.TYPE_NULL);
//
//		// 根据输入框输入值的改变来过滤搜索
//		mClearEditText.addTextChangedListener(new TextWatcher() {
//
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before,
//					int count) {
//				// 当输入框里面的值为空，更新为原来的列表，否则为过滤数据列表
//				filterData(s.toString());
//			}
//
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,
//					int after) {
//
//			}
//
//			@Override
//			public void afterTextChanged(Editable s) {
//			}
//		});
//	}
//
//	/**
//	 * 为ListView填充数据
//	 *
//	 * @param date
//	 * @return
//	 */
//	private List<SortModel> filledData(String[] date) {
//		List<SortModel> mSortList = new ArrayList<SortModel>();
//		for (int i = 0; i < date.length; i++) {
//			SortModel sortModel = new SortModel();
//			sortModel.setName(date[i]);
//			// 汉字转换成拼音
//			String pinyin = new HanyuParser().getStringPinYin(date[i]);
//			String sortString = pinyin.substring(0, 1).toUpperCase(
//					Locale.getDefault());
//			// 正则表达式，判断首字母是否是英文字母
//			if (sortString.matches("[A-Z]")) {
//				sortModel.setSortLetters(sortString.toUpperCase(Locale
//						.getDefault()));
//			} else {
//				sortModel.setSortLetters("#");
//			}
//			mSortList.add(sortModel);
//		}
//		return mSortList;
//
//	}
//
//	/**
//	 * 为ListView填充数据
//	 *
//	 * @param date
//	 * @return
//	 */
//	private List<SortModel> filledData(List<SortModel> list) {
//		List<SortModel> mSortList = new ArrayList<SortModel>();
//		for (int i = 0; i < list.size(); i++) {
//			SortModel sortModel = new SortModel();
//			sortModel.setName(list.get(i).getName());
//			sortModel.setNumber(list.get(i).getNumber());
//			// 汉字转换成拼音
//			String pinyin = new HanyuParser().getStringPinYin(list.get(i)
//					.getName());
//			String sortString = pinyin.substring(0, 1).toUpperCase(
//					Locale.getDefault());
//			// 正则表达式，判断首字母是否是英文字母
//			if (sortString.matches("[A-Z]")) {
//				sortModel.setSortLetters(sortString.toUpperCase(Locale
//						.getDefault()));
//			} else {
//				sortModel.setSortLetters("#");
//			}
//			mSortList.add(sortModel);
//		}
//		return mSortList;
//
//	}
//
//	/**
//	 * 根据输入框中的值来过滤数据并更新ListView
//	 *
//	 * @param filterStr
//	 */
//	private void filterData(String filterStr) {
//		List<SortModel> filterDateList = new ArrayList<SortModel>();
//
//		if (TextUtils.isEmpty(filterStr)) {
//			filterDateList = SourceDateList;
//		} else {
//			filterDateList.clear();
//			for (SortModel sortModel : SourceDateList) {
//				String name = sortModel.getName();
//				// if (name.indexOf(filterStr.toString()) != -1 ||
//				// characterParser.getSelling(name).startsWith(filterStr.toString()))
//				// {
//				// if (name.contains(filterStr.toString()) ||
//				// characterParser.getSelling(name).contains(filterStr.toString()))
//				// {
//				// filterDateList.add(sortModel);
//				// }
//				if (containString(name, filterStr)) {
//					filterDateList.add(sortModel);
//				}
//			}
//		}
//
//		// 根据a-z进行排序
//		Collections.sort(filterDateList, pinyinComparator);
//		adapter.updateListView(filterDateList);
//	}
//
//	private boolean containString(String name, String filterStr) {
//		String namePinyin = new HanyuParser().getStringPinYin(name);
//		for (int i = 0; i < filterStr.length(); i++) {
//			String singleStr = filterStr.substring(i, i + 1);
//			// 汉字
//			if (name.contains(singleStr)) {
//				if (i == filterStr.length() - 1) {
//					return true;
//				}
//				continue;
//			}
//			// 英文
//			if (namePinyin.contains(singleStr)) {
//				int currentIndex = namePinyin.indexOf(singleStr);
//				namePinyin = namePinyin.substring(currentIndex + 1,
//						namePinyin.length());
//			} else {// 不包含
//				break;
//			}
//			if (i == filterStr.length() - 1) {
//				return true;
//			}
//		}
//		return false;
//	}
//
//	@Override
//	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
//	public void onStickyHeaderOffsetChanged(StickyListHeadersListView l,
//			View header, int offset) {
//		if (fadeHeader
//				&& Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//			header.setAlpha(1 - (offset / (float) header.getMeasuredHeight()));
//		}
//	}
//
//	@Override
//	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
//	public void onStickyHeaderChanged(StickyListHeadersListView l, View header,
//			int itemPosition, long headerId) {
//		header.setAlpha(1);
//	}
//
//	@SuppressWarnings("unchecked")
//	@Override
//	public void onItemClick(AdapterView<?> parent, View view, int position,
//			long id) {
//
//		SortModel model = new SortModel();
//		model = (SortModel) adapter.getItem(position);
//		Bundle bundle = new Bundle();
//
//		bundle.putSerializable("data", model);
//		Intent intent = new Intent(getActivity(), ContactsDetailsActivity.class);
//		intent.putExtra("dataSource", bundle);
//		startActivity(intent);
//	}
//
//	@Override
//	public void onHeaderClick(StickyListHeadersListView l, View header,
//			int itemPosition, long headerId, boolean currentlySticky) {
//		Toast.makeText(getActivity(),
//				"Header " + headerId + " currentlySticky ? " + currentlySticky,
//				Toast.LENGTH_SHORT).show();
//	}
//
//	@Override
//	public void onClick(View v) {
//		getActivity().finish();
//
//	}
//
//	/**
//	 * 加载手机中的联系人
//	 */
//	private void loadContacts() {
//		new Thread(new Runnable() {
//			@Override
//			public void run() {
//				try {
//					ContentResolver resolver = getActivity()
//							.getApplicationContext().getContentResolver();
//					Cursor phoneCursor = resolver.query(Phone.CONTENT_URI,
//							new String[] { Phone.DISPLAY_NAME, Phone.NUMBER,
//									Phone.CONTACT_ID, "sort_key" }, null, null,
//							"sort_key COLLATE LOCALIZED ASC");
//					if (phoneCursor == null || phoneCursor.getCount() == 0) {
//						Toast.makeText(getActivity().getApplicationContext(),
//								"未获得读取联系人权限 或 未获得联系人数据", Toast.LENGTH_SHORT)
//								.show();
//						return;
//					}
//					/** 电话号码 **/
//					int PHONES_NUMBER_INDEX = phoneCursor
//							.getColumnIndex(Phone.NUMBER);
//					/** 联系人显示名称 **/
//					int PHONES_DISPLAY_NAME_INDEX = phoneCursor
//							.getColumnIndex(Phone.DISPLAY_NAME);
//					int SORT_KEY_INDEX = phoneCursor.getColumnIndex("sort_key");
//					/** 联系人的ID **/
//					int PHONES_CONTACT_ID_INDEX = phoneCursor
//							.getColumnIndex(Phone.CONTACT_ID);
//
//					if (phoneCursor.getCount() > 0) {
//						contactList = new ArrayList<SortModel>();
//						while (phoneCursor.moveToNext()) {
//							// 得到联系人电话
//							String phoneNumber = phoneCursor
//									.getString(PHONES_NUMBER_INDEX);
//							if (TextUtils.isEmpty(phoneNumber))
//								continue;
//							// 得到联系人姓名
//							String contactName = phoneCursor
//									.getString(PHONES_DISPLAY_NAME_INDEX);
//							// 得到联系人ID
//							Long contactsID = phoneCursor
//									.getLong(PHONES_CONTACT_ID_INDEX);
//							// String sortKey =
//							// phoneCursor.getString(SORT_KEY_INDEX);
//							SortModel sortModel = new SortModel();
//
//							// System.out.println(sortKey);
//							// SortModel sortModel = new SortModel(contactName,
//							// phoneNumber, sortKey);
//							// 优先使用系统sortkey取,取不到再使用工具取
//							/*
//							 * String sortLetters =
//							 * getSortLetterBySortKey(sortKey); if (sortLetters
//							 * == null) { sortLetters =
//							 * getSortLetter(contactName); }
//							 * sortModel.sortLetters = sortLetters;
//							 * sortModel.sortToken = parseSortKey(sortKey);
//							 */
//							sortModel.setName(contactName);
//							sortModel.setNumber(phoneNumber);
//							sortModel.setContactsID(contactsID);
//							contactList.add(sortModel);
//						}
//					}
//					phoneCursor.close();
//					getActivity().runOnUiThread(new Runnable() {
//						public void run() {
//
//							String[] names = new String[contactList.size()];
//							// 屏蔽掉，这段代码只是对名字过滤，不包含电话
//							/*
//							 * for (int i = 0; i < contactList.size(); i++) {
//							 * names[i] = contactList.get(i).getName(); }
//							 * SourceDateList = filledData(names);
//							 */
//							SourceDateList = filledData(contactList);
//
//							Collections.sort(SourceDateList, pinyinComparator);
//							adapter = new SortAdapter(getActivity(),
//									SourceDateList);
//							stickyList.setAdapter(adapter);
//						}
//					});
//				} catch (Exception e) {
//					Log.e("xbc", e.getLocalizedMessage());
//				}
//			}
//		}).start();
//	}
//
//}
