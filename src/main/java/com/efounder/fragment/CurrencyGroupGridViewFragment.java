package com.efounder.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.core.xml.StubObject;
import com.efounder.adapter.CurrencyGroupGridViewAdapter;
import com.efounder.bean.CurrencyGroupBean;
import com.efounder.frame.baseui.BaseFragment;
import com.efounder.imageloader.GlideImageLoader;
import com.efounder.ospmobilelib.R;
import com.efounder.service.Registry;
import com.efounder.util.AbFragmentManager;
import com.efounder.util.MenuGnqxUtil;
import com.efounder.widget.AbSlidingPlayView;
import com.pansoft.resmanager.ResFileManager;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cimu
 * @date 2018/10/19
 * 配置文件通用分组fragment
 */

public class CurrencyGroupGridViewFragment extends BaseFragment {
    private View mView;
    private RecyclerView rvWork;
    private StubObject mainMenuItem;
    private List<CurrencyGroupBean> currencyGroupBeans = new ArrayList<>();
    // 应用模块中子模块stubList
    private List<StubObject> xmlMenus;

    private CurrencyGroupGridViewAdapter mCurrencyGroupGridViewAdapter;
    //轮播图
    private AbSlidingPlayView mSlidingPlayView;
    private AbFragmentManager abFragmentManager;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_currency_group_gridview, container, false);
        } else {
            //缓存的rootView需要判断是否已经被加过parent， 如果有parent需要从parent删除，要不然会发生这个rootview已经有parent的错误。
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null) {
                parent.removeView(mView);
            }
        }
        initView();
        initData();
        return mView;
    }

    private void initView() {
        rvWork = mView.findViewById(R.id.rv_work);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvWork.setLayoutManager(linearLayoutManager);
        mSlidingPlayView = mView.findViewById(R.id.abslidingplayview);
    }

    private void initData() {
        if (getArguments() != null) {
            mainMenuItem = (StubObject) getArguments().getSerializable("stubObject");
            if (mainMenuItem != null) {
                initBannerData();
                initRecycleViewData();
            }
        }
    }

    /**
     * 初始化recycleview 菜单数据
     */
    private void initRecycleViewData() {
        xmlMenus = Registry.getRegEntryList((String) mainMenuItem.getID());
        if (xmlMenus != null) {
            MenuGnqxUtil.handleGNQX(xmlMenus);
            String title = "";
            for (int i = 0; i < xmlMenus.size(); i++) {
                title = xmlMenus.get(i).getCaption();
                List<StubObject> xmlFourMenus = Registry.getRegEntryList((String) xmlMenus.get(i).getID());
                MenuGnqxUtil.handleGNQX(xmlFourMenus);
                if (xmlFourMenus != null && xmlFourMenus.size() == 0) {
                    continue;
                }
                CurrencyGroupBean currencyGroupBean = new CurrencyGroupBean();
                currencyGroupBean.setTitle(title);
                currencyGroupBean.setMenuIcon(xmlMenus.get(i).getString("menuIcon", ""));
                currencyGroupBean.setGridView(xmlFourMenus);
                currencyGroupBeans.add(currencyGroupBean);
            }
            mCurrencyGroupGridViewAdapter = new CurrencyGroupGridViewAdapter(getContext(), currencyGroupBeans);
            rvWork.setAdapter(mCurrencyGroupGridViewAdapter);
        }
    }

    //初始化轮播图
    private void initBannerData() {
        String showBanner = mainMenuItem.getString("showBanner", "0");
        if (!showBanner.equals("1")) {
            return;
        }
        // banner 轮播图
        final List<StubObject> bannerStubObjects = Registry.getRegEntryList((String) mainMenuItem.getString("bannerMenuID", ""));
        if (bannerStubObjects == null) {
            return;
        }

        abFragmentManager = new AbFragmentManager(getActivity());
        mSlidingPlayView.setVisibility(View.VISIBLE);
        mSlidingPlayView.setNavHorizontalGravity(Gravity.CENTER);
        //轮播图放置图片
        setBanner(bannerStubObjects);
        mSlidingPlayView.startPlay();
        // 设置高度
        Display d = this.getActivity().getWindowManager().getDefaultDisplay();
        // 更改次参数可以改变首页上方显示图片的大小
        mSlidingPlayView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (d.getWidth() / 2)));
        // LayoutParams.FILL_PARENT, (int) (d.getWidth() / 3.14)));
        mSlidingPlayView.setPadding(0, 0, 0, 10);
        mSlidingPlayView.setOnItemClickListener(new AbSlidingPlayView.AbOnItemClickListener() {
            @Override
            public void onClick(int position) {
                try {
                    abFragmentManager.startActivity(bannerStubObjects.get(position), 0, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void setBanner(List<StubObject> bannerList) {
        for (int i = 0; i < bannerList.size(); i++) {
            StubObject stubObject = bannerList.get(i);
            String bannerIcon = "file://" + ResFileManager.IMAGE_DIR + "/" + stubObject.getString("bannerIcon", "");
            try {
                ImageView iv = new ImageView(this.getActivity());
                iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
                GlideImageLoader.getInstance().displayImage(getContext(), iv, bannerIcon, R.drawable.bg_about, R.drawable.bg_about);
                mSlidingPlayView.addView(iv);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
