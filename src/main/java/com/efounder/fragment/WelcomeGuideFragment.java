package com.efounder.fragment;

import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;

import com.efounder.adapter.ViewPagerAdapterforWebview;
import com.efounder.frame.baseui.BaseFragment;
import com.efounder.ospmobilelib.R;

import java.util.ArrayList;


/**
 * Created by lch on 2016/7/25.
 */
public class WelcomeGuideFragment extends BaseFragment {
	private ArrayList<WebView> mPageViews;

	private LayoutInflater mInflater;

	private ViewPager mViewPager;
	Button btn;

	boolean mishow = false;
    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.act_main, null);
        //setContentView(R.layout.act_main);
        
        btn = (Button) rootView.findViewById(R.id.btn_enter);
 
        btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			     //  enterMainActivity();
			}
		});
		mInflater = this.getActivity().getLayoutInflater();
		mPageViews = new ArrayList<WebView>();
		mPageViews.add((WebView) mInflater.inflate(R.layout.page_01, null));
		mPageViews.add((WebView) mInflater.inflate(R.layout.page_01, null));
		mPageViews.add((WebView) mInflater.inflate(R.layout.page_01, null));
		
		mViewPager = (ViewPager) rootView.findViewById(R.id.myviewpager);
		mViewPager.setAdapter(new ViewPagerAdapterforWebview(mPageViews));
		mViewPager.setOnPageChangeListener(new OnPageChange());
        return rootView;

    }

    public void setstartbuttonShow(Boolean isshow){
    	this.mishow = isshow;
    }
	class OnPageChange implements OnPageChangeListener {

		@Override
		public void onPageSelected(int arg0) {
			// TODO Auto-generated method stub
			if(arg0!=2){
			//	WelcomeGuideFragment.this.setstartbuttonShow(false);
				btn.setVisibility(View.GONE);	
			}else{
				if(mishow)
					btn.setVisibility(View.VISIBLE);		
				
			}
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onPageScrollStateChanged(int arg0) {
			// TODO Auto-generated method stub

		}
	}

	
//	private void enterMainActivity() {
//        Intent intent = new Intent(this.getActivity(),
//                WelcomeActivity.class);
//        startActivity(intent);
//        SharedPreferencesUtil.putBoolean(WelcomeGuideFragment.this.getActivity(), SharedPreferencesUtil.FIRST_OPEN, false);
//       // finish();
//    }
}
