package com.efounder.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;

import com.core.xml.StubObject;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.baseui.BaseFragment;
import com.efounder.ospmobilelib.R;
import com.efounder.util.MyStaticWebView;
import com.efounder.util.StorageUtil;
import com.efounder.widget.ESPWebView;

import java.util.Hashtable;

import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_ADDRESS;
import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_HTTP_TYPE;
import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_PATH;
import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_PORT;

/**
 * 此类用于加载web表单
 *
 * @author Xinqing
 */
public class WebFragmentForGwtForms1 extends BaseFragment {

    private static final String TAG = "SimpleWebFragment";
    private StubObject mMenuItem;
    private String titleStr;
    private View rootView;// 缓存Fragment view
    private FrameLayout fl;
    private String forms;
    private ESPWebView wv;
    private ImageView imageView;
    private StorageUtil storageUtil;
    private String URLofThisPage;
    private String ServerURL;

    public WebFragmentForGwtForms1() {
        super();
    }

    public WebFragmentForGwtForms1(StubObject menuItem, String title) {
        this.mMenuItem = menuItem;
        this.titleStr = title;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.normalwebview_forms, container, false);

        Log.i(TAG, "test-----====================SimpleWebFragmentNoRefresh: onCreateView：" + "--" + titleStr);
        fl = (FrameLayout) rootView.findViewById(R.id.root);

        if (null != savedInstanceState) {
            mMenuItem = (StubObject) savedInstanceState.getSerializable("databack");
            titleStr = (String) savedInstanceState.getSerializable("title");
        }

        Hashtable menuTable = mMenuItem.getStubTable();
        forms = (String) menuTable.get("forms");
        String address = EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_ADDRESS);
        String path = EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_PATH);
        String port = EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_PORT);

        String protocol = EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_HTTP_TYPE);
        ServerURL = protocol + "://" + address + "/" + path;


        URLofThisPage = (String) menuTable.get("url");

        // Window级别 开启硬件加速 注意：现阶段你不能在Window级别对它禁用硬件加速。
        getActivity().getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);

        super.onCreateView(inflater, container, savedInstanceState);
        return rootView;

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        // BroadCastutil.getinstance().unregisterWebBroadcastList((Context)SimpleWebFragmentNoRefresh.this.getActivity());
    }


    @Override
    public void onVisible() {
        Log.i(TAG, "test-----====================SimpleWebFragmentNoRefresh: onVisible：" + "--" + titleStr);
        //1 加载数据
        Log.i(TAG, "1 webView---------------加载数据：onVisible:" + titleStr);
        wv = MyStaticWebView.getWv();
        wv.loadUrl(URLofThisPage);
        addWebView(fl);
        super.onVisible();
    }

    @Override
    public void onInvisible() {
        Log.i(TAG, "test-----====================SimpleWebFragmentNoRefresh: onInvisible：" + "--" + titleStr);
        removeWebView();
        super.onInvisible();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);
        outState.putSerializable("databack", mMenuItem);
        outState.putSerializable("title", titleStr);
    }

    /**
     * 添加webview
     *
     * @param layout
     * @return
     */
    public ESPWebView addWebView(ViewGroup layout) {
        removeWebView();
        wv.setLayoutParams(new ViewGroup.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));
        if (wv.getParent() != null) ((ViewGroup) wv.getParent()).removeView(wv);
        layout.addView(wv);

        return wv;
    }

    /**
     * 移除webview
     *
     * @return
     */
    public ESPWebView removeWebView() {
//		if (wv.getParent() != null){
//		Log.i(TAG, "4 webView---------------移除webView： onInvisible:"+titleStr);
//			((ViewGroup) wv.getParent()).removeView(wv);
//		}
        if (wv != null && wv.getParent() == fl) {
            Log.i(TAG, "4 webView---------------移除webView： onInvisible:" + titleStr);
            fl.removeView(wv);
        }
        return wv;
    }

}
