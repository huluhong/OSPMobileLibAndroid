package com.efounder.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.efounder.frame.baseui.BaseFragment;
import com.efounder.ospmobilelib.R;
import com.efounder.view.titlebar.AbBottomBar;
import com.efounder.view.titlebar.AbTitleBar;

public class FragmentFirst extends BaseFragment {
	public static final String TAG = "FragmentFirst";
	AbTitleBar mTitleBar;
	AbBottomBar mBottomBar;
	
	View rootView;
	
	/** 获取主菜单时用到的键值， 也是XML中主菜单的标签名 ： <menuRoot> */
	public static final String KEY_MENU_ROOT = "menuRoot";
	public FragmentFirst(){
		
	}
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}
	 @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		 
		//加载布局文件
		rootView = inflater.inflate(R.layout.fragment_fir, null);
		
		//mTitleBar = ((AbActivity)getActivity()).getTitleBar();
		return rootView;
	}

	
	
	
	 @Override
	public void onResume() {
		// TODO Auto-generated method stub
		// mTitleBar.setTitleText("ESPMobile");
		super.onResume();
	}

}
