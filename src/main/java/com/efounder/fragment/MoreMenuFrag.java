package com.efounder.fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.core.xml.StubObject;
import com.efounder.activity.AbActivity;
import com.efounder.activity.MainFrameActivity;
import com.efounder.adapter.TypedListAdapter;
import com.efounder.ospmobilelib.R;
import com.efounder.view.titlebar.AbBottomBar;
import com.efounder.view.titlebar.AbTitleBar;
import com.efounder.widget.TabBarNew;
import com.efounder.widget.TabBarNew.OnCurrentTabChangedListener;

import java.util.List;

public class MoreMenuFrag extends Fragment implements OnItemClickListener{
	public static final String TAG = "MoreMenuFrag";
	
	private ListView mListView;
	int position;/**菜单开始位置*/
	List<StubObject> menuItems;/**菜单列表*/
	OnCurrentTabChangedListener tabListener;
	int pos;
	AbTitleBar mTitleBar;
	AbBottomBar mBottomBar;
	public MoreMenuFrag(List<StubObject> menuItems,OnCurrentTabChangedListener tabListener,int pos){
		this.menuItems = (List<StubObject>) menuItems;
		this.tabListener = tabListener;
		this.pos = pos;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.layout_frag_next_level, null);
		mTitleBar = ((AbActivity)getActivity()).getTitleBar();
		mTitleBar.getLogoView().setVisibility(View.INVISIBLE);
		mTitleBar.setLogo(R.drawable.ef_title_view_back);
		mTitleBar.setTitleBarGravity(Gravity.CENTER, Gravity.CENTER);
		mListView = (ListView) rootView.findViewById(R.id.lv_list);
		mListView.setAdapter(new TypedListAdapter(getActivity().getApplicationContext(),"menu",menuItems));
		mListView.setOnItemClickListener(this);
		return rootView;
	}
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		MainFrameActivity menuFrag = MainFrameActivity.getInstance();
		if(menuFrag!=null){
			if (tabListener != null){
				TabBarNew.mCurrentTabIndex = pos+position;
				tabListener.onCurrentTabChanged(pos+position);
			}
		}
		
		
		}
	@Override
	public void onStart() {
		super.onStart();
		mTitleBar.setTitleText("更多");
	}
}
