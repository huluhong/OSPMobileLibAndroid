/*package com.efounder.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.ospmobilelib.R;
import com.efounder.activity.TabBottomActivity;
import com.efounder.adapter.CommonAdapter;
import com.efounder.adapter.ViewHolder;
import com.efounder.util.AbToastUtil;
import com.efounder.view.titlebar.AbTitleBar;
import com.efounder.widget.AbPullToRefreshView;
import com.efounder.widget.AbPullToRefreshView.OnFooterLoadListener;
import com.efounder.widget.AbPullToRefreshView.OnHeaderRefreshListener;
import com.efounder.widget.AbSlidingPlayView;

public class NewsFragment extends Fragment implements OnHeaderRefreshListener,OnFooterLoadListener{

	private StubObject mParentMenuItem; 
	private ArrayList<Object> mMenuItems;
	private int mContainerId;
	private AbPullToRefreshView mAbPullToRefreshView = null;
	private ListView mListView = null;
	private AbSlidingPlayView mSlidingPlayView = null;
	
	private CommonAdapter<Map<String, Object>> mAdapter;
	*//**
	 * 数据源
	 *//*
	private List<Map<String, Object>> list = null;
	
	AbTitleBar title;
	String titleStr;
	
	public NewsFragment(StubObject parentMenuItem, ArrayList<Object> menuItems,
			int containerId,String titleStr) {
		setAttachedData(parentMenuItem, menuItems, containerId,titleStr);
	}
	
	public void setAttachedData(StubObject parentMenuItem, ArrayList<Object> menuItems,
			int containerId,String titleStr) {
		this.mParentMenuItem = parentMenuItem;
		this.mMenuItems = menuItems;
		this.mContainerId = containerId;
		this.titleStr = titleStr;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.pull_to_refresh_list, null);
		
		//获取ListView对象
        mAbPullToRefreshView = (AbPullToRefreshView)rootView.findViewById(R.id.mPullRefreshView);
        mListView = (ListView)rootView.findViewById(R.id.mListView);
      //设置监听器
        mAbPullToRefreshView.setOnHeaderRefreshListener(this);
        mAbPullToRefreshView.setOnFooterLoadListener(this);
      //设置进度条的样式
        mAbPullToRefreshView.getHeaderView().setHeaderProgressBarDrawable(this.getResources().getDrawable(R.drawable.progress_circular));
        mAbPullToRefreshView.getFooterView().setFooterProgressBarDrawable(this.getResources().getDrawable(R.drawable.progress_circular));
        
      //ListView数据
    	list = new ArrayList<Map<String, Object>>();
//    	for(int i=0;i<10;i++){
//    		HashMap map = new HashMap();
//    		map.put("itemsIcon", BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher));
//    		map.put("itemsTitle", "苹果发布新产品");
//    		map.put("itemsText", "3月10日凌晨一点,苹果将在旧金山芳草地艺术中心举行新品发布会,其中一个主角已经没什么悬念了,它就是Apple Watch。其实去年的iPad发布会上,库克就带..");
//    		list.add(map);
//    	}
    	HashMap<String,Object> map = new HashMap<String,Object>();
    	map.put("itemsIcon", BitmapFactory.decodeResource(getResources(), R.drawable.img0));
		map.put("itemsTitle", "苹果发布新产品");
		map.put("itemsText", "3月10日凌晨一点,苹果将在旧金山芳草地艺术中心举行新品发布会,其中一个主角已经没什么悬念了,它就是Apple Watch。其实去年的iPad发布会上,库克就带..");
		list.add(map);
		HashMap map1 = new HashMap();
    	map1.put("itemsIcon", BitmapFactory.decodeResource(getResources(), R.drawable.img5));
		map1.put("itemsTitle", "缅方：投弹事件非政府军行动");
		map1.put("itemsText", "称已提前向中方通报作战计划，或为果敢同盟军所为。");
		list.add(map1);
		HashMap map2 = new HashMap();
    	map2.put("itemsIcon", BitmapFactory.decodeResource(getResources(), R.drawable.img5));
		map2.put("itemsTitle", "缅机事件云南5名遇难边民火化");
		map2.put("itemsText", "缅政府仍未有任何回应；还有3人伤势较重在住院。");
		list.add(map2);
		HashMap map3 = new HashMap();
    	map3.put("itemsIcon", BitmapFactory.decodeResource(getResources(), R.drawable.img2));
		map3.put("itemsTitle", "昆明男子与地铁列车相撞死亡");
		map3.put("itemsText", "原因公安机关正在调查中，地铁列车已恢复运营。");
		list.add(map3);
		HashMap map4 = new HashMap();
    	map4.put("itemsIcon", BitmapFactory.decodeResource(getResources(), R.drawable.img4));
		map4.put("itemsTitle", "央视315晚会曝光重点大汇总");
		map4.put("itemsText", "汽车仍是重头戏，移动联通齐曝光，三大行上榜。");
		list.add(map4);
		HashMap map5 = new HashMap();
    	map5.put("itemsIcon", BitmapFactory.decodeResource(getResources(), R.drawable.img4));
		map5.put("itemsTitle", "多家车企回应：将查“小病大修”");
		map5.put("itemsText", "央视315晚会曝光汽车企业4S店存在过度维修猫腻。");
		list.add(map5);
		HashMap map6 = new HashMap();
    	map6.put("itemsIcon", BitmapFactory.decodeResource(getResources(), R.drawable.img8));
		map6.put("itemsTitle", "央视：晚会延时与临时撤稿无关");
		map6.put("itemsText", "总理记者会节目时间长导致延迟，网络传言不实。");
		list.add(map6);
		HashMap map7 = new HashMap();
    	map7.put("itemsIcon", BitmapFactory.decodeResource(getResources(), R.drawable.img3));
		map7.put("itemsTitle", "香港各界呼吁早日落实普选");
		map7.put("itemsText", "有团体致信请愿；行业代表：民粹式恶斗或拖垮香港。");
		list.add(map7);
		HashMap map8 = new HashMap();
    	map8.put("itemsIcon", BitmapFactory.decodeResource(getResources(), R.drawable.img6));
		map8.put("itemsTitle", "浙江：火锅店须公式自制品成分");
		map8.put("itemsText", "含火锅底料、调味品等；部分商家通过App展示厨房。");
		list.add(map8);
		HashMap map9 = new HashMap();
    	map9.put("itemsIcon", BitmapFactory.decodeResource(getResources(), R.drawable.img7));
		map9.put("itemsTitle", "报告：中国成年人近四成失眠");
		map9.put("itemsText", "失眠危害大，抑郁症发病率比非失眠者高3到4倍。");
		list.add(map9);
    	mAdapter = new CommonAdapter<Map<String,Object>>(getActivity(),list,R.layout.item_list) 
    			{
			@Override
			public void convert(ViewHolder helper, Map<String, Object> item) {
				// TODO Auto-generated method stub
				helper.setImageBitmap(R.id.itemsIcon, (Bitmap)item.get("itemsIcon"));
				helper.setText(R.id.itemsTitle, (String)item.get("itemsTitle"));
				helper.setText(R.id.itemsText, (String)item.get("itemsText"));
			}
    		
		};
		//item被点击事件
    	mListView.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				AbToastUtil.showToast(getActivity(),"点击"+position);
			}
    	});
    	//组和个AbSlidingPlayView
        mSlidingPlayView = new AbSlidingPlayView(getActivity());
        
        final View mPlayView = inflater.inflate(R.layout.play_view_item, null);
		ImageView mPlayImage = (ImageView) mPlayView.findViewById(R.id.mPlayImage);
		TextView mPlayText = (TextView) mPlayView.findViewById(R.id.mPlayText);
		mPlayText.setText("1111111111111");
		mPlayImage.setBackgroundResource(R.drawable.pic1);
		
		final View mPlayView1 = inflater.inflate(R.layout.play_view_item, null);
		ImageView mPlayImage1 = (ImageView) mPlayView1.findViewById(R.id.mPlayImage);
		TextView mPlayText1 = (TextView) mPlayView1.findViewById(R.id.mPlayText);
		mPlayText1.setText("2222222222222");
//		mPlayImage1.setBackgroundResource(R.drawable.pic2);
		
		final View mPlayView2 = inflater.inflate(R.layout.play_view_item, null);
		ImageView mPlayImage2 = (ImageView) mPlayView2.findViewById(R.id.mPlayImage);
		TextView mPlayText2 = (TextView) mPlayView2.findViewById(R.id.mPlayText);
		mPlayText2.setText("33333333333333333");
//		mPlayImage2.setBackgroundResource(R.drawable.pic3);
		
		mSlidingPlayView.setNavHorizontalGravity(Gravity.RIGHT);
		mSlidingPlayView.addView(mPlayView);
		mSlidingPlayView.addView(mPlayView1);
		mSlidingPlayView.addView(mPlayView2);
		mSlidingPlayView.startPlay();
		//设置高度
		mSlidingPlayView.setLayoutParams(new AbsListView.LayoutParams(LayoutParams.FILL_PARENT,300));
		mListView.addHeaderView(mSlidingPlayView);
		//解决冲突问题
		mSlidingPlayView.setParentListView(mListView);
		mSlidingPlayView.setOnItemClickListener(new AbSlidingPlayView.AbOnItemClickListener() {
			
			@Override
			public void onClick(int position) {
				AbToastUtil.showToast(getActivity(),"点击"+position);
			}
		});
	    
        mSlidingPlayView.setOnPageChangeListener(new AbSlidingPlayView.AbOnChangeListener() {
			
			@Override
			public void onChange(int position) {
//				AbToastUtil.showToast(getActivity(),"改变"+position);
			}
		});
        
        mListView.setAdapter(mAdapter);
        
		return rootView;
	}
	@Override
	public void onStart() {
		title = ((TabBottomActivity)getActivity()).getTitleBar();
		title.setTitleText(titleStr);
		super.onStart();
	}

	@Override
	public void onFooterLoad(AbPullToRefreshView view) {
		// TODO Auto-generated method stub
		mAbPullToRefreshView.onFooterLoadFinish();
	}

	@Override
	public void onHeaderRefresh(AbPullToRefreshView view) {
		// TODO Auto-generated method stub
		mAbPullToRefreshView.onHeaderRefreshFinish();
	}
	
}
*/