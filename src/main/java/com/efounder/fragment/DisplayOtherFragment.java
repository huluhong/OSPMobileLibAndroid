package com.efounder.fragment;

import android.app.Activity;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.core.xml.StubObject;
import com.efounder.frame.baseui.BaseFragment;
import com.efounder.ospmobilelib.R;
import com.efounder.util.AbfragmentDataUtil;

import java.util.Hashtable;
import java.util.Map;

/**
 * 显示第三方的fragment（例如集成内控）
 *
 * @author yqs
 */
public class DisplayOtherFragment extends BaseFragment {
    public static final String TAG = "DisplayOtherFragment";

    View rootView;
    private StubObject stubObject;
    Fragment frag = null;

    public DisplayOtherFragment(StubObject stubObject) {
        this.stubObject = stubObject;
    }

    public DisplayOtherFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "加载第三方fragment");
        //加载布局文件
        rootView = inflater.inflate(R.layout.fragment_diaplayother, null);

        initFragment();

        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        super.setUserVisibleHint(isVisibleToUser);
        if (isVisible)
            resumeFragment();
    }

    private void initFragment() {

        Hashtable menuTable = stubObject.getStubTable();
        String className = (String) menuTable.get("show");
        Bundle bundle = new Bundle();
        Map<String, String> map = AbfragmentDataUtil.getParamAndValue(menuTable);
        for (String key : map.keySet()) {
            Log.i(TAG, "key=" + key + ":value=" + map.get(key));
            bundle.putString(key, map.get(key));
        }
        try {
            Class fragClass = Class.forName(className);
            if (fragClass != null) {
                {
                    frag = (Fragment) fragClass.newInstance();
                    frag.setArguments(bundle);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (frag == null) {
                frag = new FragmentFirst();
            }
        }

        getChildFragmentManager().beginTransaction()
                .replace(R.id.contentLayout, frag)
                .commitAllowingStateLoss();
    }


    private void resumeFragment() {
        if (frag != null) {
            frag.onResume();
            return;
        }
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        Log.i(TAG, "-----onResume---");
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "-----onStop---");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "-----onPause---");
    }
}
