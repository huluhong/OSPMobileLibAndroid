package com.efounder.fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Environment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.efounder.activity.FragmentContainActivity;
import com.efounder.builder.base.data.EFRowSet;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.ospmobilelib.R;
import com.efounder.util.LoadingDataUtilBlack;
import com.efounder.util.StorageUtil;
import com.efounder.view.titlebar.AbTitleBar;
import com.pansoft.espcomp.ESPWebView;

import static com.efounder.frame.utils.Constants.KEY_SETTING_APPID;

public class SimpleWebFragmentWithEFrowset extends Fragment {

	private static final String TAG = "SimpleWebFragment";
	/*private StubObject mMenuItem;*/
	private String titleStr;
	EFRowSet efRowset;
//	CustomProgressDialog dialog;
	private View rootView;// 缓存Fragment view
	AbTitleBar title ;
	ESPWebView web ;
	String webUrl;
	Boolean isShowBottom;
	Activity activity;

	public SimpleWebFragmentWithEFrowset(EFRowSet efRowset,String title) {
		this.efRowset = efRowset;
		webUrl = efRowset.getString("F_URL", "");
		titleStr = title;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	/**
	 * 拦截处理url
	 * @param url
	 * @return
	 */
	private String handleURL(String url){
		String matchURL = "http://10.80.0.104:8080/EnterpriseServer";
		if (url != null && url.contains(matchURL)) {
			FragmentActivity activity = getActivity();
			StorageUtil storageUtil = new StorageUtil(activity, "storage");
			String serverURL = storageUtil.getString("serverURL");
			url = url.replace(matchURL, serverURL);
		}
		Log.i(TAG, "新闻url-------" + url);
		return url;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.activity = activity;
		
	}
	

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
/*		TabBottomActivity menuFrag = TabBottomActivity.getInstance();
        isShowBottom = (menuFrag.getBottomBar().getVisibility()==View.VISIBLE);
		if(isShowBottom)
		menuFrag.getBottomBar().setVisibility(View.GONE);*/
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onPause();
	/*	if(isShowBottom){
		TabBottomActivity menuFrag = TabBottomActivity.getInstance();
		menuFrag.getBottomBar().setVisibility(View.VISIBLE);
		}*/
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.webviewwithrowset, container, false);

	
		RelativeLayout include = (RelativeLayout) rootView.findViewById(R.id.include);
		TextView title = (TextView)rootView.findViewById(R.id.fragmenttitle);
		title.setText(titleStr);
		LinearLayout leftbacklayout = (LinearLayout) rootView.findViewById(R.id.leftbacklayout);
		leftbacklayout.setVisibility(View.VISIBLE);

		leftbacklayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 SimpleWebFragmentWithEFrowset.this.getFragmentManager().popBackStack();
			}
		});
		
		Button rightButton = (Button) rootView.findViewById(R.id.closeButton);
		// 获取

		// 设置

		//隐藏分享按钮
		rightButton.setVisibility(View.INVISIBLE);
		rightButton.setBackgroundResource(R.drawable.sharebutton);
		rightButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
			/*	Toast.makeText(activity, String.valueOf(webUrl),
						Toast.LENGTH_SHORT).show();*/
				
				String xwbt=efRowset.getString("F_XWBT", "");
				String imagePath=Environment.getExternalStorageDirectory().toString()+"/"+
				EnvironmentVariable.getProperty(KEY_SETTING_APPID)
						+"/res"+ "/unzip_res"+"/Image"+"/ic_launcher.png";
			}
		});
		
		
		web = (ESPWebView) rootView.findViewById(R.id.mywebview);
		
		webUrl = handleURL(webUrl);
		web.loadUrl(String.valueOf(webUrl));
		return rootView;
	}

	private class webClient extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
		//	dialog.show();
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			LoadingDataUtilBlack.dismiss();
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);
			LoadingDataUtilBlack.show(SimpleWebFragmentWithEFrowset.this.getActivity());
		}
		
		public void onReceivedSslError(WebView view, SslErrorHandler handler,
				SslError error) {
			handler.proceed();// 接受证书
			LoadingDataUtilBlack.dismiss();
		}

	}
	@Override
	public void onStart() {
		//title = ((TabBottomActivity)getActivity()).getTitleBar();
		title = ((FragmentContainActivity)getActivity()).getTitleBar();
		title.setTitleText(titleStr);
		title.setVisibility(View.GONE);
		super.onStart();
	}
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		//BroadCastutil.getinstance().unregisterWebBroadcastList((Context)SimpleWebFragmentWithEFrowset.this.getActivity());
	}
}
