package com.efounder.fragment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.ospmobilelib.R;
import com.efounder.widget.EspGridView;
import com.pansoft.resmanager.ResFileManager;

@SuppressLint("NewApi") 
public class GridFragment extends Fragment {


	
	
	
	 private EspGridView gview;
	    private List<Map<String, Object>> data_list;
	    FragmentManager fragmentManager;
	    
	    /**
		 * 三级菜单
		 */
		private  List<StubObject> threeLevelMenus; // 二级菜单
		
		private static final String CAPTION="caption";
		private static final String MENUICON="menuIcon";
		
		ArrayList<String> titleList;
		
		private List<Bitmap> bitmapList = new ArrayList<Bitmap>();
		
		

	    
	public GridFragment(List<StubObject> threeMenus) {
		threeLevelMenus = threeMenus;
		
		 titleList = new ArrayList<String>();
		for(int i=0; i<threeLevelMenus.size(); i++){
			String menuIcon=((StubObject)threeLevelMenus.get(i)).getObject(MENUICON, "").toString();
			menuIcon = ResFileManager.IMAGE_DIR + "/" + menuIcon;
			File file = new File(menuIcon); 
			if (file.exists()) { 
            	Bitmap bm = BitmapFactory.decodeFile(menuIcon); 
            	bitmapList.add(bm);  
            }else{
            	Bitmap bm = BitmapFactory.decodeResource(this.getActivity().getResources(),R.drawable.ic_launcher);
            	bitmapList.add(bm);
            }
            String caption=((StubObject)threeLevelMenus.get(i)).getObject(CAPTION, "").toString();
            titleList.add(caption);
    		
    		
    		
		}
		
		}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.my_fragment_grid, null);
		  gview = (EspGridView) rootView.findViewById(R.id.gview);
		  gview.fillGridData(threeLevelMenus);
	      
		return rootView;
	}
	
	public class gridadapter extends BaseAdapter {

	    private LayoutInflater inflater;

	    ArrayList<String> titlelist;
	    List<Bitmap> bitmapList;

	    public gridadapter(Context context, ArrayList<String> list,List<Bitmap> bitmapList) {

	        this.inflater = LayoutInflater.from(context);

	        this.titlelist = list;
	        this.bitmapList = bitmapList;


	    }

	 

	    @Override

	    public int getCount() {

	        return titlelist.size();

	    }

	 

	    @Override

	    public Object getItem(int position) {

	        return position;

	    }

	 

	    @Override

	    public long getItemId(int position) {

	        return position;

	    }

	 

	    @Override

	    public View getView(int position, View convertView, ViewGroup parent) {

	        ViewHolder holder;

	        if (convertView==null) {

	            holder=new ViewHolder();

	            convertView=this.inflater.inflate(R.layout.second_grid, null);

	            holder.iv=(ImageView) convertView.findViewById(R.id.image);

	            holder.tv=(TextView) convertView.findViewById(R.id.text);
	            
	            holder.cross_tv = (TextView) convertView.findViewById(R.id.cross_text);

	            convertView.setTag(holder);

	        }

	        else {

	           holder=(ViewHolder) convertView.getTag();

	        }

	        holder.iv.setImageBitmap(bitmapList.get(position));

	        holder.tv.setText((CharSequence) titleList.get(position));
	        holder.cross_tv.setText(position+"");

	        return convertView;

	    }

	    private class ViewHolder{

	        ImageView iv;

	        TextView tv;
	        TextView cross_tv;

	    }

	 

	}
	
}
