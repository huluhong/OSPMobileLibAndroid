package com.efounder.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.activity.AbActivity;
import com.efounder.activity.BaseMainHomeActivity;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.chat.event.RightTopMenuClickEvent;
import com.efounder.chat.event.SystemInitOverEvent;
import com.efounder.chat.fragment.ChatListSwipeMenuFragment;
import com.efounder.chat.fragment.ContactsFragment;
import com.efounder.chat.model.UpdateBadgeViewEvent;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.chat.view.AddPopWindow;
import com.efounder.chat.view.CommonOtherPopWindow;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.forwechat.BaseApp;
import com.efounder.frame.baseui.BaseFragment;
import com.efounder.interfaces.BadgeUtil;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.ospmobilelib.R;
import com.efounder.util.AbFragmentManager;
import com.efounder.util.MenuGnqxUtil;
import com.efounder.util.StorageUtil;
import com.efounder.utils.JfResourceUtil;
import com.efounder.utils.ResStringUtil;
import com.efounder.view.titlebar.ScaleTransitionPagerTitleView;
import com.gyf.immersionbar.ImmersionBar;
import com.pansoft.resmanager.ResFileManager;
import com.utilcode.util.SizeUtils;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.badge.BadgeAnchor;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.badge.BadgePagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.badge.BadgeRule;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import static com.efounder.frame.utils.Constants.CHAT_USER_ID;
import static com.efounder.frame.utils.Constants.KEY_SETTING_APPID;

//import com.efounder.view.SimplePagerTitleView;

@SuppressWarnings("rawtypes")
public class PagerSlidingTab2Fragment extends BaseFragment implements OnClickListener {
    private boolean isViewCreated = false;
    private MagicIndicator topMagicTabs;
    CommonNavigator commonNavigator;
    private ViewPagerCompat pager;
    private MyPagerAdapter adapter;

    private AbActivity activity;

    private ImageView menu_moreIV;
    private ImageView leftImageView;
    private User user;

    /**
     * 二级菜单
     */
    private List<StubObject> mSecondLevelMenus; // 二级菜单

    ArrayList<String> TITLES = new ArrayList<String>();

    private String CAPTION = "caption";

    private StorageUtil storageUtil;

    /*
     * 设置信息初始化
     */
    private String port, service, address, path, APPID, sign;

    private String ServerURL;


//    private ImageLoader imageLoader;
//    private DisplayImageOptions options;

    private View rootView;
    private ImageView progressBar;
    RelativeLayout topLayout;
    //外部的标题栏
    private LinearLayout llTitle;

    private boolean isShowBackImage = false;//是否显示返回按钮
    /*首次显示的item*/
    private int firstShowItem = 0;

    public PagerSlidingTab2Fragment() {
        super();
    }

    @SuppressLint({"NewApi", "ValidFragment"})
    public PagerSlidingTab2Fragment(List<StubObject> subMenus) {
        super();
        mSecondLevelMenus = subMenus;
        this.setAlone(false);
        if (mSecondLevelMenus != null) {
            for (int i = 0; i < mSecondLevelMenus.size(); i++) {
                StubObject stubObject = mSecondLevelMenus.get(i);
                Hashtable menuTable = stubObject.getStubTable();
                String parentID = (String) menuTable.get("_parentID");
                if (parentID != null && (parentID.equals("kaifa") || parentID.equals("shengchan")
                        || parentID.equals("kantan"))) {
                    isShowBackImage = true;
                }
                //stubObject中currentShow为1，那么显示这个fragment
                if ("1".equals(menuTable.get("currentShow"))) {
                    firstShowItem = i;
                }
            }
            if ("1".equals(mSecondLevelMenus.get(0).getStubTable().get("showBackImg"))) {
                isShowBackImage = true;
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (AbActivity) activity;
    }


    @SuppressWarnings("unchecked")
    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_pager_sliding_tab2,
                container, false);
        topLayout = (RelativeLayout) rootView.findViewById(R.id.tabs_ll);
        llTitle = rootView.findViewById(R.id.ll_title);

        if (null != savedInstanceState) {
            mSecondLevelMenus = (List<StubObject>) savedInstanceState.getSerializable("databack");
        }

        MenuGnqxUtil.handleGNQX(mSecondLevelMenus);//处理功能权限


        if (null != mSecondLevelMenus && mSecondLevelMenus.size() > 0) {
            for (int i = 0; i < mSecondLevelMenus.size(); i++) {
                String cap = ((StubObject) mSecondLevelMenus.get(i)).getObject(
                        CAPTION, "").toString();
                TITLES.add(cap);
            }
        } else {
            TITLES.add("配置文件出错");
        }

        pager = (ViewPagerCompat) rootView.findViewById(R.id.pager);
        //设置ViewPager的预加载数量
        String viewPagerLimit = EnvironmentVariable.getProperty("ViewPagerLimit","");
        if(!viewPagerLimit.equals("")){
            pager.setOffscreenPageLimit(Integer.valueOf(viewPagerLimit));
        }

        //TODO 处理左侧图标
        leftImageView = (ImageView) rootView.findViewById(R.id.ttip);

        //initLeftImageAndTitleView();


        // fragment里面套嵌fragment 必须使用getchildfragmentmanager
        FragmentManager fm = getChildFragmentManager();
        fm.popBackStackImmediate(PagerSlidingTab2Fragment.class.getName(),
                FragmentManager.POP_BACK_STACK_INCLUSIVE);
        pager.addOnPageChangeListener(onPageChangeListener);
        adapter = new MyPagerAdapter(getChildFragmentManager());
        pager.setAdapter(adapter);
        //topMagicTabs.setViewPager(pager);
        final int pageMargin = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                        .getDisplayMetrics());
        pager.setPageMargin(pageMargin);


        topMagicTabs = (MagicIndicator) rootView.findViewById(R.id.tabs);
        //topMagicTabs.setBackgroundColor(getResources().getColor(R.color.title_Background));
        // topMagicTabs.setBackgroundResource(R.color.title_Background);
        commonNavigator = new CommonNavigator(this.getContext());
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return TITLES == null ? 0 : TITLES.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {

                final BadgePagerTitleView badgePagerTitleView = new BadgePagerTitleView(context);

                //只颜色变化
//                SimplePagerTitleView simplePagerTitleView = new ColorTransitionPagerTitleView(context);
                //文字大小同时变化
                SimplePagerTitleView simplePagerTitleView = new ScaleTransitionPagerTitleView(context);
                int padding = SizeUtils.dp2px(5);
                simplePagerTitleView.setPadding(padding, 0, padding, 0);

                if (mSecondLevelMenus.size() >= index) {
                    StubObject indexStubobject = mSecondLevelMenus.get(index);
                    //展示星球名称前缀
//                    if ("open_planet_name".equals(indexStubobject.getID())) {
//                        //判断相应的页面，展示标题
//                        simplePagerTitleView.setText(OpenPlanetTitleUtil.getPlanetName() + TITLES.get(index));
//                    } else {
                    simplePagerTitleView.setText(TITLES.get(index));
//                    }

                } else {
                    simplePagerTitleView.setText(TITLES.get(index));
                }


                simplePagerTitleView.setTextSize(TypedValue.COMPLEX_UNIT_PX, SizeUtils.dp2px(17));

                simplePagerTitleView.setNormalColor(getResources().getColor(R.color.title_NormalTextColor));

                if (TITLES.size() == 1) {
                    simplePagerTitleView.setSelectedColor(getResources().getColor(R.color.title_TextColor));

                } else {
                    simplePagerTitleView.setSelectedColor(getResources().getColor(R.color.title_SelectTextColor));
                }
                simplePagerTitleView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pager.setCurrentItem(index);
                        //badgePagerTitleView.setBadgeView(null); // cancel badge when click tab
                    }
                });
                badgePagerTitleView.setInnerPagerTitleView(simplePagerTitleView);

                // setup badge
//                if (index != 2) {
                TextView badgeTextView = (TextView) LayoutInflater.from(context).inflate(R.layout.simple_count_badge_layout, null);
                badgeTextView.setVisibility(View.GONE);
//                    badgeTextView.setText("" + (index + 1));
                badgePagerTitleView.setBadgeView(badgeTextView);

                badgePagerTitleView.setXBadgeRule(new BadgeRule(BadgeAnchor.CONTENT_RIGHT, -UIUtil.dip2px(context, 3)));
                badgePagerTitleView.setYBadgeRule(new BadgeRule(BadgeAnchor.CONTENT_TOP, -UIUtil.dip2px(context, 2)));

                badgePagerTitleView.setAutoCancelBadge(false);

                return badgePagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
//                WrapPagerIndicator indicator = new WrapPagerIndicator(context);
//                if (TITLES.size() == 1) {
//                    indicator.setFillColor(getResources().getColor(R.color.title_Background));
//                } else {
//                    indicator.setFillColor(getResources().getColor(R.color.page2_color));
//                }
//                //indicator.setFillColor(Color.parseColor("#ca3032"));
//                indicator.setVerticalPadding(100);
//                indicator.setRoundRadius(0);
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setStartInterpolator(new AccelerateInterpolator());
                indicator.setEndInterpolator(new DecelerateInterpolator(1.6f));
                indicator.setYOffset(UIUtil.dip2px(context, 39));
                indicator.setLineHeight(UIUtil.dip2px(context, 0));
                indicator.setColors(getResources().getColor(R.color.title_SelectTextColor));

                return indicator;
            }
        });
        topMagicTabs.setNavigator(commonNavigator);
        ViewPagerHelper.bind(topMagicTabs, pager);


//        topMagicTabs.setIndicatorColor(Color.WHITE);
//        topMagicTabs.setMsgMargin(3, 0, 5);
//
//        topMagicTabs.setViewPager(pager);

        progressBar = (ImageView) rootView.findViewById(R.id.loadProgressBar);

        //获取progressBar设置图片的宽度和高度
        AnimationDrawable animationDrawable = (AnimationDrawable) progressBar.getBackground();

        animationDrawable.start();

        if (TITLES.size() == 1) {
//            RelativeLayout.LayoutParams progressBarparams = (RelativeLayout.LayoutParams) progressBar
//                    .getLayoutParams();
//            progressBarparams.rightMargin = 20;
//            progressBarparams.addRule(RelativeLayout.LEFT_OF, R.id.topMagicTabs);
//            progressBar.setLayoutParams(progressBarparams);
        } else {

            RelativeLayout.LayoutParams progressBarparams = (RelativeLayout.LayoutParams) progressBar
                    .getLayoutParams();
            // progressBarparams.rightMargin = 20;
//            progressBarparams.addRule(RelativeLayout.RIGHT_OF, R.id.ttip);
            progressBar.setLayoutParams(progressBarparams);





        }

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        initMenuMore(rootView);

        if (((BaseApp) getContext().getApplicationContext()).isSystemInited()) {
            initBadge();
        }
        isViewCreated = true;
        pager.setCurrentItem(firstShowItem);

        return rootView;
    }

    @Subscribe(threadMode = ThreadMode.MAIN, priority = 1, sticky = true)
    public void onSolveMessageEvent(SystemInitOverEvent event) {
        initBadge();
    }

    private void initBadge() {
        if (null != mSecondLevelMenus && mSecondLevelMenus.size() > 0) {
            for (int i = 0; i < mSecondLevelMenus.size(); i++) {
                StubObject stubObject = mSecondLevelMenus.get(i);
                String id = stubObject.getString("id", "");
                String isShowBadge = stubObject.getString("showBadge", "false");
                BadgePagerTitleView v = (BadgePagerTitleView) commonNavigator.getPagerTitleView(i);

                if (isShowBadge.equals("true")) {
                    int unReadCount = BadgeUtil.getCount(id);


                    if (unReadCount > 0) {
                        ((TextView) v.getBadgeView()).setVisibility(View.VISIBLE);

                        ((TextView) v.getBadgeView()).setText(unReadCount + "");

                    } else {
                        ((TextView) v.getBadgeView()).setVisibility(View.GONE);
                    }

//                        if (v instanceof Ibadge) {
//                            if (((Ibadge) v).getBadgeView() == null) {
//                                BadgeView badgeView = new BadgeView(getContext(), v);
//                                badgeView.setBadgeMargin(0, 0);
//                                badgeView.setTextSize(13);
//
//                                ((Ibadge) v).initBadgeView(badgeView);
//                            }
//                            ((Ibadge) v).setBadgeCount(unReadCount);
//                        }


                } else {
                    ((TextView) v.getBadgeView()).setVisibility(View.GONE);

                }
            }
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSolveMessageEvent(UpdateBadgeViewEvent event) {
        int fromUserId = Integer.parseInt(event.getUserID());
        byte chatType = event.getChatType();
        boolean mustRefresh = event.isRefreshAll();
        // ViewGroup tabViews = topMagicTabs.getViewList();
        for (int i = 0; i < mSecondLevelMenus.size(); i++) {


            StubObject stubObject = mSecondLevelMenus.get(i);

            String isShowBadge = stubObject.getString("showBadge", "false");
            if (!isShowBadge.equals("true")) {
                continue;
            }

            String id = (String) stubObject.getID();
            List chatIDs = BadgeUtil.badgeMap.get(id);
            if (!mustRefresh)
                if (!BadgeUtil.isNeedRefreshBadge(chatIDs, fromUserId, chatType)) continue;
            int unReadCount = BadgeUtil.getCount(id);

            //View v = (View) commonNavigator.getPagerTitleView(i);

            BadgePagerTitleView v = (BadgePagerTitleView) commonNavigator.getPagerTitleView(i);


            if (unReadCount > 0) {
                ((TextView) v.getBadgeView()).setVisibility(View.VISIBLE);

                ((TextView) v.getBadgeView()).setText(unReadCount + "");

            } else {
                ((TextView) v.getBadgeView()).setVisibility(View.GONE);
            }
//            if (!(v instanceof Ibadge)) {
//                if (v instanceof ViewGroup)
//                    v = ((ViewGroup) v).getChildAt(0);
//            }
//            if (v instanceof Ibadge) {
//                if (((Ibadge) v).getBadgeView() == null) {
//                    BadgeView badgeView = new BadgeView(getContext(), v);
//                    badgeView.setBadgeMargin(0, 0);
//                    badgeView.setTextSize(8);
//
//                    ((Ibadge) v).initBadgeView(badgeView);
//                }
//                if (v instanceof Ibadge) {
//                    ((Ibadge) v).setBadgeCount(unReadCount);
//                }
//            }
        }
    }

    /**
     * 处理左侧图标
     *
     * @param
     */
    private void initLeftImageAndTitleView() {
//        ImageView leftImageView = (ImageView) rootView.findViewById(R.id.ttip);

        //是否隐藏标题栏
        for (int i = 0; i < mSecondLevelMenus.size(); i++) {
            String isHideTitle = ((StubObject) mSecondLevelMenus.get(i)).getObject(
                    "hideTitle", "").toString();
            if ("1".equals(isHideTitle)) {
                if (topLayout == null) {
                    return;
                }
                topLayout.setVisibility(View.GONE);
                llTitle.setVisibility(View.GONE);
            }
        }

        /**
         * 不显示左边头像，默认tabs居中
         */
        if ("0".equals(EnvironmentVariable.getProperty("isShowLeftAvatar"))) {
            rootView.findViewById(R.id.user_icon_container).setVisibility(View.GONE);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) rootView.findViewById(R.id.fl_tab).getLayoutParams();
            layoutParams.leftMargin = UIUtil.dip2px(getContext(),40);
            return;
        }

        if (isShowBackImage) {
            leftImageView.setVisibility(View.INVISIBLE);
            ImageView backImageView = (ImageView) rootView.findViewById(R.id.ttip_back);
            backImageView.setVisibility(View.VISIBLE);
            // backImageView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ef_title_view_back));
            backImageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().finish();
                }
            });
            return;
        }
        try {
            user = WeChatDBManager.getInstance().getOneUserById(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (user != null && leftImageView != null) {
//            imageLoader.displayImage(URLModifyDynamic.getInstance().replace(user.getAvatar()), leftImageView, options);
            LXGlideImageLoader.getInstance().showUserAvatar(getActivity(), leftImageView, user.getAvatar());
            leftImageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getActivity() != null && getActivity() instanceof BaseMainHomeActivity) {
                        if (((BaseMainHomeActivity) getActivity()).isAllowLeftDrawer()) {
                            activity.getDrawerLayout().openDrawer(Gravity.LEFT);
                        }
                        return;
                    }
                    activity.getDrawerLayout().openDrawer(Gravity.LEFT);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        initLeftImageAndTitleView();
    }

    private void initMenuMore(View rootView) {

        menu_moreIV = (ImageView) rootView.findViewById(R.id.menu_more);
        if (this.getRightButtonType() == null || this.getRightButtonType().equals("")) {
            menu_moreIV.setVisibility(View.GONE);
        } else {
            menu_moreIV.setVisibility(View.VISIBLE);
        }
        menu_moreIV.setOnClickListener(this);
        if ("import".equals(getRightButtonType())
                || "star_asset_wallet_change".equals(getRightButtonType())) {
            menu_moreIV.setImageDrawable(JfResourceUtil.getSkinDrawable(R.drawable.right_top_import_menu));
        } else if ("add".equals(getRightButtonType())) {
            menu_moreIV.setImageDrawable(JfResourceUtil.getSkinDrawable(R.drawable.main_activtiy_add_normal));
        } else {
            //读取配置的图标并展示
            Object localImagePath = ResFileManager.IMAGE_DIR + "/" + getRightTopButtonICon();
            if (getRightTopButtonICon() == null || "".equals(getRightTopButtonICon())) {
                localImagePath = JfResourceUtil.getSkinDrawable(R.drawable.right_top_import_menu);
            }
            LXGlideImageLoader.getInstance().displayImage(getActivity(), menu_moreIV, localImagePath, R.drawable.right_top_import_menu
                    , R.drawable.right_top_import_menu);
        }
        updateMoreMenuVisibile(0);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.menu_more) {
            if ("share".equals(getRightButtonType())) {
                int position1 = pager.getCurrentItem();

                StubObject stubObject = (StubObject) mSecondLevelMenus.get(position1);
                String forms = stubObject.getObject(
                        "forms", "").toString();
                String caption = stubObject.getObject(
                        "caption", "").toString();
                storageUtil = new StorageUtil(this.getActivity(), "storage");
                String address = storageUtil.getString("address", "");
                String path = storageUtil.getString("path", "");
                String port = storageUtil.getString("port", "8080");
                boolean isSafe = storageUtil.getBoolean("isSafe", true);
                String protocol = null;
                if (isSafe) {
                    protocol = "https";
                } else {
                    protocol = "http";
                }
                ServerURL = protocol + "://" + address + "/" + path;

                String url = ServerURL + "/AndroidForm/" + forms + ".html";
                System.out.println("分享的网址" + url);
                String imagePath = Environment.getExternalStorageDirectory().toString() + "/" + EnvironmentVariable.getProperty(KEY_SETTING_APPID) +
                        "/res" + "/unzip_res" + "/Image" + "/ic_launcher.png";

            } else if ("add".equals(getRightButtonType())) {
                AddPopWindow addPopWindow = new AddPopWindow(activity);
                addPopWindow.showPopupWindow(menu_moreIV);
            } else if ("import".equals(getRightButtonType())) {
                final CommonOtherPopWindow commonOtherPopWindow = new CommonOtherPopWindow(getActivity());
                commonOtherPopWindow.addMenuButton(ResStringUtil.getString(R.string.openplanet_title_import_fans), R.drawable.open_plant_import_fans, true)
                        .setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //给星际星球使用
                                try {
                                    String className = getResources().getString(R.string.pageslidetab_import_activity);
                                    Class class1 = Class.forName(className);
                                    Intent intent = new Intent(getActivity(), class1);
                                    //intent.putExtra("number", user.getName());
                                    intent.putExtra("number", EnvironmentVariable.getUserName());
                                    intent.putExtra("hasRightButton", "false");
                                    startActivity(intent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                commonOtherPopWindow.dismiss();
                            }
                        });
                commonOtherPopWindow.showPopupWindow(menu_moreIV);
            } else {
                //todo 通用右上角菜单点击事件 业务界面自己处理 20180728
                int position = pager.getCurrentItem();
                StubObject stubObject = (StubObject) mSecondLevelMenus.get(position);
                EventBus.getDefault().post(new RightTopMenuClickEvent(menu_moreIV, stubObject));
            }
        }

    }


    //FragmentStatePagerAdapter 会执行ondestroy  FragmentPagerAdapter只会执行到ondestroyview
    public class MyPagerAdapter extends FragmentStatePagerAdapter {
        Map<Integer, Fragment> fragmentMap = new HashMap<Integer, Fragment>();
        private Fragment currentFragment;

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            currentFragment = (Fragment) object;
            super.setPrimaryItem(container, position, object);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES.get(position);
        }

        @Override
        public int getCount() {
            return TITLES.size();
        }

        @Override
        public synchronized Fragment getItem(int position) {
            BaseFragment frag = null;

            if (null != mSecondLevelMenus) {
                StubObject mainMenuItem = (StubObject) mSecondLevelMenus
                        .get(position);
                AbFragmentManager ab = new AbFragmentManager(PagerSlidingTab2Fragment.this.getActivity());

                frag = ab.getFragment(mainMenuItem);
                if (TITLES.size() > 1) {
                    frag.setAlone(false);
                } else {
                    frag.setAlone(true);
                }
                frag.setBadgeType(PagerSlidingTab2Fragment.this.getBadgeType());
                if (frag instanceof SimpleWebFragmentNoRefreshNoForms) {
                    ((SimpleWebFragmentNoRefreshNoForms) frag)
                            .setTitleVisible(false);
                    // ((SimpleWebFragmentNoRefreshNoForms)frag).setUserVisibleHint(true);
                }
                else if (frag instanceof ChatListSwipeMenuFragment) {//新组件的聊天列表
                    ((ChatListSwipeMenuFragment) frag).setInitprogressDismiss(new ChatListSwipeMenuFragment.InitprogressDismiss() {
                        @Override
                        public void trigger(boolean show) {
                            if (show) {
                                initnetprogressdismissHandler.sendEmptyMessage(1);
                            } else {
                                initnetprogressdismissHandler.sendEmptyMessage(0);
                            }

                        }
                    });
                    progressBar.setVisibility(View.VISIBLE);
                } else if (frag instanceof ContactsFragment) {

                }


            } else {
                // frag = new BaseFragment();
            }

            fragmentMap.put(position, frag);
            return frag;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            fragmentMap.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getFragment(int key) {
            return fragmentMap.get(key);
        }

        public Fragment getCurrentFragment() {
            return currentFragment;
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (pager != null) {
            int position = pager.getCurrentItem();
            Fragment fragment = adapter.getFragment(position);
            StubObject menuItem = (StubObject) mSecondLevelMenus.get(position);
            // 缓存中的fragment前台显示时，如果是viewType="webView"，通知webView开始加载
            if (hidden) {
                if (isViewCreated && fragment instanceof BaseFragment) {
                    BaseFragment baseFragment = (BaseFragment) fragment;
                    baseFragment.onInvisible();
                }
            }
            if (!hidden) {
                if (isViewCreated && fragment instanceof BaseFragment) {
                    BaseFragment baseFragment = (BaseFragment) fragment;
                    baseFragment.onVisible();
                }
            }
        }

        //处理左上角头像
        initLeftImageAndTitleView();

        //给ViewPager的 当前显示的fragment派发 onHiddenChanged
        if (adapter != null) {
            Fragment currentFragment = adapter.getCurrentFragment();
            if (currentFragment != null) {
                currentFragment.onHiddenChanged(hidden);
            }
        }

        super.onHiddenChanged(hidden);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        pager.removeOnPageChangeListener(onPageChangeListener);
        EventBus.getDefault().unregister(this);

    }


    @Override
    public void onDestroy() {
        isViewCreated = false;
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);
        outState.putSerializable("databack", (Serializable) mSecondLevelMenus);
    }

    public MyPagerAdapter getAdapter() {
        return adapter;
    }

    Handler initnetprogressdismissHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0://隐藏
                    progressBar.setVisibility(View.INVISIBLE);
                    break;
                case 1://显示
                    progressBar.setVisibility(View.VISIBLE);
                    break;
            }
            super.handleMessage(msg);
        }
    };

    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            updateMoreMenuVisibile(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private void updateMoreMenuVisibile(int position) {
        StubObject stubObject = mSecondLevelMenus.get(pager.getCurrentItem());
        if (menu_moreIV == null) {
            return;
        }
        if (stubObject.getString("showRightTopButton", "1").equals("0")) {
            menu_moreIV.setVisibility(View.INVISIBLE);
        } else {
            menu_moreIV.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void initImmersionBar() {
        //是否隐藏标题栏
        for (int i = 0; i < mSecondLevelMenus.size(); i++) {
            String isHideTitle = ((StubObject) mSecondLevelMenus.get(i)).getObject(
                    "hideTitle", "").toString();
            if (!"1".equals(isHideTitle)) {
                //如果没有隐藏标题栏，自己处理标题栏，否则留给fragment自己处理
                ImmersionBar.with(this).init();
            }
        }

    }
}
