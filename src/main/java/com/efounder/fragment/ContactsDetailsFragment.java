package com.efounder.fragment;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.RawContacts;
import android.provider.ContactsContract.RawContacts.Data;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.efounder.ospmobilelib.R;
import com.pansoft.espcomp.sortlistview.SortModel;
import com.pansoft.utils.ContactsUtil;

public class ContactsDetailsFragment extends Fragment implements OnClickListener{

	private static final String TAG_MESSAGE="message";
	private static final String TAG_CALL="call";
	private static final String TEL_NUMBER="tel_number";
	private TextView saveContact;//保存联系人
	private TextView contactName;//联系人姓名
	private TextView contactNumber1;//联系人电话1
	
	SortModel model;//存有联系人的相关信息
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.fragment_contacts_details, container, false);
		
		
		model = new SortModel();
		Bundle bundle = getActivity().getIntent().getBundleExtra("dataSource");
		model=(SortModel) bundle.getSerializable("data");
		
		
		initView(view);
		return view;
	}
	private void initView(View view){
		saveContact = (TextView) view.findViewById(R.id.mobile_saveContact);
		saveContact.setOnClickListener(this);
		contactName = (TextView) view.findViewById(R.id.contact_name);
		contactNumber1 = (TextView) view.findViewById(R.id.mobilenumber);
		contactName.setText(model.getName());
		contactNumber1.setText(model.getNumber());
		
		
		recurView(view);
	}
	
	private void recurView(View view){
		if (view instanceof ViewGroup) {
			ViewGroup viewGroup = (ViewGroup) view;
			for (int i = 0; i < viewGroup.getChildCount(); i++) {
				View subView = viewGroup.getChildAt(i);
				recurView(subView);
			}
		}else {
			Object tag = view.getTag();
			if (tag != null &&(TAG_MESSAGE.equals(String.valueOf(tag)) || TAG_CALL.equals(String.valueOf(tag)))) {
				view.setOnClickListener(this);
			}
		}
	}
	@Override
	public void onClick(View v) {
		
		 //保存联系人
		if (v.getId()== R.id.mobile_saveContact) {
			ContactsUtil.init(getActivity());
			ContactsUtil.AddContact(model.getName()+"1", model.getNumber());
			//save(model.getName()+"1",model.getNumber());
			Toast.makeText(getActivity(), "保存成功！", Toast.LENGTH_SHORT).show();
		}
		if (v.getTag() == null) {
			return ;
		}
		
		TextView telTV = (TextView) ((View)v.getParent()).findViewWithTag(TEL_NUMBER);
		String tel;
		if (TextUtils.isEmpty(telTV.getText())) {
			return ;
		}else {
			tel = telTV.getText().toString();
			System.out.println(tel);
		}
		if (TAG_MESSAGE.equals(v.getTag().toString())) {
			// 发送短信
	        Intent intent2 = new Intent();
	        intent2.setAction(Intent.ACTION_SENDTO);
	        intent2.setData(Uri.parse("smsto:"+tel));
	        intent2.putExtra("sms_body", "");
	        startActivity(intent2);
		}else if (TAG_CALL.equals(v.getTag().toString())) {
			//拨打电话
	        Intent intent = new Intent();
	        intent.setAction(Intent.ACTION_CALL);
	        intent.setData(Uri.parse("tel:"+tel));
	        startActivity(intent);
		}
		
	}
	/**
	 * 保存联系人
	 */
	private void save(String name, String number) {
		ContentValues values = new ContentValues();
		Uri rawContactUri = 
		getActivity().getContentResolver().insert(RawContacts.CONTENT_URI, values);
		long rawContactId = ContentUris.parseId(rawContactUri);
		// 向data表插入姓名数据
		if (name != "")
		{
		values.clear();
		values.put(Data.RAW_CONTACT_ID, rawContactId);
		values.put(Data.MIMETYPE, 
		StructuredName.CONTENT_ITEM_TYPE);
		values.put(StructuredName.GIVEN_NAME, name);
		getActivity().getContentResolver().insert(ContactsContract.Data.CONTENT_URI, 
		values);
		}

		// 向data表插入电话数据
		if (number != "")
		{
		values.clear();
		values.put(Data.RAW_CONTACT_ID, rawContactId);
		values.put(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE);
		values.put(Phone.NUMBER, number);
		values.put(Phone.TYPE, Phone.TYPE_MOBILE);
		getActivity().getContentResolver().insert(ContactsContract.Data.CONTENT_URI, 
		values);
		}


		
	}
	
	

	
}
