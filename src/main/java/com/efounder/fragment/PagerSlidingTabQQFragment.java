package com.efounder.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.activity.AbActivity;
import com.efounder.activity.BaseMainHomeActivity;
import com.efounder.chat.db.WeChatDBManager;
import com.efounder.chat.event.RightTopMenuClickEvent;
import com.efounder.chat.event.SystemInitOverEvent;
import com.efounder.chat.event.TabReselectedEvent;
import com.efounder.chat.fragment.ChatListSwipeMenuFragment;
import com.efounder.chat.fragment.ContactsFragment;
import com.efounder.chat.model.UpdateBadgeViewEvent;
import com.efounder.chat.utils.LXGlideImageLoader;
import com.efounder.chat.view.AddPopWindow;
import com.efounder.chat.view.CommonOtherPopWindow;
import com.efounder.chat.widget.BadgeView;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.baseui.BaseFragment;
import com.efounder.interfaces.BadgeUtil;
import com.efounder.interfaces.Ibadge;
import com.efounder.mobilecomps.contacts.User;
import com.efounder.model.UpdatePageSlideTitleEvent;
import com.efounder.ospmobilelib.R;
import com.efounder.util.AbFragmentManager;
import com.efounder.util.MenuGnqxUtil;
import com.efounder.util.StorageUtil;
import com.efounder.utils.JfResourceUtil;
import com.efounder.utils.ResStringUtil;
import com.efounder.view.EFTextView;
import com.efounder.widget.NoScrollViewPager;
import com.gyf.immersionbar.ImmersionBar;
import com.marlonmafra.android.widget.SegmentedTab;
import com.pansoft.resmanager.ResFileManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.efounder.frame.utils.Constants.CHAT_USER_ID;
import static com.efounder.frame.utils.Constants.KEY_SETTING_APPID;

/**
 * 仿qq样式
 */
public class PagerSlidingTabQQFragment extends BaseFragment implements OnClickListener {
    private static final String TAG = "PagerTabQQ";

    private boolean isViewCreated = false;
    // private PagerSlidingTabStrip tabs;
    private NoScrollViewPager myViewPager;
    private MyPagerAdapter adapter;
    private AbActivity activity;
    private ImageView menu_moreIV;

    /**
     * 二级菜单
     */
    private List<StubObject> mSecondLevelMenus; // 二级菜单
    private int tabColor = 0x1A000000;
    String[] TITLES;

    private String CAPTION = "caption";
    private Fragment currentfragment;

    private StorageUtil storageUtil;
    private String ServerURL;

    private View rootView;
    private SegmentedTab mSegmentHorzontal;

    private ImageView progressBar;
    private ImageView leftImageView;
    private User user;

    private boolean isShowBackImage = false;//是否显示返回按钮
    private boolean viewPagerCanScroll = true;//view pager 是否可滑动
    //右上角左边按钮的菜单
    private StubObject rightTopLeftMenuObject;
    private ImageView ivRightTopLeft;
    BadgeView rightTopLeftbadgeView;
    /*首次显示的item*/
    private int firstShowItem = 0;

    public PagerSlidingTabQQFragment() {
        super();
    }

    @SuppressLint({"NewApi", "ValidFragment"})
    public PagerSlidingTabQQFragment(List<StubObject> subMenus) {
        super();
        //copy方法可以使下面显示通讯录的角标，现在我们现实红点，暂时不用他
//        mSecondLevelMenus =copySubMenu(subMenus);
        mSecondLevelMenus = subMenus;

        this.setAlone(false);
        if (mSecondLevelMenus != null) {
            if ("1".equals(mSecondLevelMenus.get(0).getStubTable().get("showBackImg"))) {
                isShowBackImage = true;
            }
            Iterator<StubObject> iterator = mSecondLevelMenus.iterator();
            while (iterator.hasNext()) {
                StubObject stubObject = iterator.next();
                //StubObject stubObject = mSecondLevelMenus.get(i);
                Hashtable menuTable = stubObject.getStubTable();
                String parentID = (String) menuTable.get("_parentID");
                if (parentID != null && (parentID.equals("kaifa") || parentID.equals("shengchan")
                        || parentID.equals("kantan"))) {
                    isShowBackImage = true;
                }
                if ("0".equals((String) menuTable.get("canScroll"))) {
                    viewPagerCanScroll = false;
                }
                //stubObject中currentShow为1，那么显示这个fragment
                if ("1".equals(menuTable.get("currentShow"))) {
                    firstShowItem = mSecondLevelMenus.indexOf(stubObject);
                }

                //判断右上角左边那个按钮的菜单是否是主菜单（目前用于星际通讯通讯录的按钮）
                if (stubObject.getString("primaryMenu", "").equals("0")) {
                    //此菜单是右上角的菜单
                    rightTopLeftMenuObject = stubObject;
                    iterator.remove();
                }
            }


        }
    }

    private List<StubObject> copySubMenu(List<StubObject> subMenus) {
        List<StubObject> stubObjects = new ArrayList<>();
        stubObjects.addAll(subMenus);
        return stubObjects;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (AbActivity) activity;
    }

    @SuppressWarnings("unchecked")
    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_pager_sliding_qqtab,
                container, false);
        if (null != savedInstanceState) {
            mSecondLevelMenus = (List<StubObject>) savedInstanceState.getSerializable("databack");
        }
        MenuGnqxUtil.handleGNQX(mSecondLevelMenus);//处理功能权限
        if (null != mSecondLevelMenus && mSecondLevelMenus.size() > 0) {
            TITLES = new String[mSecondLevelMenus.size()];
            for (int i = 0; i < mSecondLevelMenus.size(); i++) {
                String cap = ((StubObject) mSecondLevelMenus.get(i)).getObject(
                        CAPTION, "").toString();
                TITLES[i] = cap;
            }
        } else {
            //TITLES.add("配置文件出错");
            TITLES = new String[1];
            TITLES[0] = "配置文件出错";
        }
        ivRightTopLeft = (ImageView) rootView.findViewById(R.id.iv_right_top_left);
        rightTopLeftbadgeView = new BadgeView(getActivity(), ivRightTopLeft);
        mSegmentHorzontal = (SegmentedTab) rootView.findViewById(R.id.segment_control);
        progressBar = (ImageView) rootView.findViewById(R.id.loadProgressBar);

        //获取progressBar设置图片的宽度和高度
        AnimationDrawable animationDrawable = (AnimationDrawable) progressBar.getBackground();
        animationDrawable.start();

        myViewPager = (NoScrollViewPager) rootView.findViewById(R.id.pager);
        myViewPager.setNoScroll(!viewPagerCanScroll);
        // fragment里面套嵌fragment 必须使用getchildfragmentmanager
        FragmentManager fm = getChildFragmentManager();
        fm.popBackStackImmediate(PagerSlidingTabQQFragment.class.getName(),
                FragmentManager.POP_BACK_STACK_INCLUSIVE);
        adapter = new MyPagerAdapter(getChildFragmentManager());
        myViewPager.setAdapter(adapter);
        myViewPager.addOnPageChangeListener(onPageChangeListener);

        //TODO 处理左侧图标
        leftImageView = (ImageView) rootView.findViewById(R.id.ttip);
        initLeftImageView();

        if (TITLES.length == 1) {
            //目前不支持为1的情况
        } else {
            RelativeLayout.LayoutParams progressBarparams = (RelativeLayout.LayoutParams)
                    progressBar.getLayoutParams();
            progressBar.setLayoutParams(progressBarparams);
        }

        List<String> titles = new ArrayList<>();
        for (int i = 0; i < TITLES.length; i++) {
            titles.add(TITLES[i]);
        }

        this.mSegmentHorzontal.setupWithViewPager(this.myViewPager);
        this.mSegmentHorzontal.setup(titles);
        mSegmentHorzontal.addOnTabSelectedListener(onTabSelectedListener);
        //mSegmentHorzontal.getTabAt(0).select();
        //检查并显示第一个下拉箭头 通常是0 也就是idiyige
        checkAndShowArrow(firstShowItem);
        myViewPager.setCurrentItem(firstShowItem);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        final int pageMargin = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                        .getDisplayMetrics());
        myViewPager.setPageMargin(pageMargin);

        initMenuMore(rootView);
        changeTopRightButton(firstShowItem);

        isViewCreated = true;
        myViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //TODO 换肤功能导致selected不正常，动态处理下。第一次的时候有闪屏情况，待处理
                try {
                    if (((ViewGroup) mSegmentHorzontal.getTabAt(0).getCustomView()).getChildAt(0) instanceof EFTextView) {
                        return;
                    }
                    if (!((ViewGroup) ((ViewGroup) mSegmentHorzontal.getTabAt(0).getCustomView()).getChildAt(0)).isSelected()) {
                        ((TextView) (((ViewGroup) ((ViewGroup) mSegmentHorzontal.getTabAt(0).getCustomView()).getChildAt(0))).getChildAt(0)).setSelected(false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        initTopRightLeft();

        return rootView;
    }

    //初始化右上角左边的图标
    private void initTopRightLeft() {
        if (rightTopLeftMenuObject != null) {
            ivRightTopLeft.setVisibility(View.VISIBLE);
            ivRightTopLeft.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    AbFragmentManager fragmentManager = new AbFragmentManager(getActivity());
                    try {
                        fragmentManager.startActivity(rightTopLeftMenuObject, 0, 0);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, priority = 1, sticky = true)
    public void onSolveMessageEvent(SystemInitOverEvent event) {
        initBadge();
    }

    private void initBadge() {
        if (null != mSecondLevelMenus && mSecondLevelMenus.size() > 0) {
            for (int i = 0; i < mSecondLevelMenus.size(); i++) {
                StubObject stubObject = mSecondLevelMenus.get(i);
                String id = stubObject.getString("id", "");
                String isShowBadge = stubObject.getString("showBadge", "false");
                if (isShowBadge.equals("true")) {
                    int unReadCount = BadgeUtil.getCount(id);
                    View v = ((ViewGroup) mSegmentHorzontal.getTabAt(i).getCustomView()).getChildAt(0);
                    if (!(v instanceof Ibadge)) {
                        if (v instanceof ViewGroup) {
                            v = ((ViewGroup) v).getChildAt(0);
                        }
                    }
                    if (v instanceof Ibadge) {
                        if (((Ibadge) v).getBadgeView() == null) {
                            BadgeView badgeView = new BadgeView(getContext(), v);
                            badgeView.setBadgeMargin(0, 0);
                            badgeView.setTextSize(11);

                            ((Ibadge) v).initBadgeView(badgeView);
                        }
                        ((Ibadge) v).setBadgeCount(unReadCount);
                    }
                }
            }
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSolveMessageEvent(UpdateBadgeViewEvent event) {
        int fromUserId = Integer.parseInt(event.getUserID());
        byte chatType = event.getChatType();
        boolean mustRefresh = event.isRefreshAll();
        for (int i = 0; i < mSecondLevelMenus.size(); i++) {
            StubObject stubObject = mSecondLevelMenus.get(i);
            String isShowBadge = stubObject.getString("showBadge", "false");
            if (!isShowBadge.equals("true")) {
                continue;
            }

            String id = (String) stubObject.getID();
            List chatIDs = BadgeUtil.badgeMap.get(id);
            if (!mustRefresh) {
                if (!BadgeUtil.isNeedRefreshBadge(chatIDs, fromUserId, chatType)) {
                    continue;
                }
            }
            int unReadCount = BadgeUtil.getCount(id);

            View v = ((ViewGroup) mSegmentHorzontal.getTabAt(i).getCustomView()).getChildAt(0);
            if (!(v instanceof Ibadge)) {
                if (v instanceof ViewGroup) {
                    v = ((ViewGroup) v).getChildAt(0);
                }
            }
            if (v instanceof Ibadge) {
                if (((Ibadge) v).getBadgeView() == null) {
                    BadgeView badgeView = new BadgeView(getContext(), v);
                    badgeView.setBadgeMargin(0, 0);
                    badgeView.setTextSize(8);

                    ((Ibadge) v).initBadgeView(badgeView);
                }
                if (v instanceof Ibadge) {
                    ((Ibadge) v).setBadgeCount(unReadCount);
                }
            }
        }

        //todo 处理右上角通讯录的角标
        if (ivRightTopLeft.getVisibility() == View.VISIBLE) {
            int newFriendCount = WeChatDBManager.getInstance().getNewFriendUnread();
            int groupNotiveCount = WeChatDBManager.getInstance().getGroupNoticveUnreadCount();
            int allCount = newFriendCount + groupNotiveCount;
            rightTopLeftbadgeView.setText("");
            rightTopLeftbadgeView.setTag("badge");
            rightTopLeftbadgeView.setBadgeMargin(3);
            rightTopLeftbadgeView.setBadgeBackgroundColor(JfResourceUtil.getSkinColor(
                    R.color.chat_badge_color));
            rightTopLeftbadgeView.setTextSize(7);
            if (allCount > 0) {
                rightTopLeftbadgeView.show();
            } else {
                rightTopLeftbadgeView.hide();
            }
        }

    }

    /**
     * 处理左侧图标
     *
     * @param
     */
    private void initLeftImageView() {

        if ("0".equals(EnvironmentVariable.getProperty("isShowLeftAvatar"))) {
            leftImageView.setVisibility(View.INVISIBLE);
            return;
        }

        if (isShowBackImage) {
            leftImageView.setVisibility(View.INVISIBLE);
            ImageView backImageView = (ImageView) rootView.findViewById(R.id.ttip_back);
            backImageView.setVisibility(View.VISIBLE);
            // backImageView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ef_title_view_back));
            backImageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().finish();
                }
            });
            return;
        }
        try {
            user = WeChatDBManager.getInstance().getOneUserById(Integer.valueOf(EnvironmentVariable.getProperty(CHAT_USER_ID)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (user != null && leftImageView != null) {
            LXGlideImageLoader.getInstance().showUserAvatar(getActivity(), leftImageView, user.getAvatar());
            leftImageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getActivity() != null && getActivity() instanceof BaseMainHomeActivity) {
                        if (((BaseMainHomeActivity) getActivity()).isAllowLeftDrawer()) {
                            activity.getDrawerLayout().openDrawer(Gravity.LEFT);
                        }
                        return;
                    }
                    activity.getDrawerLayout().openDrawer(Gravity.LEFT);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        initLeftImageView();
    }

    //初始化右上角的图标
    private void initMenuMore(View rootView) {
        menu_moreIV = (ImageView) rootView.findViewById(R.id.menu_more);
        if (this.getRightButtonType() == null || this.getRightButtonType().equals("")) {
            menu_moreIV.setVisibility(View.GONE);
        } else {
            menu_moreIV.setVisibility(View.VISIBLE);
            menu_moreIV.setOnClickListener(new TopRightOnclickListener(getRightButtonType()));
        }
        if ("import".equals(getRightButtonType())
                || "star_asset_wallet_change".equals(getRightButtonType())) {
            menu_moreIV.setImageDrawable(JfResourceUtil.getSkinDrawable(R.drawable.right_top_import_menu));
        } else if ("add".equals(getRightButtonType())) {
            menu_moreIV.setImageDrawable(JfResourceUtil.getSkinDrawable(R.drawable.main_activtiy_add_normal));
        } else {
            //读取配置的图标并展示
            Object localImagePath = ResFileManager.IMAGE_DIR + "/" + getRightTopButtonICon();
            if (getRightTopButtonICon() == null || "".equals(getRightTopButtonICon())) {
                localImagePath = JfResourceUtil.getSkinDrawable(R.drawable.right_top_import_menu);
            }
            LXGlideImageLoader.getInstance().displayImage(getActivity(), menu_moreIV, localImagePath, R.drawable.right_top_import_menu
                    , R.drawable.right_top_import_menu);
        }
    }

    @Override
    public void onClick(View v) {

    }

    public class MyPagerAdapter extends FragmentPagerAdapter {
        Map<Integer, Fragment> fragmentMap = new HashMap<Integer, Fragment>();
        private Fragment currentFragment;

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            currentFragment = (Fragment) object;
            super.setPrimaryItem(container, position, object);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int position) {
            BaseFragment frag = null;

            if (null != mSecondLevelMenus) {
                StubObject mainMenuItem = (StubObject) mSecondLevelMenus
                        .get(position);
                AbFragmentManager ab = new AbFragmentManager(PagerSlidingTabQQFragment.this.getActivity());

                frag = ab.getFragment(mainMenuItem);
                //	if (TITLES.size() > 1) {
                frag.setAlone(false);
                frag.setBadgeType(PagerSlidingTabQQFragment.this.getBadgeType());
                //}
                if (frag instanceof SimpleWebFragmentNoRefreshNoForms) {
                    ((SimpleWebFragmentNoRefreshNoForms) frag)
                            .setTitleVisible(false);
                    // ((SimpleWebFragmentNoRefreshNoForms)frag).setUserVisibleHint(true);
                }
//                else if (frag instanceof ChatListFragment) {
////                    PagerSlidingTabFragment.this.setFragmentType("Message");
//
//                    ((ChatListFragment) frag).setInitprogressDismiss(new ChatListFragment.InitprogressDismiss() {
//                        @Override
//                        public void trigger(boolean show) {
//                            if (show) {
//                                initnetprogressdismissHandler.sendEmptyMessage(1);
//                            } else {
//                                initnetprogressdismissHandler.sendEmptyMessage(0);
//                            }
//
//                        }
//                    });
//
//                    progressBar.setVisibility(View.VISIBLE);
//                }
                else if (frag instanceof ChatListSwipeMenuFragment) {//新组件的聊天列表
                    ((ChatListSwipeMenuFragment) frag).setInitprogressDismiss(new ChatListSwipeMenuFragment.InitprogressDismiss() {
                        @Override
                        public void trigger(boolean show) {
                            if (show) {
                                initnetprogressdismissHandler.sendEmptyMessage(1);
                            } else {
                                initnetprogressdismissHandler.sendEmptyMessage(0);
                            }

                        }
                    });
                    progressBar.setVisibility(View.VISIBLE);
                } else if (frag instanceof ContactsFragment) {

                }


            } else {
                // frag = new BaseFragment();
            }

            fragmentMap.put(position, frag);
            return frag;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            fragmentMap.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getFragment(int key) {
            return fragmentMap.get(key);
        }

        public Fragment getCurrentFragment() {
            return currentFragment;
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (myViewPager != null) {
            int position = myViewPager.getCurrentItem();
            Fragment fragment = adapter.getFragment(position);
            StubObject menuItem = (StubObject) mSecondLevelMenus.get(position);
            // 缓存中的fragment前台显示时，如果是viewType="webView"，通知webView开始加载
            if (hidden) {
                if (isViewCreated && fragment instanceof BaseFragment) {
                    BaseFragment baseFragment = (BaseFragment) fragment;
                    baseFragment.onInvisible();
                }
            }
            if (!hidden) {
                if (isViewCreated && fragment instanceof BaseFragment) {
                    BaseFragment baseFragment = (BaseFragment) fragment;
                    baseFragment.onVisible();
                }
            }
        }

        //处理左上角头像
        initLeftImageView();

        //给ViewPager的 当前显示的fragment派发 onHiddenChanged
        if (adapter != null) {
            Fragment currentFragment = adapter.getCurrentFragment();
            if (currentFragment != null) {
                currentFragment.onHiddenChanged(hidden);
            }
        }
        super.onHiddenChanged(hidden);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mSegmentHorzontal.removeOnTabSelectedListener(onTabSelectedListener);
        myViewPager.removeOnPageChangeListener(onPageChangeListener);
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroy() {
        isViewCreated = false;
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("databack", (Serializable) mSecondLevelMenus);
    }

    public MyPagerAdapter getAdapter() {
        return adapter;
    }

    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    Handler initnetprogressdismissHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0://隐藏
                    progressBar.setVisibility(View.GONE);
                    break;
                case 1://显示
                    progressBar.setVisibility(View.VISIBLE);
                    break;
            }
            super.handleMessage(msg);
        }
    };

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateTitle(UpdatePageSlideTitleEvent event) {
        StubObject stubObject = mSecondLevelMenus.get(myViewPager.getCurrentItem());
        if (stubObject.getID().equals(event.getStubObject().getID())) {
            EFTextView efTextView = mSegmentHorzontal.getTabAt(myViewPager.getCurrentItem()).getCustomView()
                    .findViewById(R.id.txtTitle);
            efTextView.setText(event.getTitle());
        }

    }

    private TabLayout.OnTabSelectedListener onTabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            checkAndShowArrow(tab.getPosition());
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {
            checkAndShowArrow(tab.getPosition());
            StubObject stubObject = mSecondLevelMenus.get(tab.getPosition());
            if (stubObject.getString("hasArrow", "0").equals("1")) {
                EventBus.getDefault().post(new TabReselectedEvent(tab.getCustomView(), stubObject));
            }
        }
    };


    /**
     * 判断是否显示qq样式的下拉箭头
     *
     * @param position tab position
     */
    private void checkAndShowArrow(int position) {
        StubObject stubObject = mSecondLevelMenus.get(position);
        mSegmentHorzontal.hideAllDropDownArrow();
        if (stubObject.getString("hasArrow", "0").equals("1")) {
            mSegmentHorzontal.showArrow(position);
        }
    }

    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            StubObject stubObject = mSecondLevelMenus.get(myViewPager.getCurrentItem());
            if (stubObject.getString("showRightTopButton", "1").equals("0")) {
                menu_moreIV.setVisibility(View.INVISIBLE);
            } else {
                menu_moreIV.setVisibility(View.VISIBLE);
            }
            changeTopRightButton(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    //更改右上角按钮图标，这个角标不是父类的是 每一个子菜单页面配置的
    private void changeTopRightButton(int position) {
        StubObject itemStubObject = mSecondLevelMenus.get(position);
        if (itemStubObject.getStubTable().containsKey("itemMenuType")) {
            String itemMenuType = itemStubObject.getString("itemMenuType", "");
            if ("import".equals(itemMenuType)
                    || "star_asset_wallet_change".equals(itemMenuType)) {
                menu_moreIV.setImageDrawable(JfResourceUtil.getSkinDrawable(R.drawable.right_top_import_menu));
            } else if ("add".equals(itemMenuType)) {
                menu_moreIV.setImageDrawable(JfResourceUtil.getSkinDrawable(R.drawable.main_activtiy_add_normal));
            } else {
                //读取配置的图标并展示
                String itemMenuIcon = itemStubObject.getString("itemMenuIcon", "");
                Object localImagePath = ResFileManager.IMAGE_DIR + "/" + itemMenuIcon;
                if ("".equals(itemMenuIcon)) {
                    localImagePath = JfResourceUtil.getSkinDrawable(R.drawable.right_top_import_menu);
                }
                LXGlideImageLoader.getInstance().displayImage(getActivity(), menu_moreIV, localImagePath, R.drawable.right_top_import_menu
                        , R.drawable.right_top_import_menu);
            }
            menu_moreIV.setOnClickListener(new TopRightOnclickListener(itemMenuType));
        }
    }


    private class TopRightOnclickListener implements OnClickListener {
        private String menuType;

        public TopRightOnclickListener(String menuType) {
            this.menuType = menuType;
        }

        @Override
        public void onClick(View v) {
            if ("share".equals(menuType)) {
                int position1 = myViewPager.getCurrentItem();

                StubObject stubObject = (StubObject) mSecondLevelMenus.get(position1);
                String forms = stubObject.getObject(
                        "forms", "").toString();
                String caption = stubObject.getObject(
                        "caption", "").toString();

                storageUtil = new StorageUtil(getActivity(), "storage");
                String address = storageUtil.getString("address", "");
                String path = storageUtil.getString("path", "");
                String port = storageUtil.getString("port", "8080");
                boolean isSafe = storageUtil.getBoolean("isSafe", true);
                String protocol = null;
                if (isSafe) {
                    protocol = "https";
                } else {
                    protocol = "http";
                }
                ServerURL = protocol + "://" + address + "/" + path;

                String url = ServerURL + "/AndroidForm/" + forms + ".html";
                System.out.println("分享的网址" + url);

                String imagePath = Environment.getExternalStorageDirectory().toString() + "/" + EnvironmentVariable.getProperty(KEY_SETTING_APPID) +
                        "/res" + "/unzip_res" + "/Image" + "/ic_launcher.png";

            } else if ("add".equals(menuType)) {
                AddPopWindow addPopWindow = new AddPopWindow(activity);
                addPopWindow.showPopupWindow(menu_moreIV);
            } else if ("import".equals(menuType)) {
                final CommonOtherPopWindow commonOtherPopWindow = new CommonOtherPopWindow(getActivity());
                commonOtherPopWindow.addMenuButton(ResStringUtil.getString(R.string.openplanet_title_import_fans), R.drawable.open_plant_import_fans)
                        .setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //给星际星球使用
                                try {
                                    String className = getResources().getString(R.string.pageslidetab_import_activity);
                                    Class class1 = Class.forName(className);
                                    Intent intent = new Intent(getActivity(), class1);
                                    //intent.putExtra("number", user.getName());
                                    intent.putExtra("number", EnvironmentVariable.getUserName());
                                    intent.putExtra("hasRightButton", "false");
                                    startActivity(intent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                commonOtherPopWindow.dismiss();
                            }
                        });
                commonOtherPopWindow.showPopupWindow(menu_moreIV);
            } else {
                //todo 通用右上角菜单点击事件 业务界面自己处理 20180728
                int position = myViewPager.getCurrentItem();
                StubObject stubObject = (StubObject) mSecondLevelMenus.get(position);
                EventBus.getDefault().post(new RightTopMenuClickEvent(menu_moreIV, stubObject));
            }
        }
    }

    @Override
    public void initImmersionBar() {
        super.initImmersionBar();
        ImmersionBar.with(this).init();
    }
}
