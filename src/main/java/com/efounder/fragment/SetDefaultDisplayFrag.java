package com.efounder.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.baseui.BaseFragment;
import com.efounder.imageloader.GlideImageLoader;
import com.efounder.ospmobilelib.R;
import com.efounder.service.Registry;
import com.pansoft.resmanager.ResFileManager;

import java.util.List;

public class SetDefaultDisplayFrag extends BaseFragment {
    public static final String TAG = "SetDefaultDisplayFrag";
    public static final String KEY_MENU_ROOT = "menuRoot";


    private View mView;
    private MyAdapter mAdapter;
    private List<StubObject> list;
//    private ImageLoader imageLoader;
//    private DisplayImageOptions options;

    private String currentDisplayBH = "";


//	int currentDisplayPos = 0;

    private static SetDefaultDisplayFrag newInstance() {
        SetDefaultDisplayFrag fragment = new SetDefaultDisplayFrag();
        return fragment;
    }

    @Deprecated
    public SetDefaultDisplayFrag(StubObject stubObject) {
//        imageLoader = ImageLoader.getInstance();
//        options = ImageUtil.getImageLoaderOptions(R.drawable.iframe_icon_refresh);
    }

    public SetDefaultDisplayFrag() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.frag_setdefaultdispay, container, false);

        ListView mListView = (ListView) mView.findViewById(R.id.setDefalutDisPlaylist);
        list = Registry.getRegEntryList(KEY_MENU_ROOT);
        String currentDisplay = EnvironmentVariable.getProperty("currentDisplayFrag", "");

        ImageView back_button = (ImageView) mView.findViewById(R.id.back_button);
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SetDefaultDisplayFrag.this.getActivity().finish();
            }
        });
        for (int i = 0; i < list.size(); i++) {

            StubObject stubObject = list.get(i);
            if (!currentDisplay.equals("")) {
                String id = stubObject.getString("id", "");
                stubObject.setString("defaultDisplay", "false");
                if (id.equals(currentDisplay)) {
//					currentDisplayPos = i;
                    stubObject.setString("defaultDisplay", "true");
                    currentDisplayBH = id;
                }
            }
//			else{
//				String defaultDisplay = stubObject.getString("defaultDisplay","");
//				if(defaultDisplay.equals("true"))
//					currentDisplayPos = i;
//			}

        }
        mAdapter = new MyAdapter(this.getContext());
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //获取选中的参数
                for (int i = 0; i < list.size(); i++) {
                    if (i == position) {
                        list.get(i).setString("defaultDisplay", "true");
                        currentDisplayBH = list.get(i).getString("id", "");

                    } else {
                        list.get(i).setString("defaultDisplay", "false");
                    }
                }
                mAdapter.notifyDataSetChanged();
            }
        });

        Button save = (Button) mView.findViewById(R.id.saveDefaultDisplay);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EnvironmentVariable.setProperty("currentDisplayFrag", currentDisplayBH);
                SetDefaultDisplayFrag.this.getActivity().finish();
            }
        });


        return mView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

//	private final OnItemClickListener mOnItemClickListener = new OnItemClickListener() {
//
//		@Override
//		public void onItemClick(AdapterView adapterView, View view, int position, long arg3) {
//			for (Bean bean : mDatas) {//全部设为未选中
//				bean.setChecked(false);
//			}
//			mDatas.get(position).setChecked(true);//点击的设为选中
//			mAdapter.notifyDataSetChanged();
//
//		}
//	};

    //自定义adapter
    private class MyAdapter extends BaseAdapter {
        private LayoutInflater inflater;

        public MyAdapter(Context context) {
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return list.size();
        }

        @Override
        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.frag_setdefaultdispay_list, null);
                holder = new ViewHolder();
                holder.img = (ImageView) convertView.findViewById(R.id.setDefalutDisPlaylist_avatar);

                holder.txt = (TextView) convertView.findViewById(R.id.setDefalutDisPlaylist_title);
                holder.checkBox = (CheckBox) convertView
                        .findViewById(R.id.setDefalutDisPlaylist_check);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            StubObject stubObject = list.get(position);
            holder.txt.setText(stubObject.getString("caption", ""));
            String defaultDisplay = stubObject.getString("defaultDisplay", "");
            if (defaultDisplay.equals("true")) {
                holder.checkBox.setChecked(true);
            } else {
                holder.checkBox.setChecked(false);

            }

            String menuIcon = (String) stubObject.getString(
                    "menuIcon", "");
            menuIcon = "file://" + ResFileManager.IMAGE_DIR + "/" + menuIcon;
            Bitmap bm = null;

            GlideImageLoader.getInstance().displayImage(getContext(), holder.img, menuIcon, R.drawable.iframe_icon_refresh, R.drawable.iframe_icon_refresh);
//            imageLoader.displayImage(menuIcon, holder.img, options);


//			holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//				@Override
//				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//					// TODO Auto-generated method stub
//					if (isChecked) {
//						myList.get(position).setString("defaultDisplay","true");
//						checkPosition(position);
//					} else {
//                          buttonView.setChecked(true);
//						//checkList.set(position, false);//将已选择的位置设为选择
//							}
//				}
//			});

//			convertView.setOnClickListener(new View.OnClickListener() {//item单击进行单选设置
//				@Override
//				public void onClick(View v) {
//					// TODO Auto-generated method stub
//					checkPosition(position);
//				}
//			});

            return convertView;
        }

        public final class ViewHolder {
            public ImageView img;
            public TextView txt;
            public CheckBox checkBox;
        }
    }

    //设置选中的位置，将其他位置设置为未选
    public void checkPosition(int position) {
        for (int i = 0; i < list.size(); i++) {
            if (position == i) {// 设置已选位置
                list.get(i).setString("defaultDisplay", "true");
            } else {
                list.get(i).setString("defaultDisplay", "false");
            }
        }
        mAdapter.notifyDataSetChanged();
    }

}
