package com.efounder.fragment;

import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.View;

/**
 * @author : zzj
 * @e-mail : zhangzhijun@pansoft.com
 * @date : 2019/7/18
 * @desc : 解决与百度地图滑动冲突的viewPager
 * @version: 1.0
 */
public class ViewPagerCompat extends ViewPager {

    public ViewPagerCompat(Context context) {
        super(context);
    }

    public ViewPagerCompat(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
       /* LogUtils.e("canScroll---->"+v.getClass().getName());
        if(v.getClass().getName().equals("com.baidu.mapapi.map.TextureMapView")) {
            if (Math.abs(dx) > 50) {
                return super.canScroll(v, checkV, dx, x, y);
            } else {
                return true;
            }
        }*/
        //if(v instanceof MapView){
        //    return true;
        //}
        return super.canScroll(v, checkV, dx, x, y);
    }
}
