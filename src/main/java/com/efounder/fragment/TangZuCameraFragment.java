//package com.efounder.fragment;
//
//import android.content.Context;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Matrix;
//import android.graphics.drawable.Drawable;
//import android.hardware.Camera;
//import android.media.ExifInterface;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.SurfaceHolder;
//import android.view.SurfaceView;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.RadioButton;
//import android.widget.RadioGroup;
//import android.widget.TextView;
//
//import com.core.xml.StubObject;
//import com.efounder.chat.utils.ImageUtil;
//import com.efounder.constant.EnvironmentVariable;
//import com.efounder.frame.baseui.BaseFragment;
//import com.efounder.ospmobilelib.R;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.util.Hashtable;
//
//import static com.efounder.frame.utils.Constants.CHAT_USER_ID;
//
////com.efounder.frame.baseui.BaseFragment
//
///**
// * 糖足照相fragment
// */
//public class TangZuCameraFragment extends BaseFragment implements SurfaceHolder.Callback, View.OnClickListener, RadioGroup.OnCheckedChangeListener {
//    private static final String ARG_PARAM1 = "param1";
//    private static final String ARG_PARAM2 = "param2";
//
//    private String mParam1;
//    private String mParam2;
//    private boolean hasSurface = false;
//
//    private boolean hasCameraHardware;
//    private Camera mCamera;
//    SurfaceView surfaceView;
//    private ImageView mengban;
//    private RadioGroup radioGroup;
//    private SurfaceHolder holder;
//
//
//    public TangZuCameraFragment(StubObject stubObject) {
//        Hashtable<String, String> hashtable = stubObject.getStubTable();
//        Bundle bundle = new Bundle();
//        bundle.putSerializable("stubObject", stubObject);
//        setArguments(bundle);
//
//    }
//
//    public TangZuCameraFragment() {
//    }
//
//    ;
//
//
//    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {
//
//        @Override
//        public void onPictureTaken(byte[] data, Camera camera) {
//
//
//            RadioButton radioButton = (RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());
//            int iii = radioGroup.indexOfChild(radioButton);
//
//            String picName = "tzImageIndex" + String.valueOf(iii) + ".JPG";
//            File file = new File(ImageUtil.chatpath, picName);
//            FileOutputStream fileOutputStream = null;
//            try {
//                fileOutputStream = new FileOutputStream(file);
//
//                Intent intent = null;
//
//                Bitmap bitmapOrigin = BitmapFactory.decodeByteArray(data, 0, data.length);
//
//                Bitmap bitmap = rotateBitmapByDegree(bitmapOrigin, 90);
//
//                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
//
//
//                intent = new Intent(TangZuCameraFragment.this.getContext(), Class.forName("com.efounder.SDDiabetesActivity"));
//           /*         image: this.props.image,
//                            type: this.props.index,
//                            id: this.props.userId,*/
//                intent.putExtra("image", file.getAbsolutePath());
//
//
//                intent.putExtra("index", String.valueOf(iii));
//
//                String chat_user_id = EnvironmentVariable.getProperty(CHAT_USER_ID);
//                intent.putExtra("chatUserId", chat_user_id);
//                String userName = EnvironmentVariable.getUserName();
//                intent.putExtra("loginUserName", userName);
//
//                startActivity(intent);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    };
//
//        @Override
//        public void onCreate(Bundle savedInstanceState) {
//
//            super.onCreate(savedInstanceState);
//            if (getArguments() != null) {
//                mParam1 = getArguments().getString(ARG_PARAM1);
//                mParam2 = getArguments().getString(ARG_PARAM2);
//            }
//
//            hasCameraHardware = checkCameraHardware(getContext());
//
//            Log.e(TAG, "onCreate() called with: savedInstanceState = [" + savedInstanceState + "]");
//            hasSurface = false;
//        }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                                 Bundle savedInstanceState) {
//            Log.e(TAG, "onCreateView() called with: inflater = [" + inflater + "], container = [" + container + "], savedInstanceState = [" + savedInstanceState + "]");
//
//            // Inflate the layout for this fragment
//            if (hasCameraHardware) {
//
//                return inflater.inflate(R.layout.fragment_tang_zu_camera, container, false);
//            } else {
//                TextView textView = new TextView(getContext());
//                textView.setText("此设备照相设备");
//
//                return textView;
//
//            }
//
//        }
//
//        @Override
//        public void onHiddenChanged(boolean hidden) {
//            super.onHiddenChanged(hidden);
//            Log.e(TAG, "onHiddenChanged() called with: hidden = [" + hidden + "]");
//
//            if (hidden) {
//                releaseCamera();
//
//            } else {
//                if (mCamera == null) {
//                    mCamera = getCameraInstance();
//                }
//
//                if (hasSurface ) {
//                    startPreview(holder);
//                }else{
//                    holder = surfaceView.getHolder();
//                    holder.addCallback(this);
//                }
//
//
//            }
//        }
//
//        private void releaseCamera() {
//            if (mCamera != null) {
//                mCamera.release();
//                mCamera = null;
//
//            }
//        }
//
//        /**
//         * Check if this device has a camera
//         */
//        private boolean checkCameraHardware(Context context) {
//            if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
//                // this device has a camera
//                return true;
//            } else {
//                // no camera on this device
//                return false;
//            }
//        }
//
//        @Override
//        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//            if (!hasCameraHardware) {
//                return;
//            }
//            mCamera = getCameraInstance();
//            surfaceView = (SurfaceView) view.findViewById(R.id.preview_view);
//            view.findViewById(R.id.takePicker).setOnClickListener(this);
//            mengban = (ImageView) view.findViewById(R.id.mengban);
//            mengban.setOnClickListener(this);
//            radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);
//            radioGroup.setOnCheckedChangeListener(this);
//            holder = surfaceView.getHolder();
//            holder.addCallback(this);
//            Log.d(TAG, "onViewCreated() called with: view = [" + view + "], savedInstanceState = [" + savedInstanceState + "]");
//        }
//
//
//        private static final String TAG = "TangZuCameraFragment";
//
//        @Override
//        public void onResume() {
//            Log.e(TAG, "onResume() called");
//            super.onResume();
//            if (mCamera == null) {
//                mCamera = getCameraInstance();
//            }
//
//        }
//
//        @Override
//        public void onAttach(Context context) {
//            super.onAttach(context);
//
//        }
//
//        @Override
//        public void onDetach() {
//            super.onDetach();
//            Log.e(TAG, "onDetach() called");
//
//        }
//
//        @Override
//        public void surfaceCreated(SurfaceHolder holder) {
//            startPreview(holder);
//        }
//
//        private void startPreview(SurfaceHolder holder) {
//            hasSurface = true;
//            try {
//                if(holder==null){
//                    Log.e(TAG, "startPreview: holder is null  " );
//                }
//
//                mCamera.setPreviewDisplay(holder);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            mCamera.startPreview();
//
//            mCamera.setDisplayOrientation(90);
//            // get Camera parameters
//            Camera.Parameters params = mCamera.getParameters();
//// set the focus mode
//            params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
//// set Camera parameters
//            mCamera.setParameters(params);
//            mCamera.autoFocus(new Camera.AutoFocusCallback() {
//                @Override
//                public void onAutoFocus(boolean success, Camera camera) {
//
//                }
//            });
//        }
//
//        @Override
//        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
//
//        }
//
//        @Override
//        public void onPause() {
//            Log.e(TAG, "onPause: ");
//            super.onPause();
//            hasSurface  = false;
//            releaseCamera();
//        }
//
//        /**
//         * 将图片按照某个角度进行旋转
//         *
//         * @param bm
//         *            需要旋转的图片
//         * @param degree
//         *            旋转角度
//         * @return 旋转后的图片
//         */
//        public static Bitmap rotateBitmapByDegree(Bitmap bm, int degree) {
//            Bitmap returnBm = null;
//
//            // 根据旋转角度，生成旋转矩阵
//            Matrix matrix = new Matrix();
//            matrix.postRotate(degree);
//            try {
//                // 将原始图片按照旋转矩阵进行旋转，并得到新的图片
//                returnBm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
//            } catch (OutOfMemoryError e) {
//            }
//            if (returnBm == null) {
//                returnBm = bm;
//            }
//            if (bm != returnBm) {
//                bm.recycle();
//            }
//            return returnBm;
//        }
//
//        private int getBitmapDegree(String path) {
//            int degree = 0;
//            try {
//                // 从指定路径下读取图片，并获取其EXIF信息
//                ExifInterface exifInterface = new ExifInterface(path);
//                // 获取图片的旋转信息
//                int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
//                        ExifInterface.ORIENTATION_NORMAL);
//                switch (orientation) {
//                    case ExifInterface.ORIENTATION_ROTATE_90:
//                        degree = 90;
//                        break;
//                    case ExifInterface.ORIENTATION_ROTATE_180:
//                        degree = 180;
//                        break;
//                    case ExifInterface.ORIENTATION_ROTATE_270:
//                        degree = 270;
//                        break;
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return degree;
//        }
//
//        @Override
//        public void onDestroy() {
//            super.onDestroy();
//            Log.e(TAG, "onDestroy: ");
//
//        }
//
//        @Override
//        public void surfaceDestroyed(SurfaceHolder holder) {
//            hasSurface = false;
//
//        }
//
//        /**
//         * A safe way to get an instance of the Camera object.
//         */
//        public static Camera getCameraInstance() {
//            Camera c = null;
//            try {
//                c = Camera.open(); // attempt to get a Camera instance
//            } catch (Exception e) {
//                // Camera is not available (in use or does not exist)
//            }
//            return c; // returns null if camera is unavailable
//        }
//
//        @Override
//        public void onClick(View v) {
//            if (v.getId() == R.id.takePicker) {
//                mCamera.takePicture(null, null, mPicture);
//            } else if (v.getId() == R.id.mengban) {
//                mCamera.autoFocus(null);
//
//            }
//        }
//
//        @Override
//        public void onCheckedChanged(RadioGroup group, int checkedId) {
//            Drawable drawable = null;
//            if (checkedId == R.id.leftfootin) {
//                drawable = getActivity().getResources().getDrawable(R.drawable.leftin_rightout);
//            } else if (checkedId == R.id.leftfootout) {
//
//                drawable = getActivity().getResources().getDrawable(R.drawable.leftout_rightin);
//            } else if (checkedId == R.id.leftfootsuface) {
//                drawable = getActivity().getResources().getDrawable(R.drawable.leftfootface);
//
//            } else if (checkedId == R.id.leftfootheart) {
//                drawable = getActivity().getResources().getDrawable(R.drawable.leftcootcenter);
//            } else if (checkedId == R.id.rightfootheart) {
//                drawable = getActivity().getResources().getDrawable(R.drawable.rightfootcenter);
//            } else if (checkedId == R.id.rightfootin) {
//                drawable = getActivity().getResources().getDrawable(R.drawable.leftout_rightin);
//            } else if (checkedId == R.id.rightfootsurface) {
//                drawable = getActivity().getResources().getDrawable(R.drawable.rightfootface);
//
//            } else if (checkedId == R.id.rightfootout) {
//                drawable = getActivity().getResources().getDrawable(R.drawable.leftin_rightout);
//
//            }
//            mengban.setBackgroundDrawable(drawable);
//
//
//        }
//    }
