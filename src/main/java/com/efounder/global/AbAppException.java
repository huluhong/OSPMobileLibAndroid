/*
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.efounder.global;

import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;

import com.efounder.util.AbStrUtil;


// TODO: Auto-generated Javadoc

/**
 * 鍚岖О锛欰bAppException.java 
 * 鎻忚堪锛氩叕鍏卞纾甯哥被.
 *
 */
public class AbAppException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1;

	
	/** 寮傚父娑堟伅. */
	private String msg = null;

	/**
	 * 鏋勯€犲纾甯哥被.
	 *
	 * @param e 寮傚父
	 */
	public AbAppException(Exception e) {
		super();

		try {
			if( e instanceof HttpHostConnectException) {  
				msg = AbAppConfig.UNKNOWN_HOST_EXCEPTION;
			}else if (e instanceof ConnectException) {
				msg = AbAppConfig.CONNECT_EXCEPTION;
			}else if (e instanceof ConnectTimeoutException) {
				msg = AbAppConfig.CONNECT_EXCEPTION;
			}else if (e instanceof UnknownHostException) {
				msg = AbAppConfig.UNKNOWN_HOST_EXCEPTION;
			}else if (e instanceof SocketException) {
				msg = AbAppConfig.SOCKET_EXCEPTION;
			}else if (e instanceof SocketTimeoutException) {
				msg = AbAppConfig.SOCKET_TIMEOUT_EXCEPTION;
			}else if( e instanceof NullPointerException) {  
				msg = AbAppConfig.NULL_POINTER_EXCEPTION;
			}else if( e instanceof ClientProtocolException) {  
				msg = AbAppConfig.CLIENTP_ROTOCOL_EXCEPTION;
			}else {
				if (e == null || AbStrUtil.isEmpty(e.getMessage())) {
					msg = AbAppConfig.NULL_MESSAGE_EXCEPTION;
				}else{
				    msg = e.getMessage();
				}
			}
		} catch (Exception e1) {
		}
		
	}

	/**
	 * 鐢ㄤ竴涓秷鎭瀯阃犲纾甯哥被.
	 *
	 * @param message 寮傚父镄勬秷鎭?	 */
	public AbAppException(String message) {
		super(message);
		msg = message;
	}

	/**
	 * 鎻忚堪锛氲幏鍙栧纾甯镐俊鎭?
	 *
	 * @return the message
	 */
	@Override
	public String getMessage() {
		return msg;
	}

}
