package com.efounder.org;

import com.efounder.baseadapter.listview.ViewHolder;
import com.efounder.ospmobilelib.R;
import com.zhy.tree.bean.TreeNode;




/**
 * Created by YQS on 2018/1/8.
 */

public class DepartmentItemDelagate implements com.efounder.baseadapter.listview.ItemViewDelegate<TreeNode> {
    @Override
    public int getItemViewLayoutId() {
        return R.layout.layout_org_depart_item;

    }

    @Override
    public boolean isForViewType(TreeNode item, int position) {
        return !item.getAttrs().get("TYPE").equals("USER");
    }

    @Override
    public void convert(ViewHolder holder, TreeNode treeNode, int position) {
        holder.setText(R.id.title, treeNode.getName());

        if (treeNode.isHasCheckBox() ){
            holder.setChecked(R.id.checkbox,"1".equals(treeNode.getAttrs().get("isChecked"))?true:false) ;
            holder.setVisible(R.id.checkbox, true);
            holder.setVisible(R.id.rightarrow, false);

        }else{
            holder.setChecked(R.id.checkbox,false) ;
            holder.setVisible(R.id.checkbox, false);
            holder.setVisible(R.id.rightarrow, true);
        }
    }
}
