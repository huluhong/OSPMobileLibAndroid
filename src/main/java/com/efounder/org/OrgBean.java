package com.efounder.org;


import com.zhy.tree.bean.TreeNode;

/**
 * Created by YQS on 2017/12/28.
 */

public class OrgBean {

    private TreeNode treeBean;
    private boolean isSelected;

    public OrgBean() {
    }

    public OrgBean(TreeNode treeBean, boolean isSelected) {
        this.treeBean = treeBean;
        this.isSelected = isSelected;
    }

    public TreeNode getTreeNode() {
        return treeBean;
    }

    public OrgBean setTreeNode(TreeNode treeBean) {
        this.treeBean = treeBean;
        return this;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public OrgBean setSelected(boolean selected) {
        isSelected = selected;
        return this;
    }
}
