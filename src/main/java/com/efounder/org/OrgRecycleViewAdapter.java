package com.efounder.org;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.efounder.ospmobilelib.R;

import java.util.List;


/**
 * 新版组织机构 上方适配器
 * Created by yqs on 2017/12/28/008.
 */

public class OrgRecycleViewAdapter extends RecyclerView.Adapter<OrgRecycleViewAdapter.MyViewHolder> {

    private List<OrgBean> mDatas;
    private Context mContext;
    private LayoutInflater inflater;
    OnRecycleViewItemClickListener listener;


    public OrgRecycleViewAdapter(Context context, List<OrgBean> datas) {
        this.mContext = context;
        this.mDatas = datas;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getItemCount() {

        return mDatas == null ? 0 : mDatas.size();
    }

    //填充onCreateViewHolder方法返回的holder中的控件
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

            holder.title.setText(mDatas.get(position).getTreeNode().getName());
            if (position==mDatas.size()-1 || mDatas.size()==1){
               holder.title.setTextColor(ContextCompat.getColor(mContext, R.color.black));
            }else{
                holder.title.setTextColor(ContextCompat.getColor(mContext,R.color.chat_red));
        }
        if (position==0){
            holder.org_iv_arrow.setVisibility(View.GONE);
        }else{
            holder.org_iv_arrow.setVisibility(View.VISIBLE);

        }

        holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(v, mDatas.get(position), position);
                }

            }
        });
    }


    //重写onCreateViewHolder方法，返回一个自定义的ViewHolder
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = inflater.inflate(R.layout.layout_org_topitem, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView start_time;
        TextView title;
        TextView location;
        RelativeLayout relativeLayout;
        View lineView;
        ImageView org_iv_arrow;

        public MyViewHolder(View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.title);
            org_iv_arrow= (ImageView) view.findViewById(R.id.org_iv_arrow);
        }

    }

//    static class FootViewHolder extends RecyclerView.ViewHolder {
//        public FootViewHolder(View itemView) {
//            super(itemView);
//        }
//    }

    public void setOnRecycleViewItemClickListener(OnRecycleViewItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnRecycleViewItemClickListener {
        public void onClick(View view, OrgBean bean, int position);
    }
}