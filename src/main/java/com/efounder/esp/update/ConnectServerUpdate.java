package com.efounder.esp.update;

import java.util.ArrayList;

import org.json.JSONObject;

import android.util.Log;

import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;

/**
 * @author long
 * 根据设备的APPID和本地版本号去请求服务器
 * args appid 应用ID
 * args appbb 本地版本号
 * return  status 0为没有更新 1为有更新
 * return  url    apk更新包的地址
 * return  appbb  服务器版本号
 *
 */
public class ConnectServerUpdate {

	private Thread runnable;
	private ArrayList<String> updatelist;

	/**
	 * 
	 * @param requestType
	 * @param APPID
	 * @param APPBB
	 * @param URL
	 * @retur 服务器检查更新结果   更新包地址    服务器版本号
	 *     
	 */
	public ArrayList<String> ConnectServerUpdates(String requestType, String APPID ,String APPVersion,String URL) {
		return returnArraydpdate(requestType,APPID,APPVersion,URL);
	}

	private ArrayList<String> returnArraydpdate(final String requestType, final String APPID ,final String APPVersion,final String URL){
		// TODO Auto-generated method stub
		runnable = new Thread(new Runnable() {
			@Override
			public void run() {
				// 这里写入子线程需要做的工作
				JResponseObject RO;
				JParamObject PO = new JParamObject();
				PO.setValue("requestType",requestType);
				// setEnvValue("requestType", "mam_checkAppUpgrade");
				PO.setValue("appid", APPID);
				PO.setValue("appbb", APPVersion);
				PO.setServiceURL(URL);
				try {
					RO = EAI.DAL.SVR("MoblieDMService", PO);
					// EAI.DAL.IOM("MoblieDMService", "",PO);
					if (RO != null) {
						System.out.println("-------->" + RO);
						JSONObject jsonObject = new JSONObject(RO.getResponseMap().get("json").toString());
						String status = jsonObject.getString("status");
						String url = jsonObject.getString("url");
						String appbb = jsonObject.getString("appbb");
						updatelist = new ArrayList<String>();
						updatelist.add(status);
						updatelist.add(url);
						updatelist.add(appbb);
					}
				} catch (Exception e) {
					e.printStackTrace();
					Log.i("xxxx", "登陆抛出异常");
				}

			}
		});
		try {
			runnable.start(); // 启动线程
			runnable.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return updatelist;
	}

}
