//package com.efounder.data.model;
//
//import java.io.Serializable;
///**
// * 会议室 数据模型
// */
//public class HYSModel implements Serializable {
//
//	private static final long serialVersionUID = 1L;
//
//	private String F_HYJJ;
//
//	private String F_SDATE;
//
//	private String F_EDATE;
//
//	private String F_HYBH;
//
//	private String F_LRDWMC;
//
//	private String F_HYSMC;
//
//	private String F_HYZT;
//
//
//	public String getF_HYZT() {
//		return F_HYZT;
//	}
//
//	public void setF_HYZT(String f_HYZT) {
//		F_HYZT = f_HYZT;
//	}
//
//	public String getF_HYSMC() {
//		return F_HYSMC;
//	}
//
//	public void setF_HYSMC(String f_HYSMC) {
//		F_HYSMC = f_HYSMC;
//	}
//
//	public String getF_HYJJ() {
//		return F_HYJJ;
//	}
//
//	public void setF_HYJJ(String f_HYJJ) {
//		F_HYJJ = f_HYJJ;
//	}
//
//	public String getF_SDATE() {
//		return F_SDATE;
//	}
//
//	public void setF_SDATE(String f_SDATE) {
//		F_SDATE = f_SDATE;
//	}
//
//	public String getF_EDATE() {
//		return F_EDATE;
//	}
//
//	public void setF_EDATE(String f_EDATE) {
//		F_EDATE = f_EDATE;
//	}
//
//	public String getF_HYBH() {
//		return F_HYBH;
//	}
//
//	public void setF_HYBH(String f_HYBH) {
//		F_HYBH = f_HYBH;
//	}
//
//	public String getF_LRDWMC() {
//		return F_LRDWMC;
//	}
//
//	public void setF_LRDWMC(String f_LRDWMC) {
//		F_LRDWMC = f_LRDWMC;
//	}
//
//	private int hysImg;
//
//	private String hysmc;
//
//	private String hysqbm;
//
//	private boolean status;
//
//	public String getHysmc() {
//		return hysmc;
//	}
//
//	public void setHysmc(String hysmc) {
//		this.hysmc = hysmc;
//	}
//
//	public String getHysqbm() {
//		return hysqbm;
//	}
//
//	public void setHysqbm(String hysqbm) {
//		this.hysqbm = hysqbm;
//	}
//
//	public boolean getStatus() {
//		return status;
//	}
//
//	public void setStatus(boolean status) {
//		this.status = status;
//	}
//
//	public int getHysImg() {
//		return hysImg;
//	}
//
//	public void setHysImg(int hysImg) {
//		this.hysImg = hysImg;
//	}
//
//	@Override
//	public String toString() {
//		return "HYSModel [F_HYJJ=" + F_HYJJ + ", F_SDATE=" + F_SDATE
//				+ ", F_EDATE=" + F_EDATE + ", F_HYBH=" + F_HYBH + ", F_LRDWMC="
//				+ F_LRDWMC + ", F_HYSMC=" + F_HYSMC + ", F_HYZT=" + F_HYZT
//				+ ", hysImg=" + hysImg + ", hysmc=" + hysmc + ", hysqbm="
//				+ hysqbm + ", status=" + status + "]";
//	}
//}
