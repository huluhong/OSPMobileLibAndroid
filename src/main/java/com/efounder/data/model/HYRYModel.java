//package com.efounder.data.model;
//
//import java.io.Serializable;
//
//public class HYRYModel implements Serializable {
//
//	private static final long serialVersionUID = 1L;
//
//	private String F_YHBH;
//	private String F_TEL;
//	private String F_NOTE;
//	private String F_RYLX;
//	private String F_YHMC;
//	private String F_DWBH;
//	private String F_DWMC;
//	private String F_HYBH;
//	private String F_EMAIL;
//
//	private String F_QDZT;
//	private String F_CHRBH;
//
//
//	public String getF_QDZT() {
//		return F_QDZT;
//	}
//	public void setF_QDZT(String f_QDZT) {
//		F_QDZT = f_QDZT;
//	}
//	public String getF_CHRBH() {
//		return F_CHRBH;
//	}
//	public void setF_CHRBH(String f_CHRBH) {
//		F_CHRBH = f_CHRBH;
//	}
//	private int  headImg;
//	public int getHeadImg() {
//		return headImg;
//	}
//	public void setHeadImg(int headImg) {
//		this.headImg = headImg;
//	}
//	public String getF_YHBH() {
//		return F_YHBH;
//	}
//	public void setF_YHBH(String f_YHBH) {
//		F_YHBH = f_YHBH;
//	}
//	public String getF_TEL() {
//		return F_TEL;
//	}
//	public void setF_TEL(String f_TEL) {
//		F_TEL = f_TEL;
//	}
//	public String getF_NOTE() {
//		return F_NOTE;
//	}
//	public void setF_NOTE(String f_NOTE) {
//		F_NOTE = f_NOTE;
//	}
//	public String getF_RYLX() {
//		return F_RYLX;
//	}
//	public void setF_RYLX(String f_RYLX) {
//		F_RYLX = f_RYLX;
//	}
//	public String getF_YHMC() {
//		return F_YHMC;
//	}
//	public void setF_YHMC(String f_YHMC) {
//		F_YHMC = f_YHMC;
//	}
//	public String getF_DWBH() {
//		return F_DWBH;
//	}
//	public void setF_DWBH(String f_DWBH) {
//		F_DWBH = f_DWBH;
//	}
//	public String getF_DWMC() {
//		return F_DWMC;
//	}
//	public void setF_DWMC(String f_DWMC) {
//		F_DWMC = f_DWMC;
//	}
//	public String getF_HYBH() {
//		return F_HYBH;
//	}
//	public void setF_HYBH(String f_HYBH) {
//		F_HYBH = f_HYBH;
//	}
//	public String getF_EMAIL() {
//		return F_EMAIL;
//	}
//	public void setF_EMAIL(String f_EMAIL) {
//		F_EMAIL = f_EMAIL;
//	}
//	@Override
//	public String toString() {
//		return "HYRYModel [F_YHBH=" + F_YHBH + ", F_TEL=" + F_TEL + ", F_NOTE="
//				+ F_NOTE + ", F_RYLX=" + F_RYLX + ", F_YHMC=" + F_YHMC
//				+ ", F_DWBH=" + F_DWBH + ", F_DWMC=" + F_DWMC + ", F_HYBH="
//				+ F_HYBH + ", F_EMAIL=" + F_EMAIL + "]";
//	}
//
//}
