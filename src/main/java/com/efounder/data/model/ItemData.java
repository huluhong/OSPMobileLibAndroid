//package com.efounder.data.model;
//
//import android.graphics.Bitmap;
//
//import com.google.gson.annotations.Expose;
//
//import java.io.Serializable;
//
//public class ItemData implements Serializable {
//
//	private static final long serialVersionUID = 1L;
//	/** 标题 */
//	@Expose
//	public String caption;
//	/** 头像 */
//	@Expose
//	public Bitmap menuIcon;
//	/** 服务器地址 */
//	private String serverIP;
//	/** 摄像头ID */
//	@Expose
//	public String cameraID;
//	/** 设备ID */
//	@Expose
//	public String deviceID;
//	/** 海康SDK 用户名 */
//	private String userName;
//	/** 海康SDK 用户名密码 */
//	private String password;
//	@Expose
//	private String bitmapURL;
//
//	public String getBitmapURL() {
//		return bitmapURL;
//	}
//
//	public void setBitmapURL(String bitmapURL) {
//		this.bitmapURL = bitmapURL;
//	}
//
//	public String getCaption() {
//		return caption;
//	}
//
//	public void setCaption(String caption) {
//		this.caption = caption;
//	}
//
//	public Bitmap getMenuIcon() {
//		return menuIcon;
//	}
//
//	public void setMenuIcon(Bitmap menuIcon) {
//		this.menuIcon = menuIcon;
//	}
//
//	public String getServerIP() {
//		return serverIP;
//	}
//
//	public void setServerIP(String serverIP) {
//		this.serverIP = serverIP;
//	}
//
//	public String getCameraID() {
//		return cameraID;
//	}
//
//	public void setCameraID(String cameraID) {
//		this.cameraID = cameraID;
//	}
//
//	public String getDeviceID() {
//		return deviceID;
//	}
//
//	public void setDeviceID(String deviceID) {
//		this.deviceID = deviceID;
//	}
//
//	public String getUserName() {
//		return userName;
//	}
//
//	public void setUserName(String userName) {
//		this.userName = userName;
//	}
//
//	public String getPassword() {
//		return password;
//	}
//
//	public void setPassword(String password) {
//		this.password = password;
//	}
//
//	@Override
//	public String toString() {
//		return "ItemData [caption=" + caption + ", menuIcon=" + menuIcon
//				+ ", serverIP=" + serverIP + ", cameraID=" + cameraID
//				+ ", deviceID=" + deviceID + ", userName=" + userName
//				+ ", password=" + password + "]";
//	}
//
//	/**
//	 * 如果对象类型是User 的话 则返回true 去比较hashCode值
//	 */
//	@Override
//	public boolean equals(Object obj) {
//		if (obj == null)
//			return false;
//		if (this == obj)
//			return true;
//		if (obj instanceof ItemData) {
//			ItemData item = (ItemData) obj;
//			if (item.cameraID.equals(this.cameraID)
//					&& item.caption.equals(this.caption))
//				return true;
//		}
//		return false;
//	}
//
//	/**
//	 * 重写hashcode 方法，返回的hashCode 不一样才认定为不同的对象
//	 */
//	@Override
//	public int hashCode() {
//		// return id.hashCode(); // 只比较id，id一样就不添加进集合
//		return cameraID.hashCode();
//	}
//}
