//package com.efounder.data.model;
//
//import java.io.Serializable;
//
//import android.graphics.Bitmap;
//
//public class HYZLModel implements Serializable {
//
//	private static final long serialVersionUID = 1L;
//
//	private String EXT_STR05;
//	private String F_FJBZBH;
//	private String EXT_STR03;
//	private String F_CRDATE;
//	private String F_PATH;
//	private String F_SIZE;
//	private String F_CHDATE;
//	private String F_CCLX;
//	private String F_NAME;
//	private String EXT_STR02;
//	private String F_ORDE;
//	private String _Self_RowSet;
//	private String EXT_STR04;
//	private String F_USER;
//	private String F_WJLX;
//	private String F_GUID;
//	private String EXT_STR01;
//	private String F_SYZT;
//
//	private Bitmap headImg;
//	private String tableName;
//
//	public Bitmap getHeadImg() {
//		return headImg;
//	}
//	public void setHeadImg(Bitmap headImg) {
//		this.headImg = headImg;
//	}
//	public String getF_CRDATE() {
//		return F_CRDATE;
//	}
//	public void setF_CRDATE(String f_CRDATE) {
//		F_CRDATE = f_CRDATE;
//	}
//	public String getF_PATH() {
//		return F_PATH;
//	}
//	public void setF_PATH(String f_PATH) {
//		F_PATH = f_PATH;
//	}
//	public String getF_SIZE() {
//		return F_SIZE;
//	}
//	public void setF_SIZE(String f_SIZE) {
//		F_SIZE = f_SIZE;
//	}
//	public String getF_CHDATE() {
//		return F_CHDATE;
//	}
//	public void setF_CHDATE(String f_CHDATE) {
//		F_CHDATE = f_CHDATE;
//	}
//	public String getF_NAME() {
//		return F_NAME;
//	}
//	public void setF_NAME(String f_NAME) {
//		F_NAME = f_NAME;
//	}
//	public String getF_USER() {
//		return F_USER;
//	}
//	public void setF_USER(String f_USER) {
//		F_USER = f_USER;
//	}
//	public String getF_GUID() {
//		return F_GUID;
//	}
//	public void setF_GUID(String f_GUID) {
//		F_GUID = f_GUID;
//	}
//	public String getF_SYZT() {
//		return F_SYZT;
//	}
//	public void setF_SYZT(String f_SYZT) {
//		F_SYZT = f_SYZT;
//	}
//
//	public String getF_ORDE() {
//		return F_ORDE;
//	}
//	public void setF_ORDE(String f_ORDE) {
//		F_ORDE = f_ORDE;
//	}
//	public String getF_WJLX() {
//		return F_WJLX;
//	}
//	public void setF_WJLX(String f_WJLX) {
//		F_WJLX = f_WJLX;
//	}
//
//	public String getTableName() {
//		return tableName;
//	}
//	public void setTableName(String tableName) {
//		this.tableName = tableName;
//	}
//	@Override
//	public String toString() {
//		return "HYZLModel [EXT_STR05=" + EXT_STR05 + ", F_FJBZBH=" + F_FJBZBH
//				+ ", EXT_STR03=" + EXT_STR03 + ", F_CRDATE=" + F_CRDATE
//				+ ", F_PATH=" + F_PATH + ", F_SIZE=" + F_SIZE + ", F_CHDATE="
//				+ F_CHDATE + ", F_CCLX=" + F_CCLX + ", F_NAME=" + F_NAME
//				+ ", EXT_STR02=" + EXT_STR02 + ", F_ORDE=" + F_ORDE
//				+ ", _Self_RowSet=" + _Self_RowSet + ", EXT_STR04=" + EXT_STR04
//				+ ", F_USER=" + F_USER + ", F_WJLX=" + F_WJLX + ", F_GUID="
//				+ F_GUID + ", EXT_STR01=" + EXT_STR01 + ", F_SYZT=" + F_SYZT
//				+ ", tableName=" + tableName + "]";
//	}
//
//
//}
