package com.efounder.skin;

import android.content.Context;

import com.efounder.chat.model.Constant;

import java.io.File;

import skin.support.load.SkinSDCardLoader;

public class CustomSDCardLoader extends SkinSDCardLoader {
    public static final int SKIN_LOADER_STRATEGY_SDCARD = Integer.MAX_VALUE;

    @Override
    protected String getSkinPath(Context context, String skinName) {

        String skinFolderPath = Constant.SKIN_DIR;
        return new File(skinFolderPath, skinName).getAbsolutePath();
    }

    @Override
    public int getType() {
        return SKIN_LOADER_STRATEGY_SDCARD;
    }
}