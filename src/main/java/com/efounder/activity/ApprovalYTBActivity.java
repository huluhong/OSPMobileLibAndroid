package com.efounder.activity;

/**
 * @author ruanjianjiagou@163.com
 * @date 2015.1.24
 */

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.ListView;
import android.widget.Toast;

import com.efounder.adapter.TimelineAdapter;
import com.efounder.builder.base.data.EFDataSet;
import com.efounder.builder.base.data.EFRowSet;
import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;
import com.efounder.ospmobilelib.R;
import com.efounder.util.AndroidEnvironmentVariable;
import com.efounder.util.CommonPo;
import com.efounder.util.LoadingDataUtilBlack;
import com.efounder.util.ToastUtil;
import com.efounder.view.titlebar.AbTitleBar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0
 *          {@link}
 *          </p>
 */
public class ApprovalYTBActivity extends AbActivity {

    private ListView listView;
    List<String> data;
    private TimelineAdapter timelineAdapter;
    AbTitleBar mTitleBar;

    private BackgroundAsyncTask backgroundAsyncTask;
    /*
     * 连接服务器，并获得返回结果 po，ro
     */
    private JParamObject PO;
    private JResponseObject RO;
    List<Map<String, Object>> list;

    private EFRowSet flowRowSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setAbContentView(R.layout.approval_listview);
        mTitleBar = getTitleBar();
        mTitleBar.setTitleText("审批流程");

        mTitleBar.setLogo(R.drawable.ef_title_view_back);
        mTitleBar.setTitleTextColor(R.color.title_TextColor);
        mTitleBar.setTitleBarBackground(R.color.title_Background);

        mTitleBar.setTitleBarGravity(Gravity.CENTER, Gravity.CENTER);
        listView = (ListView) this.findViewById(R.id.listview);
        listView.setDividerHeight(0);

        Bundle flowRowSetBundle = getIntent().getBundleExtra("data");
        if (flowRowSetBundle != null) {
            flowRowSet = (EFRowSet) flowRowSetBundle.get("data");
        }

        backgroundAsyncTask = new BackgroundAsyncTask();
        backgroundAsyncTask.execute();

    }


    //	----------------------------------
    /*
     * 内部asyncTask类
	 */
    class BackgroundAsyncTask extends AsyncTask<Void, Integer, JResponseObject> {

        // 进度条显示
        protected void onPreExecute() {
            // 进度条显示
            super.onPreExecute();
            // 加载dialog显示
            LoadingDataUtilBlack.show(ApprovalYTBActivity.this);
        }

        @Override
        protected JResponseObject doInBackground(Void... params) {
            PO = CommonPo.getPo(ApprovalYTBActivity.this);
            Map<String, Object> map = flowRowSet.getDataMap();
//            EAI.Protocol = "http";
//            EAI.Server = "192.168.191.1";
//            // EAI.Server = "192.168.253.6";
//            EAI.Port = "8080";
//            EAI.Path = "fmis";
//            EAI.Service = "Android";

            PO.SetValueByParamName("FLOW_ID", (String) map.get("FLOW_ID"));
            PO.SetValueByParamName("MDL_ID", (String) map.get("MDL_ID"));
            PO.SetValueByParamName("OBJ_GUID", (String) map.get("OBJ_GUID"));
            PO.SetValueByParamName("NODE_ID", (String) map.get("NODE_TAG"));
            PO.SetValueByParamName("BIZ_MDL", (String) map.get("MDL_ID"));
            PO.SetValueByParamName("OP_ID", (String) map.get("OP_ID"));
            PO.SetValueByParamName("BIZ_LOGIN_UNIT", (String) map.get("BIZ_UNIT"));
            PO.SetValueByParamName("TASK_UNIT", (String) map.get("TASK_UNIT"));
            PO.SetValueByParamName("BIZ_UNIT", (String) map.get("BIZ_UNIT"));
            PO.SetValueByParamName("BIZ_DJBH", (String) map.get("BIZ_DJBH"));
            PO.SetValueByParamName("NODE_TAG_NAME", (String) map.get("NODE_TAG_NAME"));
            PO.SetValueByParamName("OP_USER_NAME", (String) map.get("OP_USER_NAME"));
            PO.SetValueByParamName("BIZ_DATE", (String) map.get("BIZ_DATE"));
            PO.SetValueByParamName("UserCaption", (String) map.get("OP_SUBMIT_NAME"));
            PO.SetValueByParamName("OP_LEVEL", "00");
            PO.SetValueByParamName("OP_PROC_NOTE", "同意_虚拟提交");

            String userName = (String) PO.getEnvValue("UserName", "");
            userName = userName.replace(".zyyt", "");

            //PO.SetValueByEnvName("UserName", "szefeng");
            //PO.SetValueByEnvName("UserCaption", "孙泽峰");
            PO.SetValueByEnvName("UserName", userName);
            PO.SetValueByEnvName("UserCaption", AndroidEnvironmentVariable.getUserName());

            LoadingDataUtilBlack.getDialog().setOnCancelListener(new OnCancelListener() {
                @Override
                public void onCancel(DialogInterface arg0) {
                    // TODO Auto-generated method stub
                    backgroundAsyncTask.cancel(true);
                }
            });
            try {
                PO.SetValueByParamName("serviceName", "FlowTaskService");
                PO.SetValueByParamName("serviceMethod", "autoSubmitTaskMachine");
                RO = EAI.DAL.SVR("YTBService", PO);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

            CommonPo.setPoToNull();
            if (RO == null) {
                return RO;
            }
            JResponseObject responseObject = (JResponseObject) RO.getResponseObject();
            return responseObject;
        }

        // 当后台操作结束时，此方法将会被调用，计算结果将做为参数传递到此方法中，直接将结果显示到UI组件上
        protected void onPostExecute(JResponseObject result) {

            // RO返回值为空
            if (result == null) {
                Toast toast = Toast.makeText(ApprovalYTBActivity.this,
                        "连接服务器异常", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                LoadingDataUtilBlack.dismiss();
                toast.show();
                return;
            }
            if (result.ErrorCode == -1) {
                Builder alert = new Builder(ApprovalYTBActivity.this);
                alert.setTitle("确认");
                alert.setMessage(result.ErrorString);
                alert.setPositiveButton("是", null);
                LoadingDataUtilBlack.dismiss();
                alert.show();
                return;
            }
            LoadingDataUtilBlack.dismiss();
            list = new ArrayList<Map<String, Object>>();

            Map<String, Object> map = new HashMap<String, Object>();

            list.add(map);

		/*	EFDataSet taskDataSet = (EFDataSet) result.ResponseObject;
            if (taskDataSet == null || taskDataSet.getRowCount() == 0)
			return ;*/
            Map<String, EFDataSet> map1 = (Map<String, EFDataSet>) result.ResponseObject;
            if (map1 == null) {
                ToastUtil.showToast(ApprovalYTBActivity.this, "获取流程数据失败");
                return;
            }
            EFDataSet efdata = map1.get("nodeTaskDataSet");
            Map<String, EFDataSet> map2 = efdata.getDataSetMap();
            EFDataSet efdata1 = map2.get("FLOW_TASK_LIST");
            for (int i = 0; i < efdata1.getRowCount(); i++) {
                EFRowSet ef = efdata1.getRowSet(i);
                map = new HashMap<String, Object>();
                map.put("FLOW_NAME", ef.getObject("FLOW_NAME", ""));//审批任务的名称
                map.put("approver", ef.getObject("TASK_TO_UNIT_NAME", ""));//单位名称
                //map.put("preparer", ef.getObject("OP_USER_NAME", ""));
                map.put("preparer", ef.getObject("OP_PROC_NAME", ""));//审批人名
                map.put("make_date", ef.getObject("EXT_DAT09", ""));//审批日起
                //map.put("state", "2");
                //waiting pending processed(已办 前两个待办)
                String state = ef.getString("RESR_STATUS", "");
                map.put("state", state.equals("processed") ? "2" : "0");
                map.put("note", ef.getObject("OP_PROC_NOTE", ""));//审批意见
                map.put("NODE_TAG_NAME", ef.getObject("NODE_TAG_NAME", ""));//节点名称

                //进行判断
                if (ef.getObject("OP_PROC_NAME", "").equals("")) {
//                    String preparer = ef.getString("TASK_TO_USER_NAME", "");
                    String preparer = "待审批";
                    if ("".equals(preparer)) {
                        preparer = ef.getString("TASK_TO_USER", "");
                    }
                    map.put("preparer", preparer);
                }


                list.add(map);
            }

            map = new HashMap<String, Object>();

            list.add(map);

            timelineAdapter = new TimelineAdapter(ApprovalYTBActivity.this, list);
            listView.setAdapter(timelineAdapter);

        }

        // 进度条更新、
        protected void onProgressUpdate(Integer... values) {

            super.onProgressUpdate(values);
            //progressDialog.setProgress(values[0]);
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }

        @Override
        protected void onCancelled(JResponseObject result) {
            // TODO Auto-generated method stub
            super.onCancelled(result);
        }
    }
}
