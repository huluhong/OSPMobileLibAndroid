//package com.efounder.activity;
//
//import android.app.Dialog;
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Color;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.Button;
//import android.widget.ListView;
//import android.widget.TextView;
//
//import com.efounder.ospmobilelib.R;
//import com.efounder.builder.base.data.EFDataSet;
//import com.efounder.builder.base.data.EFRowSet;
//import com.efounder.builder.base.data.ESPRowSet;
//import com.efounder.eai.EAI;
//import com.efounder.eai.data.JParamObject;
//import com.efounder.eai.data.JResponseObject;
//import com.efounder.form.EFFormDataModel;
//import com.efounder.forwechat.BaseApp;
//import com.efounder.util.AndroidEnvironmentVariable;
//import com.efounder.util.LoadingDataUtilBlack;
//import com.efounder.util.MyYTBDialog;
//import com.efounder.view.titlebar.AbTitleBar;
//import com.pansoft.espflow.util.FlowTaskUtil;
//import com.pansoft.espmodel.FormModel;
//import com.pansoft.xmlparse.FormatSet;
//import com.pansoft.xmlparse.FormatTable;
//import com.pansoft.xmlparse.MobileFormatUtil;
//import com.test.DataTest;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
///**
// * 待办详情  油田部
// * 2015-3-17 15:01:58
// *
// * @author long
// */
//public class AgentDetailYTBAct extends AbActivity implements OnClickListener {
//    public static final String TAG = "AgentDetailAct";
//
//    /**
//     * 提交审核
//     */
//    private static final int DIALOG_SHEN_HE = 0x0000;
//    /**
//     * 取回动作
//     */
//    private static final int DIALOG_TAKE_BACK = 0x0001;
//    /**
//     * 退回动作
//     */
//    private static final int DIALOG_ROLL_BACK = 0x0002;
//
//    public static String TYPE_TITLE = "type_title";
//    public static String TYPE_CONTENT = "type_content";
//
//    private AbTitleBar mTitleBar = null;
//
//    /**
//     * 审批流程 button
//     */
//    private Button btnProcess;
//    /**
//     * 审核 button
//     */
//    private Button btnShenhe;
//    /**
//     * 取回 button
//     */
//    private Button btnTakeBack;
//    /**
//     * 退回 button
//     */
//    private Button btnRollBack;
//
//    private ArrayList<String> mArrayList = new ArrayList<String>();
//
//    private AgentDetailAdapter adapter;
//
//    ListView listView;
//    EFRowSet flowRowSet;
//
//    ESPRowSet rs;
//    FormatSet formatSet;
//    FormatTable ft;
//    private BackgroundAsyncTask backgroundAsyncTask;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        BaseApp.actManager.putActivity(TAG, this);
//        setAbContentView(R.layout.agent_detail);
//        Bundle bundle = getIntent().getBundleExtra("dataSource");
//        if (bundle != null)
//            flowRowSet = (EFRowSet) bundle.getSerializable("data");
//        initView();
//        initData();
//        initListener();
//    }
//
//    ArrayList<DataTest> listTest;
//
//    FormModel formModel;
//    EFDataSet efdata;
//
//    private void initData() {
//        listTest = new ArrayList<DataTest>();
//        backgroundAsyncTask = new BackgroundAsyncTask();
//        backgroundAsyncTask.execute();
//    }
//
//    private void initView() {
//        mTitleBar = this.getTitleBar();
//        mTitleBar.setTitleText("单据信息");
//        mTitleBar.setTitleTextColor(Color.WHITE);
//
//        mTitleBar.setLogo(R.drawable.ef_title_view_back);
//        mTitleBar.setTitleBarGravity(Gravity.CENTER, Gravity.CENTER);
//        //mTitleBar.setTitleBarBackground(R.color.title_bar_bg);
//        //  mTitleBar.setTitleBarBackground(R.color.red);
//        mTitleBar.setTitleBarBackground(R.color.red_ios);
//
//        btnProcess = (Button) findViewById(R.id.btn_approval_process);
//        btnShenhe = (Button) findViewById(R.id.btn_approve);
//        btnTakeBack = (Button) findViewById(R.id.btn_takeback);
//        btnRollBack = (Button) findViewById(R.id.btn_rollback);
//        btnRollBack.setOnClickListener(this);
//        btnTakeBack.setOnClickListener(this);
//        btnShenhe.setOnClickListener(this);
//        btnProcess.setOnClickListener(this);
//
//        listView = (ListView) findViewById(R.id.agency_details);
//
//    }
//
//    /**
//     * 设置监听事件
//     */
//    private void initListener() {
//        //绑定返回按键
//        mTitleBar.setLogoOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                BaseApp.actManager.removeActivity(TAG);
//            }
//        });
//
//    }
//
//    @Override
//    public void onClick(View v) {
//        // TODO
//        if (v == btnProcess) {
//            Intent intent = new Intent(AgentDetailYTBAct.this, ApprovalYTBActivity.class);
//            Bundle bundle = new Bundle();
//            bundle.putSerializable("data", flowRowSet);
//            intent.putExtra("data", bundle);
//            startActivity(intent);
//        } else if (v == btnShenhe) {
//            createDialog(DIALOG_SHEN_HE);
//        } else if (v == btnTakeBack) {
//            createDialog(DIALOG_TAKE_BACK);
//        } else if (v == btnRollBack) {
//            createDialog(DIALOG_ROLL_BACK);
//        }
//    }
//
//
//    EFFormDataModel ef;
//
//    class BackgroundAsyncTask extends AsyncTask<Void, Integer, EFDataSet> {
//        EFDataSet infoDataSet = null;
//
//        // 进度条显示
//        protected void onPreExecute() {
//            // 进度条显示
//            super.onPreExecute();
//            // 加载dialog显示
//            LoadingDataUtilBlack.show(AgentDetailYTBAct.this);
//        }
//
//        @Override
//        protected EFDataSet doInBackground(Void... params) {
//            try {
////                EAI.Protocol = "http";
////                EAI.Server = "192.168.191.1";
////               // EAI.Server = "192.168.253.6";
////                EAI.Port = "8080";
////                EAI.Path = "fmis";
////                EAI.Service = "Android";
//                EFDataSet flowTaskDataSet = null;
//                JParamObject PO = JParamObject.Create();
//                JResponseObject RO = null;
//                PO.SetValueByParamName("OBJ_GUID", flowRowSet.getString("OBJ_GUID", ""));
//                PO.SetValueByParamName("OP_ID", flowRowSet.getString("OP_ID", ""));
//                PO.SetValueByParamName("OP_SUBMIT_USER", flowRowSet.getString("OP_SUBMIT_USER", ""));
//                // PO.SetValueByParamName("OP_SUBMIT_USER", flowRowSet.getString("OP_SUBMIT_USER", ""));
////                PO.SetValueByEnvName("UserName", "ldingwei");
////                PO.SetValueByEnvName("UserCaption", "李定伟");
//                String userName = (String) PO.getEnvValue("UserName", "");
//                userName = userName.replace(".zyyt", "");
//                System.out.printf(userName);
//                // PO.SetValueByEnvName("UserName", "szefeng");
//                // PO.SetValueByEnvName("UserCaption", "孙泽峰");
//                PO.SetValueByEnvName("UserName", userName);
//                PO.SetValueByEnvName("UserCaption", AndroidEnvironmentVariable.getUserName());
//                PO.SetValueByParamName("serviceName", "JFxgkCommonServerManager");
//                PO.SetValueByParamName("serviceMethod", "getTaskListForMobile");
//                RO = EAI.DAL.SVR("YTBService", PO);
//                if (RO != null) {
//                    JResponseObject object = (JResponseObject) RO.getResponseObject();
//                    if (object == null) {
//                        return null;
//                    }
//                    EFDataSet taskDataSet = (EFDataSet) object.getResponseObject();
//                    if (taskDataSet == null || taskDataSet.getRowCount() == 0) {
//                        return null;
//                    }
//                    infoDataSet = taskDataSet;
//
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            // formModel.setDataSet(infoDataSet);
//            return infoDataSet;
//        }
//
//        // 当后台操作结束时，此方法将会被调用，计算结果将做为参数传递到此方法中，直接将结果显示到UI组件上
//        protected void onPostExecute(EFDataSet result) {
//            LoadingDataUtilBlack.dismiss();
//            if (result != null && result.getRowCount() > 0) {
//                ESPRowSet rowSet = infoDataSet.getRowSet();
//                //  rowSet.putString(""); flowRowSet.getString("OBJ_GUID", "")
//                formatSet = MobileFormatUtil.getInstance().getFormatSet();
//                ft = formatSet.getFormatTableById("YTBTASK");
//                List<Map<String, String>> list = FlowTaskUtil
//                        .openDetaiFromRowsetToMap(rowSet, ft);
//                for (int i = 0; i < list.size(); i++) {
//                    DataTest dataTest = new DataTest();
//                    dataTest.setType(TYPE_TITLE);
//                    Map<String, String> map = list.get(i);
//                    String title = null;
//                    for (String key : map.keySet()) {
//                        if (key != null && !key.equals("")) {
//                            title = key + "  " + map.get(key);
//                        } else {
//                            title = "  " + map.get(key);
//                        }
//                    }
//
//                    dataTest.setTitle(title);
//                    listTest.add(dataTest);
//                }
//                adapter = new AgentDetailAdapter(
//                        AgentDetailYTBAct.this, listTest);
//                listView.setAdapter(adapter);
//            }
//        }
//
//
//    }
//
//    class AgentDetailAdapter extends BaseAdapter {
//        private ArrayList<DataTest> mList;
//        private Context mContext;
//        private LayoutInflater mInflater;
//
//        public AgentDetailAdapter(Context context, ArrayList<DataTest> list) {
//            this.mContext = context;
//            this.mList = list;
//            mInflater = LayoutInflater.from(context);
//        }
//
//        @Override
//        public int getCount() {
//            return mList.size();
//        }
//
//        @Override
//        public Object getItem(int position) {
//            return mList.get(position);
//        }
//
//        @Override
//        public long getItemId(int position) {
//            return 0;
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            ViewHolder holder = null;
//            if (convertView == null || (holder = (ViewHolder) convertView.getTag()).flag != position) {
//                holder = new ViewHolder();
//                holder.flag = position;
//                if (TYPE_TITLE.equals(mList.get(position).getType())) {
//                    convertView = mInflater.inflate(R.layout.agency_detail_item_title, null);
//                    holder.title = (TextView) convertView.findViewById(R.id.title);
//                    convertView.setTag(holder);
//                } else if (TYPE_CONTENT.equals(mList.get(position).getType())) {
//                    convertView = mInflater.inflate(R.layout.agency_detail_item_content, null);
//                    holder.title = (TextView) convertView.findViewById(R.id.title);
//                    holder.content = (TextView) convertView.findViewById(R.id.content);
//                    convertView.setTag(holder);
//                }
//            } else {
//                holder = (ViewHolder) convertView.getTag();
//            }
//            holder.title.setText(mList.get(position).getTitle());
//            if (TYPE_CONTENT.equals(mList.get(position).getType()) && holder.content != null) {
//                holder.content.setText(mList.get(position).getContent());
//            }
//            return convertView;
//        }
//
//        class ViewHolder {
//            int flag = -1;
//            TextView title;
//            TextView content;
//        }
//    }
//
//
//    protected Dialog createDialog(int id) {
//        Dialog dialog = null;
//        switch (id) {
//            case DIALOG_SHEN_HE:
//                dialog = new MyYTBDialog(AgentDetailYTBAct.this, R.style.shenhedialog, DIALOG_SHEN_HE, flowRowSet);
//                // 设置它的ContentView
//                //dialog.setContentView(R.layout.shen_he_dialog);
//                //dialog.setContentView(R.layout.new_shen_he_dialog);
//                dialog.show();
//                break;
//            case DIALOG_TAKE_BACK:
//                dialog = new MyYTBDialog(AgentDetailYTBAct.this, R.style.shenhedialog, DIALOG_TAKE_BACK, flowRowSet);
//                // 设置它的ContentView
//                //dialog.setContentView(R.layout.shen_he_dialog);
//                //dialog.setContentView(R.layout.new_shen_he_dialog);
//                dialog.show();
//                break;
//            case DIALOG_ROLL_BACK:
//                dialog = new MyYTBDialog(AgentDetailYTBAct.this, R.style.shenhedialog, DIALOG_ROLL_BACK, flowRowSet);
//                // 设置它的ContentView
//                //dialog.setContentView(R.layout.shen_he_dialog);
//                //dialog.setContentView(R.layout.new_shen_he_dialog);
//                dialog.show();
//                break;
//
//        }
//
//        return dialog;
//    }
//
//}
