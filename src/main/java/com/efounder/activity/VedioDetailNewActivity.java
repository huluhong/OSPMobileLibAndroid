//package com.efounder.activity;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.HashSet;
//import java.util.Hashtable;
//import java.util.List;
//import java.util.Random;
//import java.util.Set;
//
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.content.Intent;
//import android.content.res.Configuration;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Color;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.os.PowerManager;
//import android.os.PowerManager.WakeLock;
//import android.util.Log;
//import android.view.MotionEvent;
//import android.view.SurfaceHolder;
//import android.view.SurfaceHolder.Callback;
//import android.view.SurfaceView;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.View.OnTouchListener;
//import android.view.WindowManager;
//import android.widget.AdapterView;
//import android.widget.AdapterView.OnItemClickListener;
//import android.widget.Button;
//import android.widget.ImageButton;
//import android.widget.LinearLayout;
//import android.widget.ProgressBar;
//import android.widget.RadioGroup;
//import android.widget.RadioGroup.OnCheckedChangeListener;
//import android.widget.RelativeLayout;
//import android.widget.ScrollView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.core.xml.StubObject;
//import com.efounder.ospmobilelib.R;
//import com.efounder.adapter.HorizontalListViewAdapterN;
//import com.efounder.constant.Constant;
//import com.efounder.constant.ConstantLive;
//import com.efounder.data.model.ItemData;
//import com.efounder.dm.vedio.TempData;
//import com.efounder.dm.vedio.utils.UtilAudioPlay;
//import com.efounder.dm.vedio.utils.UtilFilePath;
//import com.efounder.http.utils.GsonUtil;
//import com.efounder.image.utils.ImageUtils;
//import com.efounder.util.StorageUtil;
//import com.efounder.util.ToastUtil;
//import com.efounder.widget.CustomGV;
//import com.google.gson.reflect.TypeToken;
//import com.hikvision.vmsnetsdk.CameraInfo;
//import com.hikvision.vmsnetsdk.DeviceInfo;
//import com.hikvision.vmsnetsdk.RealPlayURL;
//import com.hikvision.vmsnetsdk.VMSNetSDK;
//import com.live.LiveCallBack;
//import com.live.LiveControl;
//import com.pansoft.resmanager.ResFileManager;
//
//import static com.efounder.ospmobilelib.R.id.cloud_vedio;
//import static com.efounder.ospmobilelib.R.id.cloud_voice;
//import static com.efounder.ospmobilelib.R.id.leftbacklayout;
//
///**
// * 为了支持横竖屏切换，视频播放改为支持全屏 2016.11.18
// *
// * @author cherise
// */
//public class VedioDetailNewActivity extends Activity implements
//        OnClickListener, OnCheckedChangeListener, OnTouchListener, Callback,
//        LiveCallBack {
//    private static final String TAG = "VedioDetailNewActivity";
//
//    private static final String CAPTION = "caption";
//    private static final String MENUICON = "menuIcon";
//    private static final String SERVERIP = "serverIP";
//    private static final String CAMERAID = "cameraID";
//    private static final String DEVICEID = "deviceID";
//    private static final String USERNAME = "userName";
//    private static final String PASSWORD = "password";
//
//    /**
//     * 码流类型
//     */
//    private int mStreamType = -1;
//    /**
//     * 通过VMSNetSDK返回的预览地址对象
//     */
//    private RealPlayURL mRealPlayURL;
//    /**
//     * 登录设备的用户名
//     */
//    private String mName = "admin";
//    /**
//     * 登录设备的密码
//     */
//    private String mPassword = "12345";
//    /**
//     * 控制层对象
//     */
//    private LiveControl mLiveControl;
//    /**
//     * 播放视频的控件对象
//     */
//    private SurfaceView mSurfaceView;
//    /**
//     * 创建取流等待bar
//     */
//    private ProgressBar mProgressBar;
//    /**
//     * 创建消息对象
//     */
//    private Handler mMessageHandler = new MyHandler();
//    /**
//     * 音频是否开启
//     */
//    private boolean mIsAudioOpen;
//    /**
//     * 是否正在录像
//     */
//    private boolean mIsRecord;
//    /**
//     * 播放流量
//     */
//    private long mStreamRate = 0;
//    /**
//     * 监控点信息对象
//     */
//    // private CameraInfo cameraInfo;
//    private ItemData itemData;
//    /**
//     * 云台控制对话框
//     */
//    private AlertDialog mDialog;
//
//    private VMSNetSDK mVmsNetSDK = null;
//
//    // private HorizontalListView hListView;
//    private HorizontalListViewAdapterN hlvadapter;
//    /**
//     * 列表数据
//     */
//    List<ItemData> listData;
//    /**
//     * 初次加载数据列表
//     */
//    static List<ItemData> firstLoadListData = new ArrayList<ItemData>();
//    /**
//     * 收藏的列表
//     */
//    Set<ItemData> collectListData = new HashSet<ItemData>();
//    /**
//     * 最近使用 数据
//     */
//    Set<ItemData> recentlyListData = new HashSet<ItemData>();
//    private static final String RECENT_LIST_DATA = "recent_list_data";
//    public static final String COLLECT_LIST_DATA = "collect_list_data1";
//    public static final String LIST_DATA = "listdata";
//
//    /**
//     * 初次进入该界面播放视频在list中的位置
//     */
//    private int pos = 0;
//
//    private ImageButton cloudL, cloudR, cloudT, cloudB;
//    private TextView cloudNear, cloudRemote;
//    private TextView cloudVideo, cloudVoice, cloudCamera;
//    private ScrollView mScrollView;
//    private Button organizeButton;// 组织机构
//
//    private Button backBtn, rightBtn;
//    private TextView titleTv;
//    private RelativeLayout controlContener;
//    RelativeLayout include;// 标题栏
//    LinearLayout bottomLayout;// 底部区域
//    // PowerManager powerManager = null;
//    // WakeLock wakeLock = null;
//
//    private RadioGroup rg;
//    private CustomGV gv;
//    StorageUtil storageUtil;
//    String userName;
//
//    // --------------------------
//    private String mDeviceID = "";
//
//    /**
//     * 二级菜单
//     */
//    private ArrayList<Object> mSecondLevelMenus; // 二级菜单
//    private StubObject MmainMenuItem;
//
//    private String currentcameraID;
//    String titleStr;
//
//    int numPerPage = 10000;
//    int curPage = 1;
//
//    List<CameraInfo> cameraInfoList;
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
//                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);// 保持屏幕不变黑
//        setContentView(R.layout.activity_live_n);
//        // powerManager = (PowerManager)
//        // this.getSystemService(this.POWER_SERVICE);
//        // wakeLock = this.powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK,
//        // "My Lock");
//
//        include = (RelativeLayout) findViewById(R.id.include);
//
//        LinearLayout leftbacklayout = (LinearLayout) findViewById(R.id.leftbacklayout);
//        leftbacklayout.setVisibility(View.VISIBLE);
//        leftbacklayout.setOnClickListener(this);
//        TextView title = (TextView) findViewById(R.id.fragmenttitle);
//        title.setText("视频监控");
//        title.setTextColor(Color.WHITE);
//        // include.setVisibility(View.GONE);
//        include.setVisibility(View.VISIBLE);
//        MmainMenuItem = (StubObject) getIntent().getSerializableExtra("type");
//
//        cameraInfoList = new ArrayList<CameraInfo>();
//        storageUtil = new StorageUtil(this, "storage");
//        userName = storageUtil.getString("loginUserID");
//        String str = storageUtil.getString(userName + RECENT_LIST_DATA);
//        String collectStr = storageUtil.getString(userName + COLLECT_LIST_DATA);
//        if (str != null && !"".equals(str)) {
//            recentlyListData = GsonUtil.fromJson(str,
//                    new TypeToken<Set<ItemData>>() {
//                    });
//            Log.i(TAG, "---recentlyListData.size:" + recentlyListData.size());
//
//        }
//        if (collectStr != null && !"".equals(collectStr)) {
//            collectListData = GsonUtil.fromJson(collectStr,
//                    new TypeToken<Set<ItemData>>() {
//                    });
//        }
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                // 获取区域中摄像头信息
//                Hashtable menuTable = MmainMenuItem.getStubTable();
//                // 暂时存放某一个区域下的摄像头列表
//                List<CameraInfo> listTemp = new ArrayList<CameraInfo>();
//                String reginIDs = (String) menuTable.get("orgID");
//                String[] reginIDS = reginIDs.split(",");
//                if (reginIDS != null && reginIDS.length > 0) {
//                    for (String reginID : reginIDS) {
//                        listTemp.clear();
//                        int i = 1;
//                        while (null == TempData.getIns().getLoginData()
//                                && i <= 20) {
//                            try {
//                                Thread.sleep(100);
//                            } catch (InterruptedException e) {
//                                e.printStackTrace();
//                            }
//                            i++;
//                        }
//                        if (i > 20)
//                            continue;
//                        boolean ret = VMSNetSDK
//                                .getInstance()
//                                .getCameraListFromRegion(
//                                        Constant.SERVER_ADDRESS,
//                                        TempData.getIns().getLoginData().sessionID,
//                                        Integer.valueOf(reginID), numPerPage,
//                                        curPage, listTemp);
//                        if (ret && listTemp.size() > 0) {
//                            cameraInfoList.addAll(listTemp);
//                        }
//                    }
//                }
//                mHandler.sendEmptyMessage(0);
//            }
//        }).start();
//
//    }
//
//    private Handler mHandler = new Handler() {
//        public void handleMessage(Message msg) {
//
//            initUI();
//            initData();
//            if (null == TempData.getIns().getLoginData()) {
//                Toast.makeText(VedioDetailNewActivity.this,
//                        "视频初始化失败，请检查视频网络是否正常或者联系网络管理员", 200).show();
//            } else {
//                startBtnOnClick();
//
//                initListener();
//            }
//
//        }
//
//        ;
//    };
//
//    private void initListener() {
//        if (gv != null)
//            gv.setOnItemClickListener(new OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view,
//                                        int position, long id) {
//                    if (pos == position) {
//                        stopBtnOnClick();
//                        initData();
//                        // titleTv.setText(itemData.getCaption());
//                        startBtnOnClick();
//                        return;
//                    }
//                    TempData.getIns().setData(listData.get(position));
//                    pos = position;
//                    gv.setSelection(pos);
//                    stopBtnOnClick();
//                    initData();
//                    // titleTv.setText(itemData.getCaption());
//                    startBtnOnClick();
//                }
//            });
//    }
//
//    /**
//     * 初始化网络库和控制层对象
//     *
//     * @since V1.0
//     */
//    private void initData() {
//        if (listData == null || listData.size() <= 0) {
//            Log.i(TAG, "mVmsNetSDK is null");
//            return;
//        }
//        final ItemData data = listData.get(pos);
//        TempData.getIns().setData(data);
//        recentlyListData.add(data);
//        // if(!recentlyListData.contains(data)){
//        // recentlyListData.add(data);
//        // }
//        mStreamType = ConstantLive.SUB_STREAM;
//        mRealPlayURL = new RealPlayURL();
//        mLiveControl = new LiveControl();
//        mLiveControl.setLiveCallBack(this);
//        itemData = TempData.getIns().getData();
//        mDeviceID = itemData.getDeviceID();
//        mVmsNetSDK = VMSNetSDK.getInstance();
//        final DeviceInfo deviceInfo = new DeviceInfo();
//        if (mVmsNetSDK == null) {
//            Log.i(TAG, "mVmsNetSDK is null");
//            return;
//        }
//        mVmsNetSDK.openLog(false);
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                if (TempData.getIns().getLoginData() == null) {
//                    try {
//                        Thread.sleep(5000);
//                    } catch (InterruptedException e) {
//
//                        e.printStackTrace();
//                    }
//                }
//                boolean ret = mVmsNetSDK.getDeviceInfo(Constant.SERVER_ADDRESS,
//                        TempData.getIns().getLoginData().sessionID, mDeviceID,
//                        deviceInfo);
//                Log.i(TAG, "sessionID="
//                        + TempData.getIns().getLoginData().sessionID
//                        + ",mDeviceID=" + mDeviceID + ",ret=" + ret);
//                if (ret && deviceInfo != null) {
//                    mName = deviceInfo.userName;
//                    mPassword = deviceInfo.password;
//                } else {
//                    mName = "admin";
//                    mPassword = "12345";
//                }
//                Log.i(TAG, "mName is " + mName + "---" + mPassword + "-----"
//                        + mDeviceID);
//            }
//        }).start();
//    }
//
//    /**
//     * 初始化控件
//     *
//     * @since V1.0
//     */
//    private void initUI() {
//
//        controlContener = (RelativeLayout) findViewById(R.id.cotrol_contener);
//        controlContener.setVisibility(View.GONE);
//        bottomLayout = (LinearLayout) findViewById(R.id.bottom_linearlayout);
//        // 测试 正式时 删除该部分
//        // controlContener.setVisibility(View.VISIBLE);
//        mScrollView = (ScrollView) findViewById(R.id.cloud_areaContent);
//
//        cloudB = (ImageButton) findViewById(R.id.cloud_b);
//        cloudT = (ImageButton) findViewById(R.id.cloud_t);
//        cloudL = (ImageButton) findViewById(R.id.cloud_l);
//        cloudR = (ImageButton) findViewById(R.id.cloud_r);
//        cloudNear = (TextView) findViewById(R.id.cloud_near_focuse);
//        cloudRemote = (TextView) findViewById(R.id.cloud_remote_focuse);
//
//        cloudVideo = (TextView) findViewById(cloud_vedio);
//        cloudVoice = (TextView) findViewById(cloud_voice);
//        cloudCamera = (TextView) findViewById(R.id.cloud_cut);
//        cloudVideo.setOnClickListener(this);
//        cloudVoice.setOnClickListener(this);
//        cloudCamera.setOnClickListener(this);
//
//        cloudB.setOnTouchListener(this);
//        cloudT.setOnTouchListener(this);
//        cloudR.setOnTouchListener(this);
//        cloudL.setOnTouchListener(this);
//        cloudNear.setOnTouchListener(this);
//        cloudRemote.setOnTouchListener(this);
//
//        // 加载ListView后 将scrollView置顶
//        mScrollView.setFocusable(true);
//        mScrollView.setFocusableInTouchMode(true);
//        mScrollView.requestFocus();
//
//        mSurfaceView = (SurfaceView) findViewById(R.id.surfaceView);
//        mSurfaceView.getHolder().addCallback(this);
//
//        mProgressBar = (ProgressBar) findViewById(R.id.liveProgressBar);
//        mProgressBar.setVisibility(View.INVISIBLE);
//
//        organizeButton = (Button) findViewById(R.id.rb_organization);
//        organizeButton.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                if (collectListData != null) {
//                    storageUtil.putString(userName + COLLECT_LIST_DATA,
//                            GsonUtil.str2Json(collectListData));
//                    storageUtil.commit();
//                }
//                startActivityForResult(new Intent(VedioDetailNewActivity.this,
//                        VideoTreeAllActivity.class), 2);
//
//            }
//        });
//
//        listData = new ArrayList<ItemData>();
//
//        gv = (CustomGV) findViewById(R.id.gv_container);
//
//        rg = (RadioGroup) findViewById(R.id.rg_contener);
//        rg.setOnCheckedChangeListener(this);
//
//        if (cameraInfoList.size() > 0) {
//            int i = 0;
//            for (CameraInfo cameraInfo : cameraInfoList) {
//                ItemData itemData = new ItemData();
//                // 为了使图片不再图库显示 所以不加后缀名 .png
//                String menuIcon = ResFileManager.IMAGE_DIR + "/"
//                        + cameraInfo.cameraID;
//                i++;
//                File file = new File(menuIcon);
//                if (file.exists()) {
//                    // Bitmap bm = BitmapFactory.decodeFile(menuIcon);
//                    FileInputStream is;
//                    try {
//                        is = new FileInputStream(menuIcon);
//                        // 2.为位图设置100K的缓存
//                        BitmapFactory.Options opts = new BitmapFactory.Options();
//                        opts.inTempStorage = new byte[100 * 1024];
//                        // 3.设置位图颜色显示优化方式
//                        // ALPHA_8：每个像素占用1byte内存（8位）
//                        // ARGB_4444:每个像素占用2byte内存（16位）
//                        // ARGB_8888:每个像素占用4byte内存（32位）
//                        // RGB_565:每个像素占用2byte内存（16位）
//                        // Android默认的颜色模式为ARGB_8888，这个颜色模式色彩最细腻，显示质量最高。但同样的，占用的内存//也最大。也就意味着一个像素点占用4个字节的内存。我们来做一个简单的计算题：3200*2400*4
//                        // bytes //=30M。如此惊人的数字！哪怕生命周期超不过10s，Android也不会答应的。
//                        opts.inPreferredConfig = Bitmap.Config.RGB_565;
//                        // 4.设置图片可以被回收，创建Bitmap用于存储Pixel的内存空间在系统内存不足时可以被回收
//                        opts.inPurgeable = true;
//                        // 5.设置位图缩放比例
//                        // width，hight设为原来的四分一（该参数请使用2的整数倍）,这也减小了位图占用的内存大小；例如，一张//分辨率为2048*1536px的图像使用inSampleSize值为4的设置来解码，产生的Bitmap大小约为//512*384px。相较于完整图片占用12M的内存，这种方式只需0.75M内存(假设Bitmap配置为//ARGB_8888)。
//                        opts.inSampleSize = 4;
//                        // 6.设置解码位图的尺寸信息
//                        opts.inInputShareable = true;
//                        // 7.解码位图
//                        Bitmap btp = BitmapFactory.decodeStream(is, null, opts);
//                        itemData.setMenuIcon(btp);
//                    } catch (FileNotFoundException e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    InputStream is = this.getResources().openRawResource(
//                            R.drawable.vediodefault);
//                    BitmapFactory.Options options = new BitmapFactory.Options();
//                    options.inJustDecodeBounds = false;
//                    options.inSampleSize = 4; // width，hight设为原来的十分一
//                    Bitmap btp = BitmapFactory.decodeStream(is, null, options);
//                    // Bitmap bm =
//                    // BitmapFactory.decodeResource(VedioDetailNewActivity.this.getResources(),R.drawable.vediodefault);
//                    itemData.setMenuIcon(btp);
//                }
//                itemData.setCaption(cameraInfo.name);
//                itemData.setCameraID(cameraInfo.cameraID);
//                itemData.setDeviceID(cameraInfo.deviceID);
//                // 为了使图片不再图库显示 所以不加后缀名 .png
//                String menuIcon1 = ResFileManager.IMAGE_DIR + "/"
//                        + cameraInfo.cameraID;
//                File file1 = new File(menuIcon1);
//                if (file1.exists()) {
//                    itemData.setBitmapURL(menuIcon1);
//                } else {
//                    itemData.setBitmapURL("");
//                }
//                listData.add(itemData);
//                // hListView = (HorizontalListView)
//                // findViewById(R.id.horizontalListView);
//
//                // firstLoadListData.add(itemData);
//            }
//        }// 如果最近有数据，显示最近的数据。否则显示第一条数据
//        if (recentlyListData != null && recentlyListData.size() > 0) {
//            listData.clear();
//            for (ItemData item : recentlyListData) {
//                listData.add(item);
//            }
//        } else {
//            if (listData.size() > 0) {
//                listData = listData.subList(0, 1);
//            }
//
//        }
//        hlvadapter = new HorizontalListViewAdapterN(
//                VedioDetailNewActivity.this, listData);
//        hlvadapter.notifyDataSetChanged();
//        gv.setAdapter(hlvadapter);
//    }
//
//    /**
//     * 横竖屏切换调用此方法
//     */
//    @Override
//    public void onConfigurationChanged(Configuration config) {
//        super.onConfigurationChanged(config);
//        if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            include.setVisibility(View.GONE);
//            bottomLayout.setVisibility(View.GONE);
//            this.getWindow().setFlags(
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
//            // controlContener.setVisibility(View.GONE);
//
//        } else if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
//
//            include.setVisibility(View.VISIBLE);
//            bottomLayout.setVisibility(View.VISIBLE);
//            this.getWindow().clearFlags(
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
//
//            // controlContener.setVisibility(View.VISIBLE);
//
//        }
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == 2 && resultCode == 2) {
//            if (data.hasExtra("itemData")) {
//                ItemData itemData = (ItemData) data
//                        .getSerializableExtra("itemData");
//                if (listData != null && recentlyListData != null
//                        && recentlyListData.size() > 0) {
//                    listData.clear();
//                    for (ItemData data1 : recentlyListData) {
//                        listData.add(data1);
//                    }
//                    recentlyListData.add(itemData);
//                    listData.add(0, itemData);
//                    if (hlvadapter != null && listData != null) {
//                        hlvadapter.setListData(listData);
//                        hlvadapter.notifyDataSetChanged();
//                    }
//                }
//
//            }
//        }
//
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        // wakeLock.acquire();
//        if (mLiveControl != null && mSurfaceView != null && listData != null
//                && listData.size() > 0) {
//            TempData.getIns().setData(listData.get(0));
//            pos = 0;
//            gv.setSelection(pos);
//            stopBtnOnClick();
//            initData();
//            startBtnOnClick();
//        }
//
//        if (listData != null
//                ) {
//            String collectStr = storageUtil.getString(userName
//                    + COLLECT_LIST_DATA);
//            collectListData = GsonUtil.fromJson(collectStr,
//                    new TypeToken<Set<ItemData>>() {
//                    });
//            if (collectListData != null && collectListData.size() >= 0
//                    && rg.getCheckedRadioButtonId() == R.id.rb_my_collection) {
//                // && !isSaveingPNG) {
//                listData.clear();
//                for (ItemData itemData : collectListData) {
//                    listData.add(itemData);
//
//                }
//                if (hlvadapter != null && listData != null) {
//                    hlvadapter.setListData(listData);
//                    hlvadapter.notifyDataSetChanged();
//                }
//            }
//        }
//
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        // wakeLock.release();
//    }
//
//    @Override
//    public void onCheckedChanged(RadioGroup group, int checkedId) {
//
//        if (checkedId == R.id.rb_recent_used) {
//            if (listData != null && recentlyListData != null
//                // && recentlyListData.size() > 0 && !isSaveingPNG) {
//                // && recentlyListData.size() > 0) {
//                    ) {
//                listData.clear();
//                for (ItemData itemData : recentlyListData) {
//                    listData.add(itemData);
//                }
//                if (hlvadapter != null && listData != null) {
//                    hlvadapter.setListData(listData);
//                    hlvadapter.notifyDataSetChanged();
//                }
//            }
//        }
//        if (checkedId == R.id.rb_my_collection) {
//
//            String collectStr = storageUtil.getString(userName
//                    + COLLECT_LIST_DATA);
//            collectListData = GsonUtil.fromJson(collectStr,
//                    new TypeToken<Set<ItemData>>() {
//                    });
//
//            if (listData != null) {
//                listData.clear();
//                if (collectListData != null) {
//                    for (ItemData itemData : collectListData) {
//                        listData.add(itemData);
//                    }
//                }
//                if (hlvadapter != null && listData != null) {
//                    hlvadapter.setListData(listData);
//                    hlvadapter.notifyDataSetChanged();
//                }
//            }
//        }
//
//    }
//
//    @Override
//    public void onDestroy() {
//        // TODO Auto-generated method stub
//        super.onDestroy();
//        if (recentlyListData != null && recentlyListData.size() > 0) {
//            storageUtil.putString(userName + RECENT_LIST_DATA,
//                    GsonUtil.str2Json(recentlyListData));
//            storageUtil.putString(userName + COLLECT_LIST_DATA,
//                    GsonUtil.str2Json(collectListData));
//            storageUtil.commit();
//            Log.i(TAG, "---onDestroy");
//        }
//        firstLoadListData.clear();
//        stopBtnOnClick();
//
//    }
//
//    @Override
//    public void onClick(View v) {
//        int id = v.getId();
//        if (id == cloud_vedio) {
//            recordBtnOnClick();
//        } else if (id == cloud_voice) {
//            audioBtnOnClick();
//        } else if (id == R.id.cloud_cut) {
//            captureBtnOnClick();
//        } else if (id == leftbacklayout) {
//            VedioDetailNewActivity.this.finish();
//        }
//
////		switch (v.getId()) {
////		case cloud_vedio:
////			recordBtnOnClick();
////			break;
////		case cloud_voice:
////			audioBtnOnClick();
////			break;
////		case R.id.cloud_cut:
////			captureBtnOnClick();
////			break;
////
////		case leftbacklayout:
////			VedioDetailNewActivity.this.finish();
////			break;
////		default:
////			break;
////		}
//    }
//
//    /**
//     * 启动播放 void
//     *
//     * @since V1.0
//     */
//    String url = "";
//
//    private void startBtnOnClick() {
//        mProgressBar.setVisibility(View.VISIBLE);
//        new Thread() {
//            @Override
//            public void run() {
//                super.run();
//                Log.i(TAG, "---line---484---mStreamType=" + mStreamType
//                        + ",getPlayUrl(mStreamType)=" + getPlayUrl(mStreamType)
//                        + ",mName=" + mName + ",mPassword=" + mPassword);
//                url = getPlayUrl(mStreamType);
//                Log.i(TAG, "------line---486---url---" + url + ",mName="
//                        + mName + ",mPassword=" + mPassword);
//                if (mLiveControl == null)
//                    return;
//                mLiveControl.setLiveParams(url, mName, mPassword);
//                if (mLiveControl.LIVE_PLAY == mLiveControl.getLiveState()) {
//                    mLiveControl.stop();
//                }
//
//                if (mLiveControl.LIVE_INIT == mLiveControl.getLiveState()) {
//                    mLiveControl.startLive(mSurfaceView);
//                }
//            }
//        }.start();
//    }
//
//    /**
//     * 该方法是获取播放地址的，当mStreamType=2时，获取的是MAG，当mStreamType =1时获取的子码流，当mStreamType =
//     * 0时获取的是主码流 由于该方法中部分参数是监控点的属性，所以需要先获取监控点信息，具体获取监控点信息的方法见resourceActivity。
//     *
//     * @param streamType 2、表示MAG取流方式；1、表示子码流取流方式；0、表示主码流取流方式；
//     * @return String 播放地址 ：2、表示返回的是MAG的播放地址;1、表示返回的是子码流的播放地址；0、表示返回的是主码流的播放地址。
//     * @since V1.0
//     */
//    private String getPlayUrl(int streamType) {
//        String url = "";
//        // 登录平台地址
//        String mAddress = Constant.SERVER_ADDRESS;
//        // 登录返回的sessiond
//        String mSessionID = TempData.getIns().getLoginData().sessionID;
//        if (itemData == null) {
//            return url;
//        }
//        if (streamType == 2) {
//            // TODO 原有代码streamType传0
//            VMSNetSDK.getInstance().getRealPlayURL(mAddress, mSessionID,
//                    itemData.getCameraID(), streamType, mRealPlayURL);
//            if (null == mRealPlayURL) {
//                return "";
//            }
//            // MAG地址
//            url = mRealPlayURL.url2;
//        } else {
//            VMSNetSDK.getInstance().getRealPlayURL(mAddress, mSessionID,
//                    itemData.getCameraID(), streamType, mRealPlayURL);
//            if (null == mRealPlayURL) {
//                return "";
//            }
//            // mRealPlayURL.url1 是主码流还是子码流取决于 streamType，见上面注释
//            url = mRealPlayURL.url1;
//        }
//        DeviceInfo deviceInfo = new DeviceInfo();
//        currentcameraID = itemData.getCameraID();
//        Log.i(TAG, "cameraInfo.deviceID:" + itemData.getDeviceID()
//                + ",cameraInfo.cameraID:" + itemData.getCameraID());
//        boolean ret = VMSNetSDK.getInstance().getDeviceInfo(mAddress,
//                mSessionID, mDeviceID, deviceInfo);
//        if (ret && deviceInfo != null) {
//            mName = deviceInfo.userName;
//            mPassword = deviceInfo.password;
//        } else {
//            mName = "admin";
//            mPassword = "12345";
//        }
//
//        if (null == mName || "".equals(mName)) {
//            mName = "admin";
//        }
//
//        if (null == mPassword || "".equals(mPassword)) {
//            mPassword = "12345";
//        }
//        return url;
//    }
//
//    /**
//     * 停止播放 void
//     *
//     * @since V1.0
//     */
//    private void stopBtnOnClick() {
//        if (null != mLiveControl) {
//            mLiveControl.stop();
//        }
//    }
//
//    /**
//     * 抓拍 void
//     *
//     * @since V1.0
//     */
//    private void captureBtnOnClick() {
//        if (null != mLiveControl) {
//            // 随即生成一个1到10000的数字，用于抓拍图片名称的一部分，区分图片，开发者可以根据实际情况修改区分图片名称的方法
//            int recordIndex = new Random().nextInt(10000);
//            boolean ret = mLiveControl.capture(UtilFilePath.getPictureDirPath()
//                    .getAbsolutePath(), "Picture" + recordIndex + ".jpg");
//            // boolean ret =
//            // mLiveControl.capture(UtilFilePath.getSysCameraPath(this),
//            // "Picture"
//            // + recordIndex + ".jpg");
//            if (ret) {
//                Toast.makeText(VedioDetailNewActivity.this, "抓拍成功", 0).show();
//                UtilAudioPlay.playAudioFile(VedioDetailNewActivity.this,
//                        R.raw.paizhao);
//                String filePath = UtilFilePath.getPictureDirPath()
//                        .getAbsolutePath()
//                        + File.separator
//                        + "Picture"
//                        + recordIndex + ".jpg";
//                // String filePath =
//                // UtilFilePath.getSysCameraPath(this)+File.separator+"Picture"
//                // + recordIndex + ".jpg";
//                try {
//                    ImageUtils.saveImageToSD(VedioDetailNewActivity.this,
//                            filePath, ImageUtils.getSmallBitmap(filePath), 80);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//            } else {
//                Toast.makeText(VedioDetailNewActivity.this, "抓拍失败", 0).show();
//                Log.e(TAG, "---line---678---captureBtnOnClick():: 抓拍失败");
//            }
//        }
//    }
//
//    /**
//     * change 缩略图
//     *
//     * @since V1.0
//     */
//    private boolean isSaveingPNG;
//
//    private synchronized void saveChangePNG() {
//        if (null != mLiveControl) {
//            isSaveingPNG = true;
//            boolean ret = mLiveControl.capture(ResFileManager.IMAGE_DIR,
//                    currentcameraID);
//            if (listData == null || listData.size() <= pos) {
//                isSaveingPNG = false;
//                return;
//            }
//            ItemData itemDataBefor = listData.get(pos);
//            if (ret) {
//                ItemData itemData = new ItemData();
//                // 为了使图片不再图库显示 所以不加后缀名 .png
//                String menuIcon = ResFileManager.IMAGE_DIR + "/"
//                        + currentcameraID;
//                File file = new File(menuIcon);
//                Bitmap bm;
//                if (file.exists()) {
//                    bm = BitmapFactory.decodeFile(menuIcon);
//                    itemData.setMenuIcon(bm);
//                    itemDataBefor.setBitmapURL(menuIcon);
//                } else {
//                    bm = BitmapFactory.decodeResource(
//                            VedioDetailNewActivity.this.getResources(),
//                            R.drawable.vediodefault);
//                    itemData.setMenuIcon(bm);
//                    itemDataBefor.setBitmapURL("");
//                }
//
//                if (hlvadapter != null && listData != null) {
//                    hlvadapter.setListData(listData);
//                    hlvadapter.notifyDataSetChanged();
//                }
//            }
//            isSaveingPNG = false;
//        }
//        if (rg.getCheckedRadioButtonId() == R.id.rb_my_collection) {
//            if (collectListData != null) {
//                storageUtil.putString(userName + COLLECT_LIST_DATA,
//                        GsonUtil.str2Json(collectListData));
//                storageUtil.commit();
//            }
//        }
//    }
//
//    /**
//     * 录像 void
//     *
//     * @since V1.0
//     */
//    String vedioPath = null;
//
//    private void recordBtnOnClick() {
//        if (null != mLiveControl) {
//            if (!mIsRecord) {
//                // 随即生成一个1到10000的数字，用于录像名称的一部分，区分图片，开发者可以根据实际情况修改区分录像名称的方法
//                int recordIndex = new Random().nextInt(10000);
//                vedioPath = UtilFilePath.getVideoDirPath().getAbsolutePath()
//                        + File.separator + "Video" + recordIndex + ".mp4";
//                mLiveControl.startRecord(UtilFilePath.getVideoDirPath()
//                        .getAbsolutePath(), "Video" + recordIndex + ".mp4");
//                mIsRecord = true;
//                Toast.makeText(VedioDetailNewActivity.this, "启动录像成功", 0).show();
//                cloudVideo.setText("停止");
//            } else {
//                mLiveControl.stopRecord();
//                mIsRecord = false;
//                if (vedioPath != null) {
//                    ImageUtils.updateGallery(VedioDetailNewActivity.this,
//                            vedioPath);
//                }
//                Toast.makeText(VedioDetailNewActivity.this, "停止录像成功", 0).show();
//                cloudVideo.setText("录像");
//            }
//        }
//    }
//
//    /**
//     * 音频 void
//     *
//     * @since V1.0
//     */
//    private void audioBtnOnClick() {
//        if (null != mLiveControl) {
//            if (mIsAudioOpen) {
//                mLiveControl.stopAudio();
//                mIsAudioOpen = false;
//                cloudVoice.setText("开启语音");
//                Toast.makeText(VedioDetailNewActivity.this, "关闭音频", 0).show();
//            } else {
//                boolean ret = mLiveControl.startAudio();
//                if (!ret) {
//                    mIsAudioOpen = false;
//                    ;
//                    Toast.makeText(VedioDetailNewActivity.this, "开启音频失败", 0)
//                            .show();
//                } else {
//                    mIsAudioOpen = true;
//                    cloudVoice.setText("关闭语音");
//                    // 开启音频成功，并不代表一定有声音，需要设备开启声音。
//                    Toast.makeText(VedioDetailNewActivity.this, "开启音频成功", 0)
//                            .show();
//                }
//            }
//        }
//
//    }
//
//    @Override
//    public void surfaceChanged(SurfaceHolder holder, int format, int width,
//                               int height) {
//
//    }
//
//    @Override
//    public void surfaceCreated(SurfaceHolder holder) {
//
//    }
//
//    @Override
//    public void surfaceDestroyed(SurfaceHolder holder) {
//        if (null != mLiveControl) {
//            if (mIsRecord) {
//                // mRecordBtn.setText("开始录像");
//                mLiveControl.stopRecord();
//                mIsRecord = false;
//            }
//            mLiveControl.stop();
//        }
//    }
//
//    @Override
//    public void onMessageCallback(int messageID) {
//        sendMessageCase(messageID);
//    }
//
//    /**
//     * 返回已经播放的流量 void
//     *
//     * @return long
//     * @since V1.0
//     */
//    public long getStreamRate() {
//        return mStreamRate;
//    }
//
//    /**
//     * 发送消息
//     *
//     * @param i void
//     * @since V1.0
//     */
//    private void sendMessageCase(int i) {
//        if (null != mMessageHandler) {
//            Message msg = Message.obtain();
//            msg.arg1 = i;
//            mMessageHandler.sendMessage(msg);
//        }
//    }
//
//    /**
//     * 消息类
//     *
//     * @author huangweifeng
//     * @Data 2013-10-23
//     */
//    @SuppressLint("HandlerLeak")
//    boolean isFirstFailed = true;
//
//    private final class MyHandler extends Handler {
//        public void handleMessage(Message msg) {
//            switch (msg.arg1) {
//                case ConstantLive.RTSP_SUCCESS:
//                    // Toast.makeText(LiveAct.this, "启动取流成功", 0).show();
//                    break;
//
//                case ConstantLive.STOP_SUCCESS:
//                    // Toast.makeText(LiveAct.this, "停止成功", 0).show();
//                    break;
//
//                case ConstantLive.START_OPEN_FAILED:
//                    if (VedioDetailNewActivity.this != null) {
//                        ToastUtil.showToast(VedioDetailNewActivity.this, "开启播放库失败");
//                    }
//
//                    if (null != mProgressBar) {
//                        mProgressBar.setVisibility(View.GONE);
//                    }
//                    break;
//
//                case ConstantLive.PLAY_DISPLAY_SUCCESS:
//                    // Toast.makeText(LiveAct.this, "播放成功", 0).show();
//                    if (null != mProgressBar) {
//                        mProgressBar.setVisibility(View.GONE);
//
//                    }
//                    saveChangePNG();
//                    break;
//
//                case ConstantLive.RTSP_FAIL:
//                    // Toast.makeText(VedioDetailNewActivity.this, "RTSP链接失败",
//                    // 0).show();
//                    UtilAudioPlay.loadCameraInfo(VedioDetailNewActivity.this);
//                    if (isFirstFailed) {
//                        try {// 重新登陆海康平台 预留时间
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                        startBtnOnClick();
//                        isFirstFailed = false;
//                    } else {
//                        if (null != mProgressBar) {
//                            mProgressBar.setVisibility(View.GONE);
//                        }
//                        if (null != mLiveControl) {
//                            mLiveControl.stop();
//                        }
//                        if (null != VedioDetailNewActivity.this)
//                            Toast.makeText(VedioDetailNewActivity.this,
//                                    "网络异常，请稍后再试", 0).show();
//                    }
//                    break;
//
//                case ConstantLive.GET_OSD_TIME_FAIL:
//                    if (null != VedioDetailNewActivity.this)
//                        Toast.makeText(VedioDetailNewActivity.this, "获取OSD时间失败", 0)
//                                .show();
//                    break;
//
//                case ConstantLive.SD_CARD_UN_USEABLE:
//                    if (null != VedioDetailNewActivity.this)
//                        Toast.makeText(VedioDetailNewActivity.this, "SD卡不可用", 0)
//                                .show();
//                    break;
//
//                case ConstantLive.SD_CARD_SIZE_NOT_ENOUGH:
//                    if (null != VedioDetailNewActivity.this)
//                        Toast.makeText(VedioDetailNewActivity.this, "SD卡空间不足", 0)
//                                .show();
//                    break;
//                case ConstantLive.CAPTURE_FAILED_NPLAY_STATE:
//                    if (null != VedioDetailNewActivity.this)
//                        Toast.makeText(VedioDetailNewActivity.this, "非播放状态不能抓拍", 0)
//                                .show();
//                    break;
//                case Constant.START_VEDIO:
//                    startBtnOnClick();
//                    break;
//                case Constant.LOAD_PIEMENU:
//
//                    break;
//            }
//        }
//    }
//
//    @Override
//    public boolean onTouch(View v, MotionEvent event) {
//        switch (event.getAction()) {
//            case MotionEvent.ACTION_DOWN:
//                v.setPressed(true);
//                int id = v.getId();
//                if (id == R.id.cloud_b) {
//                    Toast.makeText(VedioDetailNewActivity.this, "---云台转下---", 0)
//                            .show();
//                } else if (id == R.id.cloud_t) {
//                    Toast.makeText(VedioDetailNewActivity.this, "---云台转上---", 0)
//                            .show();
//                } else if (id == R.id.cloud_r) {
//                    Toast.makeText(VedioDetailNewActivity.this, "---云台转右---", 0)
//                            .show();
//                } else if (id == R.id.cloud_l) {
//                    Toast.makeText(VedioDetailNewActivity.this, "---云台转左---", 0)
//                            .show();
//                } else if (id == R.id.cloud_near_focuse) {
//                    if (controlContener.getVisibility() == View.VISIBLE) {
//                        // sendCtrlCmd(7);
//                    } else {
//                        Toast.makeText(VedioDetailNewActivity.this, "---近焦不可用---",
//                                0).show();
//                    }
//                } else if (id == R.id.cloud_remote_focuse) {
//                    if (controlContener.getVisibility() == View.VISIBLE) {
//                        // sendCtrlCmd(8);
//                    } else {
//                        Toast.makeText(VedioDetailNewActivity.this, "---远焦不可用---",
//                                0).show();
//                    }
//                }
//
//
//                break;
//            case MotionEvent.ACTION_MOVE:
//
//                break;
//            case MotionEvent.ACTION_UP:
//                v.setPressed(false);
////			switch (v.getId()) {
//////			case R.id.cloud_b:
//////			case R.id.cloud_t:
//////			case R.id.cloud_r:
//////			case R.id.cloud_l:
//////			case R.id.cloud_remote_focuse:
//////			case R.id.cloud_near_focuse:
////				// stopCloudCtrl();
////				break;
////			}
////			break;
//
//            default:
//                break;
//        }
//        return true;
//    }
//
//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        // TODO Auto-generated method stub
//        super.onSaveInstanceState(outState);
//        outState.putSerializable("databack", MmainMenuItem);
//        outState.putSerializable("title", titleStr);
//    }
//
//}
