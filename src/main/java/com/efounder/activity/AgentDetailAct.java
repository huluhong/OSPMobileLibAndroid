package com.efounder.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.efounder.builder.base.data.EFDataSet;
import com.efounder.builder.base.data.EFRowSet;
import com.efounder.builder.base.data.ESPRowSet;
import com.efounder.form.EFFormDataModel;
import com.efounder.forwechat.BaseApp;
import com.efounder.ospmobilelib.R;
import com.efounder.util.LoadingDataUtilBlack;
import com.efounder.util.MyDialog;
import com.efounder.view.titlebar.AbTitleBar;
import com.pansoft.espflow.util.FlowTaskUtil;
import com.pansoft.espmodel.FormModel;
import com.pansoft.xmlparse.FormatSet;
import com.pansoft.xmlparse.FormatTable;
import com.pansoft.xmlparse.MobileFormatUtil;
import com.test.DataTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
/**
 * 待办详情 
 * 2015-3-17 15:01:58
 * @author long
 *
 */
public class AgentDetailAct extends AbActivity implements OnClickListener{
	public static final String TAG = "AgentDetailAct";

	/**提交审核*/
	private static final int DIALOG_SHEN_HE = 0x0000;
	/**取回动作*/
	private static final int DIALOG_TAKE_BACK = 0x0001;
	/**退回动作*/
	private static final int DIALOG_ROLL_BACK = 0x0002;
	
	public static String TYPE_TITLE = "type_title";
	public static String TYPE_CONTENT = "type_content";
	
	private AbTitleBar mTitleBar = null;
	
	/**审批流程 button*/
	private Button btnProcess;
	/**审核 button*/
	private Button btnShenhe;
	/**取回 button*/
	private Button btnTakeBack;
	/**退回 button*/
	private Button btnRollBack;
	
	private ArrayList<String> mArrayList = new ArrayList<String>();
	
	private AgentDetailAdapter adapter;
	
	ListView listView;
	EFRowSet flowRowSet;
	
	ESPRowSet rs;
	FormatSet formatSet;
	FormatTable ft;
	private BackgroundAsyncTask backgroundAsyncTask;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		BaseApp.actManager.putActivity(TAG, this);
		setAbContentView(R.layout.agent_detail);
		Bundle bundle = getIntent().getBundleExtra("dataSource");
		if(bundle!=null)
			flowRowSet = (EFRowSet) bundle.getSerializable("data");
		initView();
		initData();
		initListener();
	}
	ArrayList<DataTest> listTest;
	
	FormModel formModel;
	EFDataSet efdata;
	
	private void initData() {
		listTest = new ArrayList<DataTest>();
		backgroundAsyncTask = new BackgroundAsyncTask();
		backgroundAsyncTask.execute();
	}
	
	private void initView() {
		mTitleBar = this.getTitleBar();
		mTitleBar.setTitleText("单据信息");
		mTitleBar.setTitleTextColor(R.color.title_TextColor);
		mTitleBar.setLogo(R.drawable.ef_title_view_back);
		mTitleBar.setTitleBarBackground(R.color.title_Background);
	
        mTitleBar.setLogo(R.drawable.ef_title_view_back);
        mTitleBar.setTitleBarGravity(Gravity.CENTER, Gravity.CENTER);
        //mTitleBar.setTitleBarBackground(R.color.title_bar_bg);
        //  mTitleBar.setTitleBarBackground(R.color.red);

        
        btnProcess = (Button) findViewById(R.id.btn_approval_process);
        btnShenhe = (Button) findViewById(R.id.btn_approve);
        btnTakeBack = (Button) findViewById(R.id.btn_takeback);
        btnRollBack = (Button) findViewById(R.id.btn_rollback);
        btnRollBack.setOnClickListener(this);
        btnTakeBack.setOnClickListener(this);
        btnShenhe.setOnClickListener(this);
        btnProcess.setOnClickListener(this);
        
        listView = (ListView) findViewById(R.id.agency_details);
       
	}
	
	/**
	 * 设置监听事件
	 */
	private void initListener() {
		//绑定返回按键
		mTitleBar.setLogoOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				BaseApp.actManager.removeActivity(TAG);
			}
		});
		
	}

	@Override
	public void onClick(View v) {
		// TODO 
		if(v==btnProcess){
			Intent intent = new Intent(AgentDetailAct.this,ApprovalActivity.class);
			Bundle bundle = new Bundle();
			bundle.putSerializable( "data", flowRowSet);
			intent.putExtra("data", bundle);
			startActivity(intent);
		}else if(v==btnShenhe){
			createDialog(DIALOG_SHEN_HE);
		}else if(v==btnTakeBack){
			createDialog(DIALOG_TAKE_BACK);
		}else if(v==btnRollBack){
			createDialog(DIALOG_ROLL_BACK);
		}
	}
	
	  
	EFFormDataModel ef;
	
	class BackgroundAsyncTask extends AsyncTask<Void, Integer, FormModel> {

		// 进度条显示
		protected void onPreExecute() {
			// 进度条显示
			super.onPreExecute();
			// 加载dialog显示
			LoadingDataUtilBlack.show(AgentDetailAct.this);
		}

		@Override
		protected FormModel doInBackground(Void... params) {
			try {
				formModel = FlowTaskUtil.openTaskWithRowSet(flowRowSet);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return formModel;
		}

		// 当后台操作结束时，此方法将会被调用，计算结果将做为参数传递到此方法中，直接将结果显示到UI组件上
		protected void onPostExecute(FormModel result) {
			LoadingDataUtilBlack.dismiss();
			if(formModel!=null)
			ef = formModel.getFormDataModel();
			if (ef != null) {
				EFDataSet efdata = ef.getBillDataSet();
				List<ESPRowSet> rsl = efdata.getRowSetList();

				if (rsl != null) {
					rs = (ESPRowSet) rsl.get(0);
					String aa = rs.getString("HY_HYSQB.F_HYDDMC", "");

					formatSet = MobileFormatUtil.getInstance().getFormatSet();
					ft = formatSet.getFormatTableById("HY_HYSQB");
					List<Map<String, String>> list = FlowTaskUtil
							.openDetaiFromRowsetToMap(rs, ft, "HY_HYSQB");
					for (int i = 0; i < list.size(); i++) {
						DataTest dataTest = new DataTest();
						dataTest.setType(TYPE_TITLE);
						Map<String, String> map = list.get(i);
						String title = null;
						for (String key : map.keySet()) {
							if (key != null && !key.equals("")) {
								title = key + "  " + map.get(key);
							} else {
								title = "地点名称: "+"  "+map.get(key);
							}
						}
						
						dataTest.setTitle(title);
						listTest.add(dataTest);
					}
					adapter = new AgentDetailAdapter(
							AgentDetailAct.this, listTest);
					listView.setAdapter(adapter);
				}
			}
		}

	}
	
	  class AgentDetailAdapter extends BaseAdapter{
		  private ArrayList<DataTest> mList;
		  private Context mContext;
		  private LayoutInflater mInflater;
		  public AgentDetailAdapter(Context context,ArrayList<DataTest> list){
			  this.mContext = context;
			  this.mList = list;
			  mInflater = LayoutInflater.from(context);
		  }
		@Override
		public int getCount() {
			return mList.size();
		}

		@Override
		public Object getItem(int position) {
			return mList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			if(convertView == null|| (holder = (ViewHolder) convertView.getTag()).flag != position){
				holder = new ViewHolder();
				holder.flag = position;
				if(TYPE_TITLE.equals(mList.get(position).getType())){
					convertView = mInflater.inflate(R.layout.agency_detail_item_title, null);
					holder.title = (TextView) convertView.findViewById(R.id.title);
					convertView.setTag(holder);
				}else if(TYPE_CONTENT.equals(mList.get(position).getType())){
					convertView = mInflater.inflate(R.layout.agency_detail_item_content, null);
					holder.title = (TextView) convertView.findViewById(R.id.title);
					holder.content = (TextView) convertView.findViewById(R.id.content);
					convertView.setTag(holder);
				}
			}else{
				holder = (ViewHolder) convertView.getTag();
			}
			holder.title.setText(mList.get(position).getTitle());
			if(TYPE_CONTENT.equals(mList.get(position).getType())&&holder.content!=null){
				holder.content.setText(mList.get(position).getContent());
			}
			return convertView;
		}
		
		class ViewHolder{
			int flag = -1;
			TextView title;
			TextView content;
		}
	  }
	  
	  
	  protected Dialog createDialog(int id) {
			Dialog dialog = null;
			switch (id) {
			case DIALOG_SHEN_HE:
				dialog = new MyDialog(AgentDetailAct.this, R.style.shenhedialog,DIALOG_SHEN_HE,formModel);
				// 设置它的ContentView
				//dialog.setContentView(R.layout.shen_he_dialog);
				//dialog.setContentView(R.layout.new_shen_he_dialog);
				dialog.show();
				break;
			case DIALOG_TAKE_BACK:
				dialog = new MyDialog(AgentDetailAct.this, R.style.shenhedialog,DIALOG_TAKE_BACK,formModel);
				// 设置它的ContentView
				//dialog.setContentView(R.layout.shen_he_dialog);
				//dialog.setContentView(R.layout.new_shen_he_dialog);
				dialog.show();
				break;
			case DIALOG_ROLL_BACK:
				dialog = new MyDialog(AgentDetailAct.this, R.style.shenhedialog,DIALOG_ROLL_BACK,formModel);
				// 设置它的ContentView
				//dialog.setContentView(R.layout.shen_he_dialog);
				//dialog.setContentView(R.layout.new_shen_he_dialog);
				dialog.show();
				break;
				
			}
			
			return dialog;
		}
	  
}
