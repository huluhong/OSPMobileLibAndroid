package com.efounder.activity;

import android.os.Bundle;

import com.efounder.chat.activity.BaseActivity;
import com.efounder.ospmobilelib.R;

/**
 * Created by XinQing on 2016/12/30.
 */

public class TestActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
    }
}
