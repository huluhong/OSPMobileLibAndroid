package com.efounder.activity;

import android.content.ClipboardManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.efounder.chat.zxing.qrcode.QRCode;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.utils.AppInfoUtils;
import com.efounder.ospmobilelib.R;
import com.efounder.util.EnvSupportManager;
import com.efounder.util.ToastUtil;
import com.efounder.view.titlebar.AbTitleBar;
import com.utilcode.util.ReflectUtils;

import static com.efounder.frame.utils.Constants.KEY_APKDOWN_URL;
import static com.efounder.frame.utils.Constants.KEY_SETTING_APPID;
import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_ADDRESS;


public class AboutActivityNew extends AbActivity implements OnClickListener {
    private static final String TAG = "AboutActivityNew";

    AbTitleBar mTitleBar;
    private TextView versionTextView;//版本号
    private ImageView iv_erweMa;
    private TextView appNameView;
    private ImageView appIconView;
    /**
     * 功能介绍
     */
    private TextView tvAboutNewGuide;
    private  String url = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // BaseApp.actManager.putActivity(TAG, this);
        setAbContentView(R.layout.activity_about_new);
        initView();
        initdata();
    }


    private void initView() {
        mTitleBar = this.getTitleBar();
        mTitleBar.setTitleText(getResources().getString(R.string.about));
        mTitleBar.setLogo(R.drawable.ef_title_view_back);

       // mTitleBar.setTitleBarBackground(R.color.title_Background);
        mTitleBar.clearRightView();
        mTitleBar.setTitleBarGravity(Gravity.CENTER, Gravity.CENTER);
        mTitleBar.setLogoOnClickListener(this);

        versionTextView = (TextView) findViewById(R.id.versionId);
        tvAboutNewGuide = (TextView) findViewById(R.id.tv_about_new_guide);
        if(EnvSupportManager.isSupportAboutShowGuide()){
            tvAboutNewGuide.setVisibility(View.VISIBLE);
        }else {
            tvAboutNewGuide.setVisibility(View.GONE);
        }
        tvAboutNewGuide.setOnClickListener(this);
        iv_erweMa = (ImageView) findViewById(R.id.about_erweima);
        iv_erweMa.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // 为了兼容低版本我们这里使用旧版的android.text.ClipboardManager，虽然提示deprecated，但不影响使用。
                ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                // 将文本内容放到系统剪贴板里。
                cm.setText(url);
                ToastUtil.showToast(getApplicationContext(),getResources().getString(R.string.about_activity_tip_copy));
                return true;
            }
        });
        appNameView = (TextView) findViewById(R.id.about_app_name);
        appIconView = (ImageView) findViewById(R.id.about_app_icon);
    }


    private void initdata() {
        PackageManager pm = this.getPackageManager();//context为当前Activity上下文
        PackageInfo pi = null;
        try {
            pi = pm.getPackageInfo(this.getPackageName(), 0);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }

        versionTextView.setText(pi.versionName);
        //动态生成二维码
        if (EnvironmentVariable.getProperty(KEY_APKDOWN_URL) != null && !EnvironmentVariable.getProperty(KEY_APKDOWN_URL).equals("")) {
            url = EnvironmentVariable.getProperty(KEY_APKDOWN_URL);
        } else {
//            url = "http://" + com.efounder.constant.EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_ADDRESS)
//                    + "/ESTMobile/ESPMobile/" + com.efounder.constant.EnvironmentVariable.getProperty(KEY_SETTING_APPID)
//                    + ".apk";
            url = "https://" + com.efounder.constant.EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_ADDRESS)
                    + "/ospstore/OSPMobileLiveAPP/" + com.efounder.constant.EnvironmentVariable.getProperty(KEY_SETTING_APPID)
                    + ".apk";
        }
        Log.i(TAG, getResources().getString(R.string.about_activity_tip_qr)+ url);
        // https://mobile.osp.cloud/ospstore/OSPMobileLiveAPP/OSPMobileLiveApp.apk
        Bitmap bm = QRCode.setQRcode(url, 800, 800);
        if (bm != null) {
            iv_erweMa.setImageBitmap(bm);
        }
        //设置app名称以及图标
        AppInfoUtils appInfoUtils = new AppInfoUtils(this);
        Drawable icon = appInfoUtils.getAppIcon(pi.packageName);
        appIconView.setImageDrawable(icon);
        String appName = appInfoUtils.getAppName(pi.packageName);
        appNameView.setText(appName);

    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        if (v == mTitleBar.getLogoView()) {
           // BaseApp.actManager.removeActivity(TAG);
            finish();
        }else if(v.getId() == R.id.tv_about_new_guide){
            ReflectUtils reflectUtils = ReflectUtils.reflect("com.efounder.activity.GuideActivity");
            reflectUtils.method("start", AboutActivityNew.this, true);
        }
    }

}
