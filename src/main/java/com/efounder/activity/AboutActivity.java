package com.efounder.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;

import com.efounder.ospmobilelib.R;
import com.efounder.forwechat.BaseApp;
import com.efounder.view.titlebar.AbTitleBar;

/*
 * @author zhenglaikun
 * 2014.11.26
 * 关于界面，版本更新
 * 技术支持
 */
public class AboutActivity extends AbActivity implements OnClickListener{

	private static final String TAG = "AboutActivity";

	AbTitleBar mTitleBar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		BaseApp.actManager.putActivity(TAG, this);
		// 自定义标题栏，requestWindowFeature必须在setContent之前
//		this.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
				setAbContentView(R.layout.activity_about);
//		this.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
//				R.layout.activity_title);

		//初始化标题栏控件
//		TextView text_title = (TextView) findViewById(R.id.text_title);
//		text_title.setText("关于");
//		Button button_back = (Button)findViewById(R.id.button_back);
//		button_back.setOnClickListener(this);
		initView();

	}

	private void initView() {
		// TODO Auto-generated method stub
		mTitleBar = this.getTitleBar();
		mTitleBar.setTitleText("关于");
		mTitleBar.setTitleTextColor(Color.BLACK);
        mTitleBar.setLogo(R.drawable.ef_title_view_back);
        mTitleBar.setTitleBarBackground(R.color.title_bar_bg);
		mTitleBar.clearRightView();
		mTitleBar.setTitleBarGravity(Gravity.CENTER, Gravity.CENTER);
//		mTitleBar.setTitleTextMargin(20, 0, 0, 0);
		mTitleBar.setLogoOnClickListener(this);
	}

	@Override
	public void onClick(View v) {

		int id = v.getId();
		if(v==mTitleBar.getLogoView()){
			BaseApp.actManager.removeActivity(TAG);
		}
	}

}
