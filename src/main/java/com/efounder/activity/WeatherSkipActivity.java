//package com.efounder.activity;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.app.FragmentActivity;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentTransaction;
//import android.view.Window;
//
//import com.efounder.chat.fragment.ContactsFragment;
//import com.efounder.forwechat.BaseApp;
//import com.efounder.fragment.SimpleSettingFragment;
//import com.efounder.fragment.SuggestFragment;
//import com.efounder.fragment.SugguestDetailFragment;
//import com.efounder.ospmobilelib.R;
//
////import com.efounder.fragment.ContactsFragment;
//
//
//
///**
// * 此页面作用是在天气页面跳转设置和意见两个fragment的时候使用的
// *
// * @author cherise
// *
// */
//
//public class WeatherSkipActivity extends FragmentActivity {
//	public static final String TAG = "WeatherSkipActivity";
//	protected void onCreate(Bundle savedInstanceState) {
//		// TODO Auto-generated method stub
//		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
//		BaseApp.actManager.putActivity(TAG, this);
//		setContentView(R.layout.weatherskip);
//		Intent intent=getIntent();
//	String fragmentName =	intent.getStringExtra("fragname");
//		SimpleSettingFragment settingFragment = SimpleSettingFragment.newInstance(null, " ");
//	SuggestFragment suggestFragment=new SuggestFragment(true);
//	SugguestDetailFragment sugguestDetailFragment = new SugguestDetailFragment();
//	ContactsFragment contactsFragment=new ContactsFragment();
//		FragmentManager fm=getSupportFragmentManager();
//		FragmentTransaction fragmentTransaction=fm.beginTransaction();
//		if(fragmentName.equals("suggestfragment")){
//			fragmentTransaction.replace(R.id.weatherskip, suggestFragment);
//		}else if(fragmentName.equals("settingfragment")){
//			fragmentTransaction.replace(R.id.weatherskip, settingFragment);
//		}else if(fragmentName.equals("suggestDetailfragment")){
//			fragmentTransaction.replace(R.id.weatherskip, sugguestDetailFragment);
//		}else if(fragmentName.equals("contactfragment")){
//
//			fragmentTransaction.replace(R.id.weatherskip, contactsFragment);
//		}
//
//	fragmentTransaction.commit();
//
//	}
//
//	@Override
//	protected void onResume() {
//		// TODO Auto-generated method stub
//		super.onResume();
//
//		Intent intent=getIntent();
//		String fragmentName =	intent.getStringExtra("fragname");
//		SimpleSettingFragment settingFragment = SimpleSettingFragment.newInstance(null, "");
//		SuggestFragment suggestFragment=new SuggestFragment(true);
//			FragmentManager fm=getSupportFragmentManager();
//			FragmentTransaction fragmentTransaction=fm.beginTransaction();
//			if(fragmentName.equals("suggestfragment")){
//				fragmentTransaction.replace(R.id.weatherskip, suggestFragment);
//			}else if(fragmentName.equals("settingfragment")){
//				fragmentTransaction.replace(R.id.weatherskip, settingFragment);
//			}
//
//		fragmentTransaction.commit();
//	}
//}
