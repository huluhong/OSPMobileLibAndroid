//package com.efounder.activity;
//
//import android.app.Activity;
//import android.app.ProgressDialog;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.graphics.Color;
//import android.graphics.drawable.ColorDrawable;
//import android.net.Uri;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.support.v7.app.AlertDialog;
//import android.util.Log;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.Window;
//import android.widget.AdapterView;
//import android.widget.AdapterView.OnItemClickListener;
//import android.widget.GridView;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.efounder.adapter.CommonAdapter;
//import com.efounder.adapter.MeetingFileAdapter;
//import com.efounder.adapter.ViewHolder;
//import com.efounder.builder.base.data.EFDataSet;
//import com.efounder.builder.base.data.ESPRowSet;
//import com.efounder.constant.EnvironmentVariable;
//import com.efounder.data.model.HYRYModel;
//import com.efounder.data.model.HYSModel;
//import com.efounder.data.model.HYZLModel;
//import com.efounder.eai.EAI;
//import com.efounder.eai.data.JParamObject;
//import com.efounder.eai.data.JResponseObject;
//import com.efounder.mdm.EFMDMDataModel;
//import com.efounder.ospmobilelib.R;
//import com.efounder.util.AbStrUtil;
//import com.efounder.util.DateUtil;
//import com.efounder.util.FileUtil;
//import com.efounder.util.HessionRequestUtil;
//import com.efounder.util.LoadingDataUtilBlack;
//import com.efounder.util.StorageUtil;
//import com.efounder.util.ToastUtil;
//import com.efounder.widget.MylistView;
//import com.pansoft.espcomp.DateWindows;
//import com.pansoft.espcomp.DateWindows.DateOnclickListener;
//import com.pansoft.espmodel.EFWebNoticationObject;
//import com.zjl.http.AjaxCallBack;
//import com.zjl.http.tools.HttpTools;
//
//import java.io.File;
//import java.io.UnsupportedEncodingException;
//import java.net.URLEncoder;
//import java.util.ArrayList;
//import java.util.List;
//
//import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_ADDRESS;
//import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_HTTP_TYPE;
//import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_PATH;
//import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_PORT;
//
//public class MeetingDetailActivity extends Activity implements OnClickListener {
//
//	protected static final String TAG = "MeetingDetailActivity";
//	MylistView listView;
//	MeetingFileAdapter<HYZLModel> adapter;
//	CommonAdapter<HYRYModel> gridAdapter;//, gridAdapter2;
//
//	private ImageView downImg;
//	private ImageView upImg;
//	private ImageView sign;
//	private GridView gridView, gridView2;
//	private LinearLayout moreContenerLl;
//	private StorageUtil storageUtil;
//
//	private TextView hyjjTv, hyjjStartTime, hyjjEndTime;
//	private TextView userNameTextView;
//	/** 会议 */
//	private HYSModel hymodel;
//	/**会议人员*/
//	private List<ESPRowSet> hyryList;
//	/**会议资料*/
//	private List<ESPRowSet> hyzlList;
//	private Boolean isQd = false;// 是否签到
//
//	private List<HYRYModel> hyryListData = new ArrayList<HYRYModel>();
//	private List<HYZLModel> hyzlListData = new ArrayList<HYZLModel>();
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.meeting_manager_detail_ui);
//
//		storageUtil = new StorageUtil(getApplicationContext(), "storage");
//		hymodel = (HYSModel) getIntent().getSerializableExtra("hymodel");
//		Log.i(TAG, "会议数据：:" + hymodel);
//		initView();
//		loadDataByNet();
//		initData();
//		initListener();
//
//	}
//
//	private void initView() {
//		RelativeLayout include = (RelativeLayout) findViewById(R.id.include);
//		//include.setBackgroundResource(R.drawable.meetingnav);
//		include.setBackgroundResource(R.color.chat_red);
//		//include.setBackgroundColor(getResources().getColor(R.color.red));
//		LinearLayout leftbacklayout = (LinearLayout) findViewById(R.id.leftbacklayout);
//		leftbacklayout.setVisibility(View.VISIBLE);
//		leftbacklayout.setOnClickListener(this);
//		TextView meetingDate = (TextView) findViewById(R.id.meeting_date);
//		meetingDate.setVisibility(View.INVISIBLE);
//
//		meetingDate.setText("日期");
//		meetingDate.setTextColor(this.getResources().getColor(R.color.white));
//		meetingDate.setOnClickListener(this);
//		meetingDate.setVisibility(View.INVISIBLE);
//
//		TextView title = (TextView) findViewById(R.id.fragmenttitle);
//		title.setText("会议详情");
//		title.setTextColor(Color.WHITE);
//		include.setVisibility(View.VISIBLE);
//
//		userNameTextView = (TextView) findViewById(R.id.tv_user_name);
//		userNameTextView.setText(storageUtil.getString("loginUserName"));
//		gridView = (GridView) findViewById(R.id.meeting_people_grid);
//		gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));
//		gridView2 = (GridView) findViewById(R.id.gv_users);
//		gridView2.setSelector(new ColorDrawable(Color.TRANSPARENT));
//		listView = (MylistView) findViewById(R.id.lv_list);
//		downImg = (ImageView) findViewById(R.id.iv_arrow);
//		sign = (ImageView) findViewById(R.id.iv_sign);
//
//		sign.setOnClickListener(this);
//		upImg = (ImageView) findViewById(R.id.up);
//		moreContenerLl = (LinearLayout) findViewById(R.id.ll_more);
//		// 会议简介、开始时间、结束时间
//		hyjjTv = (TextView) findViewById(R.id.tv_hyjj);
//		hyjjStartTime = (TextView) findViewById(R.id.tv_hyjj_start_time);
//		hyjjEndTime = (TextView) findViewById(R.id.tv_hyjj_end_time);
//		String hyjj = "简介：" + hymodel.getF_HYJJ();
//		String startTime = "开始：" + DateUtil.getTime(hymodel.getF_SDATE());
//		String endTime = "结束：" + DateUtil.getTime(hymodel.getF_EDATE());
//
//		hyjjTv.setText(hyjj);
//		hyjjStartTime.setText(startTime);
//		hyjjEndTime.setText(endTime);
//
//	}
//
//	private void initListener() {
//		downImg.setOnClickListener(this);
//		upImg.setOnClickListener(this);
//		listView.setOnItemClickListener(new OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view,
//					int position, long id) {
//				HYZLModel meetingFile = hyzlListData.get(position);
//				Log.i(TAG, "2.获取url，打开word，pdf文件-------=======HYZLModel:" + meetingFile.toString());
//				if (meetingFile.getF_NAME() != null  && !meetingFile.getF_NAME().equals("")) {
//					//FIXME 1.获取url，打开word，pdf文件
//					String downLoadFileUrl = getDownloadFileURL(meetingFile);
//					Log.i(TAG, "2.获取url，打开word，pdf文件-------=======downLoadFileUrl:" + downLoadFileUrl);
////					openFile(downLoadFileUrl);
//					String fileName = meetingFile.getF_NAME().substring(0,meetingFile.getF_NAME().lastIndexOf("."))+".pdf";
//					String localFileUrl = FileUtil.FILE_ROOT + File.separator + "meeting" + File.separator + hymodel.getF_HYBH() +File.separator + fileName;
//					File file = new File(localFileUrl);
//					if (!file.exists()) {
//						System.out.println("文件不存在,请求下载文件");
//						//下载文件
//						downLoadFile(downLoadFileUrl, localFileUrl);
//					} else {
//						//FIXME 2.打开文档
////						String uriString= "file://" + localFileUrl;
////						openFile(Uri.parse(uriString));
////						openFile(downLoadFileUrl);
//						openFile(downLoadFileUrl,localFileUrl);
//					}
//				}
//			}
//
//		});
//	}
//
//	/**
//	 * 获取下载url
//	 * @return
//	 */
//	private String getDownloadFileURL(HYZLModel model){
////		String urlIp = storageUtil.getString("address");//ip
////		String urlPort = storageUtil.getString("port");//port
////		String urlServer = storageUtil.getString("path");//server
////		String dataBaseName = storageUtil.getString("service");//DBName
////		String dbno = storageUtil.getString("sign");//DBNO
//		String urlIp = EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_ADDRESS);
//		String urlPort = EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_PORT);
//		String urlServer = EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_PATH);
//		String dataBaseName = EnvironmentVariable.getProperty("service");
//		String dbno = EnvironmentVariable.getProperty("sign");
//		String protocol = EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_HTTP_TYPE);
//
//		//所有文件一律下载pdf格式（不管上传什么格式文件，服务器都会生成一份pdf）
//		String fileName = model.getF_NAME();
//		Log.i(TAG, "--------------------文件名："+fileName);
//		try {
//			fileName = URLEncoder.encode(fileName, "utf-8");
//			Log.i(TAG, "--------------------文件名："+fileName);
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		}
//		fileName = fileName.substring(0,fileName.lastIndexOf("."))+".pdf";
//		String fileGUID = model.getF_GUID();
//		String fileOrde = model.getF_ORDE();
//		String WJLX = model.getF_WJLX();
//
//		String tableName = model.getTableName();
//
//
//
//		String url = protocol + "://"+urlIp+":"+urlPort+"/"+urlServer+"/CtrlService?"
//				+"action=loadFile&"
//				+"fwk=FormAffixService&"
//				+"iom=loadAffixData&"
//				+"PO=DataBaseName="+dataBaseName+";DBNO="+dbno+"&"
//				+"GUID="+fileGUID+"&"
//
//				+"ORDE="+fileOrde+"&"
//				+"MDLID=&"
//				+"affixTable="+tableName+"&"
//				+"affixTableB=SYS_AFFIX&"
//				+"CCLX=FILE&"
//
//				+"PATH=/"+fileName+"&"
//				+"SWF_FILE=0&"
//				+"PDF_FILE=0&"
//				+"WJLX="+WJLX+""
//				;
//		return url;
//	}
//
//	/**
//	 * 下载文档到本地
//	 *
//	 * @param filePath
//	 */
//	HttpTools ht = new HttpTools();
//
//	private void downLoadFile(final String downLoadFileUrl, final String localFileUrl) {
//		//创建文件夹
//		File dir = new File(localFileUrl.substring(0, localFileUrl.lastIndexOf(File.separator)));
//		if (!dir.exists()) {
//			dir.mkdirs();
//		}
//		final ProgressDialog mProgressDialog = new ProgressDialog(MeetingDetailActivity.this);
//		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//		 mProgressDialog.setProgressNumberFormat("%1d KB/%2d KB");
//		try {
//			ht.download(downLoadFileUrl, localFileUrl, new AjaxCallBack<File>() {
//				@Override
//				public void onStart() {
//					Log.i(TAG, "---onStart---");
//					super.onStart();
//					mProgressDialog.show();
//				}
//
//				@Override
//				public void onFailure(Throwable t, int errorNo, String strMsg) {
//					super.onFailure(t, errorNo, strMsg);
//					Log.i(TAG, "---onFailure---");
//					mProgressDialog.dismiss();
//					ToastUtil.showToast(MeetingDetailActivity.this, "下载失败！");
//				}
//
//				@Override
//				public void onSuccess(File t) {
//					super.onSuccess(t);
//					Log.i(TAG, "---onSuccess---");
////				File file = new File(localFileUrl);
//					//FIXME 2.打开文档
////					String uriString= "file://" + localFileUrl;
////					openFile(Uri.parse(uriString));
////				openFile(downLoadFileUrl);
//					openFile(downLoadFileUrl,localFileUrl);
//					mProgressDialog.dismiss();
//				}
//
//				@Override
//				public void onLoading(long count, long current) {
//					super.onLoading(count, current);
//					Log.i(TAG, "---onLoading---");
//					//除以1024 将文件大小单位设置为KB
//					mProgressDialog.setMax((int) count/1024);
//					mProgressDialog.setProgress((int) current/1024);
//				}
//			});
//		} catch (Exception e) {
//			ToastUtil.showToast(this, "附件名称含有非法字符！");
//			e.printStackTrace();
//		}
//	}
//
////	private void openFile(String url) {
////		Intent intent = new Intent(this,PDFWebViewActivity.class);
////		intent.putExtra("pdf_url", url);
////		startActivity(intent);
////	}
//
//	private void openFile(final String downLoadFileUrl,final String localFileUrl) {
//		//判断文件大小是否为 0
//		File file = new File(localFileUrl);
//		if (file.exists() && file.length() == 0) {
//			file.delete();
//			/*AlertDialog.Builder builder = new AlertDialog.Builder(MeetingDetailActivity.this);
//			builder.setTitle("提示").setMessage("附件打开失败，服务器文件错误，请打开会议系统重新上传").setPositiveButton("确定", null);
//			builder.create().show();*/
//			AlertDialog.Builder builder = new AlertDialog.Builder(MeetingDetailActivity.this);
//			builder.setTitle("提示").setMessage("附件打开失败，点击确定，重新下载,或者联系管理员！").setPositiveButton("确定",new DialogInterface.OnClickListener() {
//
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					//下载文件
//					//downLoadFile(downLoadFileUrl, localFileUrl);
//				}
//			}).setNegativeButton(R.string.common_text_cancel, new DialogInterface.OnClickListener() {
//
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					dialog.dismiss();
//
//				}
//			});
//			builder.create().show();
//
//			return ;
//		}
//		String uriString= "file://" + localFileUrl;
//		Uri uri = Uri.parse(uriString);
////        final Intent intent = new Intent(Intent.ACTION_VIEW, uri);
////        intent.setClass(this, EBookDroidPDFActivity.class);
//
//		Intent intent = new Intent("android.intent.action.VIEW");
//
//		intent.addCategory("android.intent.category.DEFAULT");
//
//		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//
//	//	Uri uri = Uri.fromFile(new File(param ));
//
//		intent.setDataAndType(uri, "application/pdf");
//
//        startActivity(intent);
//    }
//
//
//	/**
//	 * 通过网络 加载数据
//	 */
//	private void loadDataByNet() {
//		new AsyncTask<Void, Void, JResponseObject>() {
//
//			// 进度显示
//			protected void onPreExecute() {
//				super.onPreExecute();
//				// 加载dialog显示
//				LoadingDataUtilBlack.show(MeetingDetailActivity.this);
//			}
//
//			@Override
//			protected JResponseObject doInBackground(Void... params) {
//				JResponseObject hyryRO = HessionRequestUtil.getHYRY(
//						MeetingDetailActivity.this, hymodel.getF_HYBH());
//				JResponseObject hyzlRO = HessionRequestUtil.getHYZLZD(
//						MeetingDetailActivity.this, hymodel.getF_HYBH());
//				if (hyzlRO != null) {
//					EFMDMDataModel dataModel = (EFMDMDataModel) (hyzlRO
//							.getResponseObject());
//					EFDataSet hyzlEFDataSet = dataModel.getDCTDataSet();
//					if (hyzlEFDataSet != null
//							&& hyzlEFDataSet.getRowSetList() != null) {
//						// 1.构建会议资料模型
//						hyzlList = hyzlEFDataSet.getRowSetList();
//						Log.i(TAG, "会议资料数量:" + hyzlEFDataSet.getRowSetList().size());
//						for (ESPRowSet row : hyzlList) {
//							HYZLModel model = new HYZLModel();
//							model.setF_NAME(row.getString("F_NAME", ""));//F_NAME
//							model.setF_CRDATE(row.getString("F_CRDATE", ""));
//							model.setF_SIZE(row.getString("F_SIZE", ""));
//							model.setF_USER(row.getString("F_USER", ""));
//							System.out.println("row path:" + row.getString("F_PATH", ""));
//							model.setF_PATH(row.getString("F_PATH", ""));//F_FILEPATH
//							// model.setF_PATH(row.getString("F_FILEPATH", ""));
//							if (!AbStrUtil.isEmpty(row.getString("F_NAME", ""))) {
//								model.setHeadImg(FileUtil.ImgFromStr(MeetingDetailActivity.this, row.getString("F_NAME", "")));
//							}
//
//							model.setF_GUID(row.getString("F_GUID", ""));//F_GUID
//							model.setF_ORDE(row.getString("F_ORDE", ""));//F_ORDE
//							model.setF_WJLX(row.getString("F_WJLX", ""));//F_WJLX
//							String tableName =hyzlEFDataSet.getTableName();//tableName
//							model.setTableName(tableName);
//							Log.i("", "1.构建会议资料模型--------========会议资料HYZLModel："+model.toString());
//							hyzlListData.add(model);
//						}
//					}
//				}
//				return hyryRO;
//			}
//
//			protected void onPostExecute(JResponseObject result) {
//				if (result != null) {
//					EFMDMDataModel dataModel = (EFMDMDataModel) (result
//							.getResponseObject());
//					EFDataSet hyryEFDataSet = dataModel.getDCTDataSet();
//					if (hyryEFDataSet != null
//							&& hyryEFDataSet.getRowSetList() != null) {
//						hyryList = hyryEFDataSet.getRowSetList();
////						Log.i(TAG, "---line283---getRowSetList.size:" + hyryEFDataSet.getRowSetList().size());
//						for (ESPRowSet set : hyryList) {
//							HYRYModel model = new HYRYModel();
////							Log.i("sssssssssss", set.getString("F_DWBH", ""));
////							Log.i(TAG, set.getString("F_DWMC", ""));
////							Log.i(TAG, set.getString("F_EMAIL", ""));
////							Log.i(TAG, set.getString("F_HYBH", ""));
////							Log.i(TAG, set.getString("F_NOTE", ""));
////							Log.i(TAG, set.getString("F_RYLX", ""));
////							Log.i(TAG, set.getString("F_YHMC", ""));
////
////							Log.i(TAG, set.getString("F_QDZT", ""));
////							Log.i(TAG, set.getString("F_XH", ""));
////							Log.i(TAG, set.getString("F_QDSJ", ""));
////							Log.i(TAG, set.getString("F_CHRBH", ""));
////							Log.i(TAG, set.getString("_Self_RowSet", ""));
////							Log.i("aaaaaaaaaa", set.getString("F_YHBH", ""));
//
//							// 签到功能
//							int _qdrs = 0;// 签到人数
//							// String qdrbh = set.getString("F_CHRBH", "");
//							String qdrbh = set.getString("F_YHBH", "");
//							// 签到状态：0代表未签到    1，2代表已经签到
//							String qdzt = set.getString("F_QDZT", "");
//							if (qdzt.equals("1") || qdzt.equals("2")) {
//								_qdrs = _qdrs + 1;
//								if (qdrbh.equals(storageUtil.getString("loginUserID"))) {
//									isQd = true;
//								}
//							}
//							if (isQd) {
//								sign.setBackgroundResource(R.drawable.signed);
//							}
//
//							model.setF_DWBH(set.getString("F_DWBH", ""));
//							model.setF_DWMC(set.getString("F_DWMC", ""));
//							model.setF_EMAIL(set.getString("F_EMAIL", ""));
//							model.setF_HYBH(set.getString("F_HYBH", ""));
//							model.setF_NOTE(set.getString("F_NOTE", ""));
//							model.setF_RYLX(set.getString("F_RYLX", ""));
//							model.setF_TEL(set.getString("F_TEL", ""));
//							model.setF_YHMC(set.getString("F_YHMC", ""));
//							model.setF_YHBH(set.getString("F_YHBH", ""));
//
//							model.setF_CHRBH(set.getString("F_CHRBH", ""));
//							model.setF_QDZT(set.getString("F_QDZT", ""));
//							if (hyryList.indexOf(set) == 0) {
//								model.setHeadImg(R.drawable.meeting_people_header_presenter);// 会议人员头像
//							}else {
//								model.setHeadImg(R.drawable.meeting_people_header);// 会议人员头像
//							}
//							hyryListData.add(model);
//
//						}
//
//						gridAdapter.notifyDataSetChanged();
//						adapter.notifyDataSetChanged();
//					}
//				}
//				LoadingDataUtilBlack.dismiss();
//			};
//
//		}.execute();
//	}
//
//	private void initData() {
//		gridAdapter = new CommonAdapter<HYRYModel>(MeetingDetailActivity.this,
//				hyryListData, R.layout.meeting_people) {
//			@Override
//			public void convert(ViewHolder helper, HYRYModel item) {
//				helper.setImageResource(R.id.meeting_people, item.getHeadImg());
//				helper.setText(R.id.meeting_peoplename, item.getF_YHMC());
//				ImageView imageView = helper.getView(R.id.meeting_people);
//				String qdzt = item.getF_QDZT();
//				if ("0".equals(qdzt)) {//未签到
//					imageView.setColorFilter(getResources().getColor(R.color.gray_light));
//				}else {//已签到
//					imageView.setColorFilter(getResources().getColor(R.color.blue_my));
//				}
//			}
//		};
//
//		gridView.setAdapter(gridAdapter);
//		gridView2.setAdapter(gridAdapter);
//
//		adapter = new MeetingFileAdapter<HYZLModel>(this, hyzlListData);
//		listView.setAdapter(adapter);
//	}
//
//	@Override
//	public void onClick(View v) {
//		int id = v.getId();
//		if (downImg == v) {
//			moreContenerLl.setVisibility(View.VISIBLE);
//		} else if (upImg == v) {
//			moreContenerLl.setVisibility(View.GONE);
//		}
//		if (id == R.id.meeting_date) {// 点击日期按钮
//			final DateWindows dateWindows = new DateWindows(
//					MeetingDetailActivity.this, v);
//			dateWindows.setDateOnclickListener(new DateOnclickListener() {
//
//				@Override
//				public void ondateCalendarClick(String date) {
//					dateWindows.dismiss();
//					// web.loadUrl("http://www.baidu.com");
//					String[] param = new String[1];
//					param[0] = date;
//					sendBroadCast("date", param);
//
//				}
//			});
//		}else if (id == R.id.leftbacklayout) {// 点击返回按钮，结束当前activity
//			if (MeetingDetailActivity.this != null
//					&& !MeetingDetailActivity.this.isFinishing()) {
//				MeetingDetailActivity.this.finish();
//			}
//		}else if (id == R.id.iv_sign) {// 点击签到按钮
//			System.out.println(isQd.toString());
//			if (!isQd) {
//				//1.向服务器发送 签到数据
//				new SendSignDataAsyncTask().execute();
//			} else if (isQd) {
//				Toast.makeText(MeetingDetailActivity.this, "您已经签到过！",
//						Toast.LENGTH_SHORT).show();
//			}
//		}
//	}
//
//	/*
//	 * 文件保存成功后发送广播
//	 */
//	public void sendBroadCast(String type, String[] parm) {
//
//		String ACTION = "webView";
//		Intent intent = new Intent();
//		EFWebNoticationObject efWebNoticationObject = new EFWebNoticationObject();
//		efWebNoticationObject.setType("date");
//		efWebNoticationObject.setParam(parm);
//		intent.putExtra("name", efWebNoticationObject);
//		intent.setAction(ACTION);
//		// 普通广播
//		MeetingDetailActivity.this.sendBroadcast(intent);
//	}
//
//
//
//	/**
//	 * 发送签到数据 异步任务
//	 *
//	 * @author hudq
//	 *
//	 */
//	private class SendSignDataAsyncTask extends AsyncTask<String, Integer, JResponseObject> {
//
//		@Override
//		protected void onPreExecute() {
//			// 加载dialog显示
//			LoadingDataUtilBlack.show(MeetingDetailActivity.this);
//			super.onPreExecute();
//		}
//
//		@Override
//		protected JResponseObject doInBackground(String... params) {
//			JParamObject po = JParamObject.Create();
//			po.setValue("F_YHBH", storageUtil.getString("loginUserID"));// 用户编号
//			po.setValue("F_YHMC", storageUtil.getString("loginUserName"));// 用户名称
//			po.setValue("F_HYBH", hymodel.getF_HYBH());// 会议编号
//			try {
//				JResponseObject ro = EAI.DAL.IOM("HYGLManager", "doHyQd", po);
//
//				return ro;
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			return null;
//		}
//
//		@Override
//		protected void onPostExecute(JResponseObject ro) {
//			LoadingDataUtilBlack.dismiss();
//			if (ro != null && ro.ErrorCode == 0) {//签到成功
//				// 2.签到成功：图片变为已签到，签到列表增加签到人员
//				sign.setBackgroundResource(R.drawable.signed);
//				HYRYModel model = new HYRYModel();
//				model.setF_YHMC(storageUtil.getString("loginUserName"));
//				model.setHeadImg(R.drawable.meeting_people_header);
//				model.setF_QDZT("1");//已签到状态
//				hyryListData.add(model);
//				gridAdapter.notifyDataSetChanged();
//				isQd = true;
//				Toast.makeText(MeetingDetailActivity.this, "签到成功",
//						Toast.LENGTH_SHORT).show();
//			} else {
//				AlertDialog.Builder alert = new AlertDialog.Builder(MeetingDetailActivity.this);
//
//				alert.setMessage("签到失败！");
//				alert.setPositiveButton("确定", null);
//
//				alert.show();
//			}
//			super.onPostExecute(ro);
//		}
//	}
//
//}
