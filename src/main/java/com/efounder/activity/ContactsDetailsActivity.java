package com.efounder.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.efounder.ospmobilelib.R;
import com.efounder.forwechat.BaseApp;
import com.efounder.view.titlebar.AbTitleBar;

/**
 * 
 * @author hudq
 * 通讯录详情界面
 */
public class ContactsDetailsActivity extends AbActivity implements OnClickListener{

	private static final String TAG = "ContactsDetailsActivity";

	AbTitleBar mTitleBar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		BaseApp.actManager.putActivity(TAG, this);
		// 自定义标题栏，requestWindowFeature必须在setContent之前
//		this.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setAbContentView(R.layout.activity_contacts_details);
//		this.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
//				R.layout.activity_title);

		//初始化标题栏控件
//		TextView text_title = (TextView) findViewById(R.id.text_title);
//		text_title.setText("关于");
//		Button button_back = (Button)findViewById(R.id.button_back);
//		button_back.setOnClickListener(this);
		initView();

	}

	private void initView() {
		RelativeLayout include = (RelativeLayout) findViewById(R.id.include);
		include.setBackgroundResource(R.color.red_ios);
		LinearLayout leftbacklayout = (LinearLayout) findViewById(R.id.leftbacklayout);
		leftbacklayout.setVisibility(View.VISIBLE);
		leftbacklayout.setOnClickListener(this);
	
		TextView title = (TextView) findViewById(R.id.fragmenttitle);
		title.setText("联系人详情");
		title.setTextColor(Color.WHITE);
		include.setVisibility(View.VISIBLE);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if(id == R.id.leftbacklayout){
			//BaseApp.actManager.removeActivity(TAG);
			this.finish();
		}
		
	}

}
