package com.efounder.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.efounder.ospmobilelib.R;
import com.utilcode.util.ReflectUtils;

/**
 * 空activity作为中间跳转
 * @author wang
 */
public class BlankActivity extends AppCompatActivity {

    public static void start(Context context, String className) {
        Intent starter = new Intent(context, BlankActivity.class);
        starter.putExtra("className", className);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobilelib_activity_blank);
        //跳转
        try {
            String className = getIntent().getStringExtra("className");
            ReflectUtils reflectUtils = ReflectUtils.reflect(className);
            reflectUtils.method("start", this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finish();
    }
}
