//package com.efounder.activity;
//
//import android.app.Activity;
//import android.graphics.Color;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.util.Log;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.AdapterView;
//import android.widget.Button;
//import android.widget.LinearLayout;
//import android.widget.ListView;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.efounder.adapter.VideoTreePRAdapter;
//import com.efounder.adapter.VideoTreePRAdapter.SelectedCountCallBack;
//import com.efounder.builder.base.data.EFDataSet;
//import com.efounder.builder.base.data.EFRowSet;
//import com.efounder.data.model.ItemData;
//import com.efounder.dm.vedio.TempData;
//import com.efounder.eai.EAI;
//import com.efounder.eai.data.JParamObject;
//import com.efounder.eai.data.JResponseObject;
//import com.efounder.http.utils.GsonUtil;
//import com.efounder.ospmobilelib.R;
//import com.efounder.util.LoadingDataUtilBlack;
//import com.efounder.util.StorageUtil;
//import com.efounder.util.ToastUtil;
//import com.google.gson.reflect.TypeToken;
//import com.zhy.bean.TreeBean;
//import com.zhy.tree.bean.TreeNode;
//import com.zhy.tree.bean.VideoTreePRHelper;
//import com.zhy.tree.bean.VideoTreePRListViewAdapter;
//import com.zhy.tree.bean.VideoTreePRListViewAdapter.OnTreeNodeClickListener;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import java.util.concurrent.Executors;
//
///**
// * 请求组织机构树的数据，一次请求下所有的数据
// */
//public class VideoTreeAllActivity extends Activity implements OnClickListener,
//		SelectedCountCallBack {
//
//	protected final String TAG = "VideoTreeAllActivity";
//	private ListView listView;
//	private VideoTreePRListViewAdapter adapter;
//	private List<TreeBean> mDatas;
//	private LoadingDataTask loadingDataTask;
//	private Button enterButton;
//	private TextView tv_countView;
//	private TreeNode parentNode = null;
//	private StorageUtil storageUtil;
//	private String userName;
//	/** 收藏的列表 */
//	private Set<ItemData> collectListData = new HashSet<ItemData>();
//	private Set<ItemData> tempcollectListData = new HashSet<ItemData>();
//
//	private ListView resourceListView;
//	/**
//	 * 消息处理Handler
//	 */
//	private MsgHandler handler = new MsgHandler();
//
//	private final class MsgHandler extends Handler {
//		@Override
//		public void handleMessage(Message msg) {
//			super.handleMessage(msg);
//			switch (msg.what) {
//
//			case 1:
//				ToastUtil.showToast(VideoTreeAllActivity.this, "没有更多数据");
//				break;
//			case 2:
//				refresh();
//				break;
//
//			default:
//				break;
//			}
//		}
//
//	}
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		//requestWindowFeature(Window.FEATURE_NO_TITLE);
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_vediolist);
//		mDatas = new ArrayList<TreeBean>();
//		storageUtil = new StorageUtil(this, "storage");
//		userName = storageUtil.getString("loginUserID");
//
//		initView();
//		initData();
//
//	}
//
//	/**
//	 * 初始化view
//	 */
//	private void initView() {
//		RelativeLayout include = (RelativeLayout) findViewById(R.id.include);
//		LinearLayout leftbacklayout = (LinearLayout) findViewById(R.id.leftbacklayout);
//		leftbacklayout.setVisibility(View.VISIBLE);
//		leftbacklayout.setOnClickListener(this);
//		TextView title = (TextView) findViewById(R.id.fragmenttitle);
//		title.setText("组织机构");
//		title.setTextColor(Color.WHITE);
//		include.setVisibility(View.VISIBLE);
//
//		tv_countView = (TextView) findViewById(R.id.tv2);
//		enterButton = (Button) findViewById(R.id.enterbutton);
//		enterButton.setOnClickListener(this);
//		listView = (ListView) findViewById(R.id.videolistview);
//		// 得到收藏的数据
//		String collectStr = storageUtil.getString(userName
//				+ VedioDetailNewActivity.COLLECT_LIST_DATA);
//		if (collectStr != null && !"".equals(collectStr)) {
//			collectListData = GsonUtil.fromJson(collectStr,
//					new TypeToken<Set<ItemData>>() {
//					});
//		}
//		if (collectListData != null) {
//			tv_countView.setText(String.valueOf(collectListData.size()));
//
//			Iterator<ItemData> it = collectListData.iterator();
//			while (it.hasNext()) {
//				ItemData data = it.next();
//				tempcollectListData.add(data);
//
//			}
//		}
//
//	}
//
//	/**
//	 * 初始化数据
//	 */
//	private void initData() {
//		loadingDataTask = new LoadingDataTask(null);
//		loadingDataTask.executeOnExecutor(Executors.newCachedThreadPool());
//	}
//
//	@Override
//	public void onClick(View v) {
//		int id = v.getId();
//
//		if(id==R.id.leftbacklayout){
//			VideoTreeAllActivity.this.finish();
//		}
//		else if(id==R.id.enterbutton){
//			ArrayList<TreeNode> selectedNodes = (ArrayList<TreeNode>) VideoTreePRHelper
//					.filterCheckedLeafNodes(adapter.getAllNodes());
//			if (collectListData != null
//					&& selectedNodes.size() + collectListData.size() >= 0) {
//				List<ItemData> datas = treeNode2ItemData(selectedNodes);
//				VideoTreeAllActivity.this.finish();
//			} else {
//				ToastUtil.showToast(VideoTreeAllActivity.this, "错误！请返回重试");
//			}
//
//		}
//	}
//
//	/**
//	 *
//	 * treeBean转为ItemData
//	 *
//	 * @param selectedNodes
//	 */
//	private List<ItemData> treeNode2ItemData(ArrayList<TreeNode> selectedNodes) {
//		List<ItemData> datas = new ArrayList<ItemData>();
//		Set<ItemData> shouCangData = new HashSet<ItemData>();
//		for (int i = 0; i < selectedNodes.size(); i++) {
//			ItemData data = new ItemData();
//			TreeNode treeNode = selectedNodes.get(i);
//			Map<String, Object> map = treeNode.getAttrs();
//			// CameraInfo info = (CameraInfo) map.get("CameraInfo");
//			String cameraID = (String) map.get("camera_id");
//			String cameraName = (String) map.get("camera_name");
//			String cameraDeciveID = (String) map.get("camera_deviceid");
//			data.setCameraID(cameraID);
//			data.setCaption(cameraName);
//			data.setDeviceID(cameraDeciveID);
//			TempData.getIns().setData(data);
//			shouCangData.add(data);
//
//		}
//		shouCangData.addAll(collectListData);
//		if (shouCangData.size() >= 0) {
//			storageUtil.putString(
//					userName + VedioDetailNewActivity.COLLECT_LIST_DATA,
//					GsonUtil.str2Json(shouCangData));
//			storageUtil.commit();
//			Log.i(TAG, "---保存收列表");
//		}
//
//		return datas;
//
//	}
//
//	@Override
//	public void getSelectedCount() {
//		setButtonData();
//
//	}
//
//	/**
//	 * 设置下方选中的摄像头个数以及按钮是否可用
//	 */
//	private void setButtonData() {
//		ArrayList<TreeNode> selectedNodes = (ArrayList<TreeNode>) VideoTreePRHelper
//				.filterCheckedLeafNodes(adapter.getAllNodes());
//		if (selectedNodes != null && collectListData != null) {
//			tv_countView.setText(String.valueOf(selectedNodes.size()
//					+ collectListData.size()));
//
//		}
//	}
//
//	/**
//	 * 刷新界面
//	 */
//	private void refresh() {
//		try {
//			adapter = new VideoTreePRAdapter(listView,
//					VideoTreeAllActivity.this, mDatas, 0, collectListData);
//			((VideoTreePRAdapter) adapter)
//					.SetSelectedCountCallBack(VideoTreeAllActivity.this);
//			adapter.setOnTreeNodeClickListener(new OnTreeNodeClickListener() {
//				@Override
//				public void onClick(AdapterView<?> parent, View view,
//						TreeNode node, int position) {
//					if (loadingDataTask != null
//							&& !loadingDataTask.isCancelled()) {
//						loadingDataTask.cancel(true);
//					}
//					if (node.isLeaf()) {
//
//						boolean isCamera = node.getAttrs().containsKey(
//								"CameraInfo");
//						if (isCamera) {
//							if (node.isHasCheckBox()) {
//								boolean isChecked = !node.isChecked();
//								node.setChecked(isChecked);
//								if (isChecked) {
//									node.setIcon(R.drawable.video_camera_checked);
//								} else {
//									node.setIcon(R.drawable.video_camera_nocheck);
//								}
//								setButtonData();
//
//							}
//						} else {
//							parentNode = node;
//							ToastUtil.showToast(VideoTreeAllActivity.this, "没有更多数据");
//
//						}
//					}
//					adapter.notifyDataSetChanged();
//				}
//
//			});
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		List<TreeNode> visibleNodes = VideoTreePRHelper
//				.filterVisibleNode(adapter.getAllNodes());
//		adapter.setVisibleNodes(visibleNodes);
//		if (parentNode == null) {
//			adapter.notifyDataSetChanged();
//		} else {// 点击时调用，非初始化时调用
//			adapter.expandOrCollapse(visibleNodes.indexOf(parentNode));
//		}
//		listView.setAdapter(adapter);
//		adapter.notifyDataSetChanged();
//		List<TreeNode> allList = adapter.getAllNodes();
//		if (allList == null) {
//			return;
//		}
//		for (int i = 0; i < allList.size(); i++) {
//			setcollectNodeExpand(allList.get(i));
//		}
//		adapter.notifyDataSetChanged();
//
//	}
//
//	/**
//	 * 通过网络 加载数据
//	 */
//	private class LoadingDataTask extends
//			AsyncTask<String, Integer, List<TreeBean>> {
//
//		private TreeNode parentNode;
//
//		public LoadingDataTask(TreeNode parentNode) {
//			super();
//			this.parentNode = parentNode;
//
//		}
//
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//			// 加载dialog显示
//			LoadingDataUtilBlack.show(VideoTreeAllActivity.this);
//		}
//
//		@Override
//		protected List<TreeBean> doInBackground(String... params) {
//			List<TreeBean> regionList = null;
//
//			regionList = getDeptData("", 0);
//
//			return regionList;
//		}
//
//		@Override
//		protected void onPostExecute(List<TreeBean> regionList) {
//			super.onPostExecute(regionList);
//
//			LoadingDataUtilBlack.dismiss();
//			if (mDatas == null || regionList == null) {
//				return;
//			}
//			mDatas.addAll(regionList);
//			handler.sendEmptyMessage(2);
//
//		}
//
//		@Override
//		protected void onCancelled() {
//			super.onCancelled();
//			LoadingDataUtilBlack.dismiss();
//		}
//
//		private List<TreeBean> getDeptData(String parentId, int js) {
//			List<TreeBean> treeBeans = null;
//			// EAI.Protocol = "http";
//			// EAI.Server = "10.75.122.197";
//			// EAI.Port = "8080";
//			// EAI.Path = "fmis";
//			// EAI.Service = "Android";
//
//			JParamObject PO = JParamObject.Create();
//			JResponseObject RO = null;
//			PO.SetValueByParamName("UserName", userName);// "P0034"
//			PO.SetValueByParamName("method", "getAllResTreeWithoutCalc");
//
//			try {
//				// 连接服务器
//				RO = EAI.DAL.SVR("ZZJG2DataSetDataService", PO);
//
//				EFDataSet efDataSet = (EFDataSet) RO.getResponseObject();
//
//				treeBeans = getTreeBeans(RO);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//
//			return treeBeans;
//		}
//
//		/**
//		 * 得到全部的组织树数据
//		 *
//		 * @param RO
//		 * @return
//		 */
//		private List<TreeBean> getTreeBeans(JResponseObject RO) {
//			List<TreeBean> treeBeans = new ArrayList<TreeBean>();
//			if (RO == null) {
//				return treeBeans;
//			}
//			EFDataSet efDataSet = (EFDataSet) RO.getResponseObject();
//			if (efDataSet == null) {
//				return treeBeans;
//			}
//			List<EFRowSet> list = efDataSet.getRowSetArray();
//			if (list == null) {
//				return treeBeans;
//			}
//			for (int i = 0; i < list.size(); i++) {
//				EFRowSet efRowSet = list.get(i);
//				Map<String, Object> attrMap = efRowSet.getAttriMap();
//
//				if ("1".equals(efRowSet.getString("dwzd_js", ""))) {
//					continue;
//				}
//				if ("2".equals(efRowSet.getString("dwzd_js", ""))) {
//
//					String idString = efRowSet.getString("dwzd_bh", "");
//					String parentId = idString.substring(0,
//							idString.length() - 3);
//					String name = efRowSet.getString("dwzd_mc", "");
//					String jsString = efRowSet.getString("dwzd_js", "");
//					attrMap.put("js", jsString);
//					TreeBean treeBean = new TreeBean(idString, parentId, name);
//					treeBeans.add(treeBean);
//					treeBean.setHasCheckBox(false);
//					treeBean.setAttrs(attrMap);
//					continue;
//				}
//				if (attrMap.get("camera_id") != null) {// 说明是摄像头
//					String id = (String) attrMap.get("camera_id");
//					String dwzd_bh = (String) attrMap.get("dwzd_bh");
//					String parentId = dwzd_bh
//							.substring(0, dwzd_bh.length() - 3);
//					String name = (String) (attrMap.get("camera_name"));
//					attrMap.put("CameraInfo", "");
//
//					TreeBean treeBean = new TreeBean(id, parentId, name);
//					treeBean.setHasCheckBox(true);
//					treeBean.setAttrs(attrMap);
//					setChecked(treeBean);
//					treeBeans.add(treeBean);
//
//				} else {// 组织机构列表
//					String id = (String) attrMap.get("dwzd_bh");
//					String parentId = id.substring(0, id.length() - 3);
//					String name = (String) (attrMap.get("dwzd_mc"));
//					String jsString = (String) (attrMap.get("dwzd_js"));
//					Map<String, Object> treeMap = new HashMap<String, Object>();
//					treeMap.put("js", jsString);
//					// setExistState(node)
//					TreeBean treeBean = new TreeBean(id, parentId, name);
//					treeBean.setHasCheckBox(false);
//					treeBean.setAttrs(treeMap);
//					treeBeans.add(treeBean);
//				}
//
//			}
//
//			return treeBeans;
//		}
//
//	}
//
//	/**
//	 * 设置被收藏的状态
//	 *
//	 * @param node
//	 */
//	public void setChecked(TreeBean node) {
//		if (collectListData != null && collectListData.size() > 0
//				&& node.getAttrs().containsKey("CameraInfo")) {
//			Iterator<ItemData> it = collectListData.iterator();
//			while (it.hasNext()) {
//				try {
//					ItemData data = it.next();
//					if (data.getCameraID().equals(
//							(node.getAttrs().get("camera_id")))
//							&& data.getDeviceID().equals(
//									(node.getAttrs().get("camera_deviceid")))) {
//						node.setChecked(true);
//						it.remove();
//					}
//				} catch (Exception e) {
//
//				}
//			}
//		}
//
//	}
//
//	/**
//	 * 设置被收藏的摄像头node状态为展开状态
//	 *
//	 * @param node
//	 */
//	public void setcollectNodeExpand(TreeNode node) {
//		if (tempcollectListData != null && tempcollectListData.size() > 0
//				&& node.getAttrs().containsKey("CameraInfo")) {
//			Iterator<ItemData> it = tempcollectListData.iterator();
//			while (it.hasNext()) {
//				try {
//					ItemData data = it.next();
//					if (data.getCameraID().equals(
//							(node.getAttrs().get("camera_id")))
//							&& data.getDeviceID().equals(
//									(node.getAttrs().get("camera_deviceid")))) {
//
//						setExpand(node);
//
//					}
//				} catch (Exception e) {
//
//				}
//			}
//		}
//
//	}
//
//	/**
//	 * 设置状态
//	 *
//	 * @param node
//	 */
//	public void setExpand(TreeNode node) {
//
//		adapter.expandMyNode(node);
//		// 如果父节点没有展开，展开父节点
//		if (!node.getParent().isExpand()) {
//			setExpand(node.getParent());
//
//		}
//
//	}
//}
