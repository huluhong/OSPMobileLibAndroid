package com.efounder.activity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.efounder.constant.EnvironmentVariable;
import com.efounder.forwechat.BaseApp;
import com.efounder.ospmobilelib.R;
import com.efounder.util.StorageUtil;
import com.efounder.view.titlebar.AbTitleBar;
import com.zcw.togglebutton.ToggleButton.OnToggleChanged;

import static com.efounder.frame.utils.Constants.KEY_SETTING_APPID;
import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_ADDRESS;
import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_HTTP_TYPE;
import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_PATH;
import static com.efounder.frame.utils.Constants.KEY_SETTING_DEFAULT_SERVER_PORT;
import static com.efounder.frame.utils.Constants.KEY_SIGN;

/**
 * 
 * @author zhenglaikun
 * 2014.11.26
 * 设置与后台交互的服务端口，路径等
 *
 */
public class SettingActivity extends AbActivity implements OnClickListener{

	private static final String TAG = "SettingActivity";

	private StorageUtil storageUtil;

	// 服务地址
	private EditText editText_address;
	// 服务端口
	private EditText editText_port;
	// 服务路径
	private EditText editText_path;
	// 服务名称
	private EditText editText_name;
	// 数据服务
	private EditText editText_service;
	// 数据标示
	private EditText editText_sign;
	/*
	 * AD域用户与是否安全
	 */
 private com.zcw.togglebutton.ToggleButton toggleButton_ADuser;
	/*
	 * 连接服务器服务信息
	 */
	String address, port, path, name, service, sign;
	
	private boolean isSafe=false;
	

	AbTitleBar mTitleBar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);

		// 自定义标题栏，requestWindowFeature必须在setContent之前
//		this.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setAbContentView(R.layout.activity_setting);
		BaseApp.actManager.putActivity(TAG, this);
		mTitleBar = getTitleBar();
		mTitleBar.setTitleText("高级设置");
	//	mTitleBar.setLogo(R.drawable.button_selector_back);
        mTitleBar.setLogo(R.drawable.ef_title_view_back);
        mTitleBar.clearRightView();
		mTitleBar.setTitleBarGravity(Gravity.CENTER, Gravity.CENTER);
        mTitleBar.setLogoOnClickListener(this);
//		this.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
//				R.layout.activity_title);

		// 初始化标题栏控件
//		Button button_back = (Button) findViewById(R.id.button_back);
//		button_back.setOnClickListener((OnClickListener) this);
        
		// 初始化控件
		initSettingWidget();

	}
	
	
/*	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		  if (keyCode==KeyEvent.KEYCODE_BACK ) { 
			  Intent intent = new Intent(SettingActivity.this, Login_withTitle.class);
				startActivity(intent);
				SettingActivity.this.finish();
			  finish();
		  } 
		return super.onKeyDown(keyCode, event);
	}*/

	/*
	 * 初始化控件
	 */

	private void initSettingWidget() {
		storageUtil = new StorageUtil(getApplicationContext(), "storage");

		editText_address = (EditText) findViewById(R.id.setting_addressEdit);
		editText_port = (EditText) findViewById(R.id.setting_portEdit);
		editText_path = (EditText) findViewById(R.id.setting_pathEdit);
		editText_name = (EditText) findViewById(R.id.setting_nameEdit);
		editText_service = (EditText) findViewById(R.id.setting_serviceEidt);
		editText_sign = (EditText) findViewById(R.id.setting_signEdit);

		// 保存ad域用户按钮
		toggleButton_ADuser = (com.zcw.togglebutton.ToggleButton) findViewById(R.id.setting_ADuserButton);

		// 显示设置信息
//		editText_address.setText(storageUtil.getString("address", EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_ADDRESS)));
//		editText_port.setText(storageUtil.getString("port",EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_PORT)));
//		editText_path.setText(storageUtil.getString("path",EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_PATH)));
//		editText_name.setText(storageUtil.getString(KEY_SETTING_APPID,EnvironmentVariable.getProperty(KEY_SETTING_APPID)));
//		editText_service.setText(storageUtil.getString("service",EnvironmentVariable.getProperty("service")));
//		editText_sign.setText(storageUtil.getString("sign",EnvironmentVariable.getProperty("sign")));
//		isSafe=storageUtil.getBoolean("isSafe",EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_HTTP_TYPE)=="http"?false:true);

		editText_address.setText(EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_ADDRESS));
		editText_port.setText(EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_PORT));
		editText_path.setText(EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_PATH));
		editText_name.setText(EnvironmentVariable.getProperty(KEY_SETTING_APPID));
//		editText_service.setText(EnvironmentVariable.getProperty("service"));
		editText_service.setText("ZYYT_DB01");
		editText_sign.setText(EnvironmentVariable.getProperty(KEY_SIGN));
		isSafe = !EnvironmentVariable.getProperty(KEY_SETTING_DEFAULT_SERVER_HTTP_TYPE).equals("http");
		if (isSafe) {
			toggleButton_ADuser.toggleOn();

		
			
		}else {
			toggleButton_ADuser.toggleOff();
		}

		// 获取点击事件AD域用户，是否安全
		/*toggleButton_ADuser
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton arg0,
							boolean isChecked) {
						isSafe = isChecked;
						storageUtil.putBoolean("isSafe", isSafe);
					}
				});*/
		toggleButton_ADuser.setOnToggleChanged(new OnToggleChanged() {

			@Override
			public void onToggle(boolean on) {
				
			
				storageUtil.putBoolean("isSafe", on);
				

			}
		});
		

		
		


		// 保存按钮
		Button setting_save = (Button) findViewById(R.id.setting_saveButton);
		setting_save.setOnClickListener((OnClickListener) this);
	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		BaseApp.actManager.removeActivity(TAG);
	}

	/*
	 * 标题栏按钮，保存按钮 点击事件
	 */
	public void onClick(View v) {

		Toast toast;
		// 得到输入的值
		address = editText_address.getText().toString();
		port = editText_port.getText().toString();
		path = editText_path.getText().toString();
		name = editText_name.getText().toString();
		service = editText_service.getText().toString();
		sign = editText_sign.getText().toString();
		int id = v.getId();
		if(v==mTitleBar.getLogoView()){
			/*Intent intent = new Intent(SettingActivity.this, Login_withTitle.class);
			startActivity(intent);*/
			SettingActivity.this.finish();
			//BaseApp.actManager.removeActivity(TAG);
		}else if (id == R.id.setting_saveButton) {
			if (address == null || address.equals("") || port == null
					|| port.equals("") || path == null || path.equals("")
					|| name == null || name.equals("") || service == null
					|| service.equals("") || sign == null || sign.equals("")) {
				toast = Toast.makeText(getApplicationContext(), "信息输入不完整",
						Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
				return;
			}

			// 保存设置信息
//			storageUtil.putString("address", address);
//			storageUtil.putString("port", port);
//			storageUtil.putString("path", path);
//			storageUtil.putString(KEY_SETTING_APPID, name);
//			storageUtil.putString("service", service);
//			storageUtil.putString("sign", sign);
//			//storageUtil.putBoolean("isSafe", isSafe);
//			storageUtil.commit();
			EnvironmentVariable.setProperty(KEY_SETTING_DEFAULT_SERVER_ADDRESS,address);
			EnvironmentVariable.setProperty(KEY_SETTING_DEFAULT_SERVER_PORT,port);
			EnvironmentVariable.setProperty(KEY_SETTING_DEFAULT_SERVER_PATH,path);
			EnvironmentVariable.setProperty(KEY_SETTING_APPID,name);
			EnvironmentVariable.setProperty("service",service);
			EnvironmentVariable.setProperty("sign",sign);

			toast = Toast.makeText(getApplicationContext(), "信息已保存",
					Toast.LENGTH_SHORT);
			toast.show();
			/*Intent intent = new Intent(SettingActivity.this, Login_withTitle.class);
			startActivity(intent);*/
			SettingActivity.this.finish();
		}
	}
}
