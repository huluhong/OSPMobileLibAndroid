//package com.efounder.activity;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
//import android.graphics.Color;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.GridLayout.LayoutParams;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.efounder.frame.utils.Constants;
//import com.efounder.ospmobilelib.R;
//import com.efounder.adapter.MatrixTableAdapter;
//import com.efounder.builder.base.data.EFDataSet;
//import com.efounder.builder.base.data.EFRowSet;
//import com.efounder.constant.EnvironmentVariable;
//import com.efounder.eai.EAI;
//import com.efounder.eai.data.JParamObject;
//import com.efounder.eai.data.JResponseObject;
//import com.efounder.forwechat.BaseApp;
//import com.efounder.util.DownAttachUtil;
//import com.efounder.util.GridGeneratorPact;
//import com.efounder.util.LoadingDataUtilBlack;
//import com.efounder.view.titlebar.AbTitleBar;
//import com.inqbarna.tablefixheaders.TableFixHeaders;
//
///*
// * @author yqs
// * 合同详情界面
// *
// */
//public class ContractDetailActivity extends AbActivity implements OnClickListener {
//
//	private static final String TAG = "TaskDetailActivity";
//
//	AbTitleBar mTitleBar;
//	RelativeLayout lay1, lay2, lay3, lay4, lay5,lay6;
//	GridGeneratorPact gridGenerator;
//	RelativeLayout[] reLayouts;
//	LinearLayout linearLayout1, linearLayout2, linearLayout3, linearLayout4,
//			linearLayout5,linearLayout6;
//	ImageView arrowImageView1, arrowImageView2, arrowImageView3,
//			arrowImageView4, arrowImageView5,arrowImageView6;
//	TextView title1, title2, title3, title4, title5,title6;
//	EFRowSet flowRowSet = null;
//	String systemType;// 系统类型 OA 或者 合同管理
//	String itemId;// 请求的详情页数据的id
//	String usereId;// 用户id
//	String userName;// 用户名
//	String password;// 用户密码
//	TableFixHeaders tableFixHeaders;
//	String[][] attr= new String[2][6];
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//
//		super.onCreate(savedInstanceState);
//		BaseApp.actManager.putActivity(TAG, this);
//		setAbContentView(R.layout.contract_detailinfo);
//		initView();
//		initData();
//	}
//
//	/**
//	 * 初始化view
//	 */
//	private void initView() {
//
//		mTitleBar = this.getTitleBar();
//		mTitleBar.setTitleText("单据信息");
//		mTitleBar.setTitleTextColor(Color.WHITE);
//		mTitleBar.setLogo(R.drawable.ef_title_view_back);
//		mTitleBar.setTitleBarBackground(R.color.red_ios);
//		mTitleBar.clearRightView();
//		mTitleBar.setTitleBarGravity(Gravity.CENTER, Gravity.CENTER);
//		mTitleBar.setLogoOnClickListener(this);
//		// 得到bundle传递的数据
//		Bundle bundle = getIntent().getBundleExtra("dataSource");
//		if (bundle != null) {
//			flowRowSet = (EFRowSet) bundle.getSerializable("data");
//			systemType = (String) bundle.getSerializable("system");
//			itemId = flowRowSet.getString("itemid", "");
//		}
//
//		title1 = (TextView) findViewById(R.id.title1);
//		title2 = (TextView) findViewById(R.id.title2);
//		title3 = (TextView) findViewById(R.id.title3);
//		title4 = (TextView) findViewById(R.id.title4);
//		title5 = (TextView) findViewById(R.id.title5);
//		title6 = (TextView) findViewById(R.id.title6);
//		title1.setText("基础信息");
//		title2.setText("显示全部");
//		title3.setText("扩展字段");
//		title4.setText("相对人信息");
//		title5.setText("标的明细");
//		title6.setText("审查审批");
//		lay1 = (RelativeLayout) findViewById(R.id.bills_lay1);
//		lay2 = (RelativeLayout) findViewById(R.id.bills_lay2);
//		lay3 = (RelativeLayout) findViewById(R.id.bills_lay3);
//		lay4 = (RelativeLayout) findViewById(R.id.bills_lay4);
//		lay5 = (RelativeLayout) findViewById(R.id.bills_lay5);
//		lay6 = (RelativeLayout) findViewById(R.id.bills_lay6);
//		lay6.setVisibility(View.VISIBLE);
//
//	 tableFixHeaders = (TableFixHeaders) findViewById(R.id.table);
//
//
//
//		linearLayout1 = (LinearLayout) findViewById(R.id.linearlayout1);
//		linearLayout2 = (LinearLayout) findViewById(R.id.linearlayout2);
//		linearLayout3 = (LinearLayout) findViewById(R.id.linearlayout3);
//		linearLayout4 = (LinearLayout) findViewById(R.id.linearlayout4);
//		linearLayout5 = (LinearLayout) findViewById(R.id.linearlayout5);
//		linearLayout6 = (LinearLayout) findViewById(R.id.linearlayout6);
//		arrowImageView1 = (ImageView) findViewById(R.id.bills_arrow1);
//		arrowImageView2 = (ImageView) findViewById(R.id.bills_arrow2);
//		arrowImageView3 = (ImageView) findViewById(R.id.bills_arrow3);
//		arrowImageView4 = (ImageView) findViewById(R.id.bills_arrow4);
//		arrowImageView5 = (ImageView) findViewById(R.id.bills_arrow5);
//		arrowImageView6 = (ImageView) findViewById(R.id.bills_arrow6);
//		reLayouts = new RelativeLayout[] { lay1, lay2, lay3, lay4, lay5,lay6 };
//		// 设置点击事件监听
//		for (RelativeLayout reLayout : reLayouts) {
//			reLayout.setOnClickListener(this);
//		}
//		// 初始化GridGenerator
//		gridGenerator = new GridGeneratorPact(this);
//
//		// linearLayout5.addView(gridGenerator.generateGrid(null,
//		// getWindowManager().getDefaultDisplay().getWidth() - 20));
//
//	}
//
//	/**
//	 * 初始化数据
//	 */
//	private void initData() {
//
//		usereId = EnvironmentVariable.getProperty(Constants.CHAT_USER_ID);// 用户id
//		userName = EnvironmentVariable.getUserName();// 用户名
//		password = EnvironmentVariable.getProperty(Constants.CHAT_PASSWORD);// 用户密码
//		// 根据itemid请求数据
//		if (flowRowSet != null) {
//
//			Log.i(TAG, itemId);
//			Log.i(TAG, "systemtype:" + systemType);
//
//			loadDataByNet();
//		} else {
//			Toast.makeText(this, "获取数据失败！", Toast.LENGTH_SHORT).show();
//		}
//
//	}
//
//	@Override
//	public void onClick(View v) {
//
//		int id = v.getId();
//		if (v == mTitleBar.getLogoView()) {
//			BaseApp.actManager.removeActivity(TAG);
//		} else if (id == R.id.bills_lay1) {
//			showOrHideGridLay(linearLayout1, arrowImageView1, title1);
//		} else if (id == R.id.bills_lay2) {
//			showOrHideGridLay(linearLayout2, arrowImageView2, title2);
//		} else if (id == R.id.bills_lay3) {
//			showOrHideGridLay(linearLayout3, arrowImageView3, title3);
//		} else if (id == R.id.bills_lay4) {
//			showOrHideGridLay(linearLayout4, arrowImageView4, title4);
//		} else if (id == R.id.bills_lay5) {
//			showOrHideGridLay(linearLayout5, arrowImageView5, title5);
//		} else if (id == R.id.bills_lay6) {
//			showOrHideGridLay(linearLayout6, arrowImageView6, title6);
//		}
//	}
//
//	/**
//	 * 显示或者隐藏gridlayout
//	 *
//	 * @param layout
//	 */
//	private void showOrHideGridLay(LinearLayout layout, ImageView imageView,
//			TextView textView) {
//		if (layout.getVisibility() == View.VISIBLE) {
//			layout.setVisibility(View.GONE);
//			textView.setTextColor(this.getResources().getColor(R.color.shenhui));
//			imageView.setImageDrawable(this.getResources().getDrawable(
//					R.drawable.bills_dowm));
//		} else if (layout.getVisibility() == View.GONE) {
//			layout.setVisibility(View.VISIBLE);
//			textView.setTextColor(this.getResources().getColor(
//					R.color.billdetail_title));
//			imageView.setImageDrawable(this.getResources().getDrawable(
//					R.drawable.bills_top));
//		}
//
//	}
//
//	/**
//	 * 通过网络 加载数据
//	 */
//	private void loadDataByNet() {
//		new AsyncTask<Void, Void, JResponseObject>() {
//
//			@Override
//			protected void onPreExecute() {
//				super.onPreExecute();
//				// 加载dialog显示
//				LoadingDataUtilBlack.show(ContractDetailActivity.this);
//			}
//
//			@Override
//			protected JResponseObject doInBackground(Void... params) {
//
//				JResponseObject RO = null;
//				// 判断是OA的还是合同的详情信息，以此来向服务器请求数据
//				if (systemType.equals("OA")) {
//					RO = getOADate(itemId);
//				} else if (systemType.equals("CONTRACT")) {
//					RO = getContactDate(itemId);
//				}
//				return RO;
//			}
//
//			@SuppressWarnings({ "unused", "unchecked" })
//			@Override
//			protected void onPostExecute(JResponseObject result) {
//
//				super.onPostExecute(result);
//
//				if (result != null) {
//					List<List<EFRowSet>> lists = new ArrayList<List<EFRowSet>>();
//					Map<String, Map<String, EFDataSet>> map = result
//							.getResponseMap();
//					// 1 DataSet
//					EFDataSet dataSet = (EFDataSet) map.get(systemType);
//
//					inintRoInfo(dataSet, 0, linearLayout1, title1);// 显示 基础信息到界面
//					inintRoInfo(dataSet, 1, linearLayout2, title2);// 显示 显示全部到界面
//					inintRoInfo(dataSet, 2, linearLayout3, title3);// 显示 扩展字段到界面
//
//					inintRoInfo(dataSet, 3, linearLayout4, title4);// 显示 流转信息到界面
//					inintRoInfo(dataSet, 4, linearLayout5, title5);// 显示审批转信息到界面
//					inintRoInfo(dataSet, 5, linearLayout6, title6);// 显示审批转信息到界面
//
//				}
//				LoadingDataUtilBlack.dismiss();
//
//			}
//
//		}.execute();
//
//	}
//
//	/**
//	 * * 初始化每个LinearLayout信息
//	 *
//	 * @param dataSet
//	 * @param position
//	 *            第几个LinearLayout 从0开始
//	 * @param linearLayout
//	 */
//	@SuppressWarnings({ "unchecked", "deprecation" })
//	protected void inintRoInfo(EFDataSet dataSet, int position,
//			LinearLayout linearLayout, TextView titleView) {
//		List<List<EFRowSet>> lists = new ArrayList<List<EFRowSet>>();
//		// 2 RowSet
//		EFRowSet rowSet = dataSet.getRowSet(position);
//		// 得到每一个LinearLayout的标题
//		String title = (String) rowSet.getDataMap().get("title");
//		titleView.setText(title);
//		System.out.println("--------------" + title);
//		// 3 DataSet
//		if (rowSet.getDataSetContList().size() > 0) {
//			for (int i = 0; i < rowSet.getDataSetContList().size(); i++) {
//				List<EFRowSet> rowSets = new ArrayList<EFRowSet>();
//				EFDataSet dataSet2 = (EFDataSet) rowSet.getDataSetContList()
//						.get(i);
//				rowSets = dataSet2.getRowSetArray();
//				lists.add(rowSets);
//
//			}
//		}
//		System.out.println("lists的大小" + lists.size());
//		if (lists.size() > 0) {
//
//			for (int i = 0; i < lists.size(); i++) {
//				// List<EFRowSet> ccEfRowSets = lists.get(i);
//				// 判断得到的List<EFRowSet> 是否为空， 审批意见这里最后一个为空，会报空指针，所以要加这个判断
//				if (lists.get(i) != null) {
//					// 如果是相对人信息的数据，需要生成表格
//
//					if (position == 3) {
//                        builderForm(lists.get(i),i);
//
//					} else {
//						View view = new View(ContractDetailActivity.this);
//						view = gridGenerator.generateGrid(lists.get(i),
//								getWindowManager().getDefaultDisplay()
//										.getWidth() - 20);
//						if (view != null) {
//							linearLayout.addView(view);
//						} else {
//							view = new View(ContractDetailActivity.this);
//							LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
//									LayoutParams.MATCH_PARENT, 30);
//							view.setLayoutParams(params);
//							view.setBackgroundColor(this.getResources()
//									.getColor(R.color.white));
//							linearLayout.addView(view);
//						}
//					}
//				}
//			}
//		}
//	}
//
//
//	/***
//	 * 生成表格
//	 * @param list
//	 */
//	private void builderForm(List<EFRowSet> list,int t) {
//
//		for (int i = 0; i < list.size(); i++) {
//			Map attrMap = list.get(i).getAttriMap();
//			if (t== 0) {
//				for (int j = 0; j <6 ; j++) {
//					attr[0][j] = (String) attrMap.get("td"+j);
//
//				}
//			}else if(t== 1) {
//				attr[1][0] = (String) attrMap.get("title");
//				attr[1][1] = (String) attrMap.get("td"+1);
//				attr[1][2] = (String) attrMap.get("td"+2);
//				attr[1][3] = (String) attrMap.get("td"+3);
//				attr[1][4] = (String) attrMap.get("td"+4);
//				attr[1][5] = (String) attrMap.get("td"+5);
//
//				for (int j = 0; j < 2; j++) {
//					for (int b = 0; b < 6; b++) {
//					System.out.println(j+ " "+ b+" "+attr[j][b]);
//				}
//
//				}
//				MatrixTableAdapter<String> matrixTableAdapter = new MatrixTableAdapter<String>(ContractDetailActivity.this,attr);
//				tableFixHeaders.setAdapter(matrixTableAdapter);
//			}
//
//		}
//	}
//
//	/**
//	 * 生成附件的textview
//	 *
//	 * @param list
//	 */
//
//	@SuppressWarnings("unchecked")
//	private void builderTextview(List<EFRowSet> list) {
//		for (EFRowSet rowSet : list) {
//			Map<String, String> attrMap = rowSet.getAttriMap();
//			String text = null;
//			final String url = attrMap.get("value");
//			;
//			TextView textView = new TextView(ContractDetailActivity.this);
//			if (attrMap.containsKey("title")) {
//				text = (String) attrMap.get("title");
//				textView.setTextColor(Color.BLUE);
//			} else if (attrMap.containsKey("td0")) {
//				text = (String) attrMap.get("td0");
//				textView.setTextColor(Color.BLACK);
//			}
//
//			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
//					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
//			params.setMargins(0, 0, 0, 3);
//			textView.setLayoutParams(params);
//			textView.setText(text);
//			textView.setPadding(10, 40, 10, 40);
//			textView.setTextSize(16);
//			textView.setGravity(Gravity.CENTER);
//			textView.setBackgroundColor(this.getResources().getColor(
//					R.color.billdetail_form1));
//			textView.setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//					Log.i(TAG, "364url:" + url);
//
//					if (url != null && !url.equals("")) {
//						DownAttachUtil downAttachUtil = new DownAttachUtil(
//								ContractDetailActivity.this, url);
//						downAttachUtil.taskStart();
//					}
//
//				}
//			});
//
//			// 每个textview后面的横线
//			View view = new View(ContractDetailActivity.this);
//			LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(
//					LayoutParams.MATCH_PARENT, 1);
//			view.setLayoutParams(params1);
//			view.setBackgroundColor(this.getResources().getColor(R.color.black));
//			linearLayout3.addView(textView);
//			linearLayout3.addView(view);
//		}
//
//	}
//
//	/**
//	 * 获取公文审批（OA）数据
//	 *
//	 * @return
//	 */
//	private JResponseObject getOADate(String itemId) {
//		// 创建PO
//		JParamObject PO = JParamObject.Create();
//		JResponseObject RO = null;
//		String tokenConId = (String) EnvironmentVariable.getProperty("OATokenId", "");
//
//		PO.SetValueByParamName("WB_SYS_KEY", "OA");
//		PO.SetValueByParamName("OA_params_userid", usereId);
//		PO.SetValueByParamName("OA_params_pwd", password);
//		PO.SetValueByParamName("OA_params_type", "db");
//		PO.SetValueByParamName("OA_params_tokenIDFromMoblie", tokenConId);
//		PO.SetValueByParamName("OA_params_itemId", itemId);
//		PO.SetValueByParamName("OA_systemmethod", "GetDetailHandler");
//
//		try {
//			// 连接服务器
//			RO = EAI.DAL.SVR("ExternalInterfaceDataService", PO);
//			System.out.println("..........");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return RO;
//	}
//
//	/**
//	 * 获取合同数据
//	 *
//	 * @return
//	 */
//	private JResponseObject getContactDate(String itemId) {
//		// 创建PO
//		JParamObject PO = JParamObject.Create();
//		JResponseObject RO = null;
//		String tokenConId = (String) EnvironmentVariable.getProperty("ContractTokenId",
//				"");
//		PO.SetValueByParamName("WB_SYS_KEY", "CONTRACT");
//		// PO.SetValueByParamName("CONTRACT_params_userid", usereId);
//		// PO.SetValueByParamName("CONTRACT_params_pwd", password);
//		PO.SetValueByParamName("CONTRACT_params_type", "1");
//		PO.SetValueByParamName("CONTRACT_params_msgid", itemId);
//		PO.SetValueByParamName("CONTRACT_params_tokenIDFromMoblie", tokenConId);
//		PO.SetValueByParamName("CONTRACT_systemmethod",
//				"GetApprovalDetailHandler");
//
//		try {
//			// 连接服务器
//			RO = EAI.DAL.SVR("ExternalInterfaceDataService", PO);
//			System.out.println("..........");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return RO;
//	}
//
//
//
//}