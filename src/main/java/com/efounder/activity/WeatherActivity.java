//package com.efounder.activity;
//
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.content.Intent;
//import android.graphics.Color;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.Window;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.efounder.forwechat.BaseApp;
//import com.efounder.ospmobilelib.R;
//import com.efounder.util.DateUtil;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//
//public class WeatherActivity extends Activity implements OnClickListener {
//	/*public LocationClient mLocationClient = null;
//	public BDLocationListener myListener = new MyLocationListener();
//	private BDLocation bdLocation;*/
//	LayoutInflater inflater;
//	private ImageView aboutImageView, settingImageView, suggestImageView;// 关于，设置，建议
//	private ImageView weatherImageView;// 天气图片
//	// 时间 ，周几，最高最低温度，天气状况文字,温度,地点
//	private TextView timeTextView, weekDayView, hlWenDuView, weatherTextView,
//			wenduTextView, addressTextView;
//	public static final String TAG = "WeatherActivity";
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		 //设置无标题
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//
//
//		setContentView(R.layout.myweather);
//		BaseApp.actManager.putActivity(TAG, this);
//		RelativeLayout include = (RelativeLayout) findViewById(R.id.include);
//
//		include.setBackgroundResource(R.color.red_ios);
//
//		LinearLayout leftbacklayout = (LinearLayout) findViewById(R.id.leftbacklayoutimage);
//		leftbacklayout.setVisibility(View.VISIBLE);
//		Button closeButton = (Button) findViewById(R.id.closeButton);
//		closeButton.setVisibility(View.VISIBLE);
//		closeButton.setBackgroundResource(R.drawable.close);
//		closeButton.setOnClickListener(this);
//		TextView title = (TextView) findViewById(R.id.fragmenttitle);
//		title.setText("主页");
//		title.setTextColor(Color.WHITE);
//		include.setVisibility(View.VISIBLE);
//
//		//FragmentManager fManager=getFragmentManager();
//
///*		mLocationClient = new LocationClient(WeatherActivity.this); // 声明LocationClient类
//		mLocationClient.registerLocationListener(myListener);*/
//
//		initLocation();
//		initView();
//
//	}
//
//	private void initView() {
//
//		weatherTextView = (TextView) findViewById(R.id.pop_weathwer);
//		addressTextView = (TextView) findViewById(R.id.pop_address);
//
//		wenduTextView = (TextView) findViewById(R.id.wendu);
//		wenduTextView.setOnClickListener(this);
//		aboutImageView = (ImageView) findViewById(R.id.but1);
//		aboutImageView.setOnClickListener(this);
//		settingImageView = (ImageView) findViewById(R.id.but2);
//		settingImageView.setOnClickListener(this);
//		suggestImageView = (ImageView) findViewById(R.id.but3);
//		suggestImageView.setOnClickListener(this);
//		weatherImageView=(ImageView) findViewById(R.id.imageView1);
//		timeTextView = (TextView) findViewById(R.id.pop_time);
//		String date = DateUtil.getCurrentDate("yyyy.MM.dd");
//		timeTextView.setText(date);
//		weekDayView = (TextView) findViewById(R.id.pop_weekday);
//		String weekday = DateUtil.getWeekNumber(date, "yyyy.MM.dd");
//		weekDayView.setText(weekday);
//		hlWenDuView = (TextView) findViewById(R.id.pop_hltemp);
//
//	}
//
//
//
//	private void initLocation() {
//		/*LocationClientOption option = new LocationClientOption();
//		option.setLocationMode(LocationMode.Hight_Accuracy);// 可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
//		option.setCoorType("bd09ll");// 可选，默认gcj02，设置返回的定位结果坐标系
//		int span = 1000;
//		option.setScanSpan(0);// 可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
//		option.setIsNeedAddress(true);// 可选，设置是否需要地址信息，默认不需要
//		option.setOpenGps(true);// 可选，默认false,设置是否使用gps
//		option.setLocationNotify(true);// 可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
//		option.setIsNeedLocationDescribe(true);// 可选，默认false，设置是否需要位置语义化结果，可以在BDLocation.getLocationDescribe里得到，结果类似于“在北京天安门附近”
//		option.setIsNeedLocationPoiList(true);// 可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
//		option.setIgnoreKillProcess(false);// 可选，默认false，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认杀死
//		option.SetIgnoreCacheException(false);// 可选，默认false，设置是否收集CRASH信息，默认收集
//		option.setEnableSimulateGps(false);// 可选，默认false，设置是否需要过滤gps仿真结果，默认需要
//		mLocationClient.setLocOption(option);
//		mLocationClient.start();*/
//	}
//
///*	public class MyLocationListener implements BDLocationListener {
//
//		@Override
//		public void onReceiveLocation(BDLocation location) {
//			if (null == location) {
//				if (bdLocation != null) {
//					bdLocation = null;
//				}
//			}
//			bdLocation = location;
//			// Receive Location
//			StringBuffer sb = new StringBuffer(256);
//			sb.append("time : ");
//			sb.append(location.getTime());
//			sb.append("\nerror code : ");
//			sb.append(location.getLocType());
//			sb.append("\nlatitude : ");
//			sb.append(location.getLatitude());
//			sb.append("\nlontitude : ");
//			sb.append(location.getLongitude());
//			sb.append("\nradius : ");
//			sb.append(location.getRadius());
//			if (location.getLocType() == BDLocation.TypeGpsLocation) {// GPS定位结果
//				sb.append("\nspeed : ");
//				sb.append(location.getSpeed());// 单位：公里每小时
//				sb.append("\nsatellite : ");
//				sb.append(location.getSatelliteNumber());
//				sb.append("\nheight : ");
//				sb.append(location.getAltitude());// 单位：米
//				sb.append("\ndirection : ");
//				sb.append(location.getDirection());// 单位度
//				sb.append("\naddr : ");
//				sb.append(location.getAddrStr());
//				sb.append("\ndescribe : ");
//				sb.append("gps定位成功");
//				requestWeather();
//
//			} else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {// 网络定位结果
//				sb.append("\naddr : ");
//				sb.append(location.getAddrStr());
//				// 运营商信息
//				sb.append("\noperationers : ");
//				sb.append(location.getOperators());
//				sb.append("\ndescribe : ");
//				sb.append("网络定位成功");
//				requestWeather();
//			} else if (location.getLocType() == BDLocation.TypeOffLineLocation) {// 离线定位结果
//				sb.append("\ndescribe : ");
//				sb.append("离线定位成功，离线定位结果也是有效的");
//			} else if (location.getLocType() == BDLocation.TypeServerError) {
//				sb.append("\ndescribe : ");
//				sb.append("服务端网络定位失败，可以反馈IMEI号和大体定位时间到loc-bugs@baidu.com，会有人追查原因");
//			} else if (location.getLocType() == BDLocation.TypeNetWorkException) {
//				sb.append("\ndescribe : ");
//				sb.append("网络不同导致定位失败，请检查网络是否通畅");
//			} else if (location.getLocType() == BDLocation.TypeCriteriaException) {
//				sb.append("\ndescribe : ");
//				sb.append("无法获取有效定位依据导致定位失败，一般是由于手机的原因，处于飞行模式下一般会造成这种结果，可以试着重启手机");
//			}
//			sb.append("\nlocationdescribe : ");
//			sb.append(location.getLocationDescribe());// 位置语义化信息
//			List<Poi> list = location.getPoiList();// POI数据
//			if (list != null) {
//				sb.append("\npoilist size = : ");
//				sb.append(list.size());
//				for (Poi p : list) {
//					sb.append("\npoi= : ");
//					sb.append(p.getId() + " " + p.getName() + " " + p.getRank());
//				}
//			}
//			Log.i("BaiduLocationApiDem", sb.toString());
//		}
//	}
//*/
//	private void requestWeather() {
//		final Handler myHandler = new Handler() {
//			@SuppressLint("NewApi")
//			@Override
//			public void handleMessage(Message msg) {
//				JSONObject object = (JSONObject) msg.obj;
//				if (msg != null && null != object) {
//					try {
//
//						int code = object.getInt("errNum");
//						String states = object.getString("errMsg");
//						if (code == 0 && states.equals("success")) {
//
//							JSONObject result = object.getJSONObject("retData");
//							String temp = result.getString("temp");
//							String l_temp = result.getString("l_tmp");
//							String h_temp = result.getString("h_tmp");
//							wenduTextView.setText(temp);
//							hlWenDuView.setText(l_temp + "℃" + "/" + h_temp
//									+ "℃");
//							String theCity = result.getString("city")
//									.toString();
//							String weather = result.getString("weather")
//									.toString();
//							weatherTextView.setText(weather);
//							addressTextView.setText(theCity);
//							weatherImageView.setBackground(getResources().getDrawable(setWeatherImage(weather)));
//
//
//						} else {
//							Log.d("WeatherActivity", "天气信息获取失败,code=" + code);
//						}
//					} catch (JSONException e) {
//						Log.d("WeatherActivity", "天气信息Json解析错误");
//						e.printStackTrace();
//					}
//				}
//				super.handleMessage(msg);
//			}
//
//		};
///*
//		new Thread(new Runnable() {
//			public void run() {
//				JSONObject result = getWeatherJson();
//				Message msg = Message.obtain();
//				msg.obj = result;
//				myHandler.sendMessage(msg);
//			}
//
//			private JSONObject getWeatherJson() {
//				// TODO location获取成功调用requestWeather 所以不存在location为空的情况
//				String city = bdLocation.getCity();
//
//				if (city.contains("市")) {
//					city = city.replace("市", "");
//
//				}
//				// 拼凑百度天气请求的完整url
//				StringBuffer url = new StringBuffer();
//
//				url.append(getString(R.string.url_weather));
//
//				try {
//					Log.i("WeatherActivity", url.toString());
//					city = URLEncoder.encode(city, "UTF-8");
//
//				} catch (UnsupportedEncodingException e1) {
//
//					e1.printStackTrace();
//				}
//				url.append(city);
//
//
//				URL urlCon;
//				HttpURLConnection conn;
//				JSONObject json = null;
//				BufferedReader reader = null;
//				StringBuffer sbf = new StringBuffer();
//				String result = null;
//				try {
//					urlCon = new URL(url.toString());
//					HttpURLConnection connection = (HttpURLConnection) urlCon
//							.openConnection();
//					connection.setRequestMethod("GET");
//					// 填入apikey到HTTP header
//					connection.setRequestProperty("apikey",
//							"781bb539a173cbac28cb04f6605ed414");
//					connection.connect();
//					InputStream is = connection.getInputStream();
//					reader = new BufferedReader(new InputStreamReader(is,
//							"UTF-8"));
//					String strRead = null;
//					while ((strRead = reader.readLine()) != null) {
//						sbf.append(strRead);
//						sbf.append("\r\n");
//					}
//					reader.close();
//					result = sbf.toString();
//					Log.i("xxx", result);
//					json = new JSONObject(result);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//
//				return json;
//			}
//		}).start();*/
//
//	}
//
//	@Override
//	public void onClick(View v) {
//		if (v.getId() == R.id.closeButton) {
//			if (WeatherActivity.this != null
//					&& !WeatherActivity.this.isFinishing()) {
//
//				WeatherActivity.this.finish();
//			}
//		}else if (v.getId() == R.id.but1) {// 关于
//			Intent intent1 = new Intent(WeatherActivity.this,
//					//ShareActivity.class);
//					AboutActivityNew.class);
//			startActivity(intent1);
//				/*ShareSDK.initSDK(this);
//				OnekeyShare oks = new OnekeyShare();
//				oks.setTitle("普光移动应用--测试标题");
//				oks.setTitleUrl("http://mob.com");
//				oks.setText("普光移动办公平台---测试文本");
//				oks.setImageUrl("http://f1.sharesdk.cn/imgs/2014/02/26/owWpLZo_638x960.jpg");
//
//				// oks.setSilent(true); // 隐藏编辑页面
//
//				oks.setSilent(false); // 显示编辑页面
//				// oks.setImageUrl("http://f1.sharesdk.cn/imgs/2014/02/26/owWpLZo_638x960.jpg");
//				oks.show(this);*/
//		}else if (v.getId() == R.id.but2) {// 设置
//			Intent intent2=new Intent();
//			intent2.putExtra("fragname", "settingfragment");
//			intent2.setClass(WeatherActivity.this, WeatherSkipActivity.class);
//			startActivity(intent2);
//					 /* Intent intent2=new Intent();
//						intent2.putExtra("fragname", "contactfragment");
//						intent2.setClass(WeatherActivity.this, WeatherSkipActivity.class);
//						startActivity(intent2);*/
//		}else if (v.getId() == R.id.but3) {//建议
//			Intent intent3=new Intent();
//			intent3.putExtra("fragname", "suggestfragment");
//			intent3.setClass(WeatherActivity.this, WeatherSkipActivity.class);
//			startActivity(intent3);
//		}
//
//	}
//
//
//	//根据天气设置图片
//	public  int setWeatherImage(String weather){
//		if (weather.equals("晴")) {
//			return R.drawable.weather_fine;
//		}else if (weather.equals("多云") || weather.equals("多云转晴")) {
//			return R.drawable.weather_cloudsun;
//		}else if (weather.equals("雾") || weather.equals("雾转晴")) {
//			return R.drawable.weather_fine;
//		}else if (weather.contains("雨") ) {
//			return R.drawable.weather_rain;
//		}else if (weather.contains("雪")) {
//			return R.drawable.weather_snow;
//		}else if (weather.equals("雷阵雨") ) {
//			return R.drawable.weather_thunder;
//		}
//		else if (weather.equals("阴") ) {
//			return R.drawable.weather_cloud;
//		}
//
//		/*if (weather.equals("晴") || weather.equals("晴转多云") || weather.equals("晴")) {
//			return R.drawable.weather_fine;
//		}else if (weather.equals("多云") || weather.equals("多云转晴")) {
//			return R.drawable.weather_cloud;
//		}else if (weather.equals("雾") || weather.equals("雾转晴")) {
//			return R.drawable.weather_fine;
//		}else if (weather.equals("雨") || weather.equals("阵雨转阴")|| weather.equals("多云转阵雨")|| weather.equals("阵雨转多云")||
//				weather.equals("小雨转多云")|| weather.equals("小雨转阴")) {
//			return R.drawable.weather_rain;
//		}else if (weather.equals("雪")) {
//			return R.drawable.weather_snow;
//		}else if (weather.equals("雷阵雨") ) {
//			return R.drawable.weather_thunder;
//		}*/
//
//		return R.drawable.weather_fine;
//
//	}
//
//	@Override
//	protected void onDestroy() {
//		// TODO Auto-generated method stub
//		super.onDestroy();
//		//mLocationClient.stop();
//	}
//}
