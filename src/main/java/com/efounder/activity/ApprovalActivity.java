package com.efounder.activity;

/**
 * @author  ruanjianjiagou@163.com
 * @date 2015.1.24
 */

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import android.view.Gravity;
import android.widget.ListView;
import android.widget.Toast;

import com.efounder.adapter.TimelineAdapter;
import com.efounder.builder.base.data.EFDataSet;
import com.efounder.builder.base.data.EFRowSet;
import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;
import com.efounder.ospmobilelib.R;
import com.efounder.util.CommonPo;
import com.efounder.util.LoadingDataUtilBlack;
import com.efounder.view.titlebar.AbTitleBar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @version 1.0
 * {@link}
 * </p>
 *
 */
public class ApprovalActivity extends AbActivity {

	private ListView listView;
	List<String> data ;
	private TimelineAdapter timelineAdapter;
	AbTitleBar mTitleBar;
	
	private BackgroundAsyncTask backgroundAsyncTask;
	/*
	 * 连接服务器，并获得返回结果 po，ro
	 */
	private JParamObject PO;
	private JResponseObject RO;
	List<Map<String, Object>> list;
	
	private EFRowSet flowRowSet ;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setAbContentView(R.layout.approval_listview);
		mTitleBar = getTitleBar();
		mTitleBar.setTitleText("审批流程");
		
        mTitleBar.setLogo(R.drawable.ef_title_view_back);
		mTitleBar.setTitleTextColor(R.color.title_TextColor);
		mTitleBar.setTitleBarBackground(R.color.title_Background);
       
		mTitleBar.setTitleBarGravity(Gravity.CENTER, Gravity.CENTER);
		listView = (ListView) this.findViewById(R.id.listview);
		listView.setDividerHeight(0);
		
		Bundle flowRowSetBundle = getIntent().getBundleExtra("data");
		if(flowRowSetBundle!=null){
			flowRowSet = (EFRowSet) flowRowSetBundle.get("data");
		}
		
		backgroundAsyncTask = new BackgroundAsyncTask();
		backgroundAsyncTask.execute();
		
	}


	
//	----------------------------------
	/*
	 * 内部asyncTask类
	 */
	class BackgroundAsyncTask extends AsyncTask<Void, Integer, JResponseObject> {

		// 进度条显示
		protected void onPreExecute() {
			// 进度条显示
			super.onPreExecute();
			// 加载dialog显示
			LoadingDataUtilBlack.show(ApprovalActivity.this);
		}

		@Override
		protected JResponseObject doInBackground(Void... params) {
			PO = CommonPo.getPo(ApprovalActivity.this);
			Map<String ,Object> map  = flowRowSet.getDataMap();
			
			PO.SetValueByParamName("FLOW_ID",(String) map.get("FLOW_ID"));
			PO.SetValueByParamName("MDL_ID",(String) map.get("MDL_ID"));
			PO.SetValueByParamName("OBJ_GUID",(String) map.get("OBJ_GUID"));
			PO.SetValueByParamName("NODE_ID",(String) map.get("NODE_TAG"));
			PO.SetValueByParamName("BIZ_MDL",(String) map.get("MDL_ID"));
			PO.SetValueByParamName("OP_ID",(String) map.get("OP_ID"));
			PO.SetValueByParamName("BIZ_LOGIN_UNIT",(String) map.get("BIZ_UNIT"));
			PO.SetValueByParamName("TASK_UNIT",(String) map.get("TASK_UNIT"));
			PO.SetValueByParamName("BIZ_UNIT",(String) map.get("BIZ_UNIT"));
			PO.SetValueByParamName("BIZ_DJBH",(String) map.get("BIZ_DJBH"));
			PO.SetValueByParamName("NODE_TAG_NAME",(String) map.get("NODE_TAG_NAME"));
			PO.SetValueByParamName("OP_USER_NAME",(String) map.get("OP_USER_NAME"));
			PO.SetValueByParamName("BIZ_DATE",(String) map.get("BIZ_DATE"));
			PO.SetValueByParamName("UserCaption",(String) map.get("OP_SUBMIT_NAME"));
			PO.SetValueByParamName("OP_LEVEL","00");
			PO.SetValueByParamName("OP_PROC_NOTE","同意_虚拟提交");
			
			
			LoadingDataUtilBlack.getDialog().setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface arg0) {
				// TODO Auto-generated method stub
				backgroundAsyncTask.cancel(true);
			  }
		    }); 
			try {
				RO = EAI.DAL.IOM("FlowTaskService", "autoSubmitTaskMachine", PO);
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
			
			CommonPo.setPoToNull();
			return RO;
		}

		// 当后台操作结束时，此方法将会被调用，计算结果将做为参数传递到此方法中，直接将结果显示到UI组件上
		protected void onPostExecute(JResponseObject result) {
		
			// RO返回值为空
			if (result == null) {
				Toast toast = Toast.makeText(ApprovalActivity.this,
						"连接服务器异常", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				LoadingDataUtilBlack.dismiss();
				toast.show();
				return;
			}
			if (result.ErrorCode == -1) {
				AlertDialog.Builder alert = new AlertDialog.Builder(ApprovalActivity.this);
				alert.setTitle("确认");
				alert.setMessage(result.ErrorString);
				alert.setPositiveButton("是", null);
				LoadingDataUtilBlack.dismiss();
				alert.show();
				return;
			}
			LoadingDataUtilBlack.dismiss();
			list = new ArrayList<Map<String, Object>>();
			
			Map<String, Object> map = new HashMap<String, Object>();
			
			list.add(map);
			
		/*	EFDataSet taskDataSet = (EFDataSet) result.ResponseObject;
			if (taskDataSet == null || taskDataSet.getRowCount() == 0)
			return ;*/
			Map<String ,EFDataSet> map1 = (Map<String, EFDataSet>) result.ResponseObject;
			EFDataSet efdata = map1.get("nodeTaskDataSet");
			Map<String ,EFDataSet> map2 = efdata.getDataSetMap();
			EFDataSet efdata1 = map2.get("FLOW_TASK_LIST");
			for(int i=0;i<efdata1.getRowCount();i++){
				EFRowSet ef = efdata1.getRowSet(i);
				//{EXT_NUM05=0.0, OP_PROC_NAME=孙二召, EXT_NUM04=0.0, NODE_TAG_NAME=开始, EXT_NUM07=0.0, EXT_NUM06=0.0, EXT_NUM01=0.0, EXT_NUM03=0.0, EXT_NUM02=0.0, OP_USER=P0038, TASK_TO_USER=, EXT_DAT09=2014-12-22 00:00:00.0, EXT_DAT08=2014-12-22 00:00:00.0, EXT_DAT07=2014-12-22 00:00:00.0, _Self_RowSet=, PREVIEW_SUBMIT=0, EXT_DAT02=2014-12-22 00:00:00.0, EXT_DAT01=2014-12-22 00:00:00.0, EXT_DAT10=2014-12-22 00:00:00.0, EXT_DAT06=2014-12-22 00:00:00.0, EXT_DAT05=2014-12-22 00:00:00.0, EXT_DAT04=2014-12-22 00:00:00.0, NODE_TAG=start, EXT_DAT03=2014-12-22 00:00:00.0, BIZ_SUBMIT_USER=P0038, RESR_STATUS=processed, EXT_NUM08=0.0, NODE_SRC_NAME=, EXT_NUM09=0.0, TASK_TO_UNIT=P0001001, EXT_STR10=, OP_ID=1419207431839, BIZ_UNIT=P0001001, TASK_UNIT_NAME=经理办公室, BIZ_SUBMIT_UNIT=P0001001, OP_PROC_USER=P0038, PFLOW_ID=, TASK_UNIT=P0001001, OP_MODE=manual, AUTO_TIME=0.0, FLOW_ID=HY_HYSQ_FLOW, EXT_STR09=, EXT_STR08=, NODE_SRC=, EXT_STR07=, EXT_STR06=, EXT_STR05=, TASK_NAME=开始, OBJ_GUID=E2D02EFA-C6D7-4D37-31F8-6F5EB09457A2, RESR_IN_CAUSE=create, EXT_STR04=, EXT_STR03=, EXT_STR02=, OP_USER_NAME=孙二召, EXT_STR01=, TASK_SHOW=1, OP_RULE=, LOOP_ID=0, OP_SUBMIT_USER=P0038, OP_GUID=1419207431839, BIZ_DATE=201412, RESR_OUT_CAUSE=submit, TASK_WEIGHT=0.0, OP_ORDER=0.0, FLEX_FORM_PARAM=FLEX_PROPERTY_FORM=HY_HYSQB_ZD.form1=HY_HYSQB_ZD.form2;propertyViewWidth=300;showLeftSideView=0, TASK_TO_UNIT_NAME=经理办公室, OP_PROC_TIME=1.419207432198E12, TASK_STATUS=processing, OP_EDGES=R_3;, TASK_VIRTUAL=0, BIZ_DJBH=HYSQ20141200151, EXT_NUM10=0.0, OP_TIME=1.419207431839E12, FLEX_PROCESS_FORM=HY_HYSQB_ZD.form1, OP_GNBH=, OP_SUBMIT_NAME=孙二召, OP_PROC_NOTE=同意, FLOW_NAME=会议申请流程, MDL_ID=HY_HYSQModel}
				map = new HashMap<String, Object>();
				map.put("approver", ef.getObject("TASK_TO_UNIT_NAME", ""));
				map.put("preparer", ef.getObject("OP_USER_NAME", ""));
				map.put("make_date", ef.getObject("EXT_DAT09", ""));
				map.put("state", "2");
				map.put("note", "同意");
				list.add(map);
			}

			map = new HashMap<String, Object>();
		
			list.add(map);
			
			timelineAdapter = new TimelineAdapter(ApprovalActivity.this, list);
			listView.setAdapter(timelineAdapter);
			
		}

		// 进度条更新、
		protected void onProgressUpdate(Integer... values) {

			super.onProgressUpdate(values);
			//progressDialog.setProgress(values[0]);
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}
		 @Override
		protected void onCancelled(JResponseObject result) {
			// TODO Auto-generated method stub
			super.onCancelled(result);
		}
	}
}
