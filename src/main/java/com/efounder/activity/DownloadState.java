package com.efounder.activity;
public interface DownloadState {
	
	void onDownloadStart(Object object);
	
	void onDownloadOver(int msg);

}