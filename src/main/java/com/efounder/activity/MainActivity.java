//package com.efounder.activity;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.graphics.Color;
//import android.os.Bundle;
//import android.support.v4.view.ViewPager;
//import android.support.v4.view.ViewPager.LayoutParams;
//import android.support.v4.view.ViewPager.OnPageChangeListener;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.Gallery;
//import android.widget.ImageButton;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//
//import com.efounder.ospmobilelib.R;
//import com.efounder.adapter.ViewPagerAdapter;
//import com.efounder.forwechat.BaseApp;
//
///**
// * @author zhenglaikun 2014.11.24 主界面，实现viewpage滑动， 登陆，设置，关于 功能按钮
// */
//public class MainActivity extends Activity implements OnPageChangeListener,
//		OnClickListener {
//	public static final String TAG = "MainActivity";
//	private ViewPager viewPager;
//	private ViewGroup group;
//	/*
//	 * 装ImageView数组
//	 */
//	private ImageView[] views;
//	private ViewPagerAdapter adapter;
//
//	/*
//	 * 图片资源
//	 */
//	private int[] guideImages = { R.drawable.splash, R.drawable.ziyuan2,
//			R.drawable.yingyong3,R.drawable.renwu4,R.drawable.pengyou5 };
//
//	/*
//	 * 定义原点的imageview
//	 */
//	private ImageView[] points = new ImageView[guideImages.length];
//
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_main);
//		// 登陆按钮
//		Button loginButton = (Button) findViewById(R.id.button_login);
//		loginButton.setOnClickListener(new ButtonClickListener());
//
//		//设置按钮
//		ImageButton setting = (ImageButton) findViewById(R.id.image_settingbutton);
//		setting.setOnClickListener(new ButtonClickListener());
////		// 设置按钮
////		Button settingButton = (Button) findViewById(R.id.button_setting);
////		settingButton.setOnClickListener(new ButtonClickListener());
////		// 关于按钮
////		Button aboutButton = (Button) findViewById(R.id.button_about);
////		aboutButton.setOnClickListener(new ButtonClickListener());
//
//		//初始化控件
//		initView();
//		initData();
//	}
//
//	// 初始化view
//	private void initView() {
//		//这里viewgroup是一个linearLayout，用来动态添加imageView
//		group = (ViewGroup) findViewById(R.id.viewGroup);
//		// pointsLayout = (LinearLayout) findViewById(R.id.pointsLayout);
//		viewPager = (ViewPager) findViewById(R.id.viewPager);
//
//		views = new ImageView[guideImages.length];
//		adapter = new ViewPagerAdapter(views);/* 向ViewPagerAdapter即绑定数据类中传递参数views */
//	}
//
//	// 初始化数据,加载背景图片
//	private void initData() {
//		BaseApp.actManager.putActivity(TAG, this);
//		Gallery.LayoutParams layoutParams = new Gallery.LayoutParams(
//				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
//		for (int i = 0; i < guideImages.length; i++) {
//
//			ImageView imageView = new ImageView(this);
//			imageView.setImageResource(guideImages[i]);
//			imageView.setScaleType(ImageView.ScaleType.FIT_XY);
//			imageView.setLayoutParams(layoutParams);
//			views[i] = imageView;
//		}
//
//		// 初始化圆点
//		initPoint();
//		/*
//		 * 向ViewPagerAdapter即绑定数据类中传递参数views, 前面已对adapter初始化
//		 */
//		viewPager.setAdapter(adapter);
//		/* 设置viewPager的改变事件 */
//		viewPager.setOnPageChangeListener(this);
//	}
//
//	// 初始化圆点
//	private void initPoint() {
//		for (int i = 0; i < points.length; i++) {
//			ImageView imageView = new ImageView(this);
//			//设置原点大小
//			imageView.setLayoutParams(new ViewGroup.LayoutParams(10,10));
//			 points[i] = imageView;
//			if (i == 0) {
//				points[i].setBackgroundResource(R.drawable.radio_select);
//			} else {
//				points[i].setBackgroundResource(R.drawable.radio_normal);
//			}
//
//			//设置原点间距离
//			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
//					new ViewGroup.LayoutParams(LayoutParams.WRAP_CONTENT,
//							LayoutParams.WRAP_CONTENT));
//			layoutParams.leftMargin = 5;
//			layoutParams.rightMargin = 5;
//			//设置原点的点击事件
//			points[i].setOnClickListener(this);
//
//			//标记点击的是第几个
//			points[i].setTag(i);
//
//			group.addView(imageView, layoutParams);
//		}
//	}
//
//	// 这里是实现OnPageChangeListener的三个方法，用到了其中的onPageSelected
//	@Override
//	public void onPageScrollStateChanged(int arg0) {
//
//	}
//
//	@Override
//	public void onPageScrolled(int arg0, float arg1, int arg2) {
//
//	}
//
//	// 当新的页面被选中时调用
//	public void onPageSelected(int position) {
//
//		setImageBackground(position);
//	}
//
//	/**
//	 * 设置选中的points的背景
//	 *
//	 * @param selectItems
//	 */
//	private void setImageBackground(int selectItems) {
//		for (int i = 0; i < points.length; i++) {
//			if (i == selectItems) {
//				points[i].setBackgroundResource(R.drawable.radio_select);
//			} else {
//				points[i].setBackgroundResource(R.drawable.radio_normal);
//			}
//		}
//	}
//
//	// 点击事件的实现方法
//	public void onClick(View v) {
//		// 利用getTag方法来获取当前点击的原点
//		int i = (Integer) v.getTag();
//		// 设置当前显示页
//		viewPager.setCurrentItem(i, true);
//	}
//
//	/*
//	 * 登陆，设置，关于 按钮监听事件,跳转到selector界面
//	 *
//	 */
//	class ButtonClickListener implements OnClickListener {
//
//		@Override
//		public void onClick(View v) {
//			// TODO Auto-generated method stub
//			int id = v.getId();
////			Intent intent = new Intent(getApplicationContext(),
////					SelectorButtonActivity.class);
////			intent.putExtra("id", id);
////			startActivity(intent);
//		}
//	}
//
//	@Override
//	public void onBackPressed() {
//		if(BaseApp.actManager.containsName(MainActivity.TAG)){
//			BaseApp.exitActivity(MainActivity.TAG);
//		}
//		super.onBackPressed();
//	}
//}
