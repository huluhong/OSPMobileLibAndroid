package com.efounder.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.core.xml.StubObject;
import com.efounder.frame.baseui.BaseFragment;
import com.efounder.ospmobilelib.R;
import com.efounder.util.AbFragmentManager;
import com.efounder.util.StorageUtil;
import com.efounder.util.ToastUtil;

import java.util.Hashtable;

/**
 * 包含fragment的activity
 *
 * @author yqs
 */

public class FragmentContainActivity extends AbActivity {
    public static final String TAG = "FragmentContainActivity";
    BaseFragment baseFragment = null;

    private StubObject stubObject = null;
    private String viewType = null;
    private Hashtable menuTable;
    private FragmentManager fm;
    private StorageUtil storageUtil;
    private RelativeLayout includeLayout;
    /*
     * 设置信息初始化
     */
    private String port, service, address, path, APPID, sign;
    private String ServerURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        // requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.fragment_contains);
        includeLayout = (RelativeLayout) findViewById(R.id.include);

        // [parentId=zy, null, null, menuIcon=menu_icon_yx, null, id=email,
        // viewType=mail, caption=邮箱]
        stubObject = (StubObject) getIntent().getSerializableExtra("type");
        if (stubObject == null){
            ToastUtil.showToast(this,"抱歉，我们遇到了问题，请联系管理员");
            return;
        }
        menuTable = stubObject.getStubTable();
        if (menuTable == null){
            ToastUtil.showToast(this,"抱歉，我们遇到了问题，请联系管理员");
            return;
        }
        viewType = (String) menuTable.get("viewType");
        if (viewType==null){
            ToastUtil.showToast(this,"viewType为空");
            finish();
            return;
        }
        if (viewType.equals("FullScreenWebView")) {
            includeLayout.setVisibility(View.GONE);
        }
        String title = stubObject.getObject("caption", "").toString();
        TextView titleTextView = (TextView) this
                .findViewById(R.id.fragmenttitle);
        titleTextView.setText(title);

        LinearLayout leftbacklayout = (LinearLayout) this
                .findViewById(R.id.leftbacklayout);
        leftbacklayout.setVisibility(View.VISIBLE);
        leftbacklayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // NoticeFragment.this.getFragmentManager().popBackStack();
                finish();
            }
        });

        // 根据viewtype得到fragment
        // getFragmentFromType();
        AbFragmentManager abFragmentManager = new AbFragmentManager(this);
        baseFragment = abFragmentManager.getFragment(stubObject);
        baseFragment.setLeftButtonType("BACK");
        fm = getSupportFragmentManager();
        includeLayout.setVisibility(View.GONE);

        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        if (baseFragment != null) {
            fragmentTransaction.replace(R.id.fragment_contains, baseFragment);
        }
        fragmentTransaction.commit();
        Button menu_moreIV = (Button) this.findViewById(R.id.closeButton);

        if (baseFragment.getRightButtonType() == null || baseFragment.getRightButtonType().equals("")) {
           // menu_moreIV.setVisibility(View.GONE);
        } else if ("weather".equals(baseFragment.getRightButtonType())) {
            menu_moreIV.setVisibility(View.VISIBLE);
            menu_moreIV.setBackgroundResource(R.drawable.menu_icon_app);
        } else if ("share".equals(baseFragment.getRightButtonType())) {
            menu_moreIV.setVisibility(View.VISIBLE);
            menu_moreIV.setBackgroundResource(R.drawable.sharebutton);
        } else if ("suggest".equals(baseFragment.getRightButtonType())) {
            includeLayout.setVisibility(View.GONE);//20171.14 yqs
            menu_moreIV.setVisibility(View.GONE);
            TextView buttonRight = (TextView) this
                    .findViewById(R.id.meeting_date);
            buttonRight.setVisibility(View.VISIBLE);
            buttonRight.setText("意见列表");
            buttonRight.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Intent intent2 = new Intent(FragmentContainActivity.this,
                            SugguestDetailActivity.class);
                    startActivity(intent2);

                }
            });
        }

        menu_moreIV.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (v.getId() == R.id.menu_more && "share".equals(baseFragment.getRightButtonType())) {
                    //	ShareSDK.initSDK(FragmentContainActivity.this);

                    String forms = stubObject.getObject(
                            "forms", "").toString();
                    String caption = stubObject.getObject(
                            "caption", "").toString();

					/*storageUtil = new StorageUtil(activity, "storage");
                    port = storageUtil.getString("port");
					address = storageUtil.getString("address");
					path = storageUtil.getString("path");
					boolean isSafe = storageUtil.getBoolean("isSafe", true);
					String protocol = null;
					if (isSafe) {
						protocol = "https";
					}else {
						protocol = "http";
					}
					ServerURL = protocol + "://" + address + ":" + port + "/" + path;
					String url =  ServerURL+"/GWTMobilePlay.html?forms="+forms+"&contentView="+forms+".form1";*/

                    storageUtil = new StorageUtil(FragmentContainActivity.this, "storage");
                    String address = storageUtil.getString("address", "");
                    String path = storageUtil.getString("path", "");
                    String port = storageUtil.getString("port", "8080");
                    boolean isSafe = storageUtil.getBoolean("isSafe", true);
                    String protocol = null;
                    if (isSafe) {
                        protocol = "https";
                    } else {
                        protocol = "http";
                    }
                    ServerURL = protocol + "://" + address + "/" + path;

                    String url = ServerURL + "/AndroidForm/" + forms + ".html";
                    System.out.println("分享的网址" + url);
					/*Toast.makeText(activity, url,
							Toast.LENGTH_SHORT).show();*/

                    String imagePath = Environment.getExternalStorageDirectory().toString() + "/espmobile"
                            + "/res" + "/unzip_res" + "/Image" + "/ic_launcher.png";

					/*OnekeyShare oks = new OnekeyShare();
					oks.setTitle(caption);
					oks.setTitleUrl(url);
					oks.setText(url);
					oks.setUrl(url);
					//oks.setImageUrl("http://f1.sharesdk.cn/imgs/2014/02/26/owWpLZo_638x960.jpg");
					oks.setImagePath(imagePath);

					 oks.setSilent(true); // 隐藏编辑页面

					//oks.setSilent(false); // 显示编辑页面
					// oks.setImageUrl("http://f1.sharesdk.cn/imgs/2014/02/26/owWpLZo_638x960.jpg");
					oks.show(FragmentContainActivity.this);*/


                }
            }
        });


    }



    public void jumpAndStoreStack(Fragment frag) {
        if (frag == null) {
            return;
        }
        FragmentTransaction trasaction = fm.beginTransaction();
        trasaction.add(R.id.fragment_contains, frag);
        trasaction.addToBackStack("a");
        trasaction.commit();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

    }
}
