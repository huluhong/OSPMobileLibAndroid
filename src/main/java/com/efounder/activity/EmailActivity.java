//package com.efounder.activity;
//
//import android.app.Activity;
//import android.graphics.Color;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.Window;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.efounder.builder.base.data.EFDataSet;
//import com.efounder.builder.base.data.EFRowSet;
//import com.efounder.builder.base.data.ESPRowSet;
//import com.efounder.eai.EAI;
//import com.efounder.eai.data.JParamObject;
//import com.efounder.eai.data.JResponseObject;
//import com.efounder.ospmobilelib.R;
//import com.efounder.util.AndroidEnvironmentVariable;
//import com.efounder.util.CommonPo;
//import com.efounder.util.GlobalMap;
//import com.efounder.util.LoadingDataUtilBlack;
//import com.efounder.view.titlebar.AbTitleBar;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.concurrent.Executors;
//
///**
// * 邮箱首页
// *
// * @author yqs
// *
// */
//public class EmailActivity extends Activity implements OnClickListener {
//
//	private static final String TAG = "EmailActivity";
//	AbTitleBar mTitleBar;
//	private RelativeLayout inboxLayout1, inboxLayout2;// 收件箱
//	private RelativeLayout outboxLayout1, outboxLayout2;// 发件箱
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		// TODO Auto-generated method stub
//		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
//		//BaseApp.actManager.putActivity(TAG, this);
//
//		//setAbContentView(R.layout.activity_email_homepage);
//		setContentView(R.layout.activity_email_homepage);
//		initView();
//		loadDataByNet();
//
//	}
//
//	private void initView() {
//
//		RelativeLayout include = (RelativeLayout) findViewById(R.id.include);
//		include.setBackgroundResource(R.color.red_ios);
//		LinearLayout leftbacklayout = (LinearLayout) findViewById(R.id.leftbacklayout);
//		leftbacklayout.setVisibility(View.VISIBLE);
//		TextView title = (TextView) findViewById(R.id.fragmenttitle);
//		leftbacklayout.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				finish();
//			}
//		});
//		title.setText("邮箱");
//		title.setTextColor(Color.WHITE);
//
////		inboxLayout1 = (RelativeLayout) findViewById(R.id.relay1);
////		outboxLayout1 = (RelativeLayout) findViewById(R.id.relay2);
////		inboxLayout1.setOnClickListener(this);
////		outboxLayout1.setOnClickListener(this);
////		inboxLayout2 = (RelativeLayout) findViewById(R.id.relay3);
////		outboxLayout2 = (RelativeLayout) findViewById(R.id.relay4);
////		inboxLayout2.setOnClickListener(this);
////		outboxLayout2.setOnClickListener(this);
//	}
//
//	@Override
//	public void onClick(View v) {
//
////		int id = v.getId();
////
////
////		if (id == R.id.relay1) {
////			Intent intent = new Intent(EmailActivity.this, EmailInBoxAct.class);
////			intent.putExtra("emailtype", "DPEMAIL");
////			startActivity(intent);
////		} else if (id == R.id.relay2) {
////			Intent intent = new Intent(EmailActivity.this, EmailOutBoxAct.class);
////			intent.putExtra("emailtype", "DPEMAIL");
////			startActivity(intent);
////
////		}
////		else if (id == R.id.relay3) {
////			Intent intent = new Intent(EmailActivity.this, EmailInBoxAct.class);
////			intent.putExtra("emailtype", "EMAIL");
////			startActivity(intent);
////
////		}
////		else if (id == R.id.relay4) {
////			Intent intent = new Intent(EmailActivity.this, EmailOutBoxAct.class);
////			intent.putExtra("emailtype", "EMAIL");
////			startActivity(intent);
////
////		}
//	}
//
//	/**
//	 * 得到 token
//	 *
//	 * @return
//	 */
//	@SuppressWarnings({ "unused", "unchecked" })
//	private void getTokenID() {
//		// 创建PO
//
//		JParamObject PO = JParamObject.Create();
//		CommonPo.setPoToNull();
//		JResponseObject RO = null;
//		EFDataSet tokenEmailDataSet = null;
//
//		String usereId = AndroidEnvironmentVariable.getUserID();
//		String password = AndroidEnvironmentVariable.getPassword();
//		PO.SetValueByParamName("WB_SYS_KEY", "DPEMAIL,EMAIL");
//		//PO.SetValueByParamName("EMAIL_params_Userid", "fengjsh.zyyt");
//		//PO.SetValueByParamName("EMAIL_params_Pwd", "@@@nana110");
//		PO.SetValueByParamName("EMAIL_params_Userid", usereId);
//		PO.SetValueByParamName("EMAIL_params_Pwd", password);
//		PO.SetValueByParamName("EMAIL_systemmethod", "GetTokenID");
//
//
//		PO.SetValueByParamName("DPEMAIL_params_Userid", usereId);
//		PO.SetValueByParamName("DPEMAIL_params_Pwd", password);
//		PO.SetValueByParamName("DPEMAIL_systemmethod", "GetTokenID");
//
//		try {
//			// 连接服务器
//			RO = EAI.DAL.SVR("ExternalInterfaceDataService", PO);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		if (RO != null) {
//			HashMap<String, Object> map = (HashMap<String, Object>) RO
//					.getResponseMap();
//			if (map.containsKey("EMAIL")) {
//				tokenEmailDataSet = (EFDataSet) map.get("EMAIL");// 普光邮箱 token
//				List<ESPRowSet> EmailrowSets = tokenEmailDataSet
//						.getRowSetList();
//				if (EmailrowSets !=null) {
//
//					EFRowSet efORowSet = (EFRowSet) EmailrowSets.get(0);
//					String EmailTokenId = (String) efORowSet.getDataMap().get(
//							"TokenID");
//					System.out.println(EmailTokenId);
//					Log.i(TAG, "普光邮箱tokenid:" + EmailTokenId);
//					GlobalMap.setProperty("EmailTokenId", EmailTokenId);
//				}else {
//					Log.i(TAG, "普光邮箱tokenid:" + "普光邮箱tokenid不存在");
//				}
//				EFDataSet HeadOfficeDataSet = (EFDataSet) map.get("DPEMAIL");// 总部邮箱token
//				List<ESPRowSet> DPEmailrowSets = HeadOfficeDataSet
//						.getRowSetList();
//				if (DPEmailrowSets !=null) {
//
//
//					EFRowSet DPefORowSet = (EFRowSet) DPEmailrowSets.get(0);
//					String DPEmailTokenId = (String) DPefORowSet.getDataMap().get(
//							"TokenID");
//					Log.i(TAG, "总部邮箱tokenid:" + DPEmailTokenId);
//
//					GlobalMap.setProperty("DPEmailTokenId",
//							DPEmailTokenId);
//
//
//				}
//
//			}
//		}
//
//	}
//
//	/**
//	 * 通过网络 加载数据
//	 */
//	private void loadDataByNet() {
//		System.out.println("加载emailtoken");
//		new AsyncTask<Void, Void, String>() {
//
//			@Override
//			protected void onPreExecute() {
//				// TODO Auto-generated method stub
//				super.onPreExecute();
//				// 加载dialog显示
//				LoadingDataUtilBlack.show(EmailActivity.this);
//			}
//
//			@Override
//			protected String doInBackground(Void... params) {
//
//				getTokenID();
//
//				String emailToken = GlobalMap.getProperty(
//						"EmailTokenId", "");
//
//				return emailToken;
//			}
//
//			@SuppressWarnings({ "unused", "unchecked" })
//			@Override
//			protected void onPostExecute(String result) {
//				super.onPostExecute(result);
//
//				Log.i("emailToken", result);
//				LoadingDataUtilBlack.dismiss();
//
//			}
//
//		}.executeOnExecutor(Executors.newCachedThreadPool());
//
//	}
//
//}
