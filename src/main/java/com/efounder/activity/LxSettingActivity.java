package com.efounder.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.efounder.chat.activity.BaseActivity;
import com.efounder.chat.activity.MultiLanguageActivity;
import com.efounder.chat.activity.ThemePreferenceActivity;
import com.efounder.chat.activity.TranslateSettingActivity;
import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.bean.MyThemeBean;
import com.efounder.frame.language.MultiLanguageUtil;
import com.efounder.frame.manager.ThemeManager;
import com.efounder.ospmobilelib.R;
import com.efounder.util.AppContext;
import com.efounder.util.CommonSettingManager;
import com.efounder.util.EnvSupportManager;
import com.efounder.utils.ResStringUtil;
import com.qmuiteam.qmui.widget.roundwidget.QMUIRoundButton;
import com.utilcode.util.ReflectUtils;

import static com.efounder.frame.utils.Constants.KEY_SETTING_APPID;

/**
 * 设置界面
 *
 * @author YQS 2019/02/20
 */
public class LxSettingActivity extends BaseActivity implements OnClickListener {
    private static final String TAG = "LxSettingActivity";
    private TextView tvTitle;

    private LinearLayout llMenuParent;
    protected QMUIRoundButton btnExitApp;
    protected ConstraintLayout clLanguage;
    protected TextView tvLanguageValue;
    protected ConstraintLayout clCurrency;
    protected TextView tvCurrentyCode;
    protected ConstraintLayout clBourse;
    protected TextView tvBourseName;
    protected ConstraintLayout clSkin;
    protected TextView tvSkinValue;

    protected ConstraintLayout clTranslateLanguage;
    protected TextView tvTranslateLanguageValue;


    public static void start(Context context) {
        Intent starter = new Intent(context, LxSettingActivity.class);
        ((Activity) context).startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.wechatview_activity_setting);
        initView();

    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    protected void initView() {
        tvTitle = (TextView) findViewById(R.id.tv_title);
        llMenuParent = (LinearLayout) findViewById(R.id.ll_menu_parent);
        clLanguage = (ConstraintLayout) findViewById(R.id.cl_language);
        tvLanguageValue = (TextView) findViewById(R.id.tv_language_value);
        clCurrency = (ConstraintLayout) findViewById(R.id.cl_currency);
        tvCurrentyCode = (TextView) findViewById(R.id.tv_currenty_code);
        clBourse = (ConstraintLayout) findViewById(R.id.cl_bourse);
        tvBourseName = (TextView) findViewById(R.id.tv_bourse_name);
        clSkin = (ConstraintLayout) findViewById(R.id.cl_skin);
        tvSkinValue = (TextView) findViewById(R.id.tv_skin_value);

        clTranslateLanguage = (ConstraintLayout) findViewById(R.id.cl_translate_language);
        tvTranslateLanguageValue = (TextView) findViewById(R.id.tv_translate_language_value);
        btnExitApp = (QMUIRoundButton) findViewById(R.id.exit_app);
        clLanguage.setOnClickListener(this);
        clCurrency.setOnClickListener(this);
        clBourse.setOnClickListener(this);
        clSkin.setOnClickListener(this);
        btnExitApp.setOnClickListener(this);
        clTranslateLanguage.setOnClickListener(this);
    }

    protected void initData() {
        tvTitle.setText(R.string.common_text_setting);

        //多语言
        if (EnvSupportManager.isSupportMultiLanguage()) {
            tvLanguageValue.setText(MultiLanguageUtil.getInstance().getCurrentLanguageName());
        } else {
            clLanguage.setVisibility(View.GONE);
        }
        //翻译
        if (EnvSupportManager.isSupportTranslate()) {
            tvTranslateLanguageValue.setText(MultiLanguageUtil.getLanguageNameByType(TranslateSettingActivity.getCurrentTranlateLanguageType()));
        } else {
            clTranslateLanguage.setVisibility(View.GONE);
        }


        if (!EnvironmentVariable.getProperty(KEY_SETTING_APPID, "").equals(AppContext.getInstance()
                .getString(R.string.special_appid))) {

            clCurrency.setVisibility(View.GONE);
            clBourse.setVisibility(View.GONE);

        }
        //设置当前货币
        try {
            ReflectUtils reflectUtils = ReflectUtils.reflect("com.pansoft.openplanet.manager.CurrencyAndBourseManager");
            String value = reflectUtils.method("getCurrencyUnitCode").toString();
            tvCurrentyCode.setText(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //设置默认交易所
        try {
            ReflectUtils reflectUtils = ReflectUtils.reflect("com.pansoft.openplanet.manager.MarketExchangeSiteManager");
            String value = reflectUtils.method("getCurrentMaketName").toString();
            tvBourseName.setText(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (EnvSupportManager.isSupportSkin()) {
            //显示当前主题名称
            MyThemeBean bean = ThemeManager.getInstance(this.getApplication()).getCurrentSkin();
            String skinTitle = bean.getType() == MyThemeBean.TYPE_IN_DEFAULT ?
                    ResStringUtil.getString(R.string.common_text_default) : ThemeManager.getInstance(this.getApplication()).getSkinTitle(bean.getThemeName());
            tvSkinValue.setText(skinTitle);
        } else {
            clSkin.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.cl_language) {
            MultiLanguageActivity.start(LxSettingActivity.this);
        } else if (id == R.id.cl_currency) {
            try {
                Intent intent = new Intent(LxSettingActivity.this, Class.forName("com.pansoft.openplanet.activity.CurrencyAndBourseActivity"));
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (id == R.id.cl_bourse) {

            try {
                Intent intent = new Intent(LxSettingActivity.this, Class.forName("com.pansoft.openplanet.activity.MarketSelectActivity"));
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (id == R.id.cl_skin) {
            ThemePreferenceActivity.start(LxSettingActivity.this);
        } else if (id == R.id.cl_translate_language) {
            TranslateSettingActivity.start(LxSettingActivity.this);
        } else if (id == R.id.exit_app) {
            //退出应用
            quitApp();

        }
    }

    /**
     * 默认退出app 的方法
     */
    protected void quitApp() {
        new CommonSettingManager(this).showExitLoginDialog();

    }


}
