package com.efounder.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.efounder.ospmobilelib.R;
import com.efounder.util.LoadingDataUtilBlack;
import com.efounder.util.MyStaticWebView;
import com.efounder.util.StorageUtil;
import com.efounder.widget.ESPWebView;

public class PDFWebViewActivity extends Activity implements OnClickListener{

	private static final String TAG = "PDFWebViewActivity";
	private static final String ANDROID_JS_OBJECT = "androidObject";
	private LinearLayout rootLayout;
	private WebView webView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pdf_web_view);

		LoadingDataUtilBlack.show(this, "正在下载附件");
		initTitleBar();
		loadPDF();
	}

	private void initTitleBar() {
		RelativeLayout include = (RelativeLayout) findViewById(R.id.include);
		include.setBackgroundResource(R.color.red_ios);
		LinearLayout leftbacklayout = (LinearLayout) findViewById(R.id.leftbacklayout);
		leftbacklayout.setVisibility(View.VISIBLE);
		leftbacklayout.setOnClickListener(this);

		TextView title = (TextView) findViewById(R.id.fragmenttitle);
		title.setText("会议资料");
		title.setTextColor(Color.WHITE);
		include.setVisibility(View.VISIBLE);

	}

	public void loadPDF() {
		rootLayout = (LinearLayout) findViewById(R.id.root_ll);
//		webView = new ESPWebView(this);// (WebView) findViewById(R.id.webView);
		webView = new MyStaticWebView().getWv();


		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		webView.setLayoutParams(params);
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				rootLayout.addView(webView);
			}
		}, 2000);

		// 添加javascriptinterface，这个方法重要，this，和后面的string为js中调用android程序提供标志。
		webView.addJavascriptInterface(this, ANDROID_JS_OBJECT);

		//加载pdf的表单url
		StorageUtil storageUtil = new StorageUtil(getApplicationContext(), "storage");
//		String address = storageUtil.getString("address", "10.80.0.104");
		String port = storageUtil.getString("port");
		String address = storageUtil.getString("address");
		String path = storageUtil.getString("path");
		boolean isSafe = storageUtil.getBoolean("isSafe", true);
		String protocol = null;
		if (isSafe) {
			protocol = "https";
		}else {
			protocol = "http";
		}
		String ServerURL = protocol + "://" + address + ":" + port + "/" + path;
		String url = ServerURL + "/GWTMobilePlay.html?forms=android_mobile_showPDF&contentView=android_mobile_showPDF.form1";
		Log.i(TAG, "加载pdf的表单url:"+url);
		webView.loadUrl(url);
	}


	// js android 交互标示的方法（无参数）
	public String jsToAndroid(String msg) {
		if ("getPdfUrl".equals(msg)) {
			return getPdfUrl();
		}
		if ("stopProgress".equals(msg)) {
			return stopProgress();
		}

		return null;
	}

	@JavascriptInterface
	private String getPdfUrl() {
//		String url = "CtrlService?action=loadFile&fwk=FormAffixService&iom=loadAffixData&PO=DataBaseName=ZYYT_DB01;DBNO=ZYHYGL01&GUID=0EAF1C81-DEBA-93FD-99A5-5DB7C41564E6&ORDE=0&MDLID=&affixTable=HY_HYZLZD_AFFIX&affixTableB=SYS_AFFIX&CCLX=FILE&PATH=/安全环保监督人员招聘公告.pdf&SWF_FILE=0&PDF_FILE=0&WJLX=3";
		String url = getIntent().getStringExtra("pdf_url");
		url = url.substring(url.indexOf("CtrlService?"), url.length());
		Log.i(TAG, "pdf--Url:"+ url);
		return url;
	}

	private String stopProgress(){
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				LoadingDataUtilBlack.dismiss();
			}
		}, 1000 );

		return "1";
	}
	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.leftbacklayout) {// 点击返回按钮，结束当前activity
			if (this != null && !this.isFinishing()) {
				this.finish();
			}
		}
	}

	@Override
	protected void onDestroy() {
		if (webView.getParent() != null) {
			rootLayout.removeView(webView);
		}
		super.onDestroy();
	}
}
