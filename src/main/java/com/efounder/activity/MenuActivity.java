package com.efounder.activity;

import java.util.ArrayList;
import java.util.HashMap;

import com.efounder.ospmobilelib.R;
import com.efounder.broadcast.BroadCastutil;

import com.efounder.widget.MenuGridView;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class MenuActivity extends Activity {
	 private MenuGridView gview;
	    private ArrayList<HashMap<String,Object>> data_list;
	    ArrayList<HashMap<String,Object>> listmap = new ArrayList<HashMap<String,Object>>();
	    /*
	     * ExtEdgeRightAdapterBroadcast 广播
	     */
	    private String[] icon = { "selfservice", "selflocation",
	            "selflove" };
	    private String[] iconName = { "自助服务", "自助服务", "自助服务"};
	    
	    
	
	
  //  private final String[] array = {"Hello", "World", "Android", "is", "Awesome", "World", "Android", "is", "Awesome", "World", "Android", "is", "Awesome", "World", "Android", "is", "Awesome"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menuactivitylayout);
        
        RelativeLayout include = (RelativeLayout) findViewById(R.id.include);
		include.setBackgroundResource(R.color.red);
		TextView title = (TextView)findViewById(R.id.fragmenttitle);
		title.setText("自定义");
		title.setTextColor(Color.WHITE);
		LinearLayout leftbacklayout = (LinearLayout) findViewById(R.id.leftbacklayout);
		leftbacklayout.setVisibility(View.VISIBLE);
		/*Button backButton = (Button) findViewById(R.id.backButton);
		backButton.setVisibility(View.VISIBLE);
		backButton.setBackgroundResource(R.drawable.fanhuiarrow);*/

		leftbacklayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// SimpleWebFragment_url.this.getSupportFragmentManager().popBackStack();
				MenuActivity.this.finish();
			}
		});
		
        
        
        gview = (MenuGridView) findViewById(R.id.gview);
        //新建List
        data_list = new ArrayList<HashMap<String, Object>>();
        //获取数据
        //getData();
       /* //新建适配器
        String [] from ={"image","text"};
        int [] to = {R.id.image,R.id.text};
        sim_adapter = new SimpleAdapter(this, data_list, R.layout.item, from, to);
        //配置适配器
        gview.setAdapter(sim_adapter);*/
       //修改前的东东，以后删除此类 listmap = FilesOperationDataUtil.readFile(this);

		if (listmap == null || listmap.size() == 0){
			listmap = getData();
		}
		
      // gview.setData(listmap);
        
      
   		
        
    }
	
	 public ArrayList<HashMap<String,Object>> getData(){        
	        //cion和iconName的长度是相同的，这里任选其一都可以
	        for(int i=0;i<icon.length;i++){
	        	HashMap<String, Object> map = new HashMap<String, Object>();
	            map.put("images", icon[i]);
	            map.put("title", iconName[i]);
	            map.put("ischecked", false);
	            data_list.add(map);
	        }
	            
	        return data_list;
	    }
   
	    
	    
}
