//package com.efounder.activity;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.graphics.Color;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.efounder.ospmobilelib.R;
//import com.efounder.util.GlobalMap;
//import com.efounder.util.StorageUtil;
//
//import java.util.ArrayList;
//import java.util.List;
//
//
//
///**
// * OA设置Activity
// *
// * @author yqs
// */
//public class LeaveChildAccountSetActivity extends Activity implements
//        OnClickListener {
//
//    private Button submitButton;// 提交
//    private Button deleButton;// 删除
//    private StorageUtil storageUtil;
//    private LinearLayout bodyLayout;
//    private List<EditText> userNameList;
//    private List<EditText> userPwdList;
//    private List<TextView> OANamesList;
//    private List<String> typeList;// 类型
//
//    public static String LEAVE = "LEAVE";// 请销假
//
//    LayoutInflater inflater;
//    private String systemUserName;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        setContentView(R.layout.activity_childaccountset_parent);
//        storageUtil = new StorageUtil(getApplicationContext(), "storage");
//        systemUserName = storageUtil.getString("userName");
//        storageUtil = new StorageUtil(getApplicationContext(), systemUserName);
//
//        inflater = getLayoutInflater();
//
//        RelativeLayout include = (RelativeLayout) findViewById(R.id.include);
//        include.setBackgroundResource(R.color.red_ios);
//        LinearLayout leftbacklayout = (LinearLayout) findViewById(R.id.leftbacklayout);
//        leftbacklayout.setVisibility(View.VISIBLE);
//        TextView title = (TextView) findViewById(R.id.fragmenttitle);
//        leftbacklayout.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                saveInfo();
//                finish();
//            }
//        });
//
//        title.setText("子帐号设置");
//        title.setTextColor(Color.WHITE);
//
//        userNameList = new ArrayList<EditText>();
//        userPwdList = new ArrayList<EditText>();
//        OANamesList = new ArrayList<TextView>();
//        typeList = new ArrayList<String>();
//
//        typeList.add(LEAVE);
//
//
//        initView();
//
//    }
//
//    private void initView() {
//        submitButton = (Button) findViewById(R.id.but_oa_submit);
//        deleButton = (Button) findViewById(R.id.but_oa_delete);
//        submitButton.setOnClickListener(this);
//        deleButton.setOnClickListener(this);
//        bodyLayout = (LinearLayout) findViewById(R.id.layout_body);
//
//        addView("请销假及外培管理", LEAVE);
//
//
//    }
//
//    private void addView(String title, String type) {
//
//        ViewGroup oneLayout = (ViewGroup) inflater.inflate(
//                R.layout.activity_childaccountset_item, bodyLayout, false);
//        TextView titleView = (TextView) oneLayout
//                .findViewById(R.id.oa_company_name);
//        EditText userName = (EditText) oneLayout.findViewById(R.id.oa_username);
//        EditText userPwd = (EditText) oneLayout.findViewById(R.id.oa_userpwd);
//        titleView.setText(title);
//        userName.setText(getUserId(type));
//        userPwd.setText(getUserPwd(type));
//        bodyLayout.addView(oneLayout);
//        userNameList.add(userName);
//        userPwdList.add(userPwd);
//        OANamesList.add(titleView);
//    }
//
//    /**
//     * 获取用户名
//     *
//     * @param type
//     * @return
//     */
//    private String getUserId(String type) {
//
//        String userId = storageUtil.getString(type + "Id", "");
//
//        return userId;
//    }
//
//    /**
//     * 获取用户密码
//     *
//     * @param type
//     * @return
//     */
//    private String getUserPwd(String type) {
//
//        String passWord = storageUtil.getString(type + "Pwd", "");
//        return passWord;
//
//    }
//
//    @Override
//    public void onClick(View v) {
//        int id = v.getId();
//        if (id == R.id.but_oa_submit) {
//            saveInfo();
//
//            Toast.makeText(this, "保存成功", Toast.LENGTH_SHORT).show();
//            if (getIntent().hasExtra("type")) {
//                Intent intent = getIntent();
//                setResult(RESULT_OK, intent);
//            }
//            finish();
//        } else if (id == R.id.but_oa_delete) {
//            deleteInfo();
//            Toast.makeText(this, "删除成功", Toast.LENGTH_SHORT).show();
//            if (getIntent().hasExtra("type")) {
//                Intent intent = new Intent();
//                intent.putExtra("delete", "");
//                setResult(RESULT_OK, intent);
//            }
//            finish();
//        }
////		switch (id) {
////		case but_oa_submit:
////
////			saveInfo();
////
////			Toast.makeText(this, "保存成功", Toast.LENGTH_SHORT).show();
////			if (getIntent().hasExtra("type")) {
////				Intent intent = getIntent();
////				setResult(RESULT_OK, intent);
////			}
////			finish();
////			break;
////		case R.id.but_oa_delete:
////			deleteInfo();
////			Toast.makeText(this, "删除成功", Toast.LENGTH_SHORT).show();
////			if (getIntent().hasExtra("type")) {
////				Intent intent = new Intent();
////				intent.putExtra("delete", "");
////				setResult(RESULT_OK, intent);
////			}
////			finish();
////
////			break;
////		}
//    }
//
//    /**
//     * 删除用户名密码
//     */
//    private void deleteInfo() {
//        for (int i = 0; i < typeList.size(); i++) {
//            GlobalMap.setProperty(typeList.get(i) + "Id", "");
//            storageUtil.putString(typeList.get(i) + "Id", "");
//            storageUtil.putString(typeList.get(i) + "Pwd", "");
//            storageUtil.commit();
//            for (int j = 0; j < typeList.size(); j++) {
//                GlobalMap.setProperty(typeList.get(i) + "Id", "");
//            }
//
//        }
//
//    }
//
//    /**
//     * 保存用户名密码
//     */
//    private void saveInfo() {
//        for (int i = 0; i < typeList.size(); i++) {
//            EditText nameText = userNameList.get(i);
//            EditText pwdText = userPwdList.get(i);
//            String name = nameText.getText().toString();
//            String password = pwdText.getText().toString();
//            storageUtil.putString(typeList.get(i) + "Id", name);
//            storageUtil.putString(typeList.get(i) + "Pwd", password);
//            storageUtil.commit();
//            for (int j = 0; j < typeList.size(); j++) {
//                GlobalMap.setProperty(typeList.get(i) + "Id", "");
//            }
//
//        }
//
//    }
//}
