package com.efounder.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.efounder.chat.activity.BaseActivity;
import com.efounder.forwechat.BaseApp;
import com.efounder.frame.utils.EFAppAccountUtils;
import com.efounder.ospmobilelib.R;
import com.efounder.util.StorageUtil;
import com.efounder.utils.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yqs
 *         应用号主数据界面
 */
public class MasterDataActivity extends BaseActivity implements OnClickListener {

    private static final String TAG = "MasterDataActivity";

    private StorageUtil storageUtil;
    private ListView listView;
    private MasterListViewAdapter adapter;
    private File[] files;
    private List<File> fileList;
    private ProgressDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_masterdata);
        BaseApp.actManager.putActivity(TAG, this);
        dialog = new ProgressDialog(MasterDataActivity.this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("更新中...");

        RelativeLayout include = (RelativeLayout) findViewById(R.id.include);
        include.setVisibility(View.VISIBLE);
        TextView title = (TextView) findViewById(R.id.fragmenttitle);
        title.setText("主数据");
        LinearLayout leftbacklayout = (LinearLayout)findViewById(R.id.leftbacklayout);
        leftbacklayout.setVisibility(View.VISIBLE);

        leftbacklayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
              finish();
            }
        });
        listView = (ListView) findViewById(R.id.master_listview);
        fileList = new ArrayList<>();
        adapter = new MasterListViewAdapter();
        listView.setAdapter(adapter);



        File file = new File(EFAppAccountUtils
                .getAppAccountMDMPath());
        if (!file.exists()) {
            file.mkdirs();
        }
        files = file.listFiles();
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                fileList.add(files[i]);
                String json = FileUtils.readTextFile(files[i].getAbsolutePath());
                System.out.println(file.getName() + json);
            }
        }


        initSettingWidget();

    }


    private void initSettingWidget() {
        //storageUtil = new StorageUtil(getApplicationContext(), "storage");


    }


    public void onClick(View v) {

    }


    public class MasterListViewAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return fileList.size();
        }

        @Override
        public Object getItem(int i) {
            return fileList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder hodler;
            final File file = fileList.get(i);
            if (view == null) {
                view = LayoutInflater.from(MasterDataActivity.this).inflate(R.layout
                        .activity_masterdata_listitem, null);
                hodler = new ViewHolder();
                hodler.master_name = (TextView) view.findViewById(R.id.master_name);
                hodler.button = (TextView) view.findViewById(R.id.button);
                view.setTag(hodler);
            } else {
                hodler = (ViewHolder) view.getTag();
            }
            hodler.master_name.setText(file.getName());
            hodler.button.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.i(TAG, "请求数据：" + file.getName());
                    dialog.show();
                   dialog.setCancelable(true);
                    FileUtils.writeTextFile(file.getAbsolutePath(), "这是写入的数据");

                }
            });
            return view;
        }
    }

    class ViewHolder {
        TextView master_name;
        TextView button;

    }
}
