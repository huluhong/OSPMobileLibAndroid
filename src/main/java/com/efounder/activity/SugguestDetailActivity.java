package com.efounder.activity;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.efounder.builder.base.data.EFDataSet;
import com.efounder.builder.base.data.EFRowSet;
import com.efounder.eai.EAI;
import com.efounder.eai.data.JParamObject;
import com.efounder.eai.data.JResponseObject;
import com.efounder.forwechat.BaseApp;
import com.efounder.mdm.EFMDMDataModel;
import com.efounder.ospmobilelib.R;
import com.efounder.util.LoadingDataUtilBlack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SugguestDetailActivity extends Activity {



	private ListView suggestListView;
	

	
	
	public static final String TAG = "SugguestDetailActivity";

	
	
	

	/*@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		TabBottomActivity menuFrag = TabBottomActivity.getInstance();
        isShowBottom = (menuFrag.getBottomBar().getVisibility()==View.VISIBLE);
		if(isShowBottom)
		menuFrag.getBottomBar().setVisibility(View.GONE);
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onPause();
		if(isShowBottom){
		TabBottomActivity menuFrag = TabBottomActivity.getInstance();
		menuFrag.getBottomBar().setVisibility(View.VISIBLE);
		}
	}*/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.suggestdetail);
		BaseApp.actManager.putActivity(TAG, this);
		RelativeLayout include = (RelativeLayout) findViewById(R.id.include);
		TextView title = (TextView)findViewById(R.id.fragmenttitle);
		title.setText("意见列表");
		LinearLayout leftbacklayout = (LinearLayout) findViewById(R.id.leftbacklayout);
		leftbacklayout.setVisibility(View.VISIBLE);

		leftbacklayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 finish();
			}
		});
		
		Button rightButton = (Button) findViewById(R.id.closeButton);
		// 获取

		// 设置

		rightButton.setVisibility(View.INVISIBLE);
		
		
		suggestListView = (ListView) findViewById(R.id.suggestlistview);
	/*	web = (ESPWebView) rootView.findViewById(R.id.mywebview);
		
		webUrl = handleURL(webUrl);
		web.loadUrl(String.valueOf(webUrl));*/
		SugguestAsyncTask sugguestAsyncTask = new SugguestAsyncTask();
		sugguestAsyncTask.execute();
	
	}

	/*
	 * 内部AsyncTask类
	 */
	class SugguestAsyncTask extends AsyncTask<Void, Integer, EFMDMDataModel> {
		long time=System.currentTimeMillis();
		// 进度显示
		protected void onPreExecute() {

			// 进度显示
			super.onPreExecute();

			// 加载dialog显示
			// LoadingDataUtil.show("");
			LoadingDataUtilBlack.show(SugguestDetailActivity.this, "加载中");
		}

		@Override
		protected EFMDMDataModel doInBackground(Void... params) {
			EFMDMDataModel   model ;
			JParamObject PO;
			PO = JParamObject.Create();
			PO.SetValueByParamName("DCT_ID", "ESP_YD_YJFK");
			PO.SetValueByParamName("ESP_YD_YJFK_MDMSelfOrder", "note desc");
			JResponseObject jResponseObject;
			try {
				jResponseObject = EAI.DAL.SVR("LargeDICTService",PO);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
			
			if(jResponseObject!=null){
			/*	System.out.println("111111111");*/
				model = (EFMDMDataModel) jResponseObject.getResponseObject();
				return model;
			}else{
				System.out.println("..........");
			}
		
			return null;
				
			
		}

		// 当后台操作结束时，此方法将会被调用，计算结果将做为参数传递到此方法中，直接将结果显示到UI组件上
		protected void onPostExecute(EFMDMDataModel result) {
			LoadingDataUtilBlack.dismiss();
			if(result!=null){
				EFMDMDataModel model = result;
				
				HashMap datamap  = model.getDataSetMap();
				EFDataSet efDataSet =(EFDataSet) datamap.get("ESP_YD_YJFK");
				 List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
				for(int i=0;i<efDataSet.getRowCount();i++){
					EFRowSet rSet = efDataSet.getRowSet(i);
					//String AD = rSet.getString("AD","");
					String content = rSet.getString("content", "");
					String name = rSet.getString("F_NAME","");
					String note = rSet.getString("note", "");
					  Map<String, Object> map = new HashMap<String, Object>();
				        map.put("name", name);
				        map.put("content",content);
				        map.put("note",note);
				        list.add(map);
				}
				
			    SimpleAdapter adapter = new SimpleAdapter(SugguestDetailActivity.this,list,R.layout.suggestdetailrow,
		                new String[]{"content","name","note"},
		                new int[]{R.id.content,R.id.suggestusername,R.id.note});
			    suggestListView.setAdapter(adapter);
			}

		}

	
	}

	/*@Override
	public void onStart() {
		title = ((TabBottomActivity)getActivity()).getTitleBar();
		title.setTitleText(titleStr);
		title.setVisibility(View.GONE);
		super.onStart();
	}
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		//BroadCastutil.getinstance().unregisterWebBroadcastList((Context)SimpleWebFragmentWithEFrowset.this.getActivity());
	}*/
}
