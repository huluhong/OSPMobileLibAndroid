package com.efounder.mvp.base;

public interface BasePresenter<T> {

    void attachView(T view);
    void detachView();
    boolean isViewAttached();
}
