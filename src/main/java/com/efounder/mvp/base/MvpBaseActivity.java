package com.efounder.mvp.base;

import android.app.Activity;
import android.os.Bundle;

import com.efounder.chat.activity.BaseActivity;
import com.efounder.util.LoadingDataUtilBlack;


/**
 * mvp baseactivity
 *
 * @param
 * @param <P> BasePresenterImpl
 */
public abstract class MvpBaseActivity<P extends BasePresenter> extends BaseActivity implements BaseView {


    protected P presenter;
    protected Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;
        // 在界面未初始化之前调用的初始化窗口
        if (!initWidows()) {
            return;
        }


        presenter = initPresenter();

        //1.设置contentview
        if (getContentLayoutId() != 0) {
            setContentView(getContentLayoutId());
            //初始化view
            initView(savedInstanceState);
            //初始化数据
            initData();
        }

    }

    /**
     * 初始化窗口 返回false 就不往下执行了
     */
    protected boolean initWidows() {
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //在onDestroy()生命周期中释放P中引用的V。
        if (presenter != null && presenter.isViewAttached()) {
            presenter.detachView();
        }
    }

    protected abstract P initPresenter();

    protected abstract int getContentLayoutId();

    protected abstract void initView(Bundle savedInstanceState);

    protected abstract void initData();

    public abstract void initEvent();


    @Override
    public void showProgress(String text) {
        if (text == null) {
            LoadingDataUtilBlack.show(activity);
        } else {
            LoadingDataUtilBlack.show(activity, text);
        }
    }

    @Override
    public void dismissProgress() {
        LoadingDataUtilBlack.dismiss();
    }

}
