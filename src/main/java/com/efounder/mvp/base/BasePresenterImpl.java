package com.efounder.mvp.base;

import com.efounder.chat.activity.BaseActivity;

public  class BasePresenterImpl<T extends BaseView> implements BasePresenter<T> {
    /**
     * Presenter 中的标记
     */
    public static String TAG = "";
    /**
     * 当前Presenter的view
     */
    public T view;

    protected BaseActivity mContext;

    public BasePresenterImpl(T view) {
        attachView(view);
        TAG = view.toString();
        mContext = (BaseActivity) view;
    }
    @Override
    public void attachView(T view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
        this.mContext = null;
    }

    @Override
    public boolean isViewAttached() {
        return this.view != null;
    }
}
