package com.efounder.mvp.base;

public interface BaseView {

    void showProgress(String text);
    void dismissProgress();

}
