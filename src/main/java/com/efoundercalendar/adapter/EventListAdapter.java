package com.efoundercalendar.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.efounder.ospmobilelib.R;
import com.efoundercalendar.activity.BrowseScheduleActivity;
import com.efoundercalendar.entity.EventBean;

import java.util.List;

/**
 * Created by ldf on 17/6/14.
 */

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.ViewHolder> {

    private final LayoutInflater layoutInflater;
    private final Context context;

    private List<EventBean> mEventBeanList;

    private String[] titles;
    private String[] startTime;
    private String[] location;

    public EventListAdapter(Context context, List<EventBean> eventBeanList) {
        this.context = context;
        this.mEventBeanList = eventBeanList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public EventListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.schedule_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final EventBean mEventBean = mEventBeanList.get(position);

        long startTime = mEventBean.getStartTime();

        holder.title.setText(mEventBean.getTitle());
        holder.startTime.setText(DateFormat.format("HH:mm", startTime));
        holder.location.setText("地点：" + mEventBean.getLocation());

        if (position == 0) {
            holder.line.setVisibility(View.VISIBLE);
        }

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BrowseScheduleActivity.class);
                intent.putExtra("eventId", mEventBean.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mEventBeanList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView startTime;
        TextView location;
        RelativeLayout relativeLayout;
        View line;

        ViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.schedule_title);
            startTime = (TextView) view .findViewById(R.id.schedule_start_time);
            location = (TextView) view.findViewById(R.id.schedule_location);
            relativeLayout = (RelativeLayout) view.findViewById(R.id.cv_item);
            line = view.findViewById(R.id.line);
        }
    }

}
