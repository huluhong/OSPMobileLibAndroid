package com.efoundercalendar.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.efoundercalendar.entity.EventBean;
import com.ldf.calendar.model.CalendarDate;
import com.ldf.calendar.view.Calendar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * sunlp on 2017/10/24
 */

public class EventDBHelper {
    private Context context;
    private CalendarOpenHelper calendarOpenHelper;
    private SQLiteDatabase db;
    private EventDBHelper instance = null;

    public EventDBHelper(Context context) {
        this.context = context;
        calendarOpenHelper = new CalendarOpenHelper(context, null, null, 1);
        db = calendarOpenHelper.getWritableDatabase();
    }

    /**
     * 插入数据
     *
     * @param bean
     */
    public void insertEventData(EventBean bean) {
        db.execSQL("insert into calendarevents(year, month, day, starttime, endtime, title, location," +
                "particular) values(?,?,?,?,?,?,?,?)", new Object[]{bean.getYear(), bean.getMonth(),
                bean.getDay(), bean.getStartTime(), bean.getEndTime(), bean.getTitle(), bean.getLocation(),
                bean.getParticular()});
    }

    /**
     * 查询全部
     *
     * @return
     */
    public List<EventBean> getAll() {
        List<EventBean> beanlist = new ArrayList<EventBean>();
        Cursor cursor = getCursor();
        if (cursor.moveToFirst()) {
            do {
                EventBean bean = new EventBean();
                bean.setId(cursor.getInt(0));
                bean.setYear(cursor.getInt(1));
                bean.setMonth(cursor.getInt(2));
                bean.setDay(cursor.getInt(3));
                bean.setStartTime(cursor.getLong(4));
                bean.setEndTime(cursor.getLong(5));
                bean.setTitle(cursor.getString(6));
                bean.setLocation(cursor.getString(7));
                bean.setParticular(cursor.getString(8));

                beanlist.add(bean);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return beanlist;
    }

    /**
     * 根据具体日期查询
     *
     * @param calendarDate
     * @return
     */
    public List<EventBean> getEventByDay(CalendarDate calendarDate) {
        List<EventBean> beanList = new ArrayList<EventBean>();
        Cursor cursor = db.rawQuery("select * from calendarevents where year=? and month=? and day=? order by starttime",
                new String[]{calendarDate.getYear() + "", calendarDate.getMonth() + "", calendarDate.getDay() + ""});
        if (cursor.moveToFirst()) {
            do {
                EventBean bean = new EventBean();
                bean.setId(cursor.getInt(0));
                bean.setYear(cursor.getInt(1));
                bean.setMonth(cursor.getInt(2));
                bean.setDay(cursor.getInt(3));
                bean.setStartTime(cursor.getLong(4));
                bean.setEndTime(cursor.getLong(5));
                bean.setTitle(cursor.getString(6));
                bean.setLocation(cursor.getString(7));
                bean.setParticular(cursor.getString(8));

                beanList.add(bean);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return beanList;
    }

    /**
     * 查询全部日程，返回需要在日历中标记的信息
     * @return
     */
    public HashMap<String, String> getMarkDay() {
        HashMap<String, String> beanMap = new HashMap<>();
        Cursor cursor = getCursor();
        if (cursor.moveToFirst()) {
            do {
                String year = String.valueOf(cursor.getInt(1));
                String month = String.valueOf(cursor.getInt(2));
                String day = String.valueOf(cursor.getInt(3));
                StringBuffer sb = new StringBuffer();
                sb.append(year).append("-").append(month).append("-").append(day);
                String date = sb.toString();
                beanMap.put(date, "0");
            } while (cursor.moveToNext());
        }
        cursor.close();
        return beanMap;
    }

    /**
     * 根据id返回日程信息
     * @param id
     * @return
     */
    public EventBean getEventById(int id) {
        Cursor cursor = db.rawQuery("select * from calendarevents where _id = ?",
                new String[]{id + ""});
        cursor.moveToNext();
        EventBean bean = new EventBean();
        bean.setId(cursor.getInt(0));
        bean.setYear(cursor.getInt(1));
        bean.setMonth(cursor.getInt(2));
        bean.setDay(cursor.getInt(3));
        bean.setStartTime(cursor.getLong(4));
        bean.setEndTime(cursor.getLong(5));
        bean.setTitle(cursor.getString(6));
        bean.setLocation(cursor.getString(7));
        bean.setParticular(cursor.getString(8));

        cursor.close();
        return bean;

    }

    public Cursor getCursor() {
        String[] columns = new String[]{
                "_id",
                "year",
                "month",
                "day",
                "starttime",
                "endtime",
                "title",
                "location",
                "particular"
        };
        return db.query("calendarevents", columns, null, null, null, null,
                null);
    }
}
