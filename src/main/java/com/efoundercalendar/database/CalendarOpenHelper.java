package com.efoundercalendar.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import com.efounder.constant.EnvironmentVariable;
import com.efounder.frame.utils.EFConstants;

import java.io.File;

/**
 * sunlp on 2017/10/24
 */

public class CalendarOpenHelper extends SQLiteOpenHelper {

    public CalendarOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, path(context), factory, version);
    }

    private static String path(Context context){
        boolean var1 = false;
        String var2 = Environment.getExternalStorageState();
        if("mounted".equals(var2)) {
            var1 = true;
        }

        String var3 = null;
        String var4 = EnvironmentVariable.getUserID();
        if(var4 != null && !var4.equals("")) {
            if(var1) {
                var3 = EFConstants.APP_ROOT + "/" + var4 + "/";
            } else {
                var3 = context.getFilesDir().getPath() + "/database/" + var4 + "/";
            }

            File var5 = new File(var3);
            if(!var5.exists()) {
                var5.mkdirs();
            }

            String var6 = var3 + "/calendarevents.db";
            Log.i("db-----", "CalendarOpenHelper-----数据库名称：" + var6);
            return var6;
        } else {
            throw new RuntimeException("MessageDBHelper exception:env-userID is null");
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table calendarevents(_id integer primary key autoincrement," +
                "year int(20), month int(20), day int(20), starttime integer, endtime integer," +
                "title char(100), location char(50), particular char(1000))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
