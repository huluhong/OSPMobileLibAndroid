package com.efoundercalendar.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.efounder.ospmobilelib.R;
import com.efoundercalendar.database.EventDBHelper;
import com.efoundercalendar.entity.EventBean;

import java.util.Calendar;

public class AddScheduleActivity extends AppCompatActivity {

    //初始化的开始时间
    private Calendar showStartDate;
    //初始化的结束时间
    private Calendar showEndDate;
    private TextView tv_startTime;
    private TextView tv_StartDate;
    private TextView tv_endTime;
    private TextView tv_endDate;
    private EditText tv_title;
    private EditText tv_location;
    private EditText tv_schedule;
    //日程信息实体类
    private EventBean mEventBean = null;
    //数据库帮助类
    private EventDBHelper mDBHelper;

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case 101:
                    Toast.makeText(AddScheduleActivity.this, "添加日程成功!", Toast.LENGTH_SHORT).show();
//                    startActivity(new Intent(AddScheduleActivity.this, CalendarMonthActivity.class));
                    finish();
                    break;
                default:
                    break;
            }
            return false;
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_schedule);

        mEventBean = new EventBean();

        mDBHelper = new EventDBHelper(getApplicationContext());

        showStartDate = Calendar.getInstance();
        showEndDate = Calendar.getInstance();

        initView();
        initToolBar();
        initSeclect();
    }

    /**
     * 顶部ToolBar
     */
    private void initToolBar() {
        LinearLayout leftbacklayout = (LinearLayout) findViewById(R.id.leftbacklayout);
        leftbacklayout.setVisibility(View.VISIBLE);
        leftbacklayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        TextView meetingDate = (TextView) findViewById(R.id.meeting_date);
        meetingDate.setVisibility(View.INVISIBLE);
        meetingDate.setText("保存");
        meetingDate.setTextColor(this.getResources().getColor(R.color.white));
        meetingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //保存
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        saveEvents();
                        Message message = new Message();
                        message.what = 101;
                        mHandler.sendMessage(message);
                    }
                }).start();
            }
        });
        meetingDate.setVisibility(View.VISIBLE);

        TextView title = (TextView) findViewById(R.id.fragmenttitle);
        title.setText("添加日程");
        title.setTextColor(Color.WHITE);
    }

    private void initView(){
        tv_startTime = (TextView) findViewById(R.id.start_time);
        tv_StartDate = (TextView) findViewById(R.id.start_date);
        tv_endTime = (TextView) findViewById(R.id.end_time);
        tv_endDate = (TextView) findViewById(R.id.end_date);
        tv_title = (EditText) findViewById(R.id.tv_title) ;
        tv_location = (EditText) findViewById(R.id.tv_location);
        tv_schedule = (EditText) findViewById(R.id.tv_schedule);

        tv_startTime.setText(DateFormat.format("HH:mm",showStartDate));
        tv_StartDate.setText(DateFormat.format("yyyy-MM-dd", showStartDate));
        showEndDate.set(Calendar.HOUR_OF_DAY, showStartDate.get(Calendar.HOUR_OF_DAY)+1);
        tv_endTime.setText(DateFormat.format("HH:mm",showEndDate));
        tv_endDate.setText(DateFormat.format("yyyy-MM-dd", showEndDate));
    }

    private void initSeclect() {


        tv_startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(AddScheduleActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        showStartDate.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        showStartDate.set(Calendar.MINUTE,minute);
                        tv_startTime.setText(DateFormat.format("HH:mm",showStartDate));
                    }
                },showStartDate.get(Calendar.HOUR_OF_DAY),showStartDate.get(Calendar.MINUTE),true).show();
            }
        });

        tv_StartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(AddScheduleActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        showStartDate.set(Calendar.YEAR,year);
                        showStartDate.set(Calendar.MONTH,month);
                        showStartDate.set(Calendar.DAY_OF_MONTH,dayOfMonth);

                        tv_StartDate.setText(DateFormat.format("yyyy-MM-dd", showStartDate));
                    }
                },showStartDate.get(Calendar.YEAR), showStartDate.get(Calendar.MONTH), showStartDate.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        tv_endTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(AddScheduleActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        showEndDate.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        showEndDate.set(Calendar.MINUTE,minute);
                        if(showStartDate.before(showEndDate)){
                            tv_endTime.setText(DateFormat.format("HH:mm",showEndDate));
                        }
                    }
                },showEndDate.get(Calendar.HOUR_OF_DAY),showEndDate.get(Calendar.MINUTE),true).show();
            }
        });

        tv_endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(AddScheduleActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        showEndDate.set(Calendar.YEAR,year);
                        showEndDate.set(Calendar.MONTH,month);
                        showEndDate.set(Calendar.DAY_OF_MONTH,dayOfMonth);

                       if (showStartDate.before(showEndDate)){
                           tv_endDate.setText(DateFormat.format("yyyy-MM-dd", showEndDate));
                       }
                    }
                },showEndDate.get(Calendar.YEAR), showEndDate.get(Calendar.MONTH), showEndDate.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    //保存事件方法
    private void saveEvents(){

        String startDate = (String) tv_StartDate.getText();
        String startTime = (String) tv_startTime.getText();
        String endDate = (String) tv_endDate.getText();
        String endTime = (String) tv_endTime.getText();

        //读取textView中的String，转为int后，放入Calendar，Calendar转为long后，存数据库
//        caStart.set(2017,10,24,8,8);
        Calendar caStart = Calendar.getInstance();
        String startDays[] = startDate.split("-", 3);
        int startYear = Integer.parseInt(startDays[0]);
        int startMonth = Integer.parseInt(startDays[1]);
        int startDay = Integer.parseInt(startDays[2]);

        String startTimes[] = startTime.split(":", 2);
        int startHour = Integer.parseInt(startTimes[0]);
        int startMinute = Integer.parseInt(startTimes[1]);

        caStart.set(startYear, startMonth-1,startDay, startHour, startMinute);
        long startLong = caStart.getTimeInMillis();

        Calendar caEnd = Calendar.getInstance();
        String endDays[] = endDate.split("-", 3);
        int endYear = Integer.parseInt(endDays[0]);
        int endMonth = Integer.parseInt(endDays[1]);
        int endDay = Integer.parseInt(endDays[2]);

        String endTimes[] = endTime.split(":",2);
        int endHour = Integer.parseInt(endTimes[0]);
        int endMinute = Integer.parseInt(endTimes[1]);

        caEnd.set(endYear, endMonth-1, endDay, endHour, endMinute);
        long endLong = caEnd.getTimeInMillis();

        String title = tv_title.getText().toString();
        String location = tv_location.getText().toString();
        String particular = tv_schedule.getText().toString();

        mEventBean.setYear(startYear);
        mEventBean.setMonth(startMonth);
        mEventBean.setDay(startDay);
        mEventBean.setStartTime(startLong);
        mEventBean.setEndTime(endLong);
        mEventBean.setTitle(title);
        mEventBean.setLocation(location);
        mEventBean.setParticular(particular);

        mDBHelper.insertEventData(mEventBean);

    }
}
