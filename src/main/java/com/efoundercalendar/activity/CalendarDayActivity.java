package com.efoundercalendar.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.RectF;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alamkanak.weekview.DateTimeInterpreter;
import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;
import com.efounder.ospmobilelib.R;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CalendarDayActivity extends AppCompatActivity implements WeekView.EventClickListener,  MonthLoader.MonthChangeListener,WeekView.EventLongPressListener, WeekView.EmptyViewLongPressListener {


    private WeekView mWeekView;

    private Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_day);
        context = this;

        initToolBar();
        //时间轴View
        initWeekView();

    }

    /**
     * 时间轴view
     */
    private void initWeekView(){
        // Get a reference for the week view in the layout.
        mWeekView = (WeekView) findViewById(R.id.weekView);

        // Show a toast message about the touched event.
        mWeekView.setOnEventClickListener(this);

        // The week view has infinite scrolling horizontally. We have to provide the events of a
        // month every time the month changes on the week view.
        mWeekView.setMonthChangeListener(this);

        // Set long press listener for events.
        mWeekView.setEventLongPressListener(this);

        // Set long press listener for empty view
        mWeekView.setEmptyViewLongPressListener(this);

        // Set up a date time interpreter to interpret how the date and time will be formatted in
        // the week view. This is optional.
        setupDateTimeInterpreter(false);

        mWeekView.setNumberOfVisibleDays(1);

        // Lets change some dimensions to best fit the view.
        mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
        mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
        mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));

        //TODO 上一个界面传递的日期
//        mWeekView.goToDate();
    }

    /**
     * 顶部view
     */
    private void initToolBar() {
        LinearLayout leftbacklayout = (LinearLayout) findViewById(R.id.leftbacklayout);
        leftbacklayout.setVisibility(View.VISIBLE);
        leftbacklayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        TextView meetingDate = (TextView) findViewById(R.id.meeting_date);
        meetingDate.setVisibility(View.INVISIBLE);

//        meetingDate.setText("切换周月");
//        meetingDate.setTextColor(this.getResources().getColor(R.color.white));
//        meetingDate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //回到今天
////                onClickBackToDayBtn();
//            }
//        });
//        meetingDate.setVisibility(View.VISIBLE);

        TextView title = (TextView) findViewById(R.id.fragmenttitle);
        title.setText("时间详情");
        title.setTextColor(Color.WHITE);
    }


    /**
     * Set up a date time interpreter which will show short date values when in week view and long
     * date values otherwise.
     * @param shortDate True if the date values should be short.
     */
    private void setupDateTimeInterpreter(final boolean shortDate) {
        mWeekView.setDateTimeInterpreter(new DateTimeInterpreter() {
            @Override
            public String interpretDate(java.util.Calendar date) {
                SimpleDateFormat weekdayNameFormat = new SimpleDateFormat("EEE", Locale.getDefault());
                String weekday = weekdayNameFormat.format(date.getTime());
                SimpleDateFormat format = new SimpleDateFormat(" M/d", Locale.getDefault());

                // All android api level do not have a standard way of getting the first letter of
                // the week day name. Hence we get the first char programmatically.
                // Details: http://stackoverflow.com/questions/16959502/get-one-letter-abbreviation-of-week-day-of-a-date-in-java#answer-16959657
                if (shortDate)
                    weekday = String.valueOf(weekday.charAt(0));

                Log.i("当前日期",""+date.getTime());
//
                return weekday.toUpperCase()+format.format(date.getTime());
            }

            @Override
            public String interpretTime(int hour) {
//                return hour > 11 ? (hour - 12) + " PM" : (hour == 0 ? "12 AM" : hour + " AM");
                return String.format("%02d:00", hour);
            }
        });
    }

    //todo 时间轴View要展示的事件数据
    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        // Populate the week view with some events.
        List<WeekViewEvent> events = new ArrayList<WeekViewEvent>();

        java.util.Calendar startTime = java.util.Calendar.getInstance();
        startTime.set(java.util.Calendar.HOUR_OF_DAY, 3);
        startTime.set(java.util.Calendar.MINUTE, 0);
        startTime.set(java.util.Calendar.MONTH, newMonth - 1);
        startTime.set(java.util.Calendar.YEAR, newYear);
        java.util.Calendar endTime = (java.util.Calendar) startTime.clone();
        endTime.add(java.util.Calendar.HOUR, 1);
        endTime.set(java.util.Calendar.MONTH, newMonth - 1);
        WeekViewEvent event = new WeekViewEvent(1, getEventTitle(startTime), startTime, endTime);
        event.setColor(getResources().getColor(R.color.event_color_01));
        events.add(event);

        startTime = java.util.Calendar.getInstance();
        startTime.set(java.util.Calendar.HOUR_OF_DAY, 3);
        startTime.set(java.util.Calendar.MINUTE, 30);
        startTime.set(java.util.Calendar.MONTH, newMonth-1);
        startTime.set(java.util.Calendar.YEAR, newYear);
        endTime = (java.util.Calendar) startTime.clone();
        endTime.set(java.util.Calendar.HOUR_OF_DAY, 4);
        endTime.set(java.util.Calendar.MINUTE, 30);
        endTime.set(java.util.Calendar.MONTH, newMonth-1);
        event = new WeekViewEvent(10, getEventTitle(startTime), startTime, endTime);
        event.setColor(getResources().getColor(R.color.event_color_02));
        events.add(event);

        return events;
    }

    protected String getEventTitle(java.util.Calendar time) {
        return String.format("Event of %02d:%02d %s/%d", time.get(java.util.Calendar.HOUR_OF_DAY), time.get(java.util.Calendar.MINUTE), time.get(java.util.Calendar.MONTH)+1, time.get(java.util.Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {
//        Toast.makeText(this, "Clicked " + event.getName(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(CalendarDayActivity.this, BrowseScheduleActivity.class);
        startActivity(intent);
    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {
        Toast.makeText(this, "Long pressed event: " + event.getName(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onEmptyViewLongPress(java.util.Calendar time) {
        Toast.makeText(this, "Empty view long pressed: " + getEventTitle(time), Toast.LENGTH_SHORT).show();
    }
}
