package com.efoundercalendar.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.efounder.ospmobilelib.R;
import com.efoundercalendar.database.EventDBHelper;
import com.efoundercalendar.entity.EventBean;

/**
 * 查看日程Activity
 */
public class BrowseScheduleActivity extends AppCompatActivity {

    private TextView title;
    private TextView location;
    private TextView particular;
    private TextView startTime;
    private TextView startDate;
    private TextView endTime;
    private TextView endDate;

    int id = 0;
    EventBean mEventBean ;
    //数据库帮助类
    private EventDBHelper mDBHelper;



    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what){
                case 1:
                    bindData();
                    break;
                default:
                    break;
            }
            return false;
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_schedule);
        mDBHelper = new EventDBHelper(getApplicationContext());
        initToolBar();
        Intent intent = getIntent();
        id = intent.getIntExtra("eventId",0);

        initView();
        getData();

    }

    /**
     * 顶部ToolBar
     */
    private void initToolBar() {
        LinearLayout leftbacklayout = (LinearLayout) findViewById(R.id.leftbacklayout);
        leftbacklayout.setVisibility(View.VISIBLE);
        leftbacklayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        TextView meetingDate = (TextView) findViewById(R.id.meeting_date);
        meetingDate.setVisibility(View.INVISIBLE);

        TextView title = (TextView) findViewById(R.id.fragmenttitle);
        title.setText("日程信息");
        title.setTextColor(Color.WHITE);
    }

    private void initView(){
        title = (TextView) findViewById(R.id.tv_title);
        location = (TextView) findViewById(R.id.tv_location);
        particular = (TextView) findViewById(R.id.tv_schedule);
        startTime = (TextView) findViewById(R.id.start_time);
        startDate = (TextView) findViewById(R.id.start_date);
        endTime = (TextView) findViewById(R.id.end_time);
        endDate = (TextView) findViewById(R.id.end_date);
    }

    /**
     * 根据id查询日程数据
     */
    private void getData(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                mEventBean = mDBHelper.getEventById(id);
                Message message = new Message();
                message.what = 1;
                mHandler.sendMessage(message);
            }
        }).start();
    }

    /**
     * 给孔家绑定数据
     */
    private void bindData(){
        long longStart = mEventBean.getStartTime();
        long longEnd = mEventBean.getEndTime();

        startDate.setText(DateFormat.format("yyyy-MM-dd", longStart));
        startTime.setText(DateFormat.format("HH:mm", longStart));
        endDate.setText(DateFormat.format("yyyy-MM-dd", longEnd));
        endTime.setText(DateFormat.format("HH:mm", longEnd));
        title.setText(mEventBean.getTitle());
        location.setText("地点："+ mEventBean.getLocation());
        particular.setText(mEventBean.getParticular());
    }


}
